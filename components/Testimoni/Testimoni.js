import { Container, Grid, Paper, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import "swiper/css/bundle";

// import Swiper core and required modules
import SwiperCore, { Autoplay, Pagination, Navigation } from "swiper";
import { AccountCircleRounded, GradeRounded } from "@mui/icons-material";

// install Swiper modules
SwiperCore.use([Autoplay, Pagination, Navigation]);

const testimonial = [
  {
    image: "",
    name: "Didi",
    rating: 5,
    desc: "Keren website undangannya ",
  },
  {
    image: "",
    name: "Dodo",
    rating: 4,
    desc: "Keren website undangannya ",
  },
  {
    image: "",
    name: "Dedi",
    rating: 4,
    desc: "Keren website undangannya ",
  },
  {
    image: "",
    name: "Dede",
    rating: 4.5,
    desc: "Keren website undangannya ",
  },
  {
    image: "",
    name: "Dadi",
    rating: 5,
    desc: "Keren website undangannya ",
  },
];

export default function Testimoni() {
  return (
    <Container id="Testimoni">
      <Box pt={10} textAlign={"center"}>
        <Typography mb={5} fontWeight={500} fontSize={40}>
          Testimoni
        </Typography>
        <Grid
          container
          justifyContent={"center"}
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid item xs={12} sm={12} md={12} mb={5}>
            <Swiper
              slidesPerView={3}
              data-aos="zoom-out"
              centeredSlides={true}
              autoplay={{
                delay: 5000,
                disableOnInteraction: false,
              }}
              spaceBetween={30}
              className="mySwiper"
            >
              {testimonial.map((v, i) => (
                <SwiperSlide key={i}>
                  <Paper
                    variant="outlined"
                    style={{
                      padding: 30,
                      border: "5px solid black",
                      borderRadius: 20,
                    }}
                  >
                    <Stack direction="row" spacing={5}>
                      <Typography alignSelf={"center"} variant="h5">
                        <AccountCircleRounded style={{ fontSize: 54 }} />
                      </Typography>
                      <Typography alignSelf={"center"} variant="h5">
                        {v.name}
                      </Typography>
                      <Box display={"flex"}>
                        <Typography
                          mr={1}
                          alignSelf={"center"}
                          variant="subtitle1"
                        >
                          {v.rating}
                        </Typography>
                        <Typography alignSelf={"center"}>
                          <GradeRounded
                            style={{ fontSize: 16, color: "#FEA250" }}
                          />
                        </Typography>
                      </Box>
                    </Stack>
                    <Typography mt={3} textAlign={"start"} variant="subtitle1">
                      {v.desc}
                    </Typography>
                  </Paper>
                </SwiperSlide>
              ))}
            </Swiper>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}
