/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable @next/next/no-img-element */
import React from "react";

import NotificationsActiveIcon from "@mui/icons-material/NotificationsActive";
import MyLocationIcon from "@mui/icons-material/MyLocation";
import LiveTvIcon from "@mui/icons-material/LiveTv";
import MasksIcon from "@mui/icons-material/Masks";
import SocialDistanceIcon from "@mui/icons-material/SocialDistance";
import Timeline from "@mui/lab/Timeline";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineOppositeContent from "@mui/lab/TimelineOppositeContent";
import TimelineDot from "@mui/lab/TimelineDot";
import {
  FaInstagram,
  FaFacebookSquare,
  FaTwitter,
  FaTiktok,
  FaYoutube,
  FaTelegram,
  FaSnapchat,
  FaPinterest,
  FaWhatsapp,
  FaHandsWash,
  FaHandshakeAltSlash,
  FaHeart,
} from "react-icons/fa";
import { CardGiftcard, ContentCopy, Money } from "@mui/icons-material";

import "lightgallery.js/dist/css/lightgallery.css";
import { LightgalleryProvider, LightgalleryItem } from "react-lightgallery";
import {
  Alert,
  Dialog,
  DialogContent,
  DialogTitle,
  FormHelperText,
  Grid,
  IconButton,
  ImageList,
  ImageListItem,
  Slide,
  Snackbar,
} from "@mui/material";
import { QRCodeCanvas } from "qrcode.react";
import Footer from "../../components/Footer";
import Slider from "react-slick";
import YouTube from "react-youtube";
import Countdown from "react-countdown";
import moment from "moment";
import { useState } from "react";
import ReactPlayer from "react-player";

import { Link } from "react-scroll";

import { RiHomeHeartLine, RiHeartsLine, RiChatHeartLine } from "react-icons/ri";
import { BiCalendarHeart } from "react-icons/bi";
import {
  BottomNavigation,
  BottomNavigationAction,
  Container,
  Fab,
  Paper,
} from "@mui/material";

import * as Yup from "yup";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PlayDisabledIcon from "@mui/icons-material/PlayDisabled";
import CopyToClipboard from "react-copy-to-clipboard";
import { Box } from "@mui/system";
import { api } from "../../config/api";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import {
  createMessageApi,
  getMessageApiByInvitationId,
} from "../../features/messageApi/messageApiSlice";
import { createPresence } from "../../features/presence/presenceSlice";
const settings = {
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 10000,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 0,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function ModernGreen({ invitation }) {
  const dispatch = useDispatch();
  const router = useRouter();
  const [openInvitation, setOpenInvitation] = useState(true);
  const [play, setPlay] = useState(false);
  const [openCopied, setOpenCopied] = useState(false);
  const [openKado, setOpenKado] = useState(false);
  const [openAngpao, setOpenAngpao] = useState(false);
  const [copied, setCopied] = useState(false);
  const { message } = useSelector((state) => state.message);
  const { messageApi } = useSelector((state) => state.messageApi);

  const [successful, setSuccessful] = useState(false);
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const initialValuesConfirmation = {
    name: "",
    message: "",
    confirmation: "",
    // phoneNumber: "",
    total: "1",
  };
  const validationSchemaConfirmation = Yup.object().shape({
    name: Yup.string().required("Nama is required"),
    // phoneNumber: Yup.string().required("Nomor Handpone is required"),
    confirmation: Yup.string().required("Konfirmasi Kehadiran is required"),
  });

  const initialValuesPesan = {
    name: "",
    message: "",
  };
  const validationSchemaPesan = Yup.object().shape({
    name: Yup.string().required("Nama is required"),
    message: Yup.string().required("Pesan is required"),
  });

  const onSubmitPesan = async (
    values,
    { setErrors, setStatus, setSubmitting, resetForm }
  ) => {
    const { name, message } = values;

    const data = {
      name: name,
      message: message,
      published: true,
      invitationId: invitation.data.id,
    };
    dispatch(createMessageApi(data))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpenSnackbar(true);
        resetForm();
        dispatch(getMessageApiByInvitationId(invitation.data.id));
      })
      .catch((err) => {
        setSuccessful(false);
        setOpenSnackbar(true);
        console.log("err", err);
      });
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  };

  const onSubmitConfirmation = async (
    values,
    { setErrors, setStatus, setSubmitting, resetForm }
  ) => {
    const { name, message, phoneNumber, confirmation, total } = values;

    const data = {
      name,
      message,
      phoneNumber: "00",
      confirmation,
      total,
      published: true,
      invitationId: invitation.data.id,
    };
    dispatch(createPresence(data))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpenSnackbar(true);
        resetForm();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpenSnackbar(true);
        console.log("err", err);
      });
  };
  const handleCloseInvitation = () => {
    setOpenInvitation(false);
    setPlay(!play);
  };
  const handlePlayPause = () => {
    setPlay(!play);
  };
  const handleClickCopy = () => {
    setOpenCopied(true);
  };
  const handleCloseCopy = () => {
    setOpenCopied(false);
  };

  const handleClickKado = () => {
    setOpenKado(true);
  };

  const handleCloseKado = () => {
    setOpenKado(false);
  };
  const handleClickAngpao = () => {
    setOpenAngpao(true);
  };

  const handleCloseAngpao = () => {
    setOpenAngpao(false);
  };
  return (
    <>
      <div
        style={{
          fontFamily: invitation.data.theme?.fontType2
            ? invitation.data.theme?.fontType2
            : "Montserrat",
        }}
        className="bg-white"
      >
        {/* home */}
        <section
          className="m-auto bg-fixed bg-center bg-cover w-full h-screen"
          style={{
            backgroundImage: `url(${
              invitation.data.backgrounds[1]
                ? api.fileUrl + invitation.data.backgrounds[1].image
                : ""
            })`,
          }}
        >
          <div
            id="home"
            style={{
              backgroundImage:
                "linear-gradient(to bottom, rgba(0,0,0,0),#F6F4F2)",
            }}
            className=" w-full h-full"
          >
            <div className="bg-black/30 backdrop-opacity-30 h-full w-full py-16 px-5 flex justify-center">
              <div className="max-w-xl flex justify-center items-center text-center text-white">
                <div>
                  <div className="flex justify-center">
                    {invitation.data.avatarPasangan != "" &&
                      invitation.data.avatarPasangan != undefined && (
                        <div className="w-52 h-52 overflow-hidden rounded-full mb-2 shadow-md border-white border-[2px]">
                          <img
                            className="w-full h-full object-cover"
                            src={api.fileUrl + invitation.data.avatarPasangan}
                            alt="avatar-couple"
                          />
                        </div>
                      )}
                  </div>
                  <div
                    className="font-bold text-[35px] tracking-wide "
                    style={{
                      fontFamily: invitation.data.theme?.fontType1
                        ? invitation.data.theme?.fontType1
                        : "Montserrat",
                    }}
                  >
                    {invitation.data.namaPendekPria +
                      " & " +
                      invitation.data.namaPendekWanita}
                  </div>
                  <div className=" text-sm md:text-lg">
                    {moment(invitation.data.tanggalResepsi)
                      .locale("id")
                      .format("dddd") +
                      ", " +
                      moment(invitation.data.tanggalResepsi)
                        .locale("id")
                        .format("LL")}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* end home */}

        {/* Countdown */}
        <div className="py-12 px-5 bg-[#ECF2F0] text-[#888]">
          <div className=" text-sm text-center">
            Kami berharap anda menjadi bagian dari hari istimewa kami.
          </div>
          <div className="my-6">
            <Countdown
              date={moment(invitation.data.tanggalResepsi)
                .local("id")
                .format("YYYY-MM-DDTHH:mm")}
              renderer={({ days, hours, minutes, seconds, completed }) => {
                if (completed) {
                  // Render a complete state
                  return (
                    <div className="font-bold text-sm text-center">
                      Acara Telah Selesai
                    </div>
                  );
                } else {
                  // Render a countdown
                  return (
                    <div className="flex justify-center">
                      <div className="flex gap-5 justify-center">
                        <div className="rounded-xl">
                          <div>
                            <div className="text-2xl font-bold mb-1">
                              {days}
                            </div>
                            <div className="text-xs">Hari</div>
                          </div>
                        </div>
                        <div className="rounded-xl">
                          <div>
                            <div className="text-2xl font-bold mb-1">
                              {hours}
                            </div>
                            <div className="text-xs">Jam</div>
                          </div>
                        </div>
                        <div className="rounded-xl">
                          <div>
                            <div className="text-2xl font-bold mb-1">
                              {minutes}
                            </div>
                            <div className="text-xs">Menit</div>
                          </div>
                        </div>
                        <div className="rounded-xl">
                          <div>
                            <div className="text-2xl font-bold mb-1">
                              {seconds}
                            </div>
                            <div className="text-xs">Detik</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                }
              }}
            />
          </div>
          <div className="flex items-center justify-center">
            <a
              target="_blank"
              className=" rounded-full py-2 bg-white  px-6 text-xs shadow-md"
              href={`https://www.google.com/calendar/render?action=TEMPLATE&text=Pernikahan+${
                invitation.data.namaPendekPria
              }+dan+${invitation.data.namaPendekWanita}&dates=${moment(
                invitation.data.tanggalResepsi
              )
                .local("id")
                .format("YYYYMMDD[T]HHmmss")}/${moment(
                invitation.data.tanggalResepsi
              )
                .local("id")
                .format("YYYYMMDD[T]HHmmss")}&details=link:+${api.home}/${
                invitation.data.slug
              }&location=lokasi:+${invitation.data.mapsResepsi}`}
              rel="noreferrer"
            >
              <span className="mr-2">
                <NotificationsActiveIcon className=" w-5" />
              </span>
              Pengingat Tanggal
            </a>
          </div>
        </div>
        {/* end Countdown */}

        {/* couple  */}
        <section
          id="couple"
          className="h-full bg-white flex items-start justify-center py-12 px-5"
        >
          <div className="max-w-x text-center">
            <div>
              {invitation.data.bissmillah == false && (
                <div data-aos="zoom-in" id="couple" className="text-sm">
                  Bismillahirrahmanirrahim
                </div>
              )}
              <div
                className="text-sm my-1 "
                data-aos="zoom-in"
                data-aos-duration="200"
              >
                {invitation.data.salamPembuka == ""
                  ? "Assalamualaikum Warahmatullahi Wabarakatuh"
                  : invitation.data.salamPembuka}
              </div>
              <p
                className="text-xs my-3"
                data-aos="zoom-in"
                data-aos-duration="400"
              >
                {invitation.data.salamPembukaDeskripsi == ""
                  ? "Tanpa mengurangi rasa hormat, Kami mengundang Bapak/Ibu/Saudara/i serta kerabat sekalian untuk menghadiri acara pernikahan kami."
                  : invitation.data.salamPembukaDeskripsi}
              </p>
            </div>
            <div>
              <div className="flex justify-center py-3 md:py-5">
                <div
                  className="w-52 h-52 overflow-hidden rounded-full"
                  data-aos="zoom-in"
                  data-aos-duration="600"
                >
                  {invitation.data.avatarPria != "" &&
                  invitation.data.avatarPria != undefined ? (
                    <img
                      className="w-full h-full object-cover"
                      src={api.fileUrl + invitation.data.avatarPria}
                      alt="avatar-couple"
                    />
                  ) : (
                    <img
                      className="h-full sm:h-[200px] rounded-xl"
                      src="/static/images/avatars/avatar-frame-man.png"
                      alt="avatar-frame-man"
                    />
                  )}
                </div>
              </div>
              <div className="mt-2">
                <div
                  data-aos="zoom-in"
                  data-aos-duration="800"
                  className="font-bold text-3xl tracking-wide text-[#C8B4A0] "
                  style={{
                    fontFamily: invitation.data.theme?.fontType1
                      ? invitation.data.theme?.fontType1
                      : "Montserrat",
                  }}
                >
                  {invitation.data.namaPria}
                </div>
                <div
                  data-aos="zoom-in"
                  data-aos-duration="1000"
                  className="text-xs py-2"
                >
                  Putra dari {invitation.data.namaOrangTuaPria}
                </div>
                {invitation.data.socialmedia?.length > 0 && (
                  <div className="flex justify-center gap-2">
                    {invitation.data.socialmedia
                      ?.filter((v, i) => v.type === "pria")
                      .map((v, i) =>
                        v.name === "instagram" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaInstagram />
                          </a>
                        ) : v.name === "facebook" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaFacebookSquare />
                          </a>
                        ) : v.name === "twitter" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaTwitter />
                          </a>
                        ) : v.name === "tiktok" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaTiktok />
                          </a>
                        ) : v.name === "youtube" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaYoutube />
                          </a>
                        ) : v.name === "telegram" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaTelegram />
                          </a>
                        ) : v.name === "snapchat" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaSnapchat />
                          </a>
                        ) : v.name === "pinterest" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaPinterest />
                          </a>
                        ) : v.name === "whatsapp" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaWhatsapp />
                          </a>
                        ) : (
                          ""
                        )
                      )}
                  </div>
                )}
              </div>
            </div>
            <div className="pt-4">
              <div className="flex justify-center py-3 md:py-5">
                <div
                  className="w-52 h-52 overflow-hidden rounded-full"
                  data-aos="zoom-in"
                  data-aos-duration="1200"
                >
                  {invitation.data.avatarWanita != "" &&
                  invitation.data.avatarWanita != undefined ? (
                    <img
                      className="w-full h-full object-cover"
                      src={api.fileUrl + invitation.data.avatarWanita}
                      alt="avatar-couple"
                    />
                  ) : (
                    <img
                      className="h-full sm:h-[200px] rounded-xl"
                      src="/static/images/avatars/avatar-frame-woman.png"
                      alt="avatar-frame-woman"
                    />
                  )}
                </div>
              </div>
              <div className="mt-2">
                <div
                  data-aos="zoom-in"
                  data-aos-duration="1400"
                  className="font-bold text-3xl tracking-wide text-[#C8B4A0]"
                  style={{
                    fontFamily: invitation.data.theme?.fontType1
                      ? invitation.data.theme?.fontType1
                      : "Montserrat",
                  }}
                >
                  {invitation.data.namaWanita}
                </div>
                <div
                  data-aos="zoom-in"
                  data-aos-duration="1600"
                  className="text-xs py-2"
                >
                  Putri dari {invitation.data.namaOrangTuaWanita}
                </div>
                {invitation.data.socialmedia?.length > 0 && (
                  <div className="flex justify-center gap-2">
                    {invitation.data.socialmedia
                      ?.filter((v, i) => v.type === "wanita")
                      .map((v, i) =>
                        v.name === "instagram" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaInstagram />
                          </a>
                        ) : v.name === "facebook" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaFacebookSquare />
                          </a>
                        ) : v.name === "twitter" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaTwitter />
                          </a>
                        ) : v.name === "tiktok" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaTiktok />
                          </a>
                        ) : v.name === "youtube" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaYoutube />
                          </a>
                        ) : v.name === "telegram" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaTelegram />
                          </a>
                        ) : v.name === "snapchat" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaSnapchat />
                          </a>
                        ) : v.name === "pinterest" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaPinterest />
                          </a>
                        ) : v.name === "whatsapp" ? (
                          <a target="_blank" key={i} href={v.url}>
                            <FaWhatsapp />
                          </a>
                        ) : (
                          ""
                        )
                      )}
                  </div>
                )}
              </div>
            </div>
          </div>
        </section>
        {/* end couple  */}

        {/* doa */}
        <section className="h-full bg-[#F6F4F2] text-center flex justify-center text-[#888] text-xs py-12 px-5 ">
          <blockquote
            data-aos="zoom-in"
            className="text-sm italic font-semibold   max-w-xl"
          >
            <svg
              aria-hidden="true"
              className="w-10 h-10 "
              viewBox="0 0 24 27"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.017 18L14.017 10.609C14.017 4.905 17.748 1.039 23 0L23.995 2.151C21.563 3.068 20 5.789 20 8H24V18H14.017ZM0 18V10.609C0 4.905 3.748 1.038 9 0L9.996 2.151C7.563 3.068 6 5.789 6 8H9.983L9.983 18L0 18Z"
                fill="currentColor"
              ></path>
            </svg>
            <p>
              {invitation.data.doa != ""
                ? invitation.data.doa
                : " Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir. (Q.S Ar Rum:21)"}
            </p>
          </blockquote>
        </section>
        {/* end doa */}

        {/*  akad & resepsi */}
        <section
          id="event"
          className="h-full bg-white text-center text-[#888] text-xs flex items-start justify-center py-12 px-5"
        >
          <div className="text-center">
            <div>
              <div
                data-aos="zoom-in"
                className="font-bold text-[35px] tracking-wide text-[#C8B4A0]"
                style={{
                  fontFamily: invitation.data.theme?.fontType1
                    ? invitation.data.theme?.fontType1
                    : "Montserrat",
                }}
              >
                Akad Nikah
              </div>
              <div className="pt-5 text-sm">
                {moment(invitation.data.tanggalNikah)
                  .locale("id")
                  .format("dddd") +
                  ", " +
                  moment(invitation.data.tanggalNikah)
                    .locale("id")
                    .format("LL")}
              </div>
              <div
                data-aos="zoom-in"
                data-aos-duration="200"
                className="py-2 text-sm font-bold"
              >
                {invitation.data.jamNikah}
              </div>
              <p
                data-aos="zoom-in"
                data-aos-duration="400"
                className=" text-xs"
              >
                {invitation.data.alamatNikah}
              </p>
              <div
                data-aos="zoom-in"
                data-aos-duration="600"
                className="flex items-center justify-center py-4"
              >
                <div className="border-2 border-[#888] rounded-xl w-full h-full">
                  <iframe
                    className="rounded-xl"
                    src={invitation.data.mapsNikah}
                    style={{ border: 0, width: "100%", height: "100%" }}
                    allowFullScreen=""
                    loading="lazy"
                  ></iframe>
                </div>
              </div>
            </div>
            <div className="pt-6">
              <div
                data-aos="zoom-in"
                data-aos-duration="800"
                className="font-bold text-[35px] tracking-wide text-[#C8B4A0]"
                style={{
                  fontFamily: invitation.data.theme?.fontType1
                    ? invitation.data.theme?.fontType1
                    : "Montserrat",
                }}
              >
                Resepsi
              </div>
              <div
                data-aos="zoom-in"
                data-aos-duration="1000"
                className="pt-5 text-sm"
              >
                {moment(invitation.data.tanggalResepsi)
                  .locale("id")
                  .format("dddd") +
                  ", " +
                  moment(invitation.data.tanggalResepsi)
                    .locale("id")
                    .format("LL")}
              </div>
              <div
                data-aos="zoom-in"
                data-aos-duration="1200"
                className="py-2 text-sm font-bold"
              >
                {invitation.data.jamResepsi}
              </div>
              <p
                data-aos="zoom-in"
                data-aos-duration="1400"
                className=" text-xs"
              >
                {invitation.data.alamatResepsi}
              </p>
              <div
                data-aos="zoom-in"
                data-aos-duration="1600"
                className="flex items-center justify-center pt-4"
              >
                <div className="border-2 border-[#888] rounded-xl w-full h-full">
                  <iframe
                    className="rounded-xl"
                    src={invitation.data.mapsResepsi}
                    style={{ border: 0, width: "100%", height: "100%" }}
                    allowFullScreen=""
                    loading="lazy"
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* end  akad & resepsi */}

        {/* live Streaming */}
        {invitation.data.socialmedia
          ?.filter((v) => v.type === "pria")
          .filter((v) => v.name === "live").length > 0 ||
        invitation.data.socialmedia
          ?.filter((v) => v.type === "wanita")
          .filter((v) => v.name === "live").length > 0 ? (
          <section className="h-full bg-[#ECF2F0] text-center text-[#888] text-xs flex items-start justify-center py-12 px-5">
            <div className="text-center">
              <div>
                <div
                  data-aos="zoom-in"
                  className="font-bold text-[35px] tracking-wide text-[#C8B4A0] "
                  style={{
                    fontFamily: invitation.data.theme?.fontType1
                      ? invitation.data.theme?.fontType1
                      : "Montserrat",
                  }}
                >
                  Live Streaming
                </div>
                <div
                  data-aos="zoom-in"
                  data-aos-duration="200"
                  className="pt-5 text-sm"
                >
                  Acara akan disiarakan secara langsung.
                </div>
                <div
                  data-aos="zoom-in"
                  data-aos-duration="600"
                  className="flex items-center gap-2 flex-wrap justify-center pt-4 "
                >
                  {invitation.data.socialmedia
                    ?.filter((v) => v.type === "pria")
                    .filter((v) => v.name === "live")
                    .map((v, i) => (
                      <a
                        target="_blank"
                        className="rounded-full py-2 bg-white px-6 text-xs shadow-md"
                        key={i}
                        href={v.url}
                        rel="noreferrer"
                      >
                        <span className="mr-2">
                          <LiveTvIcon className=" w-5" />
                        </span>
                        Live {invitation.data.namaPendekPria}
                      </a>
                    ))}

                  {invitation.data.socialmedia
                    ?.filter((v) => v.type === "wanita")
                    .filter((v) => v.name === "live")
                    .map((v, i) => (
                      <a
                        target="_blank"
                        className="rounded-full py-2 bg-white px-6 text-xs shadow-md"
                        key={i}
                        href={v.url}
                        rel="noreferrer"
                      >
                        <span className="mr-2">
                          <LiveTvIcon className=" w-5" />
                        </span>
                        Live {invitation.data.namaPendekWanita}
                      </a>
                    ))}
                </div>
              </div>
            </div>
          </section>
        ) : (
          <></>
        )}
        {/* end live Streaming */}

        {/* cerita cinta */}
        {invitation.data.lovestories?.length > 0 && (
          <section className="h-full bg-white text-center text-[#888] flex justify-center text-xs py-12 px-5">
            <div className=" max-w-xl">
              <div
                data-aos="zoom-in"
                className="font-bold text-[35px] pb-6 tracking-wide text-[#C8B4A0]"
                style={{
                  fontFamily: invitation.data.theme?.fontType1
                    ? invitation.data.theme?.fontType1
                    : "Montserrat",
                }}
              >
                Cerita Cinta
              </div>
              <div data-aos="zoom-in" data-aos-duration="400" className="pt-6">
                <Timeline position="alternate" className="!m-0 !p-0">
                  {invitation.data.lovestories?.map((v, i) => (
                    <TimelineItem key={i}>
                      <TimelineOppositeContent
                        sx={{ m: "auto 0" }}
                        variant="body2"
                        color="text.secondary"
                      >
                        {v.date}
                      </TimelineOppositeContent>
                      <TimelineSeparator>
                        <TimelineConnector />
                        <TimelineDot className="!bg-red-500 text-red-300">
                          <FaHeart />
                        </TimelineDot>
                        <TimelineConnector />
                      </TimelineSeparator>
                      <TimelineContent sx={{ py: 1, px: 2 }}>
                        <div className="font-bold text-sm">{v.title}</div>
                        {v.description != "" && (
                          <p className="text-xs">{v.description}</p>
                        )}
                      </TimelineContent>
                    </TimelineItem>
                  ))}
                </Timeline>
              </div>
            </div>
          </section>
        )}
        {/* end cerita cinta */}

        {/* rspv */}
        <section
          id="wishes"
          className="m-auto bg-fixed bg-center bg-cover "
          style={{
            backgroundImage: `url(${
              invitation.data.backgrounds[2]
                ? api.fileUrl + invitation.data.backgrounds[2].image
                : invitation.data.backgrounds[0]
                ? api.fileUrl + invitation.data.backgrounds[0].image
                : "white"
            })`,
          }}
        >
          <div className="text-white w-full  h-full backdrop-blur">
            <div className="bg-black/40 backdrop-opacity-30 h-full w-full py-12 px-5 flex justify-center">
              <div className="max-w-xl">
                <div>
                  <div
                    data-aos="zoom-in"
                    className="text-center font-bold text-[35px] tracking-wide "
                    style={{
                      fontFamily: invitation.data.theme?.fontType1
                        ? invitation.data.theme?.fontType1
                        : "Montserrat",
                    }}
                  >
                    Reservasi Kehadiran
                  </div>
                  <p
                    data-aos="zoom-in"
                    data-aos-duration="400"
                    className=" text-xs text-center mt-2"
                  >
                    Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila
                    Bapak/Ibu/Saudara/i berkenan hadir untuk memberikan doa
                    restu. Atas kehadiran serta doa restu, kami ucapkan terima
                    kasih.
                  </p>

                  <Formik
                    initialValues={initialValuesConfirmation}
                    validationSchema={validationSchemaConfirmation}
                    onSubmit={onSubmitConfirmation}
                  >
                    {({
                      errors,
                      handleBlur,
                      handleChange,
                      handleSubmit,
                      isSubmitting,
                      touched,
                      values,
                    }) => (
                      <form
                        data-aos="zoom-in"
                        data-aos-duration="600"
                        noValidate
                        onSubmit={handleSubmit}
                      >
                        <div className="p-5 shadow-md my-4 text-white backdrop-blur-3xl bg-white/30 rounded-xl w-full">
                          <div className="mb-4">
                            <label
                              htmlFor="name"
                              className="block mb-2 text-sm font-medium"
                            >
                              Nama *
                            </label>
                            <input
                              value={values.name}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              placeholder="Masukan Nama"
                              type="text"
                              name="name"
                              id="name"
                              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 "
                            />
                            {touched.name && errors.name && (
                              <FormHelperText
                                error
                                id="standard-weight-helper-text--register"
                              >
                                {errors.name}
                              </FormHelperText>
                            )}
                          </div>
                          {/* <div className="mb-4">
                            <label
                              htmlFor="phoneNumber"
                              className="block mb-2 text-sm font-medium"
                            >
                              No WhatsApp *
                            </label>
                            <input
                              placeholder="Masukan No WhatsApp"
                              type="text"
                              id="phoneNumber"
                              name="phoneNumber"
                              value={values.phoneNumber}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 "
                            />
                            {touched.phoneNumber && errors.phoneNumber && (
                              <FormHelperText
                                error
                                id="standard-weight-helper-text--register"
                              >
                                {errors.phoneNumber}
                              </FormHelperText>
                            )}
                          </div> */}
                          <div className="mb-4">
                            <label
                              htmlFor="message"
                              className="block mb-2 text-sm font-medium"
                            >
                              Pesan
                            </label>
                            <textarea
                              id="message"
                              name="message"
                              rows="4"
                              value={values.message}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 "
                              placeholder="Masukan Pesan ..."
                            ></textarea>
                          </div>
                          <div className="mb-4">
                            <label
                              htmlFor="confirmation"
                              className="block mb-2 text-sm font-medium "
                            >
                              Konfirmasi Kehadiran *
                            </label>
                            <select
                              id="confirmation"
                              name="confirmation"
                              value={values.confirmation}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              defaultValue=""
                              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                            >
                              <option value="">Pilih</option>
                              <option value={"hadir"}>Hadir</option>
                              <option value={"tidak hadir"}>Tidak Hadir</option>
                              <option value={"masih ragu"}>Masih Ragu</option>
                            </select>
                            {touched.confirmation && errors.confirmation && (
                              <FormHelperText
                                error
                                id="standard-weight-helper-text--register"
                              >
                                {errors.confirmation}
                              </FormHelperText>
                            )}
                          </div>
                          {values.confirmation === "hadir" && (
                            <div className="mb-4">
                              <label
                                htmlFor="total"
                                className="block mb-2 text-sm font-medium"
                              >
                                Jumlah yang Datang
                              </label>
                              <select
                                id="total"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                value={values.total}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                defaultValue={"1"}
                              >
                                <option value={"1"}>1 Orang</option>
                                <option value={"2"}>2 Orang</option>
                                <option value={"3"}>3 Orang</option>
                              </select>
                            </div>
                          )}
                          <div className="flex items-center justify-center">
                            <button
                              type="submit"
                              className="w-full md:w-1/2 rounded-full py-2 bg-[#C8B4A0] text-white px-6 text-xs"
                            >
                              Konfirmasi
                            </button>
                          </div>
                        </div>
                      </form>
                    )}
                  </Formik>
                </div>
                <div className="mt-6">
                  <div
                    data-aos="zoom-in"
                    data-aos-duration="800"
                    className="text-center font-bold text-[35px] tracking-wide "
                    style={{
                      fontFamily: invitation.data.theme?.fontType1
                        ? invitation.data.theme?.fontType1
                        : "Montserrat",
                    }}
                  >
                    Ucapan dan Doa
                  </div>
                  <p
                    data-aos="zoom-in"
                    data-aos-duration="1000"
                    className=" text-xs text-center mt-2"
                  >
                    Kirimkan Ucapan dan Doa kepada kami, semoga doanya berbalik
                    ke anda.
                  </p>

                  <div className="p-5 shadow-md my-4 text-white backdrop-blur-3xl bg-white/30 rounded-xl w-full">
                    <Formik
                      initialValues={initialValuesPesan}
                      validationSchema={validationSchemaPesan}
                      onSubmit={onSubmitPesan}
                    >
                      {({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting,
                        touched,
                        values,
                      }) => (
                        <form
                          data-aos="zoom-in"
                          data-aos-duration="1200"
                          noValidate
                          onSubmit={handleSubmit}
                        >
                          <div>
                            <div className="mb-4">
                              <label
                                htmlFor="name"
                                className="block mb-2 text-sm font-medium"
                              >
                                Nama *
                              </label>
                              <input
                                placeholder="Masukan Nama"
                                type="text"
                                id="name"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 "
                                name="name"
                                value={values.name}
                                onBlur={handleBlur}
                                onChange={handleChange}
                              />
                              {touched.name && errors.name && (
                                <FormHelperText
                                  error
                                  id="standard-weight-helper-text--register"
                                >
                                  {errors.name}
                                </FormHelperText>
                              )}
                            </div>
                            <div className="mb-4">
                              <label
                                htmlFor="message"
                                className="block mb-2 text-sm font-medium"
                              >
                                Ucapan dan Doa Restu *
                              </label>
                              <textarea
                                value={values.message}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                name="message"
                                id="message"
                                rows="4"
                                className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 "
                                placeholder="Masukan Ucapan dan Doa Restu  ..."
                              ></textarea>
                              {touched.message && errors.message && (
                                <FormHelperText
                                  error
                                  id="standard-weight-helper-text--register"
                                >
                                  {errors.message}
                                </FormHelperText>
                              )}
                            </div>
                            <div className="flex items-center justify-center">
                              <button
                                type="submit"
                                className="w-full md:w-1/2 rounded-full py-2 bg-[#C8B4A0] text-white px-6 text-xs"
                              >
                                Kirim
                              </button>
                            </div>
                          </div>
                        </form>
                      )}
                    </Formik>

                    {messageApi?.length > 0 && (
                      <div
                        data-aos="zoom-in"
                        data-aos-duration="1400"
                        className="mt-6 max-h-80 overflow-y-auto"
                      >
                        {messageApi?.map((v, i) => (
                          <div
                            key={i}
                            className="bg-white mb-5 text-[#888] rounded-xl border px-4 py-3 border-gray-200 shadow-md"
                          >
                            <div className="text-sm font-medium">{v.name}</div>
                            <div className="text-xs font-medium">
                              {v.message}
                            </div>
                          </div>
                        ))}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* endrspv */}

        {/* kirim hadiah */}
        {invitation.data.digitalenvelopes.length > 0 ||
        invitation.data.alamatKado != "" ? (
          <section className="h-full bg-[#ECF2F0] text-center text-[#888] text-xs flex items-start justify-center py-12 px-5">
            <div className="text-center">
              <div>
                <div
                  data-aos="zoom-in"
                  className="font-bold text-[35px] tracking-wide text-[#C8B4A0]"
                  style={{
                    fontFamily: invitation.data.theme?.fontType1
                      ? invitation.data.theme?.fontType1
                      : "Montserrat",
                  }}
                >
                  Kirim Hadiah
                </div>
                <div
                  data-aos="zoom-in"
                  data-aos-duration="400"
                  className="pt-5 text-sm"
                >
                  Kami sangat berterima kasih kepada Bapak/Ibu/Saudara/i yang
                  berkenan memberikan tanda kasih kepada kami.
                </div>
                <div
                  data-aos="zoom-in"
                  data-aos-duration="600"
                  className="flex items-center gap-2 flex-wrap justify-center pt-4"
                >
                  {invitation.data.alamatKado != "" && (
                    <button
                      className="rounded-full py-2 bg-white px-6 text-xs shadow-md"
                      onClick={handleClickKado}
                    >
                      <span className="mr-2">
                        <CardGiftcard className=" w-5" />
                      </span>
                      Kado
                    </button>
                  )}
                  {invitation.data.digitalenvelopes.length > 0 && (
                    <button
                      className="rounded-full py-2 bg-white px-6 text-xs shadow-md"
                      onClick={handleClickAngpao}
                    >
                      <span className="mr-2">
                        <Money className=" w-5" />
                      </span>
                      Angpao
                    </button>
                  )}
                </div>
              </div>
            </div>
          </section>
        ) : (
          <div></div>
        )}
        {/* end kirim hadiah */}

        {/* gallery */}
        {invitation?.data?.photogalleries?.length > 0 ||
        invitation?.data?.youtubes?.length > 0 ? (
          <section className="h-full bg-white text-center text-[#888] text-xs flex items-start justify-center py-12 px-5">
            <div className="max-w-xl text-center">
              <div>
                <div
                  className="font-bold text-[35px] tracking-wide text-[#C8B4A0]"
                  style={{
                    fontFamily: invitation.data.theme?.fontType1
                      ? invitation.data.theme?.fontType1
                      : "Montserrat",
                  }}
                >
                  Galeri
                </div>
                {invitation.data.photogalleries?.length > 0 && (
                  <div className=" mt-8">
                    <LightgalleryProvider>
                      <ImageList variant="masonry" cols={2} gap={8}>
                        {invitation.data.photogalleries?.map((v, i) => (
                          <ImageListItem key={i}>
                            <LightgalleryItem
                              group={"group2"}
                              src={api.fileUrl + v?.image}
                            >
                              <img
                                className="rounded-xl"
                                src={`${api.fileUrl + v?.image}`}
                                srcSet={`${api.fileUrl + v?.image}`}
                                style={{ width: "100%" }}
                                loading="lazy"
                                alt={v?.image}
                              />
                            </LightgalleryItem>
                          </ImageListItem>
                        ))}
                      </ImageList>
                    </LightgalleryProvider>
                  </div>
                )}

                {invitation.data.youtubes?.length > 0 && (
                  <div className="my-6">
                    <Slider {...settings}>
                      {invitation.data.youtubes?.map((v, i) => (
                        <div className="px-1">
                          <YouTube
                            className="rounded-xl w-full h-40 md:h-64"
                            key={i}
                            videoId={v.url}
                          />
                        </div>
                      ))}
                    </Slider>
                  </div>
                )}
              </div>
            </div>
          </section>
        ) : (
          <></>
        )}
        {/* end gallery */}

        {/* prokes */}
        {invitation.data.theme?.publishProkes && (
          <section className="h-full bg-[#F6F4F2] text-center text-[#888] text-xs flex items-start justify-center py-12 px-5">
            <div className="max-w-xl">
              <div
                data-aos="zoom-in"
                className="font-bold text-[35px] pb-6 tracking-wide text-[#C8B4A0]"
                style={{
                  fontFamily: invitation.data.theme?.fontType1
                    ? invitation.data.theme?.fontType1
                    : "Montserrat",
                }}
              >
                Protokol Keseahatan
              </div>
              <p data-aos="zoom-in" data-aos-duration="400">
                Untuk mencegah penyebaran Covid-19, diharapkan bagi tamu
                undangan yang hadir untuk mematuhi Protokol Kesehatan dibawah
                ini :
              </p>

              <div className="py-6 ">
                <div className="grid !text-left grid-cols-1 md:grid-cols-2 gap-3">
                  <div
                    data-aos="zoom-in"
                    data-aos-duration="600"
                    className="flex flex-row py-2 px-4 gap-3 items-center border border-[#888] rounded-xl"
                  >
                    <div>
                      <MasksIcon className="text-5xl" />
                    </div>
                    <div>Menggunakan Masker</div>
                  </div>
                  <div data-aos="zoom-in" data-aos-duration="800">
                    <div className="flex flex-row py-2 px-4 gap-3 items-center border border-[#888] rounded-xl">
                      <div>
                        <FaHandsWash className="text-5xl" />
                      </div>
                      <div>Cuci Tangan dan Gunakan Sabun</div>
                    </div>
                  </div>
                  <div data-aos="zoom-in" data-aos-duration="1000">
                    <div className="flex flex-row py-2 px-4 gap-3 items-center border border-[#888] rounded-xl">
                      <div>
                        <FaHandshakeAltSlash className="text-5xl" />
                      </div>
                      <div>Dilarang Salaman</div>
                    </div>
                  </div>
                  <div data-aos="zoom-in" data-aos-duration="1200">
                    <div className="flex flex-row py-2 px-4 gap-3 items-center border border-[#888] rounded-xl">
                      <div>
                        <SocialDistanceIcon className="text-5xl" />
                      </div>
                      <div>Jaga Jarak dan Jauhkan Kerumunan</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )}
        {/* end prokes */}

        {/* wasalam */}
        <section
          className="w-full h-screen !bg-no-repeat !bg-cover !bg-center "
          style={{
            background: `url(${
              invitation.data.backgrounds[3]
                ? api.fileUrl + invitation.data.backgrounds[3].image
                : invitation.data.backgrounds[1]
                ? api.fileUrl + invitation.data.backgrounds[1].image
                : "black"
            })`,
          }}
        >
          <div
            style={{
              backgroundImage:
                "linear-gradient(to top, rgba(0,0,0,0),#F6F4F2))",
            }}
            className=" w-full h-full "
          >
            <div className="bg-black/30 backdrop-opacity-30 h-full w-full py-12 px-5 flex justify-center">
              <div className="max-w-xl flex flex-col justify-between text-center  text-white">
                <div>
                  <div data-aos="zoom-in" className="mb-3 text-xs">
                    Merupakan suatu kehormatan dan kebahagiaan bagi kami,
                    apabila Bapak/Ibu/Saudara/i berkenan hadir dan memberiakn
                    doa restu.Atas Kehadiran dan doa restunya, kami mengucapkan
                    terima kasih.
                  </div>
                  <div
                    data-aos="zoom-in"
                    data-aos-duration="200"
                    className="mb-3 text-sm"
                  >
                    Wassalamualaikum Warahmatullahi Wabarakatuh
                  </div>
                </div>
                <div>
                  <div
                    data-aos="zoom-in"
                    data-aos-duration="400"
                    className="font-bold text-[35px] tracking-wide "
                    style={{
                      fontFamily: invitation.data.theme?.fontType1
                        ? invitation.data.theme?.fontType1
                        : "Montserrat",
                    }}
                  >
                    {invitation.data.namaPendekPria +
                      " & " +
                      invitation.data.namaPendekWanita}
                  </div>
                  <div
                    data-aos="zoom-in"
                    data-aos-duration="600"
                    className="flex justify-center mt-3 mb-6"
                  >
                    <QRCodeCanvas value={router.query.to} />
                  </div>
                  <div
                    data-aos="zoom-in"
                    data-aos-duration="800"
                    className="mb-3 text-xs"
                  >
                    *Tampilkan barcode ini kepada panitia acara pernikahan untuk
                    mengisi buku tamu
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* end wasalam */}
        <Footer desc={false} />
        <div className="my-10"></div>
        <ReactPlayer
          style={{ display: "none" }}
          playing={play}
          url={
            invitation?.data?.theme?.music?.song != ""
              ? api.fileUrl + invitation?.data?.theme?.music?.song
              : "/music/Wedding Invitation 1.mp3"
          }
        />
        <Paper
          sx={{
            zIndex: 99,
            position: "fixed",
            bottom: 0,
            left: 0,
            right: 0,
          }}
          elevation={3}
        >
          <Container maxWidth="md" sx={{ position: "relative" }}>
            <button
              style={{
                background: "#FFF",
                color: "#000",
                padding: 5,
                margin: 0,
                position: "absolute",
                bottom: 65,
                right: 30,
              }}
              className="rounded-xl"
              onClick={handlePlayPause}
            >
              {play ? <PlayDisabledIcon /> : <PlayArrowIcon />}
            </button>
            {/* <Fab
          onClick={handlePlayPause}
          sx={{
            position: 'absolute',
            bottom: 65,
            right: 30,
          }}
          size="small" color="primary" aria-label="add"
        >
          {play ? <PlayDisabledIcon /> : <PlayArrowIcon />}
        </Fab> */}
            <BottomNavigation showLabels>
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="home"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Home
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="home"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <RiHomeHeartLine fontSize={23} />
                  </Link>
                }
              />
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="couple"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Couple
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="couple"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <RiHeartsLine fontSize={23} />
                  </Link>
                }
              />
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="event"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Event
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="event"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <BiCalendarHeart fontSize={23} />
                  </Link>
                }
              />
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="wishes"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Wishes
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="wishes"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <RiChatHeartLine fontSize={23} />
                  </Link>
                }
              />
            </BottomNavigation>
          </Container>
        </Paper>
        {/* first Dialog */}
        <Dialog
          maxWidth={"md"}
          fullScreen
          open={openInvitation}
          onClose={handleCloseInvitation}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogContent sx={{ padding: "0px 0px" }}>
            <section
              className="w-full !bg-black h-screen !bg-no-repeat !bg-cover !bg-center "
              style={{
                background: `url(${
                  invitation.data.backgrounds[0]
                    ? api.fileUrl + invitation.data.backgrounds[0].image
                    : "black"
                })`,
              }}
            >
              <div
                style={{
                  backgroundImage:
                    "linear-gradient(to bottom, rgba(0,0,0,0),#F6F4F2)",
                }}
                className=" w-full h-full"
              >
                <div className="bg-black/30 backdrop-opacity-30 h-full w-full py-16 px-5 flex justify-center">
                  <div
                    style={{
                      fontFamily: invitation.data.theme?.fontType2
                        ? invitation.data.theme?.fontType2
                        : "Montserrat",
                    }}
                    className="max-w-xl flex flex-col justify-between text-center  text-white"
                  >
                    <div>
                      <div className="mb-3 text-sm">Undangan</div>
                      <div
                        className="font-bold text-[35px] tracking-wide "
                        style={{
                          fontFamily: invitation.data.theme?.fontType1
                            ? invitation.data.theme?.fontType1
                            : "Montserrat",
                        }}
                      >
                        {invitation.data.namaPendekPria +
                          " & " +
                          invitation.data.namaPendekWanita}
                      </div>
                    </div>
                    <div>
                      <div className=" text-xs">
                        Kepada Yth. Bapak/Ibu/Saudara/i
                      </div>
                      <div className="my-3 font-bold text-xl">
                        {router.query.to ? router.query.to : "Kamu & Partner"}
                      </div>
                      <div className="mb-3 text-xs">
                        Tanpa mengurangi rasa hormat, kami mengundangan anda
                        untuk hadir di acara pernikahan kami.
                      </div>
                      <div className="flex items-center justify-center">
                        <button
                          onClick={handleCloseInvitation}
                          className="rounded-full py-2 bg-white text-[#888] px-6 text-xs shadow-md"
                        >
                          Buka Undangan
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </DialogContent>
        </Dialog>
        {/* end first Dialog */}

        {/* kado dialog */}
        <Dialog
          fullWidth
          open={openKado}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseKado}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Alamat"}</DialogTitle>
          <DialogContent dividers={true}>
            <Box
              component="div"
              style={{
                fontFamily: invitation.data.theme?.fontType2
                  ? invitation.data.theme?.fontType2
                  : "Montserrat",
              }}
            >
              <Box
                component="div"
                style={{
                  border: "2px dashed grey",
                  padding: 5,
                  position: "relative",
                }}
              >
                <Box
                  component="div"
                  style={{
                    position: "absolute",
                    right: 0,
                    backgroundColor: "white",
                    height: 30,
                  }}
                  onClick={handleClickCopy}
                >
                  <CopyToClipboard
                    text={invitation.data.alamatKado}
                    onCopy={() => setCopied(true)}
                  >
                    <IconButton
                      color="primary"
                      className="!text-[#888]"
                      component="span"
                    >
                      <ContentCopy fontSize="small" className="!text-xs " />
                    </IconButton>
                  </CopyToClipboard>
                </Box>
                {invitation.data.alamatKado}
              </Box>
            </Box>
          </DialogContent>
        </Dialog>
        {/* end kado dialog */}

        {/* angpao dialog */}
        <Dialog
          fullWidth
          open={openAngpao}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseAngpao}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Angpao"}</DialogTitle>
          <div
            className="px-1 md:px-5"
            style={{
              fontFamily: invitation.data.theme?.fontType2
                ? invitation.data.theme?.fontType2
                : "Montserrat",
            }}
          >
            {invitation.data.digitalenvelopes.map((v, i) => (
              <Box my={2} component="div">
                <div className=" w-60 sm:w-full md:w-full md:h-56 h-56 m-auto bg-red-100 rounded-xl relative text-white shadow-2xl transition-transform transform bg-gradient-to-tr from-gray-900 to-gray-600 bg-gradient-to-r">
                  <div className="w-full px-6 absolute top-8">
                    <div className="flex justify-between">
                      <div className=""></div>
                      <img
                        className="h-6 md:h-8"
                        src={`/static/images/digitalEnvelope/${v.name}.png`}
                        alt={v.name}
                      />
                    </div>
                    <div className="pt-4">
                      <p className="font-light text-xs">Nama</p>
                      <p className="font-medium tracking-widest text-base">
                        {v.atasNama}
                      </p>
                    </div>
                    <div className="pt-3 ">
                      <p className="font-light text-xs">
                        Nomor Rekening / Handphone
                      </p>
                      <div className="flex gap-3 text-lg">
                        <div className="font-medium tracking-more-wider">
                          {v.number}
                        </div>
                        <div
                          className=" cursor-pointer "
                          onClick={handleClickCopy}
                        >
                          <CopyToClipboard
                            text={v.number}
                            onCopy={() => setCopied(true)}
                          >
                            <IconButton
                              size="small"
                              color="inherit"
                              sx={{ padding: 0 }}
                              component="span"
                            >
                              <ContentCopy className="!text-xs" />
                            </IconButton>
                          </CopyToClipboard>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Box>
            ))}
          </div>
        </Dialog>
        {/* end angpao dialog */}

        {copied ? (
          <Snackbar
            open={openCopied}
            onClose={handleCloseCopy}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
            message="Copied"
          />
        ) : null}
        {message && (
          <Snackbar
            open={openSnackbar}
            autoHideDuration={4000}
            anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
            onClose={handleCloseSnackbar}
          >
            <Alert
              severity={successful ? "success" : "error"}
              sx={{ width: "100%" }}
              onClose={handleCloseSnackbar}
            >
              {message}
            </Alert>
          </Snackbar>
        )}
      </div>
    </>
  );
}

export default ModernGreen;
