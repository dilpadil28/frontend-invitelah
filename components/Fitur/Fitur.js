import { FlashOnRounded } from "@mui/icons-material";
import { Container, Grid, Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import ColorLensIcon from "@mui/icons-material/ColorLens";
import TimerIcon from "@mui/icons-material/Timer";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
import CollectionsIcon from "@mui/icons-material/Collections";
import VideoCameraBackIcon from "@mui/icons-material/VideoCameraBack";
import MyLocationIcon from "@mui/icons-material/MyLocation";
import MusicNoteIcon from "@mui/icons-material/MusicNote";

const keunggulan = [
  {
    icon: (
      <ColorLensIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Tema",
    desc: "Tersedia tema-tema undangan yang menarik dan elegan",
  },
  {
    icon: (
      <TimerIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Waktu Mundur",
    desc: "Waktu mundur samapai hari H kamu resepsi ",
  },
  {
    icon: (
      <PhoneAndroidIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Responsive",
    desc: "Bisa dibuka semua perangkat, seperti handphone, tablet, laptop",
  },
  {
    icon: (
      <CollectionsIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Galeri Foto",
    desc: "Unggah foto-foto kamu dan pasanganmu",
  },
  {
    icon: (
      <VideoCameraBackIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Video",
    desc: "Tampilkan video-video kamu dan pasanganmu",
  },
  {
    icon: (
      <MyLocationIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Lokasi",
    desc: "Menampilkan lokasi resepsi kamu dan pasanganmu",
  },
  {
    icon: (
      <MusicNoteIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-up"
      />
    ),
    title: "Musik",
    desc: "Undangan online akan lebih romantis sengan adanya backsound musik",
  },
];

export default function Keunggulan() {
  return (
    <Container id="Fitur">
      <Box pt={10} textAlign={"center"}>
        <Typography
          mb={5}
          fontWeight={500}
          component={"h1"}
          fontSize={{ xs: 24, sm: 40 }}
          data-aos="fade-up"
        >
          Fitur Undangan Invitelah
        </Typography>
        <Grid
          py={5}
          container
          justifyContent={"center"}
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {keunggulan?.map((v, i) => (
            <Grid item xs={2} sm={3} md={3} key={i} mb={5}>
              {v.icon}
              <Typography
                mt={3}
                mb={3}
                fontWeight="bold"
                component={"h6"}
                fontSize={{ xs: 18, md: 24 }}
                data-aos-delay="75"
                data-aos="fade-up"
              >
                {v.title}
              </Typography>
              <Typography
                component={"p"}
                fontSize={{ xs: 12, md: 14 }}
                data-aos-delay="100"
                data-aos="fade-up"
              >
                {v.desc}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  );
}
