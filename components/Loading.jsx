import { Grid } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";

function Loading({ children }) {
  // const [isLoading, setIsLoading] = useState(true);

  // useEffect(() => {
  //   setTimeout(() => {
  //     setIsLoading(false);
  //   }, 500);
  // }, []);
  return (
    <>
      {/* {isLoading ? (
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: "100vh" }}
        >
          <Box
            component="img"
            className="animate__animated animate__bounce animate__infinite"
            src="/static/images/logo/invitelah-left-green.svg"
            alt="invitelah"
            style={{ width: "200px" }}
          />
        </Grid>
      ) : (
        children
      )} */}

      {children}
    </>
  );
}

export default Loading;
