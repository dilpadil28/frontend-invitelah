/* eslint-disable react/jsx-key */
import { CheckRounded } from "@mui/icons-material";
import {
  Button,
  Container,
  Grid,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { api } from "../../config/api";
import * as ga from "../../lib/ga";

const harga = [
  {
    image: "",
    harga_diskon: "Rp 120.000",
    harga: "Rp 40.000",
    title: "Gold",
    fitur: [
      "Aktif Seumur Hidup Website",
      "Amplop Digital",
      "Autoplay Music",
      "Cerita Cinta",
      "Custom Music",
      "Download Data RVSP(Konfirmasi Kehadiran Tamu)",
      "Google Maps(Lokasi Acara)",
      "Google Calendar(Pengingat Tanggal)",
      "Hitung Mudur",
      "Kirim Kado",
      "Kolom Ucapan",
      "Sosial Media",
      "Live Streameing",
      "Protokol Kesehatan",
      "Scan Barcode Tamu",
      "Max 20 Photo",
      "Max 20 Video",
      "Unlimited Revisi",
      "Unlimited Nama Tamu",
      "Ucapan & RVSP(Konfirmasi Kehadiran Tamu)",
      <b> Undangan Image 4 buah </b>,
      <b>Undangan Video </b>,
      <div style={{ textDecoration: "line-through" }}> Custom Theme </div>,
      <div style={{ textDecoration: "line-through" }}>
        {" "}
        Domain/Subdoamin invitelah.com{" "}
      </div>,
    ],
    whatsapp: api.whatsappGold,
    bukalapak: api.bukalapak,
    tokopedia: api.tokopedia,
    shopee: api.shopee,
  },
  {
    image: "",
    harga_diskon: "Rp 100.000",
    harga: "Rp 30.000",
    title: "Silver",
    fitur: [
      "Aktif Seumur Hidup Website",
      "Amplop Digital",
      "Autoplay Music",
      "Cerita Cinta",
      "Custom Music",
      "Download Data RVSP(Konfirmasi Kehadiran Tamu)",
      "Google Maps(Lokasi Acara)",
      "Google Calendar(Pengingat Tanggal)",
      "Hitung Mudur",
      "Kirim Kado",
      "Kolom Ucapan",
      "Sosial Media",
      "Live Streameing",
      "Protokol Kesehatan",
      "Scan Barcode Tamu",
      "Max 20 Photo",
      "Max 20 Video",
      "Unlimited Revisi",
      "Unlimited Nama Tamu",
      "Ucapan & RVSP(Konfirmasi Kehadiran Tamu)",
      <div style={{ textDecoration: "line-through" }}>
        {" "}
        Undangan Image 4 buah{" "}
      </div>,
      <div style={{ textDecoration: "line-through" }}> Undangan Video </div>,
      <div style={{ textDecoration: "line-through" }}> Custom Theme </div>,
      <div style={{ textDecoration: "line-through" }}>
        {" "}
        Domain/Subdoamin invitelah.com{" "}
      </div>,
    ],
    whatsapp: api.whatsappSilver,
    bukalapak: api.bukalapak,
    tokopedia: api.tokopedia,
    shopee: api.shopee,
  },
  {
    image: "",
    harga_diskon: "Rp 500.000",
    harga: "Rp 400.000",
    title: "Diamond",
    fitur: [
      "Aktif Seumur Hidup Website",
      "Amplop Digital",
      "Autoplay Music",
      "Cerita Cinta",
      "Custom Music",
      "Download Data RVSP(Konfirmasi Kehadiran Tamu)",
      "Google Maps(Lokasi Acara)",
      "Google Calendar(Pengingat Tanggal)",
      "Hitung Mudur",
      "Kirim Kado",
      "Kolom Ucapan",
      "Sosial Media",
      "Live Streameing",
      "Protokol Kesehatan",
      "Scan Barcode Tamu",
      "Max 20 Photo",
      "Max 20 Video",
      "Unlimited Revisi",
      "Unlimited Nama Tamu",
      "Ucapan & RVSP(Konfirmasi Kehadiran Tamu)",
      <b> Undangan Image 4 buah </b>,
      <b>Undangan Video </b>,
      <b> Custom Theme </b>,
      <b> Domain/Subdoamin invitelah.com </b>,
    ],
    whatsapp: api.whatsappDiamond,
    bukalapak: api.bukalapak,
    tokopedia: api.tokopedia,
    shopee: api.shopee,
  },
];

export default function Harga() {
  const handlePrice = (param) => {
    ga.event({
      action: "price",
      params: {
        order: param,
      },
    });
  };
  return (
    <Container id="Harga">
      <Box pt={10} pb={10} textAlign={"center"}>
        <Typography
          component={"h1"}
          mb={5}
          fontSize={{ xs: 24, sm: 40 }}
          fontWeight={500}
          data-aos="zoom-in"
        >
          Harga Undangan
        </Typography>
        <Grid
          container
          justifyContent={"center"}
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {harga?.map((v, i) => (
            <Grid item xs={12} sm={4} md={4} mb={5} key={i}>
              <Paper
                data-aos-delay="50"
                data-aos="zoom-in"
                elevation={3}
                style={{
                  padding: 30,
                  borderRadius: 20,
                }}
              >
                <Typography
                  data-aos-delay="55"
                  data-aos="zoom-in"
                  mb={2}
                  fontSize={18}
                  component={"p"}
                  style={{ textDecoration: "line-through" }}
                  color={"lightgrey"}
                >
                  {v.harga_diskon}
                </Typography>
                <Typography
                  mb={2}
                  color={"#006C32"}
                  fontSize={28}
                  fontWeight={"bold"}
                  data-aos-delay="60"
                  data-aos="zoom-in"
                  component={"p"}
                >
                  {v.harga}
                </Typography>
                <Typography
                  mb={2}
                  fontWeight="bold"
                  fontSize={18}
                  data-aos-delay="65"
                  data-aos="zoom-in"
                  component={"h1"}
                >
                  {v.title}
                </Typography>
                {v.fitur.map((v, i) => (
                  <ListItem
                    key={i}
                    sx={{ p: 0 }}
                    data-aos-delay="70"
                    data-aos="zoom-in"
                  >
                    <ListItemIcon>
                      <CheckRounded color="success" />
                    </ListItemIcon>
                    <ListItemText>{v}</ListItemText>
                  </ListItem>
                ))}
                <Box my={2} />
                <Button
                  onClick={() => handlePrice("whatsapp")}
                  target="_blank"
                  href={v.whatsapp}
                  fullWidth
                  variant="contained"
                  data-aos-delay="65"
                  data-aos="zoom-in"
                >
                  Pesan Whatsapp
                </Button>
                <Box mb={1} />
                <Button
                  onClick={() => handlePrice("Bukalapak")}
                  target="_blank"
                  sx={{ backgroundColor: "#E31E52" }}
                  href={v.bukalapak}
                  fullWidth
                  variant="contained"
                  data-aos-delay="66"
                  data-aos="zoom-in"
                >
                  Bukalapak
                </Button>
                <Box mb={1} />
                <Button
                  onClick={() => handlePrice("Tokopedia")}
                  target="_blank"
                  sx={{ backgroundColor: "#03AC0E" }}
                  href={v.tokopedia}
                  fullWidth
                  variant="contained"
                  data-aos-delay="67"
                  data-aos="zoom-in"
                >
                  Tokopedia
                </Button>
                <Box mb={1} />
                <Button
                  onClick={() => handlePrice("Shopee")}
                  target="_blank"
                  sx={{ backgroundColor: "#EE4D2D" }}
                  href={v.shopee}
                  fullWidth
                  variant="contained"
                  data-aos-delay="68"
                  data-aos="zoom-in"
                >
                  Shopee
                </Button>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  );
}
