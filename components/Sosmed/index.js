import { Box } from "@mui/system";
import React from "react";
import { Container, Typography, Grid } from "@mui/material";
import Slider from "react-slick";
const images = [
  // 'static/images/sosmed/Blue-Flower-1.png',
  // 'static/images/sosmed/Blue-Flower-2.png',
  // 'static/images/sosmed/Blue-Flower-3.png',
  // 'static/images/sosmed/Brown-Crystal-1.png',
  // 'static/images/sosmed/Brown-Crystal-2.png',
  // 'static/images/sosmed/Brown-Crystal-3.png',
  // 'static/images/sosmed/Brown-Frame-1.png',
  // 'static/images/sosmed/Brown-Frame-2.png',
  // 'static/images/sosmed/Brown-Frame-3.png',
  // 'static/images/sosmed/Gold-Flower-1.png',
  // 'static/images/sosmed/Gold-Flower-2.png',
  // 'static/images/sosmed/Gold-Flower-3.png',
  // 'static/images/sosmed/Gold-Frame-1.png',
  // 'static/images/sosmed/Gold-Frame-2.png',
  // 'static/images/sosmed/Gold-Frame-3.png',
  "static/images/sosmed/Pink-Flower-1.png",
  "static/images/sosmed/Pink-Flower-2.png",
  "static/images/sosmed/Pink-Flower-3.png",
  // 'static/images/sosmed/Red-Flower-1.png',
  // 'static/images/sosmed/Red-Flower-2.png',
  // 'static/images/sosmed/Red-Flower-3.png',
  // 'static/images/sosmed/Triangle-Gold-1.png',
  // 'static/images/sosmed/Triangle-Gold-2.png',
  // 'static/images/sosmed/Triangle-Gold-3.png',
  "static/images/sosmed/White-Flower-1.png",
  "static/images/sosmed/White-Flower-2.png",
  "static/images/sosmed/White-Flower-3.png",
  // 'static/images/sosmed/Yellow-Flower-1.png',
  // 'static/images/sosmed/Yellow-Flower-2.png',
  // 'static/images/sosmed/Yellow-Flower-3.png',
];
export default function Sosmed() {
  const settings = {
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    loop: true,
    autoplay: true,
    speed: 1000,
    cssEase: "linear",
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <Container maxWidth={"md"}>
      <Grid
        container
        spacing={2}
        display={"flex"}
        justifyContent="center"
        alignItems="center"
      >
        <Grid
          item
          xs={12}
          md={6}
          display={"flex"}
          justifyContent="center"
          alignItems="center"
        >
          <div>
            <Typography
              my={2}
              fontWeight="bold"
              fontSize={{ xs: 18, md: 24 }}
              data-aos-delay="75"
              data-aos="fade-up"
              textAlign={"center"}
              component={"h1"}
            >
              Undangan Video
            </Typography>
            <div
              data-aos-delay="200"
              data-aos="fade-up"
              style={{ position: "relative" }}
            >
              <div className="iphone-frame">
                <video
                  key="/static/images/video/Brown-Crystal.mp4"
                  loop
                  autoPlay
                  muted
                  playsInline
                >
                  <source src="/static/images/video/Brown-Crystal.mp4" />
                </video>
              </div>
            </div>
          </div>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          display={"flex"}
          justifyContent="center"
          alignItems="center"
        >
          <div>
            <Typography
              my={2}
              fontWeight="bold"
              fontSize={{ xs: 18, md: 24 }}
              data-aos-delay="75"
              data-aos="fade-up"
              textAlign={"center"}
              component={"h1"}
            >
              Post Intagram
            </Typography>
            <Box component="div" position={"relative"}>
              <div data-aos-delay="200" data-aos="fade-up" className="ig-frame">
                <Slider {...settings}>
                  {images.map((v, i) => (
                    <div key={i}>
                      <Box
                        component="img"
                        style={{
                          marginLeft: 19,
                          marginTop: 44,
                        }}
                        height={{ xs: 273 }}
                        src={v}
                        alt="ig"
                      />
                    </div>
                  ))}
                </Slider>
              </div>
            </Box>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
}
