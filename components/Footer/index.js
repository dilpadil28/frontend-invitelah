import { Box, Divider, Stack, Typography } from "@mui/material";
import Link from "next/link";
import React from "react";
import { FaInstagram, FaFacebook, FaYoutube, FaTiktok } from "react-icons/fa";
import { api } from "../../config/api";
import styles from "../../styles/components/Footer.module.css";

export default function Footer({ desc }) {
  return (
    <footer className={styles.footer}>
      <div>
        {desc ? (
          <>
            <Typography
              variant="subtitle1"
              component={"p"}
              data-aos="fade-right"
            >
              Invitelah adalah sebuah website jasa pembuatan undangan online
              berupa website, video, dan foto dengan harga termurah dengan fitur
              terlengkap, hanya Rp40.000.
            </Typography>
            <Box my={2} component="div" />
          </>
        ) : (
          <></>
        )}
        <Stack
          direction="row"
          alignItems="center"
          justifyContent={"center"}
          spacing={2}
        >
          <Typography fontSize={16}>
            <a href={api.facebook} target="_blank" rel="noreferrer">
              <FaFacebook />
            </a>
          </Typography>
          <Typography fontSize={16}>
            <a href={api.instagram} target="_blank" rel="noreferrer">
              <FaInstagram />
            </a>
          </Typography>
          <Typography fontSize={16}>
            <a href={api.tiktok} target="_blank" rel="noreferrer">
              <FaTiktok />
            </a>
          </Typography>
          <Typography fontSize={16}>
            <a href={api.youtube} target="_blank" rel="noreferrer">
              <FaYoutube />
            </a>
          </Typography>
          {desc && (
            <>
              <Divider orientation="vertical" flexItem />
              <Typography fontSize={16}>
                <a href={api.bukalapak} target="_blank" rel="noreferrer">
                  <Box
                    component="img"
                    src={"/static/images/logo/bukalapak.png"}
                    alt="bukalapak"
                    height={16}
                  />
                </a>
              </Typography>
              <Typography fontSize={16}>
                <a href={api.tokopedia} target="_blank" rel="noreferrer">
                  <Box
                    component="img"
                    src={"/static/images/logo/tokopedia.png"}
                    alt="tokopedia"
                    height={16}
                  />
                </a>
              </Typography>
              <Typography fontSize={16}>
                <a href={api.shopee} target="_blank" rel="noreferrer">
                  <Box
                    component="img"
                    src={"/static/images/logo/shopee.png"}
                    alt="shopee"
                    height={16}
                  />
                </a>
              </Typography>
            </>
          )}
        </Stack>
        <Box my={2} component="div" />
        <span>
          © {new Date().getFullYear()}{" "}
          <span style={{ display: "inline-block" }}>
            {" "}
            <Link href="/">
              <a> invitelah.com.</a>
            </Link>
          </span>{" "}
          All right reserved
        </span>
      </div>
    </footer>
  );
}
