import { FlashOn, FlashOnRounded } from "@mui/icons-material";
import { Container, Grid, Paper, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import { Box } from "@mui/system";
import React from "react";
import MoneyIcon from "@mui/icons-material/Money";

const keunggulan = [
  {
    icon: (
      <FlashOnRounded
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-right"
      />
    ),
    title: "Cepat",
    desc: "Proses pembuatan undangan hanya membutuhkan beberapa jam, bahkan menit saja,tidak seperti menunggu chat gebetan :D",
  },
  {
    icon: (
      <MoneyIcon
        color="primary"
        sx={{ fontSize: { xs: 40, md: 54 } }}
        data-aos-delay="75"
        data-aos="fade-right"
      />
    ),
    title: "Murah",
    desc: "Harga yang di tawarkan relatif murah dibanding dengan undangan kertas, karena tidak dihitung perlembar :D",
  },
];

export default function Keunggulan() {
  return (
    <Container>
      <Box pt={10} pb={10} textAlign={"center"}>
        <Typography
          fontSize={{ xs: 24, sm: 40 }}
          fontWeight={500}
          data-aos="fade-right"
          component={"h1"}
        >
          Mengapa harus menggunkaan Undangan Invitelah ?
        </Typography>
        <Typography
          component={"p"}
          mt={3}
          mb={5}
          fontSize={{ xs: 12, md: 18 }}
          color={grey[500]}
          data-aos="fade-right"
        >
          Dari pada harus membeli undangan kertas yang di asumsikan harga
          perlembarnya Rp 1.000 x 500 orang = Rp 500.000, Lebih baik mengguankan
          invitelah.com hanya membayar mulai dari 20 ribu saja bisa disebar
          sebanyak-banyaknya hingga ratusan bahkan ribuan orang.
        </Typography>
        <Grid
          justifyContent={"center"}
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {keunggulan?.map((v, i) => (
            <Grid item xs={4} md={4} mb={5} key={i}>
              <Paper
                elevation={3}
                data-aos-delay="50"
                data-aos="fade-right"
                style={{
                  padding: 20,
                  borderRadius: 20,
                }}
              >
                {v.icon}
                <Typography
                  mt={3}
                  mb={5}
                  fontSize={{ xs: 18, md: 24 }}
                  component="h1"
                  fontWeight="bold"
                  data-aos-delay="100"
                  data-aos="fade-right"
                >
                  {v.title}
                </Typography>
                <Typography
                  fontSize={{ xs: 12, md: 14 }}
                  component="p"
                  data-aos-delay="125"
                  data-aos="fade-right"
                >
                  {v.desc}
                </Typography>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  );
}
