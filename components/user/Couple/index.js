import { Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";

import {
  FaInstagram,
  FaFacebookSquare,
  FaTwitter,
  FaTiktok,
  FaYoutube,
  FaTelegram,
  FaSnapchat,
  FaPinterest,
  FaWhatsapp,
} from "react-icons/fa";
import Title16 from "../Title/Title16";
import Title9 from "../Title/Title9";
import TitleHeader24 from "../Title/TitleHeader24";
import { api } from "../../../config/api";
export default function Couple() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Grid
      container
      className="col-evenly-center"
      height="100%"
      py={{ xs: 6, sm: 10 }}
      px={4}
    >
      <Box component="div">
        {invitationSlug?.bissmillah == false ? (
          <div data-aos="zoom-in">
            <Title16>Bismillahirrahmanirrahim</Title16>
          </div>
        ) : (
          <></>
        )}
        <div data-aos="zoom-in" data-aos-duration="200">
          <Title16>
            {invitationSlug?.salamPembuka == ""
              ? "Assalamualaikum Warahmatullahi Wabarakatuh"
              : invitationSlug?.salamPembuka}
          </Title16>
        </div>

        <Box component="div" px={{ xs: 3, sm: 5 }}></Box>
        <div data-aos="zoom-in" data-aos-duration="400">
          <Title9 px={2}>
            {invitationSlug?.salamPembukaDeskripsi == ""
              ? "Tanpa mengurangi rasa hormat, Kami mengundang Bapak/Ibu/Saudara/i serta kerabat sekalian untuk menghadiri acara pernikahan kami."
              : invitationSlug?.salamPembukaDeskripsi}
          </Title9>
        </div>
      </Box>
      <Box my={1} />
      <Box component="div">
        <Box component="div" className="flex justify-center" mb={2}>
          {invitationSlug?.avatarPria != "" &&
          invitationSlug?.avatarPria != undefined ? (
            <Box
              className=" rounded-xl"
              component="img"
              sx={{
                height: { xs: "200px", sm: "332px" },
              }}
              src={api.fileUrl + invitationSlug?.avatarPria}
              alt="avatar-couple"
            />
          ) : (
            <Box
              data-aos="zoom-in"
              data-aos-duration="600"
              component="img"
              sx={{
                height: { xs: "100px", sm: "232px" },
              }}
              src="/static/images/avatars/avatar-frame-man.png"
              alt="avatar-frame-man"
            />
          )}
        </Box>
        <div data-aos="zoom-in" data-aos-duration="800">
          <TitleHeader24>{invitationSlug?.namaPria}</TitleHeader24>
        </div>

        {invitationSlug?.namaOrangTuaPria != "" ? (
          <div data-aos="zoom-in" data-aos-duration="1000">
            <Title9>Putra dari {invitationSlug?.namaOrangTuaPria}</Title9>
          </div>
        ) : (
          <></>
        )}
        {invitationSlug?.socialmedia?.length > 0 ? (
          <Box
            className="flex justify-center"
            data-aos="zoom-in"
            data-aos-duration="1200"
            component="div"
            sx={{
              color: invitationSlug?.theme?.fontColor1
                ? invitationSlug?.theme?.fontColor1
                : "#C4C4C4",
              fontSize: { sm: 24 },
            }}
            mt={1}
          >
            {invitationSlug?.socialmedia
              ?.filter((v, i) => v.type === "pria")
              .map((v, i) =>
                v.name === "instagram" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaInstagram />
                  </Box>
                ) : v.name === "facebook" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaFacebookSquare />
                  </Box>
                ) : v.name === "twitter" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaTwitter />
                  </Box>
                ) : v.name === "tiktok" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaTiktok />
                  </Box>
                ) : v.name === "youtube" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaYoutube />
                  </Box>
                ) : v.name === "telegram" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaTelegram />
                  </Box>
                ) : v.name === "snapchat" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaSnapchat />
                  </Box>
                ) : v.name === "pinterest" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaPinterest />
                  </Box>
                ) : v.name === "whatsapp" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaWhatsapp />
                  </Box>
                ) : (
                  ""
                )
              )}
          </Box>
        ) : (
          <></>
        )}
        <div data-aos="zoom-in" data-aos-duration="1400">
          <Title16>Dengan</Title16>
        </div>
        <Box
          className="flex justify-center"
          component="div"
          mb={2}
          data-aos="zoom-in"
          data-aos-duration="1600"
        >
          {invitationSlug?.avatarWanita != "" &&
          invitationSlug?.avatarWanita != undefined ? (
            <Box
              className=" rounded-xl"
              component="img"
              sx={{
                height: { xs: "200px", sm: "332px" },
              }}
              src={api.fileUrl + invitationSlug?.avatarWanita}
              alt="avatar-couple"
            />
          ) : (
            <Box
              component="img"
              sx={{
                height: { xs: "100px", sm: "232px" },
              }}
              src="/static/images/avatars/avatar-frame-woman.png"
              alt="avatar-frame-man"
            />
          )}
        </Box>
        <div data-aos="zoom-in" data-aos-duration="1800">
          <TitleHeader24>{invitationSlug?.namaWanita}</TitleHeader24>
        </div>

        {invitationSlug?.namaOrangTuaWanita != "" ? (
          <div data-aos="zoom-in" data-aos-duration="2000">
            <Title9>Putri dari {invitationSlug?.namaOrangTuaWanita}</Title9>
          </div>
        ) : (
          <></>
        )}
        {invitationSlug?.socialmedia?.length > 0 ? (
          <Box
            className="flex justify-center"
            data-aos="zoom-in"
            data-aos-duration="2000"
            component="div"
            sx={{
              color: invitationSlug?.theme?.fontColor1
                ? invitationSlug?.theme?.fontColor1
                : "#C4C4C4",
              fontSize: { sm: 24 },
            }}
            mt={1}
          >
            {invitationSlug?.socialmedia
              ?.filter((v, i) => v.type === "wanita")
              .map((v, i) =>
                v.name === "instagram" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaInstagram />
                  </Box>
                ) : v.name === "facebook" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaFacebookSquare />
                  </Box>
                ) : v.name === "twitter" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaTwitter />
                  </Box>
                ) : v.name === "tiktok" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaTiktok />
                  </Box>
                ) : v.name === "youtube" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaYoutube />
                  </Box>
                ) : v.name === "telegram" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaTelegram />
                  </Box>
                ) : v.name === "snapchat" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaSnapchat />
                  </Box>
                ) : v.name === "pinterest" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaPinterest />
                  </Box>
                ) : v.name === "whatsapp" ? (
                  <Box
                    key={i}
                    component="a"
                    mr={1}
                    target="_blank"
                    href={v.url}
                  >
                    <FaWhatsapp />
                  </Box>
                ) : (
                  ""
                )
              )}
          </Box>
        ) : (
          <></>
        )}
      </Box>
    </Grid>
  );
}
