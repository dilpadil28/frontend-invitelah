import CardGiftcard from "@mui/icons-material/CardGiftcard";
import LiveTv from "@mui/icons-material/LiveTv";
import Money from "@mui/icons-material/Money";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  Slide,
  Snackbar,
  Stack,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import KirimPesan from "./KirimPesan";
import ReservasiKehadiran from "./ReservasiKehadiran";
import { useSelector } from "react-redux";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import { CopyToClipboard } from "react-copy-to-clipboard";
import MyButton from "../Button/MyButton";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function Wishes() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  const [open, setOpen] = React.useState(false);
  const [openKado, setOpenKado] = useState(false);
  const [openAngpao, setOpenAngpao] = useState(false);
  const [copied, setCopied] = useState(false);

  const handleClickCopy = () => {
    setOpen(true);
  };
  const handleCloseCopy = () => {
    setOpen(false);
  };
  const handleClickKado = () => {
    setOpenKado(true);
  };

  const handleCloseKado = () => {
    setOpenKado(false);
  };
  const handleClickAngpao = () => {
    setOpenAngpao(true);
  };

  const handleCloseAngpao = () => {
    setOpenAngpao(false);
  };
  return (
    <>
      {copied ? (
        <Snackbar
          open={open}
          onClose={handleCloseCopy}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          message="Copied"
        />
      ) : null}
      <Box py={7} px={{ xs: 4, sm: 14 }} component="div">
        <div>
          <ReservasiKehadiran />
        </div>
        {invitationSlug?.digitalenvelopes?.length > 0 ||
        invitationSlug?.alamatKado != "" ||
        invitationSlug?.socialmedia
          ?.filter((v) => v.type === "pria")
          .filter((v) => v.name === "live").length > 0 ||
        invitationSlug?.socialmedia
          ?.filter((v) => v.type === "wanita")
          .filter((v) => v.name === "live").length > 0 ? (
          <div>
            <Box my={2} />
            <Divider />
            <Box my={2} />
            <Stack
              direction="row"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              {invitationSlug?.digitalenvelopes?.length > 0 ? (
                <button
                  className="rounded-full py-1 px-5 text-xs"
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                    backgroundColor: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    p: 2,
                    color: "#fff",
                  }}
                  onClick={handleClickAngpao}
                >
                  <span className="mr-2">
                    <Money className=" w-4" />
                  </span>
                  Angpao
                </button>
              ) : (
                <></>
              )}
              {invitationSlug?.alamatKado != "" ? (
                <button
                  className="rounded-full py-1 px-5 text-xs"
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                    backgroundColor: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    p: 2,
                    color: "#fff",
                  }}
                  onClick={handleClickKado}
                >
                  <span className="mr-2">
                    <CardGiftcard className=" w-4" />
                  </span>
                  Kirim Kado
                </button>
              ) : (
                <></>
              )}
            </Stack>
            <Box my={2} />
            <Stack
              direction="row"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              {invitationSlug?.socialmedia
                ?.filter((v) => v.type === "pria")
                .filter((v) => v.name === "live")
                .map((v, i) => (
                  <a
                    target="_blank"
                    className="rounded-full py-1 px-5 text-xs"
                    style={{
                      fontFamily: invitationSlug?.theme?.fontType2
                        ? invitationSlug?.theme?.fontType2
                        : "Montserrat",
                      backgroundColor: invitationSlug?.theme?.cardColor
                        ? invitationSlug?.theme?.cardColor
                        : "#C4C4C4",
                      p: 2,
                      color: "#fff",
                    }}
                    key={i}
                    href={v.url}
                    rel="noreferrer"
                  >
                    <span className="mr-2">
                      <LiveTv className=" w-4" />
                    </span>
                    Live {invitationSlug?.namaPendekPria}
                  </a>
                ))}
              {invitationSlug?.socialmedia
                ?.filter((v) => v.type === "wanita")
                .filter((v) => v.name === "live")
                .map((v, i) => (
                  <a
                    target="_blank"
                    className="rounded-full py-1 px-5 text-xs"
                    style={{
                      fontFamily: invitationSlug?.theme?.fontType2
                        ? invitationSlug?.theme?.fontType2
                        : "Montserrat",
                      backgroundColor: invitationSlug?.theme?.cardColor
                        ? invitationSlug?.theme?.cardColor
                        : "#C4C4C4",
                      p: 2,
                      color: "#fff",
                    }}
                    key={i}
                    href={v.url}
                    rel="noreferrer"
                  >
                    <span className="mr-2">
                      <LiveTv className=" w-4" />
                    </span>
                    Live {invitationSlug?.namaPendekWanita}
                  </a>
                ))}
            </Stack>

            <Box my={2} />
            <Divider />
          </div>
        ) : (
          <></>
        )}
        <Box my={2} />
        <div>
          <KirimPesan />
        </div>

        <Dialog
          fullWidth
          open={openKado}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseKado}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Alamat"}</DialogTitle>
          <DialogContent dividers={true}>
            <Box component="div">
              <Box
                component="div"
                style={{
                  border: "2px dashed grey",
                  padding: 5,
                  position: "relative",
                }}
              >
                <Box
                  component="div"
                  style={{
                    position: "absolute",
                    right: 0,
                    backgroundColor: "white",
                    height: 30,
                  }}
                  onClick={handleClickCopy}
                >
                  <CopyToClipboard
                    text={invitationSlug?.alamatKado}
                    onCopy={() => setCopied(true)}
                  >
                    <IconButton color="primary" component="span">
                      <ContentCopyIcon />
                    </IconButton>
                  </CopyToClipboard>
                </Box>
                {invitationSlug?.alamatKado}
              </Box>
            </Box>
          </DialogContent>
          {/* <DialogActions>
            <MyButton >Konfirmasi</MyButton>
          </DialogActions> */}
        </Dialog>
        <Dialog
          fullWidth
          open={openAngpao}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseAngpao}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Angpao"}</DialogTitle>
          <div className="px-1 md:px-5">
            {invitationSlug?.digitalenvelopes?.map((v, i) => (
              <Box key={i} my={2} component="div">
                <div className=" w-72 md:w-full md:h-56 h-48 m-auto bg-red-100 rounded-xl relative text-white shadow-2xl transition-transform transform bg-gradient-to-tr from-gray-900 to-gray-600 bg-gradient-to-r">
                  <div className="w-full px-6 absolute top-8">
                    <div className="flex justify-between">
                      <div className=""></div>
                      <img
                        className="h-6 md:h-8"
                        src={`/static/images/digitalEnvelope/${v.name}.png`}
                        alt={v.name}
                      />
                    </div>
                    <div className="pt-4">
                      <p className="font-light text-xs">Nama</p>
                      <p className="font-medium tracking-widest text-base">
                        {v.atasNama}
                      </p>
                    </div>
                    <div className="pt-3 ">
                      <p className="font-light text-xs">
                        Nomor Rekening / Handphone
                      </p>
                      <div className="flex gap-3 text-lg">
                        <div className="font-medium tracking-more-wider">
                          {v.number}
                        </div>
                        <div
                          className=" cursor-pointer "
                          onClick={handleClickCopy}
                        >
                          <CopyToClipboard
                            text={v.number}
                            onCopy={() => setCopied(true)}
                          >
                            <IconButton
                              size="small"
                              color="inherit"
                              sx={{ padding: 0 }}
                              component="span"
                            >
                              <ContentCopyIcon className=" !w-4" />
                            </IconButton>
                          </CopyToClipboard>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Box>
            ))}
          </div>

          {/* <DialogActions>
            <MyButton >Konfirmasi</MyButton>
          </DialogActions> */}
        </Dialog>
      </Box>
    </>
  );
}
