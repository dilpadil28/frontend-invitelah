import {
  Alert,
  Snackbar,
  Button,
  FormGroup,
  MenuItem,
  TextField,
  FormHelperText,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import TitleHeader from "../Title/TitleHeader";
// third party
import * as Yup from "yup";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { createPresence } from "../../../features/presence/presenceSlice";
import MyButton from "../Button/MyButton";
export default function ReservasiKehadiran() {
  const initialValues = {
    name: "",
    message: "",
    confirmation: "",
    // phoneNumber: "",
    total: "1",
  };
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Nama is required"),
    // phoneNumber: Yup.string().required("Nomor Handpone is required"),
    confirmation: Yup.string().required("Konfirmasi Kehadiran is required"),
  });

  const { message } = useSelector((state) => state.message);
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  const dispatch = useDispatch();
  const [successful, setSuccessful] = useState(false);
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  };

  const onSubmit = async (
    values,
    { setErrors, setStatus, setSubmitting, resetForm }
  ) => {
    const { name, message, phoneNumber, confirmation, total } = values;

    const data = {
      name,
      message,
      phoneNumber:"00",
      confirmation,
      total,
      published: true,
      invitationId: invitationSlug?.id,
    };
    // console.log('data', data)
    dispatch(createPresence(data))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpenSnackbar(true);
        resetForm();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpenSnackbar(true);
        console.log("err", err);
      });
  };
  return (
    <Box component="div">
      <TitleHeader text="Reservasi Kehadiran" />

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <FormGroup>
              <label
                htmlFor="name"
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
              >
                Nama *
              </label>
              <input
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
                type="text"
                id="name"
                name="name"
                className="my-input"
                value={values.name}
                onBlur={handleBlur}
                onChange={handleChange}
                placeholder="Masukan Nama"
              />
              {/* <TextField
                required
                error={Boolean(touched.name && errors.name)}
                name="name"
                value={values.name}
                onBlur={handleBlur}
                onChange={handleChange}
                label="Masukan Nama" variant="outlined" size="small" fullWidth /> */}
              {touched.name && errors.name && (
                <FormHelperText
                  error
                  id="standard-weight-helper-text--register"
                >
                  {errors.name}
                </FormHelperText>
              )}
            </FormGroup>
            <Box my={1} />
            {/* <FormGroup>
              <label
                htmlFor="phoneNumber"
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
              >
                No WhatsApp *
              </label>
              <input
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
                type="number"
                id="phoneNumber"
                name="phoneNumber"
                className="my-input"
                value={values.phoneNumber}
                onBlur={handleBlur}
                onChange={handleChange}
                placeholder="Masukan No WhatsApp"
              />
              {touched.phoneNumber && errors.phoneNumber && (
                <FormHelperText
                  error
                  id="standard-weight-helper-text--register"
                >
                  {errors.phoneNumber}
                </FormHelperText>
              )}
            </FormGroup> */}
            <Box my={1} />
            <FormGroup>
              <label
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
                htmlFor="message"
              >
                Pesan
              </label>
              <textarea
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
                className="my-input"
                id="message"
                name="message"
                placeholder="Masukan Pesan"
                value={values.message}
                onBlur={handleBlur}
                onChange={handleChange}
              ></textarea>
              {/* <TextField
                name="message"
                value={values.message}
                onBlur={handleBlur}
                onChange={handleChange}
                variant="outlined" size="small"
                id="outlined-multiline-static"
                label="Message"
                multiline
                rows={2}
                fullWidth
              /> */}
            </FormGroup>
            <Box my={1} />
            <FormGroup>
              <label
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
                htmlFor="confirmation"
              >
                Konfirmasi Kehadiran *
              </label>
              <select
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                }}
                name="confirmation"
                className="my-input"
                value={values.confirmation}
                onBlur={handleBlur}
                onChange={handleChange}
                defaultValue=""
              >
                <option value="">Pilih</option>
                <option value={"hadir"}>Hadir</option>
                <option value={"tidak hadir"}>Tidak Hadir</option>
                <option value={"masih ragu"}>Masih Ragu</option>
              </select>
              {/* <TextField
                required
                error={Boolean(touched.confirmation && errors.confirmation)}
                name='confirmation'
                value={values.confirmation}
                onBlur={handleBlur}
                onChange={handleChange}
                id="filled-select-currency"
                select
                label="Konfirmasi Kehadiran"
                variant="outlined" size="small"
                fullWidth
                defaultValue={'hadir'}
              >
                <MenuItem value={'hadir'}>Hadir</MenuItem>
                <MenuItem value={'tidak hadir'}>Tidak Hadir</MenuItem>
                <MenuItem value={'masih ragu'}>Masih Ragu</MenuItem>
              </TextField> */}
              {touched.confirmation && errors.confirmation && (
                <FormHelperText
                  error
                  id="standard-weight-helper-text--register"
                >
                  {errors.confirmation}
                </FormHelperText>
              )}
            </FormGroup>
            <Box my={1} />
            {values.confirmation === "hadir" ? (
              <FormGroup>
                <label
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                  }}
                  htmlFor="total"
                >
                  Jumlah yang Datang
                </label>
                <select
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                  }}
                  name="total"
                  className="my-input"
                  value={values.total}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  defaultValue={"1"}
                >
                  <option value={"1"}>1 Orang</option>
                  <option value={"2"}>2 Orang</option>
                  <option value={"3"}>3 Orang</option>
                </select>
                {/* <TextField
                    name='total'
                    value={values.total}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    id="filled-select-currency"
                    select
                    label="Jumlah yang Datang"
                    variant="outlined" size="small"
                    fullWidth
                    defaultValue={'1'}
                  >
                    <MenuItem value={'1'}>1 Orang</MenuItem>
                    <MenuItem value={'2'}>2 Orang</MenuItem>
                    <MenuItem value={'3'}>3 Orang</MenuItem>
                  </TextField> */}
              </FormGroup>
            ) : (
              ""
            )}
            <Box my={2} />
            <Box display={"flex"} justifyContent="center">
              <button
                className="rounded-full py-2 px-6 text-xs"
                style={{
                  fontFamily: invitationSlug?.theme?.fontType2
                    ? invitationSlug?.theme?.fontType2
                    : "Montserrat",
                  backgroundColor: invitationSlug?.theme?.cardColor
                    ? invitationSlug?.theme?.cardColor
                    : "#C4C4C4",
                  p: 2,
                  color: "#fff",
                }}
                type="submit"
              >
                Konfirmasi
              </button>
            </Box>
          </form>
        )}
      </Formik>

      {message && (
        <Snackbar
          open={openSnackbar}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleCloseSnackbar}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleCloseSnackbar}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </Box>
  );
}
