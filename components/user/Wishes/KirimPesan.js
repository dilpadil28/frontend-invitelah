import {
  Alert,
  Button,
  FormGroup,
  FormHelperText,
  Snackbar,
  TextField,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import TitleHeader from "../Title/TitleHeader";

// third party
import * as Yup from "yup";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  createMessageApi,
  getMessageApiByInvitationId,
} from "../../../features/messageApi/messageApiSlice";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import MyButton from "../Button/MyButton";
export default function KirimPesan() {
  const initialValuesPesan = {
    name: "",
    message: "",
  };
  const validationSchemaPesan = Yup.object().shape({
    name: Yup.string().required("Nama is required"),
    message: Yup.string().required("Pesan is required"),
  });

  const { message } = useSelector((state) => state.message);
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  const { messageApi } = useSelector((state) => state.messageApi);
  const dispatch = useDispatch();
  const [successful, setSuccessful] = useState(false);
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  };

  const onSubmitPesan = async (
    values,
    { setErrors, setStatus, setSubmitting, resetForm }
  ) => {
    const { name, message } = values;

    const data = {
      name: name,
      message: message,
      published: true,
      invitationId: invitationSlug?.id,
    };
    dispatch(createMessageApi(data))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpenSnackbar(true);
        resetForm();
        dispatch(getMessageApiByInvitationId(invitationSlug?.id));
      })
      .catch((err) => {
        setSuccessful(false);
        setOpenSnackbar(true);
        console.log("err", err);
      });
  };
  return (
    <>
      <Formik
        initialValues={initialValuesPesan}
        validationSchema={validationSchemaPesan}
        onSubmit={onSubmitPesan}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Box component="div">
              <TitleHeader text="Kirim Pesan" />
              <FormGroup>
                <label
                  htmlFor="name"
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                  }}
                >
                  Nama *
                </label>
                <input
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                  }}
                  type="text"
                  id="name"
                  name="name"
                  className="my-input"
                  value={values.name}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Masukan Nama"
                />
                {/* <TextField
                  error={Boolean(touched.name && errors.name)}
                  name='name'
                  value={values.name}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  label="Masukan Nama" variant="outlined" size="small" fullWidth /> */}
                {touched.name && errors.name && (
                  <FormHelperText
                    error
                    id="standard-weight-helper-text--register"
                  >
                    {errors.name}
                  </FormHelperText>
                )}
              </FormGroup>
              <Box my={1} />
              <FormGroup>
                <label
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                  }}
                  htmlFor="message"
                >
                  Ucapan dan Doa Restu *
                </label>
                <textarea
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                  }}
                  className="my-input"
                  id="message"
                  name="message"
                  placeholder="Masukan Ucapan dan Doa"
                  value={values.message}
                  onBlur={handleBlur}
                  onChange={handleChange}
                ></textarea>
                {/* <TextField
                  error={Boolean(touched.message && errors.message)}
                  name='message'
                  value={values.message}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  variant="outlined"
                  size="small"
                  id="outlined-multiline-static"
                  label="Ucapan dan Doa Restu"
                  multiline
                  rows={2}
                  fullWidth
                /> */}
                {touched.message && errors.message && (
                  <FormHelperText
                    error
                    id="standard-weight-helper-text--register"
                  >
                    {errors.message}
                  </FormHelperText>
                )}
              </FormGroup>
              <Box my={2} />
              <Box display={"flex"} justifyContent="center">
                <button
                  className="rounded-full py-2 px-6 text-xs"
                  style={{
                    fontFamily: invitationSlug?.theme?.fontType2
                      ? invitationSlug?.theme?.fontType2
                      : "Montserrat",
                    backgroundColor: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    p: 2,
                    color: "#fff",
                  }}
                  type="submit"
                >
                  Kirim
                </button>
              </Box>
            </Box>
          </form>
        )}
      </Formik>
      <Box my={2} />
      {messageApi?.length > 0 ? (
        <Box component="div">
          <List
            className=" rounded-xl"
            sx={{
              width: "100%",
              bgcolor: "background.paper",
              position: "relative",
              overflow: "auto",
              maxHeight: 300,
              "& ul": { padding: 0 },
            }}
            subheader={<li />}
          >
            {messageApi?.map((v, i) => (
              <div key={i}>
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <AccountCircleIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={v.name}
                    secondary={
                      <>
                        <Typography
                          sx={{ display: "inline" }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          {v.message}
                        </Typography>
                      </>
                    }
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
              </div>
            ))}
          </List>
        </Box>
      ) : (
        <></>
      )}

      {message && (
        <Snackbar
          open={openSnackbar}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleCloseSnackbar}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleCloseSnackbar}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
