import { Box } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";
import { api } from "../../../config/api";

export default function Avatar() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <>
      {invitationSlug?.avatarPasangan != "" &&
      invitationSlug?.avatarPasangan != undefined ? (
        <Box
          className=" rounded-xl"
          component="img"
          sx={{
            height: { xs: "170px", sm: "332px" },
          }}
          src={api.fileUrl + invitationSlug?.avatarPasangan}
          alt="avatar-couple"
        />
      ) : (
        <Box
          component="img"
          sx={{
            height: { xs: "200px", sm: "332px" },
          }}
          src="/static/images/avatars/avatar-couple.svg"
          alt="avatar-couple"
        />
      )}
    </>
  );
}
