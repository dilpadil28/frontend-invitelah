import { Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { FaHeart } from "react-icons/fa";
import { useSelector } from "react-redux";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";

import styles from "../../../styles/components/LoveStory.module.css";
import Title12 from "../Title/Title12";
import Title9 from "../Title/Title9";
import TitleHeader from "../Title/TitleHeader";
import Doa from "./Doa";
export default function LoveStory() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <Box component="div" height="100%">
      <Grid
        container
        display={"flex"}
        alignItems={"center"}
        justifyContent={"center"}
        height="100%"
        width="100%"
        py={{ xs: 7, sm: 10 }}
        px={4}
      >
        {invitationSlug?.lovestories?.length > 0 ? (
          <>
            <Box component="div" data-aos="zoom-in" data-aos-duration="200">
              <TitleHeader text="Cerita Cinta" />
            </Box>
            <VerticalTimeline
              data-aos="zoom-in"
              data-aos-duration="500"
              lineColor="#fff"
            >
              {invitationSlug?.lovestories?.map((v, i) => (
                <VerticalTimelineElement
                  key={i}
                  className="vertical-timeline-element--work "
                  contentStyle={{
                    borderRadius: "10px",
                    background: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    color: "#fff",
                    padding: 5,
                  }}
                  contentArrowStyle={{
                    borderRight: `7px solid ${
                      invitationSlug?.theme?.cardColor
                        ? invitationSlug?.theme?.cardColor
                        : "#C4C4C4"
                    }`,
                  }}
                  icon={<FaHeart />}
                  iconStyle={{
                    background: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    color: "#fff",
                  }}
                  date={v.date}
                  dateClassName={styles.dateColor}
                >
                  <Title12 sx={{ color: "#fff", fontWeight: "bold" }}>
                    {v.title}
                  </Title12>

                  {v.description != "" ? (
                    <Title9 sx={{ color: "#fff" }}>{v.description}</Title9>
                  ) : (
                    ""
                  )}
                </VerticalTimelineElement>
              ))}
            </VerticalTimeline>
          </>
        ) : (
          <></>
        )}
        <div data-aos="zoom-in" data-aos-duration="1000">
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Doa />
          </Grid>
        </div>
      </Grid>
    </Box>
  );
}
