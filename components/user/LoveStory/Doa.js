import { Grid, Typography } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';
import MyCard from '../Card/MyCard'

export default function Doa() {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <MyCard>
      <Typography component="div" textAlign="justify" fontSize={{ xs: '10px', sm: '16px' }} >
        {invitationSlug?.doa != "" ?
          invitationSlug?.doa
          :
          " Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir. (Q.S Ar Rum:21)"

        }
      </Typography>
    </MyCard>
  )
}
