import { Button, Grid, Link, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect } from "react";
import TitleHeader from "../Title/TitleHeader";

import moment from "moment";

import NotificationsActiveIcon from "@mui/icons-material/NotificationsActive";
import CountDown from "./CountDown";
import { useSelector } from "react-redux";
import { api } from "../../../config/api";
import Avatar from "../Avatar";
import MyButton from "../Button/MyButton";
import Title12 from "../Title/Title12";

export default function Home() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <Grid
      container
      className="col-evenly-center"
      height="100%"
      py={{ xs: 6, sm: 10 }}
      px={4}
    >
      <Box component="div">
        <TitleHeader text="Undangan" />
      </Box>
      <Box component="div">
        <Box component="div" className="flex justify-center" mb={3}>
          <Avatar />
        </Box>
        <div>
          <TitleHeader
            text={
              invitationSlug?.namaPendekPria +
              " & " +
              invitationSlug?.namaPendekWanita
            }
          />
        </div>
        <Title12 px={3}>
          Kami berharap anda menjadi bagian dari hari istimewa kami.
        </Title12>
        <Box my={2} />
        <div>
          <CountDown />
        </div>
        <Box my={3} />
        <div className="flex items-center justify-center">
          <a
            target="_blank"
            className="rounded-full py-2 px-6 text-xs"
            style={{
              fontFamily: invitationSlug?.theme?.fontType2
                ? invitationSlug?.theme?.fontType2
                : "Montserrat",
              backgroundColor: invitationSlug?.theme?.cardColor
                ? invitationSlug?.theme?.cardColor
                : "#C4C4C4",
              p: 2,
              color: "#fff",
            }}
            href={`https://www.google.com/calendar/render?action=TEMPLATE&text=Pernikahan+${
              invitationSlug?.namaPendekPria
            }+dan+${invitationSlug?.namaPendekWanita}&dates=${moment(
              invitationSlug?.tanggalResepsi
            )
              .local("id")
              .format("YYYYMMDD[T]HHmmss")}/${moment(
              invitationSlug?.tanggalResepsi
            )
              .local("id")
              .format("YYYYMMDD[T]HHmmss")}&details=link:+${api.home}/${
              invitationSlug?.slug
            }&location=lokasi:+${invitationSlug?.mapsResepsi}`}
            rel="noreferrer"
          >
            <span className="mr-2">
              <NotificationsActiveIcon className=" w-5" />
            </span>
            Pengingat Tanggal
          </a>
        </div>
      </Box>
    </Grid>
  );
}
