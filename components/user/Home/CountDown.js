import { Grid, Paper, Typography } from "@mui/material";
import moment from "moment";
import React, { useEffect } from "react";
import Countdown from "react-countdown";
import { useSelector } from "react-redux";
import Title12 from "../Title/Title12";
import Title16 from "../Title/Title16";
import Title18 from "../Title/Title18";

const stylePaper = {
  textAlign: "center",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  minWidth: 65,
  margin: 3,
  height: 65,
};

export default function CountDown() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <>
      {/* YYYY-MM-DDTHH:mm */}
      {/* {moment(invitationSlug?.tanggalResepsi).local('id').format('YYYY-MM-DDTHH:mm[Z]')} */}
      <Countdown
        date={moment(invitationSlug?.tanggalResepsi)
          .local("id")
          .format("YYYY-MM-DDTHH:mm")}
        renderer={({ days, hours, minutes, seconds, completed }) => {
          if (completed) {
            // Render a complete state
            return <Title18>Acara Telah Selesai</Title18>;
          } else {
            // Render a countdown
            return (
              <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
              >
                <Paper
                  className="rounded-xl"
                  style={stylePaper}
                  sx={{
                    background: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                  }}
                >
                  <div>
                    <Title16 sx={{ pt: 1, color: "#fff" }}>{days}</Title16>
                    <Title12 sx={{ pb: 1, color: "#fff" }}>Hari</Title12>
                  </div>
                </Paper>
                <Paper
                  className="rounded-xl"
                  style={stylePaper}
                  sx={{
                    background: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    color: "#fff",
                  }}
                >
                  <div>
                    <Title16 sx={{ pt: 1, color: "#fff" }}>{hours}</Title16>
                    <Title12 sx={{ pb: 1, color: "#fff" }}>Jam</Title12>
                  </div>
                </Paper>
                <Paper
                  className="rounded-xl"
                  style={stylePaper}
                  sx={{
                    background: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    color: "#fff",
                  }}
                >
                  <div>
                    <Title16 sx={{ pt: 1, color: "#fff" }}>{minutes}</Title16>
                    <Title12 sx={{ pb: 1, color: "#fff" }}>Menit</Title12>
                  </div>
                </Paper>
                <Paper
                  className="rounded-xl"
                  style={stylePaper}
                  sx={{
                    background: invitationSlug?.theme?.cardColor
                      ? invitationSlug?.theme?.cardColor
                      : "#C4C4C4",
                    color: "#fff",
                  }}
                >
                  <div>
                    <Title16 sx={{ pt: 1, color: "#fff" }}>{seconds}</Title16>
                    <Title12 sx={{ pb: 1, color: "#fff" }}>Detik</Title12>
                  </div>
                </Paper>
              </Grid>
            );
          }
        }}
      />
    </>
  );
}
