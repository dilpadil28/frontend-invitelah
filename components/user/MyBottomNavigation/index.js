import React from "react";
import { Link } from "react-scroll";

import { RiHomeHeartLine, RiHeartsLine, RiChatHeartLine } from "react-icons/ri";
import { BiCalendarHeart } from "react-icons/bi";
import {
  BottomNavigation,
  BottomNavigationAction,
  Container,
  Fab,
  Paper,
} from "@mui/material";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PlayDisabledIcon from "@mui/icons-material/PlayDisabled";
import { useSelector } from "react-redux";

export default function MyBottomNavigation({ play, setPlay }) {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  const handlePlayPause = () => {
    setPlay(!play);
  };
  return (
    <Paper
      sx={{
        zIndex: 99,
        position: "fixed",
        bottom: 0,
        left: 0,
        right: 0,
      }}
      elevation={3}
    >
      <Container maxWidth="md" sx={{ position: "relative" }}>
        <button
          style={{
            background: invitationSlug?.theme?.cardColor
              ? invitationSlug?.theme?.cardColor
              : "#C4C4C4",
            color: "#fff",
            padding: 5,
            margin: 0,
            position: "absolute",
            bottom: 65,
            right: 30,
          }}
          className="rounded-xl"
          onClick={handlePlayPause}
        >
          {play ? <PlayDisabledIcon /> : <PlayArrowIcon />}
        </button>
        {/* <Fab
          onClick={handlePlayPause}
          sx={{
            position: 'absolute',
            bottom: 65,
            right: 30,
          }}
          size="small" color="primary" aria-label="add"
        >
          {play ? <PlayDisabledIcon /> : <PlayArrowIcon />}
        </Fab> */}
        <BottomNavigation showLabels>
          <BottomNavigationAction
            label={
              <Link
                activeClass="active"
                to="home"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                Home
              </Link>
            }
            icon={
              <Link
                activeClass="active"
                to="home"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                <RiHomeHeartLine fontSize={30} />
              </Link>
            }
          />
          <BottomNavigationAction
            label={
              <Link
                activeClass="active"
                to="couple"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                Couple
              </Link>
            }
            icon={
              <Link
                activeClass="active"
                to="couple"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                <RiHeartsLine fontSize={30} />
              </Link>
            }
          />
          <BottomNavigationAction
            label={
              <Link
                activeClass="active"
                to="event"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                Event
              </Link>
            }
            icon={
              <Link
                activeClass="active"
                to="event"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                <BiCalendarHeart fontSize={30} />
              </Link>
            }
          />
          <BottomNavigationAction
            label={
              <Link
                activeClass="active"
                to="wishes"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                Wishes
              </Link>
            }
            icon={
              <Link
                activeClass="active"
                to="wishes"
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                <RiChatHeartLine fontSize={30} />
              </Link>
            }
          />
        </BottomNavigation>
      </Container>
    </Paper>
  );
}
