import React from 'react'
import { Grid, Typography, Box } from '@mui/material';

import TitleHeader from '../Title/TitleHeader'
import Lottie from 'react-lottie';
import wash from './animate/editor_os5wnd6r.json'
import sd from './animate/keep-social-distancing.json'
import hands from './animate/wash-your-hands-covid-19.json'
import mask from './animate/wear-mask.json'
import Title9 from '../Title/Title9';
export default function Prokes() {
  const animationOptions1 = {
    loop: true,
    autoplay: true,
    animationData: wash,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  const animationOptions2 = {
    loop: true,
    autoplay: true,
    animationData: sd,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  const animationOptions3 = {
    loop: true,
    autoplay: true,
    animationData: hands,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  const animationOptions4 = {
    loop: true,
    autoplay: true,
    animationData: mask,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  return (
    <Grid
      container
      minHeight={{ xs: '260px', sm: '400px', md: "585px" }}
      py={{ xs: 6, sm: 10 }}
      px={{ xs: 4, md: 8 }}
      display="flex"
      justifyContent="center"
      alignItems="center"
      textAlign="center"
    >
      <div>
        <Box component="div"  >
          <TitleHeader data-aos="zoom-in"
            data-aos-duration="200" text="Protokol Keseahatan" />
          <div
            data-aos="zoom-in"
            data-aos-duration="500"
          >
            <Title9 textAlign={'justify'} px={{ xs: 3, sm: 5 }}>
              Untuk mencegah  penyebaran Covid-19, diharapkan bagi tamu undangan yang hadir untuk mematuhi Protokol Kesehatan dibawah ini :

            </Title9>
          </div>
        </Box>
        <Box my={2} />
        <Grid container spacing={2}
        >
          <Grid data-aos="zoom-in"
            data-aos-duration="700" item xs={6} md={3}>
            <Lottie options={animationOptions4} height={150} />
            <Title9>
              Menggunakan Masker
            </Title9>
          </Grid>
          <Grid data-aos="zoom-in"
            data-aos-duration="900" item xs={6} md={3}>
            <Lottie options={animationOptions3} height={150} />
            <Title9>Cuci Tangan dan Gunakan Sabun</Title9>
          </Grid>
          <Grid data-aos="zoom-in"
            data-aos-duration="1100" item xs={6} md={3}>
            <Lottie options={animationOptions2} height={150} />
            <Title9>Dilarang Salaman</Title9>
          </Grid>
          <Grid data-aos="zoom-in"
            data-aos-duration="1300" item xs={6} md={3}>
            <Lottie options={animationOptions1} height={150} />
            <Title9>Jaga Jarak dan Jauhkan Kerumunan</Title9>
          </Grid>
        </Grid>

      </div>
    </Grid>
  )
}
