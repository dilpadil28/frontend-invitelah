import { Button, Dialog, DialogContent, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Avatar from "../Avatar";
import MyButton from "../Button/MyButton";
import ContainerFrame from "../Container/ContainerFrame";
import Title12 from "../Title/Title12";
import Title18 from "../Title/Title18";
import TitleHeader from "../Title/TitleHeader";

export default function FirstDialog({ play, setPlay, to }) {
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(false);
    setPlay(!play);
  };
  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Dialog
      maxWidth={"md"}
      fullScreen
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogContent sx={{ padding: "0px 0px" }}>
        <ContainerFrame>
          <Box id="home" component="div" textAlign="center" height="100%">
            <Grid container className="col-evenly-center" height="100%" py={6}>
              <Box component="div">
                <TitleHeader text="Undangan" />
              </Box>
              <Box component="div">
                <Box className="flex justify-center" component="div" mb={3}>
                  <Avatar />
                </Box>
                <TitleHeader
                  text={
                    invitationSlug?.namaPendekPria +
                    " & " +
                    invitationSlug?.namaPendekWanita
                  }
                />
                <Box my={2} />
                <Box component="div" px={2}>
                  <Title12>Kepada Bapak/Ibu/Saudara/i</Title12>
                  <Title18>{to ? to : "Kamu & Partner"}</Title18>
                  <button
                    className="rounded-full py-2 px-5 text-xs"
                    style={{
                      fontFamily: invitationSlug?.theme?.fontType2
                        ? invitationSlug?.theme?.fontType2
                        : "Montserrat",
                      backgroundColor: invitationSlug?.theme?.cardColor
                        ? invitationSlug?.theme?.cardColor
                        : "#C4C4C4",
                      p: 2,
                      color: "#fff",
                    }}
                    onClick={handleClose}
                  >
                    Buka Undangan
                  </button>
                  <Box my={2} />
                  <Title12>
                    Tanpa mengurangi rasa hormat, kami mengundangan anda untuk
                    hadir di acara pernikahan kami.
                  </Title12>
                </Box>
              </Box>
            </Grid>
          </Box>
        </ContainerFrame>
      </DialogContent>
    </Dialog>
  );
}
