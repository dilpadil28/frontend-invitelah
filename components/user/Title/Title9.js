import { Typography } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';

export default function Title9(props) {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Typography {...props} gutterBottom component="div" fontSize={{ xs: '9px', sm: '16px' }} fontFamily={invitationSlug?.theme?.fontType2 ? invitationSlug?.theme?.fontType2 : "Montserrat"}
      color={invitationSlug?.theme?.fontColor2 ? invitationSlug?.theme?.fontColor2 : "#C4C4C4"}
    >
      {props.children}
    </Typography>
  )
}
