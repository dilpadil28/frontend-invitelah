import { Typography } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';

export default function Title18({ children }) {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Typography textTransform={'capitalize'} component="div" gutterBottom fontWeight="bold" fontSize={{ xs: '18px', sm: '24px' }} px={3} fontFamily={invitationSlug?.theme?.fontType2 ? invitationSlug?.theme?.fontType2 : "Montserrat"} color={invitationSlug?.theme?.fontColor2 ? invitationSlug?.theme?.fontColor2 : "#C4C4C4"}>
      {children}
    </Typography>
  )
}
