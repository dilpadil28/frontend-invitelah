import { Typography } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';

export default function Title12(props) {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Typography {...props} component="div" gutterBottom fontSize={{ xs: '12px', sm: '16px' }} fontFamily={invitationSlug?.theme?.fontType2 ? invitationSlug?.theme?.fontType2 : "Montserrat"}
      color={invitationSlug?.theme?.fontColor2 ? invitationSlug?.theme?.fontColor2 : "#C4C4C4"}
    >
      {props.children}
    </Typography>
  )
}
