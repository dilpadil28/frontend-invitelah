import { Typography } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';

export default function TitleHeaderCard({ text }) {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Typography
      textTransform={'capitalize'}
      component="div" textAlign="center" fontFamily={invitationSlug?.theme?.fontType1 ? invitationSlug?.theme?.fontType1 : "Alex Brush"} fontSize={{ xs: '34px', sm: '60px' }}
      color={"#fff"}
    >
      {text}
    </Typography>
  )
}
