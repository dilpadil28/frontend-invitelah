import { Typography } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux';

export default function TitleHeader24(props) {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Typography
      textTransform={'capitalize'}
      {...props} component="div" textAlign="center" fontFamily={invitationSlug?.theme?.fontType1 ? invitationSlug?.theme?.fontType1 : "Alex Brush"} fontSize={{
        xs: '24px', sm: '46px'
      }}
      color={invitationSlug?.theme?.fontColor1 ? invitationSlug?.theme?.fontColor1 : "#C4C4C4"}
    >
      {props.children}
    </Typography>
  )
}
