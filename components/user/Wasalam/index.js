import { Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { QRCodeCanvas, QRCodeSVG } from "qrcode.react";
import React from "react";
import { useSelector } from "react-redux";
import { api } from "../../../config/api";
import Avatar from "../Avatar";
import Title12 from "../Title/Title12";
import Title16 from "../Title/Title16";
import Title9 from "../Title/Title9";
import TitleHeader from "../Title/TitleHeader";

export default function Wasalam({ to }) {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <Box component="div" height="100%">
      <Grid
        container
        className="col-evenly-center"
        height="100%"
        width="100%"
        py={{ xs: 6, sm: 10 }}
        px={4}
        textAlign="center"
      >
        <div
          className="flex justify-center"
          data-aos="zoom-in"
          data-aos-duration="200"
        >
          <Avatar />
        </div>
        <Box my={1} component="div">
          <div data-aos="zoom-in" data-aos-duration="400">
            <Title12>
              Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila
              Bapak/Ibu/Saudara/i berkenan hadir dan memberiakn doa restu.Atas
              Kehadiran dan doa restunya, kami mengucapkan terima kasih.
            </Title12>
          </div>
          <div data-aos="zoom-in" data-aos-duration="600">
            <Title16>Wassalamualaikum Warahmatullahi Wabarakatuh</Title16>
          </div>
        </Box>
        <Box component="div">
          <div data-aos="zoom-in" data-aos-duration="800">
            <TitleHeader
              text={
                invitationSlug?.namaPendekPria +
                " & " +
                invitationSlug?.namaPendekWanita
              }
            />
          </div>

          <Box
            className="flex justify-center"
            data-aos="zoom-in"
            data-aos-duration="1000"
            component="div"
            my={2}
          >
            <QRCodeCanvas value={to} />
          </Box>
          <div data-aos="zoom-in" data-aos-duration="1200">
            <Title9>
              *Tampilkan barcode ini kepada panitia acara pernikahan untuk
              mengisi buku tamu
            </Title9>
          </div>
        </Box>
      </Grid>
    </Box>
  );
}
