import { Box } from '@mui/system'
import React from 'react'

export default function BlueFlower({ dialog }) {
  return (
    <>
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "130px", sm: "220px", md: "292px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,
          right: 0
        }}
        src="/static/images/themes/Blue-Flower/Blue-Flower-top.png"
        alt="top-right"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "130px", sm: "220px", md: "292px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
          left: 0
        }}
        src="/static/images/themes/Blue-Flower/Blue-Flower-bottom.png"
        alt="bottom-left"
      />
    </>
  )
}
