import { Box } from '@mui/system'
import React from 'react'

export default function RedFlower({ dialog }) {
  return (
    <>
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "180px", md: "200px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,
          left: 0,
        }}
        src="/static/images/themes/Red-Flower/Red-Flower-top-left.png"
        alt="top-left"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "180px", md: "200px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,
          right: 0,
        }}
        src="/static/images/themes/Red-Flower/Red-Flower-top-right.png"
        alt="top-right"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "180px", md: "200px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
          left: 0,
        }}
        src="/static/images/themes/Red-Flower/Red-Flower-bottom-left.png"
        alt="bottom-left"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "180px", md: "200px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
          right: 0,
        }}
        src="/static/images/themes/Red-Flower/Red-Flower-bottom-right.png"
        alt="bottom-right"
      />
    </>
  )
}
