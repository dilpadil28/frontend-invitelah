import { Box } from '@mui/system'
import React from 'react'

export default function BrownCrystal({ dialog }) {
  return (
    <>
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "200px", md: "282px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,
          left: 0,
        }}
        src="/static/images/themes/Brown-Crystal/Brown-Crystal-top-left.png"
        alt="top-left"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "200px", md: "282px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 120,
          right: 0,
        }}
        src="/static/images/themes/Brown-Crystal/Brown-Crystal-top-right.png"
        alt="top-right"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "200px", md: "282px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 120,
          left: 0,
        }}
        src="/static/images/themes/Brown-Crystal/Brown-Crystal-bottom-left.png"
        alt="bottom-left"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "120px", sm: "200px", md: "282px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
          right: 0,
        }}
        src="/static/images/themes/Brown-Crystal/Brown-Crystal-bottom-right.png"
        alt="bottom-right"
      />
    </>
  )
}
