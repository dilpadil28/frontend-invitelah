import { Box } from '@mui/system'
import React from 'react'

export default function BrownFrame({ dialog }) {
  return (
    <>
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "110px", sm: "200px", md: "272px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,
          left: 0,
        }}
        src="/static/images/themes/Brown-Frame/top-left.png"
        alt="top-left"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "110px", sm: "200px", md: "272px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,
          right: 0,
        }}
        src="/static/images/themes/Brown-Frame/top-right.png"
        alt="top-right"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "110px", sm: "200px", md: "272px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
          left: 0,
        }}
        src="/static/images/themes/Brown-Frame/bottom-left.png"
        alt="bottom-left"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          height: { xs: "110px", sm: "200px", md: "272px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
          right: 0,
        }}
        src="/static/images/themes/Brown-Frame/bottom-right.png"
        alt="bottom-right"
      />
    </>
  )
}
