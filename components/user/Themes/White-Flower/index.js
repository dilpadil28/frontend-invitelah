import { Box } from '@mui/system'
import React from 'react'

export default function WhiteFlower({ dialog }) {
  return (
    <>
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          minHeight: { xs: "60px", sm: '110px', md: "160px" },
          height: { xs: "60px", sm: '110px', md: "160px" },
          maxHeight: { xs: "160px" },

          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: 0,

        }}
        src="/static/images/themes/White-Flower/White-Flower-top.png"
        alt="top"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          minHeight: { xs: "60px", sm: '110px', md: "160px" },
          height: { xs: "60px", sm: '110px', md: "160px" },
          maxHeight: { xs: "160px" },

          position: "absolute",
          zIndex: dialog ? '0' : '1',
          bottom: 0,
        }}
        src="/static/images/themes/White-Flower/White-Flower-bottom.png"
        alt="bottom"
      />
    </>
  )
}
