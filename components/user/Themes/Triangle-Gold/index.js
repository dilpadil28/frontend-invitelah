import { Box } from '@mui/system'
import React from 'react'

export default function TriangleGold({ dialog }) {
  return (
    <>
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          width: { xs: "30px", sm: "40px", md: "50px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: { xs: 150, md: 0 },
          right: 0
        }}
        src="/static/images/themes/Triangle-Gold/Triangle-Gold-right.png"
        alt="top-right"
      />
      <Box
        data-aos="zoom-in"
        data-aos-duration="1000"
        component="img"
        sx={{
          width: { xs: "30px", sm: "40px", md: "50px" },
          position: "absolute",
          zIndex: dialog ? '0' : '1',
          top: { xs: 80, md: 0 },
          left: 0
        }}
        src="/static/images/themes/Triangle-Gold/Triangle-Gold-left.png"
        alt="bottom-left"
      />
    </>
  )
}
