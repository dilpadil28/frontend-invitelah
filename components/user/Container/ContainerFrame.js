import { Box } from '@mui/system';
import React from 'react'
import { useSelector } from 'react-redux';

import RedFlower from "../Themes/Red-Flower";
import YellowFlower from "../Themes/Yellow-Flower";
import GoldFrame from "../Themes/Gold-Frame";
import BrownCrystal from "../Themes/Brown-Crystal";
import WhiteFlower from '../Themes/White-Flower';
import BlueFlower from '../Themes/Blue-Flower';
import GoldFlower from '../Themes/Gold-Flower';
import PinkFlower from '../Themes/Pink-Flower';
import BrownFrame from '../Themes/Brown-Frame';
import TriangleGold from '../Themes/Triangle-Gold';

export default function ContainerFrame({ props, children }) {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <Box
      sx={{
        background: invitationSlug?.theme?.backgroundColor ? invitationSlug?.theme?.backgroundColor : '#FFFFFF'
      }}
      display={'flex'}
      alignItems={'center'}
      justifyContent={'center'}
      component="div" minHeight={{ xs: '260px', sm: '400px', md: "585px" }} py={{ xs: 4, sm: 10 }} width="100%" position="relative" {...props}>
      {children}
      {invitationSlug?.theme?.name === 'Brown-Frame' ?
        <BrownFrame dialog={false} />
        : invitationSlug?.theme?.name === 'Brown-Crystal' ?
          <BrownCrystal dialog={false} />
          : invitationSlug?.theme?.name === 'Blue-Flower' ?
            <BlueFlower dialog={false} />
            : invitationSlug?.theme?.name === 'Gold-Frame' ?
              <GoldFrame dialog={false} />
              : invitationSlug?.theme?.name === 'Gold-Flower' ?
                <GoldFlower dialog={false} />
                : invitationSlug?.theme?.name === 'Red-Flower' ?
                  <RedFlower dialog={false} />
                  : invitationSlug?.theme?.name === 'Pink-Flower' ?
                    <PinkFlower dialog={false} />
                    : invitationSlug?.theme?.name === 'Triangle-Gold' ?
                      <TriangleGold dialog={false} />
                      : invitationSlug?.theme?.name === 'White-Flower' ?
                        <WhiteFlower dialog={false} />
                        : invitationSlug?.theme?.name === 'Yellow-Flower' ?
                          <YellowFlower dialog={false} />
                          : <BrownFrame dialog={false} />
      }
    </Box>
  )
}
