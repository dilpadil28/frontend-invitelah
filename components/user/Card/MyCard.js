import { Paper } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";

export default function MyCard({ children }) {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <Box
      sx={{
        maxWidth: 628,
        zIndex: 99,
      }}
    >
      <Paper
        className=" rounded-xl"
        sx={{
          backgroundColor: invitationSlug?.theme?.cardColor
            ? invitationSlug?.theme?.cardColor
            : "#C4C4C4",
          p: 2,
          color: "#fff",
        }}
      >
        {children}
      </Paper>
    </Box>
  );
}
