/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import React from "react";

import "lightgallery.js/dist/css/lightgallery.css";
import { LightgalleryProvider, LightgalleryItem } from "react-lightgallery";
import { Grid, ImageList, ImageListItem } from "@mui/material";
import { Box } from "@mui/system";
import { useSelector } from "react-redux";
import { api } from "../../../config/api";

export default function Photos() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <>
      <LightgalleryProvider>
        <ImageList variant="masonry" cols={2} gap={8}>
          {invitationSlug?.photogalleries?.map((v, i) => (
            <ImageListItem key={i}>
              <LightgalleryItem group={"group2"} src={api.fileUrl + v?.image}>
                <img
                  className="rounded-xl"
                  src={`${api.fileUrl + v?.image}`}
                  srcSet={`${api.fileUrl + v?.image}`}
                  style={{ width: "100%" }}
                  loading="lazy"
                  alt={v?.image}
                />
              </LightgalleryItem>
            </ImageListItem>
          ))}
        </ImageList>
      </LightgalleryProvider>
    </>
  );
}
