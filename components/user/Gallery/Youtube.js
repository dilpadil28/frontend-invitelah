import { Box } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";

import { Carousel } from "react-responsive-carousel";
import YouTube from "react-youtube";
export default function Youtube() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <>
      <Carousel
        interval={60000}
        useKeyboardArrows={true}
        stopOnHover={true}
        autoPlay={true}
        showArrows={true}
        showThumbs={false}
        showStatus={false}
        infiniteLoop={true}
      >
        {invitationSlug?.youtubes?.map((v, i) => (
          <YouTube
            className="rounded-xl"
            key={i}
            opts={{
              height: 230,
            }}
            videoId={v.url}
          />
        ))}
      </Carousel>
      {/* // <Box
        //   key={i}
        //   component="iframe"
        //   height={230}
        //   src={v.url} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen
        // /> */}
    </>
  );
}
