import { Grid } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import { useSelector } from 'react-redux'
import TitleHeader from '../Title/TitleHeader'
import Photos from './Photos'
import Youtube from './Youtube'

export default function Gallery() {

  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <Grid
      container
      className="col-evenly-center"
      height="100%"
      width="100%"
      py={{ xs: 6, sm: 10 }}
      px={4}
    >
      <Box component="div"
        data-aos="zoom-in"
        data-aos-duration="200"
      >
        <TitleHeader text="Galeri" />
      </Box>
      {
        invitationSlug?.photogalleries?.length > 0 ?
          <Box
            data-aos="zoom-in"
            data-aos-duration="500"
            component="div" px={{ xs: 1, md: 5 }} id="gallery" >
            <Photos />
          </Box>
          : <></>
      }
      <Box my={1} />
      {
        invitationSlug?.youtubes?.length > 0 ?
          <Box
            data-aos="zoom-in"
            data-aos-duration="1000"
            component="div" px={{ xs: 1, md: 5 }} mb={4} >
            <Youtube />
          </Box>
          : <></>
      }
    </Grid>
  )
}
