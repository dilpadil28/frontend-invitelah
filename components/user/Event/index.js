import { Grid } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import AkadNikah from "./AkadNikah";
import Resepsi from "./Resepsi";
import TitleHeader from "../Title/TitleHeader";

export default function Event() {
  return (
    <Grid
      container
      className="col-evenly-center"
      height="100%"
      width="100%"
      py={{ xs: 6, sm: 10 }}
      px={4}
    >
      <Box
        component="div"
        className="mb-2"
        data-aos="zoom-in"
        data-aos-duration="200"
      >
        <TitleHeader text="Tanggal & Lokasi" />
      </Box>
      <div data-aos="zoom-in" data-aos-duration="500">
        <AkadNikah />
      </div>
      <Box my={1} />
      <div data-aos="zoom-in" data-aos-duration="1000">
        <Resepsi />
      </div>
      <Box my={2} />
    </Grid>
  );
}
