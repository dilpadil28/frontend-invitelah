import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import MyCard from "../Card/MyCard";
import TitleHeaderCard from "../Title/TitleHeaderCard";
import moment from "moment";
import { useSelector } from "react-redux";

export default function AkadNikah() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <>
      <Box component="div">
        <MyCard>
          <TitleHeaderCard text="Akad Nikah" />
          <Typography
            textAlign="center"
            gutterBottom
            component="div"
            fontSize={{ xs: "9px", sm: "16px" }}
            fontFamily="Montserrat"
          >
            {moment(invitationSlug?.tanggalNikah).locale("id").format("dddd") +
              ", " +
              moment(invitationSlug?.tanggalNikah).locale("id").format("LL")}
          </Typography>
          <Typography
            textAlign="center"
            gutterBottom
            component="div"
            fontSize={{ xs: "9px", sm: "16px" }}
            fontFamily="Montserrat"
          >
            {invitationSlug?.jamNikah}
          </Typography>
          <Typography
            textAlign="center"
            gutterBottom
            component="div"
            fontSize={{ xs: "9px", sm: "16px" }}
            fontFamily="Montserrat"
          >
            {invitationSlug?.alamatNikah}
          </Typography>
        </MyCard>
        <Box my={2} />
        <MyCard>
          <Box
            className="rounded-xl"
            src={invitationSlug?.mapsNikah}
            style={{ border: 0, width: "100%", height: "100%" }}
            allowFullScreen=""
            loading="lazy"
            component="iframe"
          />
        </MyCard>
      </Box>
    </>
  );
}
