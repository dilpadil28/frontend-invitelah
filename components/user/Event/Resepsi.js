import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import MyCard from "../Card/MyCard";
import TitleHeaderCard from "../Title/TitleHeaderCard";

import moment from "moment";
import { useSelector } from "react-redux";
import Title9 from "../Title/Title9";
export default function Resepsi() {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);
  return (
    <>
      <Box component="div">
        <MyCard>
          <TitleHeaderCard text="Resepsi" />
          <Title9 textAlign={"center"} sx={{ color: "#fff" }}>
            {moment(invitationSlug?.tanggalResepsi)
              .locale("id")
              .format("dddd") +
              ", " +
              moment(invitationSlug?.tanggalResepsi).locale("id").format("LL")}
          </Title9>
          <Title9 textAlign={"center"} sx={{ color: "#fff" }}>
            {invitationSlug?.jamResepsi}
          </Title9>
          <Title9 textAlign={"center"} sx={{ color: "#fff" }}>
            {invitationSlug?.alamatResepsi}
          </Title9>
        </MyCard>
        <Box my={2} />
        <MyCard>
          <Box
            className=" rounded-xl"
            src={invitationSlug?.mapsResepsi}
            style={{ border: 0, width: "100%", height: "100%" }}
            allowFullScreen=""
            loading="lazy"
            component="iframe"
          />
        </MyCard>
      </Box>
    </>
  );
}
