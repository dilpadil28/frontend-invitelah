import { Button } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";

export default function MyButton(props) {
  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  return (
    <Button
      className="!bg-primary"
      {...props}
      sx={{
        fontFamily: invitationSlug?.theme?.fontType2
          ? invitationSlug?.theme?.fontType2
          : "Montserrat",
      }}
      variant="contained"
      size="small"
    >
      {props.children}
    </Button>
  );
}
