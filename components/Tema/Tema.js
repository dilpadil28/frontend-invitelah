/* eslint-disable @next/next/link-passhref */
import { Card, CardActionArea, CardActions, CardContent, CardMedia, Container, Typography, Grid, Button } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import Slider from "react-slick";

function CardTheme({ img, name }) {
  return (
    <Card sx={{ maxWidth: 345, borderRadius: 5, m: 2 }} elevation={3}  >
      <Box component="a" href={`/${name}?to=didi+dan+partner`} target="_blank" >
        <CardActionArea>
          <CardMedia
            component="img"
            height="250"
            image={img}
            alt={name + " Invitelah"}
          />
          <CardContent>
            <Typography gutterBottom fontWeight="bold"
              fontSize={{ xs: 18, sm: 24 }} component="div">
              {name}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Box>
    </Card >
  )
}

const themes = [
  {
    img: '/static/images/cards/Modern-Gold.jpeg',
    name: 'modern-gold'
  },
  {
    img: '/static/images/cards/Modern-Black.jpeg',
    name: 'raja-ratu'
  },
  {
    img: '/static/images/cards/Modern-Blue.jpeg',
    name: 'modern-blue'
  },
  {
    img: '/static/images/cards/Modern-Brown.jpeg',
    name: 'modern-brown'
  },
  {
    img: '/static/images/cards/Modern-Green.jpeg',
    name: 'modern-green'
  },
  {
    img: '/static/images/cards/Modern-Pink.jpeg',
    name: 'modern-pink'
  },
  {
    img: '/static/images/cards/Modern-Red.jpeg',
    name: 'modern-red'
  },
  {
    img: '/static/images/cards/Minimalis.jpeg',
    name: 'minimalis'
  },
  {
    img: '/static/images/cards/White-Flower.png',
    name: 'white-flower'
  },
  {
    img: '/static/images/cards/Blue-Flower.png',
    name: 'blue-flower'
  },
  {
    img: '/static/images/cards/Brown-Crystal.png',
    name: 'brown-crystal'
  },
  {
    img: '/static/images/cards/Brown-Frame.png',
    name: 'brown-frame'
  },
  {
    img: '/static/images/cards/Gold-Flower.png',
    name: 'gold-flower'
  },
  {
    img: '/static/images/cards/Gold-Frame.png',
    name: 'gold-frame'
  },
  {
    img: '/static/images/cards/Pink-Flower.png',
    name: 'pink-flower'
  },
  {
    img: '/static/images/cards/Red-Flower.png',
    name: 'red-flower'
  },
  {
    img: '/static/images/cards/Triangle-Gold.png',
    name: 'triangle-gold'
  },
  {
    img: '/static/images/cards/Yellow-Flower.png',
    name: 'yellow-flower'
  },
]

export default function Tema() {
  const settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <Container id="Tema">
      <Box pt={10} pb={10} textAlign={"center"}>
        <Typography
          mb={5}
          fontWeight={500}
          fontSize={{ xs: 24, sm: 40 }}
          data-aos="zoom-in"
          
          component={"h1"}
        >
          Tema
        </Typography>
        <Grid
          container
          spacing={1}
          direction="row"
          justifyContent="center"
          alignItems="center"
          alignContent="center"
        >
          <Grid item xs={12} >
            <Slider {...settings}>
              {
                themes.map((v, i) =>
                  <CardTheme img={v.img} name={v.name} key={i} />
                )
              }
            </Slider>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}
