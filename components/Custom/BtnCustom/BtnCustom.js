import { Button } from "@mui/material";
import * as React from "react";

export default function BtnCustom(props) {
  return (
    <Button
      className="!bg-primary"
      variant="contained"
      {...props}
      style={{ borderRadius: 20, textTransform: "none" }}
    />
  );
}
