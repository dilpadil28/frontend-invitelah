import * as React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { Container, Typography, Button } from "@mui/material";
import BtnCustom from "../Custom/BtnCustom/BtnCustom";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, { EffectFade, Navigation, Pagination } from "swiper";
import Typed from "react-typed";
// install Swiper modules
SwiperCore.use([EffectFade, Navigation, Pagination]);
import "animate.css";
// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { grey } from "@mui/material/colors";
import { Link } from "react-scroll";

export default function Beranda() {
  return (
    <Container maxWidth="xl">
      <Box sx={{ width: "100%" }} pt={10}>
        <Grid
          container
          direction={{ xs: "column-reverse", md: "row" }}
          justifyContent="center"
          alignItems="center"
          spacing={1}
          rowSpacing={3}
        >
          <Grid
            display="flex"
            justifyContent={"center"}
            alignItems={"center"}
            xs={12}
            md={6}
            item
          >
            <Box component="div">
              <Typography
                data-aos="fade-left"
                data-aos-duration="200"
                component="h1"
                fontWeight="bold"
                gutterBottom
                fontSize={{ xs: 24, sm: 48 }}
              >
                Undangan Pernikahan Digital
              </Typography>

              <Box component="div" data-aos="fade-left" data-aos-duration="400">
                <Typography
                  gutterBottom
                  component="h6"
                  color={grey[500]}
                  fontSize={{ xs: 14, sm: 22 }}
                  className="inline"
                >
                  berupa
                </Typography>
                <Typography
                  gutterBottom
                  component="h6"
                  className=" inline"
                  ml={2}
                  color="primary"
                  fontWeight="bold"
                  fontSize={{ xs: 16, md: 36 }}
                >
                  <Typed
                    strings={["Website", "Video", "Image"]}
                    loop
                    backSpeed={80}
                    typeSpeed={100}
                  />
                </Typography>
              </Box>
              <Box my={2} />
              <Link
                to={"Harga"}
                spy={true}
                smooth={true}
                duration={500}
                offset={-20}
              >
                <Button
                  variant="contained"
                  size="large"
                  className="!bg-primary"
                  sx={{
                    fontSize: { xs: 12, sm: 14, md: 18 },
                  }}
                >
                  Buat Undangan
                </Button>
              </Link>
            </Box>
          </Grid>
          <Grid
            data-aos="zoom-in"
            data-aos-duration="200"
            display="flex"
            justifyContent={"center"}
            alignItems={"center"}
            item
            xs={12}
            md={6}
            mb={5}
          >
            <Box position="relative">
              <Box
                sx={{
                  width: { xs: 40, sm: 70 },
                  position: "absolute",
                  right: { xs: 20, sm: 95 },
                  top: { xs: 60, sm: 185 },
                }}
                alt="instagram invitelah"
                className="
                animate__animated
                animate__infinite
                animate__slower
                animate__shakeY
                animate__delay-2s
                "
                component="img"
                src="/static/images/icons/3d-ig.png"
              />
              <Box
                sx={{
                  width: { xs: 40, sm: 70 },
                  position: "absolute",
                  top: { xs: 212, sm: 330 },
                }}
                alt="facebook invitelah"
                className="
                animate__animated
                animate__infinite
                animate__slower
                animate__shakeY
                animate__delay-3s
                "
                component="img"
                src="/static/images/icons/3d-fb.png"
              />
              <Box
                sx={{
                  width: { xs: 40, sm: 70 },
                  position: "absolute",
                  left: { xs: 33, sm: 100 },
                }}
                alt="whatsapp invitelah"
                className="
                animate__animated
                animate__infinite
                animate__slower
                animate__shakeY
                animate__delay-5s
                "
                component="img"
                src="/static/images/icons/3d-wa.png"
              />

              <Box
                alt="invitelah"
                component="img"
                src="/static/images/bg/banner.png"
                sx={{ width: { xs: 304, sm: 700 } }}
              />
              {/* <Swiper
                spaceBetween={30}
                effect={"slide"}
                navigation={false}
                pagination={{
                  clickable: false,
                }}
                className="mySwiper"
              >
                <SwiperSlide>
                  <img
                    alt="image"
                    src="https://swiperjs.com/demos/images/nature-1.jpg"
                  />
                </SwiperSlide>
                <SwiperSlide>
                  <img
                    alt="image"
                    src="https://swiperjs.com/demos/images/nature-2.jpg"
                  />
                </SwiperSlide>
                <SwiperSlide>
                  <img
                    alt="image"
                    src="https://swiperjs.com/demos/images/nature-4.jpg"
                  />
                </SwiperSlide>
              </Swiper> */}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}
