import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeLoveStory }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeLoveStory}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        placeholder="ex: Perkenalan"
      />
      <TextField
        onChange={onValueChangeLoveStory}
        fullWidth
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        placeholder="ex: Awal bertemu dan berkenalan di tempat kerja"
      />
      <TextField
        onChange={onValueChangeLoveStory}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Date"
        name="date"
        placeholder="ex: 28 Juni 2016"
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangeLoveStory}
          defaultValue={true}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
