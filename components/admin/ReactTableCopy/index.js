import {
  Checkbox,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TableSortLabel,
  TextField,
  Menu,
} from "@mui/material";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import PropTypes from "prop-types";
// import Paper from "@mui/material/Paper";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import React, { useMemo } from "react";
import { CSVLink } from "react-csv";
import {
  useFilters,
  useGlobalFilter,
  useSortBy,
  useTable,
  usePagination,
  useRowSelect,
  useColumnOrder,
} from "react-table";
import { useExportData } from "react-table-plugins";

import XLSX from "xlsx";
import JsPDF from "jspdf";
import "jspdf-autotable";

import Papa from "papaparse";
import MOCK_DATA from "./MOCK_DATA.json";
import ColumnFilter from "./ColumnFilter";
import { COLUMNS } from "./columns";
import GlobalFilter from "./GlobalFilter";
import { Box } from "@mui/system";
import { useTheme } from "@emotion/react";
import TableToolbar from "./TableToolbar";

export default function ReactTable() {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => MOCK_DATA, []);
  const dataCsv = useMemo(() => {
    return MOCK_DATA.map((d) => Object.values(d));
  }, []);

  const defaultColumn = useMemo(() => {
    return {
      Filter: ColumnFilter,
    };
  }, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    pageOptions,
    gotoPage,
    pageCount,
    setPageSize,
    prepareRow,
    state,
    setGlobalFilter,
    selectedFlatRows,
    setColumnOrder,
    allColumns,
    getToggleHideAllColumnsProps,
    exportData,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      getExportFileBlob,
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination,
    useColumnOrder,
    useExportData,
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => {
        return [
          {
            id: "selection",
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <Checkbox {...getToggleAllRowsSelectedProps()} />
            ),
            Cell: ({ row }) => (
              <Checkbox {...row.getToggleRowSelectedProps()} />
            ),
          },
          ...columns,
        ];
      });
    }
  );

  const { globalFilter, pageIndex, pageSize, selectedRowIds } = state;

  function getExportFileBlob({ columns, data, fileType, fileName }) {
    if (fileType === "csv") {
      // CSV example
      const headerNames = columns
        .filter((c) => c.Header != "Action")
        .map((col) => col.exportValue);
      const csvString = Papa.unparse({ fields: headerNames, data });
      return new Blob([csvString], { type: "text/csv" });
    } else if (fileType === "xlsx") {
      // XLSX example

      const header = columns
        .filter((c) => c.Header != "Action")
        .map((c) => c.exportValue);
      const compatibleData = data.map((row) => {
        const obj = {};
        header.forEach((col, index) => {
          obj[col] = row[index];
        });
        return obj;
      });

      let wb = XLSX.utils.book_new();
      let ws1 = XLSX.utils.json_to_sheet(compatibleData, {
        header,
      });
      XLSX.utils.book_append_sheet(wb, ws1, "React Table Data");
      XLSX.writeFile(wb, `${fileName}.xlsx`);

      // Returning false as downloading of file is already taken care of
      return false;
    }
    //PDF example
    if (fileType === "pdf") {
      const headerNames = columns
        .filter((c) => c.Header != "Action")
        .map((column) => column.exportValue);
      const doc = new JsPDF();
      doc.autoTable({
        head: [headerNames],
        body: data,
        styles: {
          minCellHeight: 9,
          halign: "left",
          valign: "center",
          fontSize: 11,
        },
      });
      doc.save(`${fileName}.pdf`);

      return false;
    }

    // Other formats goes here
    return false;
  }

  const changeOrder = () => {
    setColumnOrder([
      "id",
      "first_name",
      "last_name",
      "phone",
      "country",
      "date_of_birth",
    ]);
  };

  const removeByIndexs = (array, indexs) =>
    array.filter((_, i) => !indexs.includes(i));

  const deleteUserHandler = (event) => {
    const newData = removeByIndexs(
      data,
      Object.keys(selectedRowIds).map((x) => parseInt(x, 10))
    );
    setData(newData);
  };

  const addUserHandler = (user) => {
    const newData = data.concat(user);
    setData(newData);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      {/* <div>
        <CSVLink data={dataCsv}>Download CSV</CSVLink>
        <button
          className="btn btnexport mr-1"
          onClick={() => {
            exportData("csv", true);
          }}
        >
          <i className="fa fa-file-csv"></i> Export as CSV
        </button>{" "}
        <button
          className="btn btnexport mr-1"
          onClick={() => {
            exportData("xlsx", true);
          }}
        >
          <i className="fa fa-file-excel"></i> Export as xlsx
        </button>{" "}
        <button
          className="btn btnexport mr-1"
          onClick={() => {
            exportData("pdf", true);
          }}
        >
          <i className="fa fa-file-pdf"></i>
          Export as PDF
        </button>
      </div>

      <button onClick={changeOrder}>Change column order</button>
      <div>
        <div>
          <Checkbox {...getToggleHideAllColumnsProps()} /> Toggle All
        </div>
        {allColumns.map((column) => (
          <div key={column.id}>
            <label>
              <input type="checkbox" {...column.getToggleHiddenProps()} />
              {column.Header}
            </label>
          </div>
        ))}
      </div> */}

      <Paper>
        <TableContainer>
          <TableToolbar
            numSelected={Object.keys(selectedRowIds).length}
            deleteUserHandler={deleteUserHandler}
            addUserHandler={addUserHandler}
            preGlobalFilteredRows={preGlobalFilteredRows}
            setGlobalFilter={setGlobalFilter}
            globalFilter={globalFilter}
            downlodPDF={() => exportData("pdf", true)}
            downlodExcel={() => exportData("xlsx", true)}
            downlodCSV={() => exportData("csv", true)}
          />
          <Table sx={{ minWidth: 500 }} {...getTableProps}>
            <TableHead>
              {headerGroups.map((headerGroup) => (
                <TableRow
                  key={headerGroup}
                  {...headerGroup.getHeaderGroupProps()}
                >
                  {headerGroup.headers.map((column) => (
                    <TableCell
                      key={column}
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column.render("Header")}
                      {column.isSorted ? (
                        <TableSortLabel
                          active={column.isSorted}
                          // react-table has a unsorted state which is not treated here
                          direction={column.isSortedDesc ? "desc" : "asc"}
                        />
                      ) : (
                        ""
                      )}
                      {/* <div>{column.canFilter ? column.render("Filter") : null}</div> */}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {page.map((row) => {
                prepareRow(row);
                return (
                  <TableRow key={row} {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      return (
                        <TableCell key={cell} {...cell.getCellProps()}>
                          {cell.render("Cell")}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
            {/* <TableFooter>
            <TableRow>
             
            </TableRow>
          </TableFooter> */}
          </Table>
        </TableContainer>
        <Box p={3}>
          <span>
            Page{" "}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>
          </span>
          <Box component={"span"} mx={1}>
            | Goto page:{" "}
            <TextField
              type={"number"}
              style={{ width: 70 }}
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const pageNumber = e.target.value
                  ? Number(e.target.value) - 1
                  : 0;
                gotoPage(pageNumber);
              }}
              size="small"
            />
          </Box>
          <FormControl>
            <InputLabel id="demo-simple-select-autowidth-label">
              Show
            </InputLabel>
            <Select
              size="small"
              labelId="demo-simple-select-autowidth-label"
              id="demo-simple-select-autowidth"
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value));
              }}
              autoWidth
              label="Show"
            >
              {[5, 10, 25, 50].map((pageSize) => (
                <MenuItem value={pageSize} key={pageSize}>
                  {pageSize}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <IconButton
            onClick={() => gotoPage(0)}
            disabled={!canPreviousPage}
            aria-label="first page"
          >
            <FirstPageIcon />
          </IconButton>
          <IconButton
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
            aria-label="previous page"
          >
            <KeyboardArrowLeft />
          </IconButton>
          <IconButton
            onClick={() => nextPage()}
            disabled={!canNextPage}
            aria-label="next page"
          >
            <KeyboardArrowRight />
          </IconButton>
          <IconButton
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
            aria-label="last page"
          >
            <LastPageIcon />
          </IconButton>
          <IconButton
            aria-label="more"
            id="long-button"
            aria-controls={open ? "long-menu" : undefined}
            aria-expanded={open ? "true" : undefined}
            aria-haspopup="true"
            onClick={handleClick}
          >
            <FileDownloadIcon />
          </IconButton>
          <Menu
            id="long-menu"
            MenuListProps={{
              "aria-labelledby": "long-button",
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            PaperProps={{
              style: {
                maxHeight: 48 * 4.5,
                width: "20ch",
              },
            }}
          >
            <MenuItem
              onClick={() => {
                exportData("xlsx", true);
              }}
            >
              Excel
            </MenuItem>
            <MenuItem
              onClick={() => {
                exportData("csv", true);
              }}
            >
              CSV
            </MenuItem>
            <MenuItem
              onClick={() => {
                exportData("pdf", true);
              }}
            >
              PDF
            </MenuItem>
          </Menu>
        </Box>
      </Paper>
      {/* <pre>
        <code>
          {JSON.stringify(
            {
              selectedFlatRows: selectedFlatRows.map((row) => row.original),
            },
            null,
            2
          )}
        </code>
      </pre> */}
    </>
  );
}
