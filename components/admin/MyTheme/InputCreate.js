import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeMyTheme }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMyTheme}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Name"
        name="name"
      />
      <TextField
        onChange={onValueChangeMyTheme}
        fullWidth
        required
        margin="dense"
        label="Url"
        multiline
        name="url"
      />
      <TextField
        onChange={onValueChangeMyTheme}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
