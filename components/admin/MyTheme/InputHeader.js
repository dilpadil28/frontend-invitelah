import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataMyThemeHeader,
  onValueChangeMyThemeHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMyThemeHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataMyThemeHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeMyThemeHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataMyThemeHeader.description || ""}
      />
    </Box>
  );
}
