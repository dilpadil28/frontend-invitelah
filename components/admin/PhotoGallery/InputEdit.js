import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputEdit({
  dataPhotoGallery,
  onValueChangePhotoGallery,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangePhotoGallery}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        placeholder="ex: Nama Foto"
        value={dataPhotoGallery.title || ""}
      />
      <TextField
        onChange={onValueChangePhotoGallery}
        fullWidth
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataPhotoGallery.description || ""}
      />
      <TextField
        onChange={onValueChangePhotoGallery}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangePhotoGallery}
          value={dataPhotoGallery.published || ""}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
