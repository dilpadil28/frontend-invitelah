import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({
  dataBackground,
  onValueChangeBackground,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeBackground}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Name"
        name="name"
        value={dataBackground.name || ""}
      />
      <TextField
        onChange={onValueChangeBackground}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangeBackground}
          value={dataBackground.published || ""}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
