import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataOrderStepHeader,
  onValueChangeOrderStepHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeOrderStepHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataOrderStepHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeOrderStepHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataOrderStepHeader.description || ""}
      />
    </Box>
  );
}
