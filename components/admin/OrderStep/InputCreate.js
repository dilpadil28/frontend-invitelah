import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeOrderStep }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeOrderStep}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
      />
      <TextField
        onChange={onValueChangeOrderStep}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
