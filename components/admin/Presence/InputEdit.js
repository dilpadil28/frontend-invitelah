import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({
  dataPresence,
  onValueChangePresence,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangePresence}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Name"
        name="name"
        value={dataPresence.name || ""}
      />
      <TextField
        onChange={onValueChangePresence}
        fullWidth
        required
        type={'number'}
        margin="dense"
        label="Phone Number(Whatsapp)"
        name="phoneNumber"
        value={dataPresence.phoneNumber || ""}
      />
      <TextField
        onChange={onValueChangePresence}
        fullWidth
        required
        margin="dense"
        label="Message"
        multiline
        rows={3}
        name="message"
        value={dataPresence.message || ""}
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Confirmation</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Confirmation"
          name="confirmation"
          onChange={onValueChangePresence}
          value={dataPresence.confirmation || ""}
        >
          <MenuItem value={'hadir'}>Hadir</MenuItem>
          <MenuItem value={'belum tahu'}>Belum Tahu</MenuItem>
          <MenuItem value={'tidak hadir'}>Tidak Hadir</MenuItem>
        </Select>
      </FormControl>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Total</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Total"
          name="total"
          onChange={onValueChangePresence}
          value={dataPresence.total || ""}
        >
          <MenuItem value={'1'}>1</MenuItem>
          <MenuItem value={'2'}>2</MenuItem>
          <MenuItem value={'3'}>3</MenuItem>
        </Select>
      </FormControl>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangePresence}
          value={dataPresence.published || ""}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
