import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeTheme, music }) {
  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel required id="demo-simple-select-label">
          Music
        </InputLabel>
        <Select
          required
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Music"
          name="musicId"
          onChange={onValueChangeTheme}
          defaultValue={""}
        >
          {music?.map((v, i) => (
            <MenuItem key={i} value={v.id}>
              {v.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Theme Name</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Theme Name"
          name="name"
          onChange={onValueChangeTheme}
          defaultValue={"White-Flower"}
        >
          <MenuItem value={"Waves-Blue"}>Waves-Blue</MenuItem>
          <MenuItem value={"Waves-Brown"}>Waves-Brown</MenuItem>
          <MenuItem value={"Waves-Cream"}>Waves-Cream</MenuItem>
          <MenuItem value={"Waves-Dark"}>Waves-Dark</MenuItem>
          <MenuItem value={"Waves-Green"}>Waves-Green</MenuItem>
          <MenuItem value={"Waves-Grey"}>Waves-Grey</MenuItem>
          <MenuItem value={"Waves-Pink"}>Waves-Pink</MenuItem>
          <MenuItem value={"Waves-Purple"}>Waves-Purple</MenuItem>
          <MenuItem value={"Waves-Red"}>Waves-Red</MenuItem>
          <MenuItem value={"Waves-Yellow"}>Waves-Yellow</MenuItem>
          <MenuItem value={"Minimalis"}>Minimalis</MenuItem>
          <MenuItem value={"Modern-Blue"}>Modern-Blue</MenuItem>
          <MenuItem value={"Modern-Brown"}>Modern-Brown</MenuItem>
          <MenuItem value={"Modern-Pink"}>Modern-Pink</MenuItem>
          <MenuItem value={"Modern-Red"}>Modern-Red</MenuItem>
          <MenuItem value={"Modern-Green"}>Modern-Green</MenuItem>
          <MenuItem value={"Modern-Gold"}>Modern-Gold</MenuItem>
          <MenuItem value={"Modern-Black"}>Modern-Black</MenuItem>
          <MenuItem value={"Blue-Flower"}>Blue-Flower</MenuItem>
          <MenuItem value={"Brown-Crystal"}>Brown-Crystal</MenuItem>
          <MenuItem value={"Brown-Frame"}>Brown-Frame</MenuItem>
          <MenuItem value={"Gold-Flower"}>Gold-Flower</MenuItem>
          <MenuItem value={"Gold-Frame"}>Gold-Frame</MenuItem>
          <MenuItem value={"Pink-Flower"}>Pink-Flower</MenuItem>
          <MenuItem value={"Red-Flower"}>Red-Flower</MenuItem>
          <MenuItem value={"Triangle-Gold"}>Triangle-Gold</MenuItem>
          <MenuItem value={"White-Flower"}>White-Flower</MenuItem>
          <MenuItem value={"Yellow-Flower"}>Yellow-Flower</MenuItem>
        </Select>
      </FormControl>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Gallery Type</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Gallery Type"
          name="galleryType"
          onChange={onValueChangeTheme}
          defaultValue={"basic"}
        >
          <MenuItem value={"basic"}>Basic</MenuItem>
        </Select>
      </FormControl>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Primary Font</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Primary Font"
          name="fontType1"
          onChange={onValueChangeTheme}
          defaultValue={"Niconne"}
        >
          <MenuItem value={"Abuget"}>
            <Typography fontFamily="Abuget">Abuget</Typography>
          </MenuItem>
          <MenuItem value={"Apalu"}>
            <Typography fontFamily="Apalu">Apalu</Typography>
          </MenuItem>
          <MenuItem value={"Arkipelago"}>
            <Typography fontFamily="Arkipelago">Arkipelago</Typography>
          </MenuItem>
          <MenuItem value={"Fake"}>
            <Typography fontFamily="Fake">Fake</Typography>
          </MenuItem>
          <MenuItem value={"Silhouetto-Script"}>
            <Typography fontFamily="Silhouetto-Script">
              Silhouetto-Script
            </Typography>
          </MenuItem>
          <MenuItem value={"Alex Brush"}>
            <Typography fontFamily="Alex Brush">Alex Brush</Typography>
          </MenuItem>
          <MenuItem value={"Allison"}>
            <Typography fontFamily="Allison">Allison</Typography>
          </MenuItem>
          {/* <MenuItem value={"Bebas Neue"}>
            <Typography fontFamily="Bebas Neue">Bebas Neue</Typography>
          </MenuItem> */}
          {/* <MenuItem value={"Cabin"}>
            <Typography fontFamily="Cabin">Cabin</Typography>
          </MenuItem> */}
          <MenuItem value={"Clicker Script"}>
            <Typography fontFamily="Clicker Script">Clicker Script</Typography>
          </MenuItem>
          <MenuItem value={"Cookie"}>
            <Typography fontFamily="Cookie">Cookie</Typography>
          </MenuItem>
          <MenuItem value={"Dancing Script"}>
            <Typography fontFamily="Dancing Script">Dancing Script</Typography>
          </MenuItem>
          <MenuItem value={"Euphoria Script"}>
            <Typography fontFamily="Euphoria Script">
              Euphoria Script
            </Typography>
          </MenuItem>
          {/* <MenuItem value={"Forum"}>
            <Typography fontFamily="Forum">Forum</Typography>
          </MenuItem> */}
          <MenuItem value={"Great Vibes"}>
            <Typography fontFamily="Great Vibes">Great Vibes</Typography>
          </MenuItem>
          <MenuItem value={"Hurricane"}>
            <Typography fontFamily="Hurricane">Hurricane</Typography>
          </MenuItem>
          {/* <MenuItem value={"Josefin Sans"}>
            <Typography fontFamily="Josefin Sans">Josefin Sans</Typography>
          </MenuItem> */}
          {/* <MenuItem value={"Lora"}>
            <Typography fontFamily="Lora">Lora</Typography>
          </MenuItem> */}
          {/* <MenuItem value={"Montserrat"}>
            <Typography fontFamily="Montserrat">Montserrat</Typography>
          </MenuItem> */}
          <MenuItem value={"Montez"}>
            <Typography fontFamily="Montez">Montez</Typography>
          </MenuItem>
          <MenuItem value={"Mr Dafoe"}>
            <Typography fontFamily="Mr Dafoe">Mr Dafoe</Typography>
          </MenuItem>
          {/* <MenuItem value={"Noto Serif"}>
            <Typography fontFamily="Noto Serif">Noto Serif</Typography>
          </MenuItem> */}
          <MenuItem value={"Niconne"}>
            <Typography fontFamily="Niconne">Niconne</Typography>
          </MenuItem>
          {/* <MenuItem value={"Open Sans"}>
            <Typography fontFamily="Open Sans">Open Sans</Typography>
          </MenuItem> */}
          <MenuItem value={"Pacifico"}>
            <Typography fontFamily="Pacifico">Pacifico</Typography>
          </MenuItem>
          <MenuItem value={"Parisienne"}>
            <Typography fontFamily="Parisienne">Parisienne</Typography>
          </MenuItem>
          <MenuItem value={"Petit Formal Script"}>
            <Typography fontFamily="Petit Formal Script">
              Petit Formal Script
            </Typography>
          </MenuItem>
          <MenuItem value={"Petemoss"}>
            <Typography fontFamily="Petemoss">Petemoss</Typography>
          </MenuItem>
          <MenuItem value={"Pinyon Script"}>
            <Typography fontFamily="Pinyon Script">Pinyon Script</Typography>
          </MenuItem>
          <MenuItem value={"Playball"}>
            <Typography fontFamily="Playball">Playball</Typography>
          </MenuItem>
          {/* <MenuItem value={"Playfair Display"}>
            <Typography fontFamily="Playfair Display">
              Playfair Display
            </Typography>
          </MenuItem> */}
          {/* <MenuItem value={"Quicksand"}>
            <Typography fontFamily="Quicksand">Quicksand</Typography>
          </MenuItem> */}
          <MenuItem value={"Rouge Script"}>
            <Typography fontFamily="Rouge Script">Rouge Script</Typography>
          </MenuItem>
          <MenuItem value={"Sacramento"}>
            <Typography fontFamily="Sacramento">Sacramento</Typography>
          </MenuItem>
          <MenuItem value={"Satisfy"}>
            <Typography fontFamily="Satisfy">Satisfy</Typography>
          </MenuItem>
          <MenuItem value={"Smooch"}>
            <Typography fontFamily="Smooch">Smooch</Typography>
          </MenuItem>
          <MenuItem value={"Style Script"}>
            <Typography fontFamily="Style Script">Style Script</Typography>
          </MenuItem>
        </Select>
      </FormControl>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Secondary Font</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Secondary Font"
          name="fontType2"
          onChange={onValueChangeTheme}
          defaultValue={"Bebas Neue"}
        >
          {/* <MenuItem value={"Alex Brush"}>
            <Typography fontFamily="Alex Brush">Alex Brush</Typography>
          </MenuItem>
          <MenuItem value={"Allison"}>
            <Typography fontFamily="Allison">Allison</Typography>
          </MenuItem>*/}
          <MenuItem value={"Anonymous Pro"}>
            <Typography fontFamily="Anonymous Pro">Anonymous Pro</Typography>
          </MenuItem>
          <MenuItem value={"Bebas Neue"}>
            <Typography fontFamily="Bebas Neue">Bebas Neue</Typography>
          </MenuItem>
          <MenuItem value={"Cabin"}>
            <Typography fontFamily="Cabin">Cabin</Typography>
          </MenuItem>
          {/* <MenuItem value={"Clicker Script"}>
            <Typography fontFamily="Clicker Script">Clicker Script</Typography>
          </MenuItem>
          <MenuItem value={"Cookie"}>
            <Typography fontFamily="Cookie">Cookie</Typography>
          </MenuItem>
          <MenuItem value={"Dancing Script"}>
            <Typography fontFamily="Dancing Script">Dancing Script</Typography>
          </MenuItem>
          <MenuItem value={"Euphoria Script"}>
            <Typography fontFamily="Euphoria Script">
              Euphoria Script
            </Typography>
          </MenuItem> */}
          <MenuItem value={"Forum"}>
            <Typography fontFamily="Forum">Forum</Typography>
          </MenuItem>
          {/* <MenuItem value={"Great Vibes"}>
            <Typography fontFamily="Great Vibes">Great Vibes</Typography>
          </MenuItem>
          <MenuItem value={"Hurricane"}>
            <Typography fontFamily="Hurricane">Hurricane</Typography>
          </MenuItem> */}
          <MenuItem value={"Josefin Sans"}>
            <Typography fontFamily="Josefin Sans">Josefin Sans</Typography>
          </MenuItem>
          <MenuItem value={"Lora"}>
            <Typography fontFamily="Lora">Lora</Typography>
          </MenuItem>
          <MenuItem value={"Montserrat"}>
            <Typography fontFamily="Montserrat">Montserrat</Typography>
          </MenuItem>
          {/* <MenuItem value={"Montez"}>
            <Typography fontFamily="Montez">Montez</Typography>
          </MenuItem>
          <MenuItem value={"Mr Dafoe"}>
            <Typography fontFamily="Mr Dafoe">Mr Dafoe</Typography>
          </MenuItem> */}
          <MenuItem value={"Noto Serif"}>
            <Typography fontFamily="Noto Serif">Noto Serif</Typography>
          </MenuItem>
          {/* <MenuItem value={"Niconne"}>
            <Typography fontFamily="Niconne">Niconne</Typography>
          </MenuItem> */}
          <MenuItem value={"Open Sans"}>
            <Typography fontFamily="Open Sans">Open Sans</Typography>
          </MenuItem>
          {/* <MenuItem value={"Pacifico"}>
            <Typography fontFamily="Pacifico">Pacifico</Typography>
          </MenuItem>
          <MenuItem value={"Parisienne"}>
            <Typography fontFamily="Parisienne">Parisienne</Typography>
          </MenuItem>
          <MenuItem value={"Petit Formal Script"}>
            <Typography fontFamily="Petit Formal Script">
              Petit Formal Script
            </Typography>
          </MenuItem>
          <MenuItem value={"Petemoss"}>
            <Typography fontFamily="Petemoss">Petemoss</Typography>
          </MenuItem>
          <MenuItem value={"Pinyon Script"}>
            <Typography fontFamily="Pinyon Script">Pinyon Script</Typography>
          </MenuItem>
          <MenuItem value={"Playball"}>
            <Typography fontFamily="Playball">Playball</Typography>
          </MenuItem> */}
          <MenuItem value={"Playfair Display"}>
            <Typography fontFamily="Playfair Display">
              Playfair Display
            </Typography>
          </MenuItem>
          <MenuItem value={"Quicksand"}>
            <Typography fontFamily="Quicksand">Quicksand</Typography>
          </MenuItem>
          {/* <MenuItem value={"Rouge Script"}>
            <Typography fontFamily="Rouge Script">Rouge Script</Typography>
          </MenuItem>
          <MenuItem value={"Sacramento"}>
            <Typography fontFamily="Sacramento">Sacramento</Typography>
          </MenuItem>
          <MenuItem value={"Satisfy"}>
            <Typography fontFamily="Satisfy">Satisfy</Typography>
          </MenuItem>
          <MenuItem value={"Smooch"}>
            <Typography fontFamily="Smooch">Smooch</Typography>
          </MenuItem>
          <MenuItem value={"Style Script"}>
            <Typography fontFamily="Style Script">Style Script</Typography>
          </MenuItem> */}
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangeTheme}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Font Color 1"
        name="fontColor1"
        placeholder="ex: #FFFFFF"
      />
      <TextField
        onChange={onValueChangeTheme}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Font Color 2"
        name="fontColor2"
        placeholder="ex: #FFFFFF"
      />
      <TextField
        onChange={onValueChangeTheme}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Background Color"
        name="backgroundColor"
        placeholder="ex: #FFFFFF"
      />
      <TextField
        onChange={onValueChangeTheme}
        fullWidth
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="BackgroundImage"
        name="backgroundImage"
      />
      <TextField
        onChange={onValueChangeTheme}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Card Color"
        name="cardColor"
        placeholder="ex: #FFFFFF"
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published Prokes</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published Prokes"
          name="publishProkes"
          onChange={onValueChangeTheme}
          defaultValue={true}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
