// material-ui
import { useTheme } from "@mui/material/styles";
import Link from "next/link";

/**
 * if you want to use image instead of <svg> uncomment following.
 *
 * import logoDark from 'assets/images/logo-dark.svg';
 * import logo from 'assets/images/logo.svg';
 *
 */

// ==============================|| LOGO SVG ||============================== //

const Logo = () => {
  const theme = useTheme();

  return (
    /**
     * if you want to use image instead of svg uncomment following, and comment out <svg> element.
     *
     * <img src={logo} alt="Berry" width="100" />
     *
     */
    <Link passHref href={"/"}>
      <img
        style={{ width: 110 }}
        src="/static/images/logo/invitelah-center-green.svg"
        alt="logo-invitelah"
      />
    </Link>
  );
};

export default Logo;
