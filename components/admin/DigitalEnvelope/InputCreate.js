import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeDigitalEnvelope }) {
  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Name</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Name"
          name="name"
          onChange={onValueChangeDigitalEnvelope}
          defaultValue={'mandiri'}
        >
          <MenuItem value={'bca'}>Bank BCA</MenuItem>
          <MenuItem value={'bni'}>Bank BNI</MenuItem>
          <MenuItem value={'bri'}>Bank BRI</MenuItem>
          <MenuItem value={'bsi'}>Bank BSI</MenuItem>
          <MenuItem value={'mandiri'}>Bank Mandiri</MenuItem>
          <MenuItem value={'permata'}>Bank Permata</MenuItem>
          <MenuItem value={'dana'}>Dana</MenuItem>
          <MenuItem value={'gopay'}>Gopay</MenuItem>
          <MenuItem value={'linkaja'}>LinkAja</MenuItem>
          <MenuItem value={'ovo'}>OVO</MenuItem>
          <MenuItem value={'shopeepay'}>Shopeepay</MenuItem>
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangeDigitalEnvelope}
        fullWidth
        margin="dense"
        required
        type={'number'}
        id="outlined-required"
        label="Number"
        name="number"
        placeholder="ex: 081987654321"
      />
      <TextField
        onChange={onValueChangeDigitalEnvelope}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Atas Nama"
        name="atasNama"
        placeholder="ex: Putra Pertama"
      />
      <TextField
        onChange={onValueChangeDigitalEnvelope}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangeDigitalEnvelope}
          defaultValue={true}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
