import { Card } from "@mui/material";
import RecentUsersTable from "./RecentUsersTable";
import { subDays } from "date-fns";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getUser } from "../../../features/user/userSlice";
import { useSelector } from "react-redux";

function RecentUsers() {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);
  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  return (
    <Card>
      <RecentUsersTable users={user} />
    </Card>
  );
}

export default RecentUsers;
