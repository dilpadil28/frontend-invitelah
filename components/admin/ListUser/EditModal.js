import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Formik } from "formik";
import { useTheme } from "@mui/material/styles";
import { Box } from "@mui/system";
import {
  Alert,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  Snackbar,
  Typography,
} from "@mui/material";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import useScriptRef from "../../../hooks/useScriptRef";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  strengthColor,
  strengthIndicator,
} from "../../../utils/password-strength";
import { updateUser } from "../../../features/user/userSlice";

import * as Yup from "yup";
import { clearMessage } from "../../../features/message/messageSlice";

export default function EditModal({
  editModalOpen,
  currentUserEdit,
  handleEditModalClose,
  setCurrentUserEdit,
  roles,
  setRoles,
  setSuccessful,
  setOpen,
}) {
  const theme = useTheme();
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const [strength, setStrength] = useState(0);
  const [level, setLevel] = useState();

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const changePassword = (value) => {
    const temp = strengthIndicator(value);
    setStrength(temp);
    setLevel(strengthColor(temp));
  };

  const { user: currentUser } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);

  const handleInputChangeEdit = (event) => {
    const { name, value } = event.target;
    setCurrentUserEdit({ ...currentUserEdit, [name]: value });
  };
  const handleInputChangeRoles = (event) => {
    const {
      target: { value },
    } = event;
    setRoles(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  // const handleUpdate = () => {
  //   dispatch(
  //     updateUser({
  //       id: currentUserEdit.id,
  //       roles: roles,
  //       password: currentUserEdit.password,
  //     })
  //   );
  // };

  const initialValues = {
    username: currentUserEdit.username,
    phoneNumber: currentUserEdit.phoneNumber,
    email: currentUserEdit.email,
    fullName: currentUserEdit.fullName,
    roles: roles,
    password: "",
  };

  const validationSchema = Yup.object().shape({
    fullName: Yup.string().required("Fullname is required"),
    roles: Yup.array().required("Roles is required"),
    password: Yup.string().max(255),
  });

  const handleUpdate = async (
    values,
    { setErrors, setStatus, setSubmitting, resetForm }
  ) => {
    const { fullName, username, phoneNumber, email, roles, password } = values;
    dispatch(
      updateUser({
        id: currentUserEdit.id,
        fullName,
        username,
        phoneNumber,
        email,
        roles,
        password,
      })
    )
      .unwrap()
      .then(() => {
        handleEditModalClose();
        setSuccessful(true);
        setStatus({ success: true });
        setSubmitting(false);
        setOpen(true);
      })
      .catch((err) => {
        console.log("err", err);
        setSuccessful(false);
        setStatus({ success: false });
        setErrors({ submit: err.message });
        setSubmitting(false);
        setOpen(true);
      });
  };

  return (
    <>
      <Dialog
        open={editModalOpen}
        onClose={handleEditModalClose}
        aria-labelledby="alert-dialog-title"
      >
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          enableReinitialize
          onSubmit={handleUpdate}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
          }) => (
            <form noValidate onSubmit={handleSubmit}>
              <DialogTitle id="responsive-dialog-title">Edit User</DialogTitle>
              <DialogContent>
                <FormControl
                  fullWidth
                  error={Boolean(touched.fullName && errors.fullName)}
                  sx={{ ...theme.typography.customInput }}
                >
                  <InputLabel htmlFor="outlined-adornment-fullName-register">
                    Fullname
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-fullName-register"
                    type="text"
                    value={values.fullName}
                    name="fullName"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    inputProps={{}}
                  />
                  {touched.fullName && errors.fullName && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text--register"
                    >
                      {errors.fullName}
                    </FormHelperText>
                  )}
                </FormControl>
                <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
                  <InputLabel htmlFor="outlined-adornment-username-register">
                    Username
                  </InputLabel>
                  <OutlinedInput
                    disabled
                    id="outlined-adornment-username-register"
                    type="text"
                    value={values.username}
                    name="username"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    inputProps={{}}
                  />
                  {touched.username && errors.username && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text--register"
                    >
                      {errors.username}
                    </FormHelperText>
                  )}
                </FormControl>
                <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
                  <InputLabel htmlFor="outlined-adornment-email-register">
                    Email Address
                  </InputLabel>
                  <OutlinedInput
                    disabled
                    required
                    id="outlined-adornment-email-register"
                    type="email"
                    value={values.email}
                    name="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    inputProps={{}}
                  />
                  {touched.email && errors.email && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text--register"
                    >
                      {errors.email}
                    </FormHelperText>
                  )}
                </FormControl>
                <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
                  <InputLabel htmlFor="outlined-adornment-phoneNumber-register">
                    Phone Number
                  </InputLabel>
                  <OutlinedInput
                    disabled
                    required
                    id="outlined-adornment-phoneNumber-register"
                    type="number"
                    value={values.phoneNumber}
                    name="phoneNumber"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    inputProps={{}}
                  />
                  {touched.phoneNumber && errors.phoneNumber && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text--register"
                    >
                      {errors.phoneNumber}
                    </FormHelperText>
                  )}
                </FormControl>

                <FormControl
                  error={Boolean(touched.fullName && errors.fullName)}
                  fullWidth
                  sx={{ ...theme.typography.customInput }}
                >
                  <InputLabel htmlFor="outlined-adornment-roles-register">
                    Role
                  </InputLabel>
                  {currentUser?.roles.includes("ROLE_ADMIN") ? (
                    <Select
                      required
                      multiple
                      style={{ paddingTop: 10 }}
                      id="outlined-adornment-roles-register"
                      renderValue={(selected) => selected.join(", ")}
                      value={values.roles}
                      name="roles"
                      onBlur={handleBlur}
                      onChange={handleChange}
                    >
                      <MenuItem value="admin">Admin</MenuItem>
                      <MenuItem value="moderator">Moderator</MenuItem>
                      <MenuItem value="user">User</MenuItem>
                    </Select>
                  ) : (
                    <Select
                      required
                      multiple
                      style={{ paddingTop: 10 }}
                      id="outlined-adornment-roles-register"
                      renderValue={(selected) => selected.join(", ")}
                      value={values.roles}
                      name="roles"
                      onBlur={handleBlur}
                      onChange={handleChange}
                    >
                      <MenuItem value="user">User</MenuItem>
                    </Select>
                  )}
                  {touched.roles && errors.roles && (
                    <FormHelperText
                      error
                      id="standard-weight-helper-text--register"
                    >
                      {errors.roles}
                    </FormHelperText>
                  )}
                </FormControl>

                <FormControl
                  error={Boolean(touched.password && errors.password)}
                  fullWidth
                  sx={{ ...theme.typography.customInput }}
                >
                  <InputLabel htmlFor="outlined-adornment-password-register">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password-register"
                    type={showPassword ? "text" : "password"}
                    value={values.password}
                    name="password"
                    label="Password"
                    onBlur={handleBlur}
                    onChange={(e) => {
                      handleChange(e);
                      changePassword(e.target.value);
                    }}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                          size="large"
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                    inputProps={{}}
                  />
                </FormControl>

                {strength !== 0 && (
                  <FormControl fullWidth>
                    <Box sx={{ mb: 2 }}>
                      <Grid container spacing={2} alignItems="center">
                        <Grid item>
                          <Box
                            style={{ backgroundColor: level?.color }}
                            sx={{
                              width: 85,
                              height: 8,
                              borderRadius: "7px",
                            }}
                          />
                        </Grid>
                        <Grid item>
                          <Typography variant="subtitle1" fontSize="0.75rem">
                            {level?.label}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </FormControl>
                )}
                {message && <FormHelperText error>{message}</FormHelperText>}
              </DialogContent>
              <DialogActions>
                <Button autoFocus onClick={handleEditModalClose}>
                  Cancel
                </Button>
                <Button
                  className="!bg-primary"
                  // onClick={handleUpdate}
                  type="submit"
                  size="large"
                  variant="contained"
                  color="primary"
                  autoFocus
                >
                  Update
                </Button>
              </DialogActions>
            </form>
          )}
        </Formik>
      </Dialog>
    </>
  );
}
