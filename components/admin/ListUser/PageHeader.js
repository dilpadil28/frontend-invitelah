import { Typography, Button, Grid } from "@mui/material";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";
import CreateModal from "./createModal";
import React, { useState } from "react";

function PageHeader() {
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const user = {
    name: "Catherine Pike",
    avatar: "/static/images/avatars/1.jpg",
  };
  return (
    <>
      <Grid container justifyContent="space-between" alignItems="center">
        <Grid item>
          <Typography variant="h5" gutterBottom>
            Users
          </Typography>
          <Typography variant="subtitle2">
            {user.name}, these are your recent users
          </Typography>
        </Grid>
        <Grid item>
          <Button
            className="!bg-primary"
            onClick={handleCreateOpen}
            sx={{ mt: { xs: 2, md: 0 } }}
            variant="contained"
            startIcon={<AddTwoToneIcon fontSize="small" />}
          >
            Create user
          </Button>
        </Grid>
      </Grid>
      <CreateModal
        createOpen={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
      />
    </>
  );
}

export default PageHeader;
