import { FC, ChangeEvent, useState, useEffect } from "react";
import { format } from "date-fns";
import numeral from "numeral";
import PropTypes from "prop-types";
import {
  Tooltip,
  Divider,
  Box,
  FormControl,
  InputLabel,
  Card,
  Checkbox,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableContainer,
  Select,
  MenuItem,
  Typography,
  useTheme,
  CardHeader,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  useMediaQuery,
  Snackbar,
  Alert,
} from "@mui/material";

import Label from "../Label";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import BulkActions from "./BulkActions";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, getUserById } from "../../../features/user/userSlice";
import DeleteModal from "./DeleteModal";
import EditModal from "./EditModal";
import userService from "../../../services/user.service";
import { clearMessage } from "../../../features/message/messageSlice";


const getStatusLabel = (userOrderStatus) => {
  const map = {
    failed: {
      text: "Failed",
      color: "error",
    },
    completed: {
      text: "Completed",
      color: "success",
    },
    pending: {
      text: "Pending",
      color: "warning",
    },
  };

  const { text, color } = map[userOrderStatus];

  return <Label color={color}>{text}</Label>;
};

const applyFilters = (users, filters) => {
  return users?.filter((userOrder) => {
    let matches = true;

    if (filters.status && userOrder.status !== filters.status) {
      matches = false;
    }

    return matches;
  });
};

const applyPagination = (users, page, limit) => {
  return users.slice(page * limit, page * limit + limit);
};

const RecentOrdersTable = ({ users }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const [selectedUsers, setSelectedUsers] = useState([]);
  const selectedBulkActions = selectedUsers.length > 0;
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(5);
  const [filters, setFilters] = useState({
    status: null,
  });

  const { message } = useSelector((state) => state.message);
  const [successful, setSuccessful] = useState(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const initialUserEditState = {
    id: null,
    fullName: "",
    username: "",
    phoneNumber: "",
    email: "",
    password: "",
  };
  const [currentUserEdit, setCurrentUserEdit] = useState(initialUserEditState);
  const [roles, setRoles] = useState([]);
  const { user: currentUser } = useSelector((state) => state.auth);

  const [open, setOpen] = useState(false);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteUser(dialogId)).unwrap().then(() => {
      setDeleteModalOpen(false);
      setSuccessful(true);
      setOpen(true);

    }).catch((err) => {
      setDeleteModalOpen(false);
      setSuccessful(false);
      setOpen(true);

    })
  };
  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getUserById(id))
      .then((response) => {
        setCurrentUserEdit(response.payload);
        setRoles([response.payload.Roles[0].name]);
        dispatch(clearMessage());
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const statusOptions = [
    {
      id: "all",
      name: "All",
    },
    {
      id: "completed",
      name: "Completed",
    },
    {
      id: "pending",
      name: "Pending",
    },
    {
      id: "failed",
      name: "Failed",
    },
  ];

  const handleStatusChange = (e) => {
    let value = null;

    if (e.target.value !== "all") {
      value = e.target.value;
    }

    setFilters((prevFilters) => ({
      ...prevFilters,
      status: value,
    }));
  };

  const handleSelectAllUsers = (event) => {
    setSelectedUsers(
      event.target.checked ? users.map((userOrder) => userOrder.id) : []
    );
  };

  const handleSelectOneUsers = (event, userOrderId) => {
    if (!selectedUsers.includes(userOrderId)) {
      setSelectedUsers((prevSelected) => [...prevSelected, userOrderId]);
    } else {
      setSelectedUsers((prevSelected) =>
        prevSelected.filter((id) => id !== userOrderId)
      );
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(parseInt(event.target.value));
  };

  const filteredUsers = applyFilters(users, filters);
  const paginatedUsers = applyPagination(filteredUsers, page, limit);
  const selectedSomeUsers =
    selectedUsers.length > 0 && selectedUsers.length < users.length;
  const selectedAllUsers = selectedUsers.length === users.length;

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };


  useEffect(() => {
    dispatch(clearMessage());
  }, [dispatch]);
  return (
    <>
      <Card>
        {selectedBulkActions && (
          <Box flex={1} p={2}>
            <BulkActions />
          </Box>
        )}
        {!selectedBulkActions && (
          <CardHeader
            action={
              <Box width={150}>
                <FormControl fullWidth variant="outlined">
                  <InputLabel>Status</InputLabel>
                  <Select
                    value={filters.status || "all"}
                    onChange={handleStatusChange}
                    label="Status"
                    autoWidth
                  >
                    {statusOptions.map((statusOption) => (
                      <MenuItem key={statusOption.id} value={statusOption.id}>
                        {statusOption.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            }
            title="Recent Users"
          />
        )}
        <Divider />
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  {/* <Checkbox
                  color="primary"
                  checked={selectedAllUsers}
                  indeterminate={selectedSomeUsers}
                  onChange={handleSelectAllUsers}
                /> */}
                </TableCell>
                <TableCell>Full Name</TableCell>
                <TableCell>Username</TableCell>
                <TableCell>Phone Number</TableCell>
                <TableCell>Email</TableCell>
                <TableCell align="right">Role</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedUsers.map((user) => {
                const isUserSelected = selectedUsers.includes(user.id);
                return (
                  <TableRow hover key={user.id} selected={isUserSelected}>
                    <TableCell padding="checkbox">
                      {/* <Checkbox
                        color="primary"
                        checked={isUserSelected}
                        onChange={(event) =>
                          handleSelectOneUsers(event, user.id)
                        }
                        value={isUserSelected}
                      /> */}
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="body1"
                        fontWeight="bold"
                        color="text.primary"
                        gutterBottom
                        noWrap
                      >
                        {user.fullName}
                      </Typography>
                      {/* <Typography variant="body2" color="text.secondary" noWrap>
                      {format(user.orderDate, "MMMM dd yyyy")}
                    </Typography> */}
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="body1"
                        fontWeight="bold"
                        color="text.primary"
                        gutterBottom
                        noWrap
                      >
                        {user.username}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="body1"
                        fontWeight="bold"
                        color="text.primary"
                        gutterBottom
                        noWrap
                      >
                        {user.phoneNumber}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography
                        variant="body1"
                        fontWeight="bold"
                        color="text.primary"
                        gutterBottom
                        noWrap
                      >
                        {user.email}
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      {user?.Roles?.map((role) => role.name + " ")}
                    </TableCell>
                    <TableCell align="right">
                      {currentUser?.roles.includes("ROLE_ADMIN") ? (
                        <Tooltip title="Edit User" arrow>
                          <IconButton
                            onClick={() => handleEditOpen(user.id)}
                            sx={{
                              "&:hover": {
                                background: theme.palette.primary.light,
                              },
                              color: theme.palette.primary.main,
                            }}
                            color="inherit"
                            size="small"
                          >
                            <EditTwoToneIcon fontSize="small" />
                          </IconButton>
                        </Tooltip>
                      ) : (
                        <></>
                      )
                      }
                      {currentUser?.roles.includes("ROLE_ADMIN") ? (
                        <Tooltip title="Delete User" arrow>
                          <IconButton
                            onClick={() => handleDeleteOpen(user.id)}
                            sx={{
                              "&:hover": {
                                background: theme.palette.error.light,
                              },
                              color: theme.palette.error.main,
                            }}
                            color="inherit"
                            size="small"
                          >
                            <DeleteTwoToneIcon fontSize="small" />
                          </IconButton>
                        </Tooltip>
                      ) : (
                        <></>
                      )
                      }
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <Box p={2}>
          <TablePagination
            component="div"
            count={filteredUsers.length}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={[5, 10, 25, 30]}
          />
        </Box>
      </Card>
      <DeleteModal
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <EditModal
        editModalOpen={editModalOpen}
        currentUserEdit={currentUserEdit}
        handleEditModalClose={handleEditModalClose}
        setCurrentUserEdit={setCurrentUserEdit}
        setRoles={setRoles}
        roles={roles}
        successful={successful}
        setSuccessful={setSuccessful}
        setOpen={setOpen}
      />

      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
};

RecentOrdersTable.propTypes = {
  users: PropTypes.array.isRequired,
};

RecentOrdersTable.defaultProps = {
  users: [],
};

export default RecentOrdersTable;
