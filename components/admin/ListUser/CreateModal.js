import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { useTheme } from "@mui/material/styles";
import { Box } from "@mui/system";
import {
  Alert,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  Snackbar,
  Typography,
} from "@mui/material";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import useScriptRef from "../../../hooks/useScriptRef";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  strengthColor,
  strengthIndicator,
} from "../../../utils/password-strength";
import { register } from "../../../features/auth/authSlice";
import { getUser } from "../../../features/user/userSlice";
import { clearMessage } from "../../../features/message/messageSlice";

export default function CreateModal({
  createOpen,
  handleCreateModalClose,
  successful,
  setSuccessful,
}) {
  const theme = useTheme();
  const scriptedRef = useScriptRef();
  const [showPassword, setShowPassword] = useState(false);

  const [strength, setStrength] = useState(0);
  const [level, setLevel] = useState();

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const changePassword = (value) => {
    const temp = strengthIndicator(value);
    setStrength(temp);
    setLevel(strengthColor(temp));
  };

  const { user: currentUser } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);

  const initialValues = {
    username: "",
    phoneNumber: "",
    email: "",
    password: "",
    fullName: "",
    roles: [],
  };

  const validationSchema = Yup.object().shape({
    fullName: Yup.string().required("First Name is required"),
    phoneNumber: Yup.string().required("Phone Number is required"),
    roles: Yup.array().required("Role is required"),
    username: Yup.string()
      .test(
        "len",
        "The username must be between 3 and 20 characters.",
        (val) =>
          val && val.toString().length >= 3 && val.toString().length <= 20
      )
      .required("This field is required!"),
    email: Yup.string()
      .email("Must be a valid email")
      .max(255)
      .required("Email is required"),
    password: Yup.string()
      .test(
        "len",
        "The password must be between 6 and 40 characters.",
        (val) =>
          val && val.toString().length >= 6 && val.toString().length <= 40
      )
      .max(255)
      .required("Password is required"),
  });
  const handleRegister = async (
    values,
    { setErrors, setStatus, setSubmitting }
  ) => {
    setSuccessful(false);
    if (scriptedRef.current) {
      const { fullName, username, phoneNumber, email, roles, password } =
        values;
      dispatch(
        register({
          fullName: fullName,
          username,
          phoneNumber,
          email,
          roles,
          password,
        })
      )
        .unwrap()
        .then(() => {
          setSuccessful(true);
          setStatus({ success: true });
          setSubmitting(false);
          dispatch(getUser());
          handleCreateModalClose();
          setOpen(true);
        })
        .catch((err) => {
          setSuccessful(false);
          setStatus({ success: false });
          setErrors({ submit: err.message });
          setSubmitting(false);
          setOpen(true);
        });
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  useEffect(() => {
    dispatch(clearMessage());
  }, [dispatch]);

  return (
    <div>
      <Dialog
        open={createOpen}
        onClose={handleCreateModalClose}
        aria-labelledby="responsive-dialog-title"
      >
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleRegister}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
          }) =>
            !successful && (
              <form noValidate onSubmit={handleSubmit}>
                <DialogTitle id="responsive-dialog-title">
                  Create User
                </DialogTitle>
                <DialogContent>
                  <FormControl
                    fullWidth
                    error={Boolean(touched.fullName && errors.fullName)}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-fullName-register">
                      Fullname
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-fullName-register"
                      type="text"
                      value={values.fullName}
                      name="fullName"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      inputProps={{}}
                    />
                    {touched.fullName && errors.fullName && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text--register"
                      >
                        {errors.fullName}
                      </FormHelperText>
                    )}
                  </FormControl>
                  <FormControl
                    fullWidth
                    error={Boolean(touched.username && errors.username)}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-username-register">
                      Username
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-username-register"
                      type="text"
                      value={values.username}
                      name="username"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      inputProps={{}}
                    />
                    {touched.username && errors.username && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text--register"
                      >
                        {errors.username}
                      </FormHelperText>
                    )}
                  </FormControl>
                  <FormControl
                    fullWidth
                    error={Boolean(touched.email && errors.email)}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-email-register">
                      Email Address
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-email-register"
                      type="email"
                      value={values.email}
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      inputProps={{}}
                    />
                    {touched.email && errors.email && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text--register"
                      >
                        {errors.email}
                      </FormHelperText>
                    )}
                  </FormControl>
                  <FormControl
                    fullWidth
                    error={Boolean(touched.phoneNumber && errors.phoneNumber)}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-phoneNumber-register">
                      Phone Number
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-phoneNumber-register"
                      type="number"
                      value={values.phoneNumber}
                      name="phoneNumber"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      inputProps={{}}
                    />
                    {touched.phoneNumber && errors.phoneNumber && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text--register"
                      >
                        {errors.phoneNumber}
                      </FormHelperText>
                    )}
                  </FormControl>

                  <FormControl
                    fullWidth
                    error={Boolean(touched.roles && errors.roles)}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-roles-register">
                      Role
                    </InputLabel>
                    {currentUser.roles.includes("ROLE_ADMIN") ? (
                      <Select
                        multiple
                        style={{ paddingTop: 10 }}
                        id="outlined-adornment-roles-register"
                        value={values.roles}
                        name="roles"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        defaultValue="user"
                      >
                        <MenuItem
                          disabled={values.roles.length >= 1 ? true : false}
                          value="admin"
                        >
                          Admin
                        </MenuItem>
                        <MenuItem
                          disabled={values.roles.length >= 1 ? true : false}
                          value="moderator"
                        >
                          Moderator
                        </MenuItem>
                        <MenuItem
                          disabled={values.roles.length >= 1 ? true : false}
                          value="user"
                        >
                          User
                        </MenuItem>
                      </Select>
                    ) : (
                      <Select
                        multiple
                        style={{ paddingTop: 10 }}
                        id="outlined-adornment-roles-register"
                        value={values.roles}
                        name="roles"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        defaultValue="user"
                      >
                        <MenuItem value="user">User</MenuItem>
                      </Select>
                    )}
                    {touched.roles && errors.roles && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text--register"
                      >
                        {errors.roles}
                      </FormHelperText>
                    )}
                  </FormControl>

                  <FormControl
                    fullWidth
                    error={Boolean(touched.password && errors.password)}
                    sx={{ ...theme.typography.customInput }}
                  >
                    <InputLabel htmlFor="outlined-adornment-password-register">
                      Password
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-password-register"
                      type={showPassword ? "text" : "password"}
                      value={values.password}
                      name="password"
                      label="Password"
                      onBlur={handleBlur}
                      onChange={(e) => {
                        handleChange(e);
                        changePassword(e.target.value);
                      }}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                            size="large"
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }
                      inputProps={{}}
                    />
                    {touched.password && errors.password && (
                      <FormHelperText
                        error
                        id="standard-weight-helper-text-password-register"
                      >
                        {errors.password}
                      </FormHelperText>
                    )}
                  </FormControl>

                  {strength !== 0 && (
                    <FormControl fullWidth>
                      <Box sx={{ mb: 2 }}>
                        <Grid container spacing={2} alignItems="center">
                          <Grid item>
                            <Box
                              style={{ backgroundColor: level?.color }}
                              sx={{
                                width: 85,
                                height: 8,
                                borderRadius: "7px",
                              }}
                            />
                          </Grid>
                          <Grid item>
                            <Typography variant="subtitle1" fontSize="0.75rem">
                              {level?.label}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Box>
                    </FormControl>
                  )}
                  {message && <FormHelperText error>{message}</FormHelperText>}
                </DialogContent>
                <DialogActions>
                  <Button autoFocus onClick={handleCreateModalClose}>
                    Cancel
                  </Button>
                  <Button
                    className="!bg-primary"
                    size="large"
                    type="submit"
                    variant="contained"
                    color="primary"
                    autoFocus
                  >
                    Create
                  </Button>
                </DialogActions>
              </form>
            )
          }
        </Formik>
      </Dialog>
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </div>
  );
}
