import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({
  dataSocialMedia,
  onValueChangeSocialMedia,
}) {
  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Name</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Name"
          name="name"
          onChange={onValueChangeSocialMedia}
          value={dataSocialMedia.name || ""}
        >
          <MenuItem value={'facebook'}>Facebook</MenuItem>
          <MenuItem value={'instagram'}>Instagram</MenuItem>
          <MenuItem value={'live'}>Live Streaming</MenuItem>
          <MenuItem value={'pinterest'}>Pinterest</MenuItem>
          <MenuItem value={'snapchat'}>SnapChat</MenuItem>
          <MenuItem value={'tiktok'}>Tiktok</MenuItem>
          <MenuItem value={'telegram'}>Telegram</MenuItem>
          <MenuItem value={'twitter'}>Twitter</MenuItem>
          <MenuItem value={'whatsapp'}>WhatsApp</MenuItem>
          <MenuItem value={'youtube'}>Youtube</MenuItem>
          <MenuItem value={'zoom'}>Zoom</MenuItem>
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangeSocialMedia}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Url"
        name="url"
        value={dataSocialMedia.url || ""}
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Type</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Type"
          name="type"
          onChange={onValueChangeSocialMedia}
          value={dataSocialMedia.type || ""}
        >
          <MenuItem value={'pria'}>Pria</MenuItem>
          <MenuItem value={'wanita'}>Wanita</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
