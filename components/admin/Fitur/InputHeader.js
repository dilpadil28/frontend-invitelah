import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataFiturHeader,
  onValueChangeFiturHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeFiturHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataFiturHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeFiturHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataFiturHeader.description || ""}
      />
    </Box>
  );
}
