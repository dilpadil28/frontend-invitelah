import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputEdit({ dataFitur, onValueChangeFitur }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeFitur}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataFitur.title || ""}
      />
      <TextField
        onChange={onValueChangeFitur}
        fullWidth
        required
        margin="dense"
        label="Description"
        name="description"
        multiline
        rows={3}
        value={dataFitur.description || ""}
      />
      <TextField
        onChange={onValueChangeFitur}
        fullWidth
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
