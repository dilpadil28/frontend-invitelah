import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeTestimonial }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeTestimonial}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Name"
        name="name"
      />
      <TextField
        onChange={onValueChangeTestimonial}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
      />
      <TextField
        onChange={onValueChangeTestimonial}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />

      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Rating</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Rating"
          name="rating"
          onChange={onValueChangeTestimonial}
        >
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
          <MenuItem value={4}>4</MenuItem>
          <MenuItem value={5}>5</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
