import {
  Box,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({ dataMusic, onValueChangeMusic }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMusic}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataMusic.title || ""}
      />
      <TextField
        onChange={onValueChangeMusic}
        fullWidth
        margin="dense"
        type="file"
        helperText="max. file size 5Mb"
        label="Song"
        name="song"
      />

      {/* <FormGroup>
        <FormControlLabel
          control={
            <Switch
              name="published"
              onChange={onValueChangeMusic}
              value={dataMusic.published || ""}
            />
          }
          label="Published"
        />
      </FormGroup> */}

      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangeMusic}
          value={dataMusic.published ? 1 : 0 || 0}
        >
          <MenuItem value={1}>Publish</MenuItem>
          <MenuItem value={0}>Not Publish</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
