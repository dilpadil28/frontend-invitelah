import {
  Box,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeMusic }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMusic}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
      />
      <TextField
        onChange={onValueChangeMusic}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 5Mb"
        label="Song"
        name="song"
      />

      {/* <FormGroup>
        <FormControlLabel
          control={<Switch name="published" onChange={onValueChangeMusic} />}
          label="Published"
        />
      </FormGroup> */}

      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangeMusic}
          defaultValue={true}
        >
          <MenuItem value={true}>Publish</MenuItem>
          <MenuItem value={false}>Not Publish</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
