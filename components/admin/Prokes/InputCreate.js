import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeProkes }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeProkes}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
      />
      <TextField
        onChange={onValueChangeProkes}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
