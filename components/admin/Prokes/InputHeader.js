import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataProkesHeader,
  onValueChangeProkesHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeProkesHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataProkesHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeProkesHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataProkesHeader.description || ""}
      />
    </Box>
  );
}
