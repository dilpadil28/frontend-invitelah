import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({ dataProkes, onValueChangeProkes }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeProkes}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataProkes.title || ""}
      />
      <TextField
        onChange={onValueChangeProkes}
        fullWidth
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
