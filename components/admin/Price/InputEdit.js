import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({ dataPrice, onValueChangePrice }) {
  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Label</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Label"
          name="label"
          onChange={onValueChangePrice}
          value={dataPrice.label || "diamond"}
        >
          <MenuItem value="silver">Silver</MenuItem>
          <MenuItem value="gold">Gold</MenuItem>
          <MenuItem value="diamond">Diamond</MenuItem>
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Price"
        name="harga"
        value={dataPrice.harga || ""}
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount Price"
        name="discountPrice"
        value={dataPrice.discountPrice || ""}
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount Amount"
        name="discountAmount"
        value={dataPrice.discountAmount || ""}
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Type"
        name="type"
        value={dataPrice.type || ""}
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="List"
        name="list"
        value={dataPrice.list || ""}
      />
    </Box>
  );
}
