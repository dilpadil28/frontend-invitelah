import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataPriceHeader,
  onValueChangePriceHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangePriceHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataPriceHeader.title || ""}
      />
      <TextField
        onChange={onValueChangePriceHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataPriceHeader.description || ""}
      />
      <TextField
        onChange={onValueChangePriceHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount"
        name="discount"
        value={dataPriceHeader.discount || ""}
      />
      <TextField
        onChange={onValueChangePriceHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount Title"
        name="discountTitle"
        value={dataPriceHeader.discountTitle || ""}
      />
      <TextField
        onChange={onValueChangePriceHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount Description"
        name="discountDescription"
        value={dataPriceHeader.discountDescription || ""}
      />
      <TextField
        onChange={onValueChangePriceHeader}
        fullWidth
        margin="dense"
        required
        type={"date"}
        id="outlined-required"
        label="Discount Expired"
        name="discountExpired"
        value={dataPriceHeader.discountExpired || ""}
      />
    </Box>
  );
}
