import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangePrice }) {
  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Label</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Label"
          name="label"
          onChange={onValueChangePrice}
        >
          <MenuItem value="silver">Silver</MenuItem>
          <MenuItem value="gold">Gold</MenuItem>
          <MenuItem value="diamond">Diamond</MenuItem>
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Price"
        name="harga"
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount Price"
        name="discountPrice"
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Discount Amount"
        name="discountAmount"
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Type"
        name="type"
      />
      <TextField
        onChange={onValueChangePrice}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="List"
        name="list"
      />
    </Box>
  );
}
