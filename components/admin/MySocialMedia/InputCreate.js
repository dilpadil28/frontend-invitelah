import { Box, MenuItem, Select, TextField } from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeMySocialMedia }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMySocialMedia}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Name"
        name="name"
      />
      <TextField
        onChange={onValueChangeMySocialMedia}
        fullWidth
        required
        margin="dense"
        label="Url"
        multiline
        rows={3}
        name="url"
      />
    </Box>
  );
}
