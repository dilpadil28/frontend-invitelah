import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({
  dataMySocialMedia,
  onValueChangeMySocialMedia,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMySocialMedia}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Name"
        name="name"
        value={dataMySocialMedia.name || ""}
      />
      <TextField
        onChange={onValueChangeMySocialMedia}
        fullWidth
        required
        margin="dense"
        label="Url"
        multiline
        rows={3}
        name="url"
        value={dataMySocialMedia.url || ""}
      />
    </Box>
  );
}
