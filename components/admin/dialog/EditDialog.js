import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { Typography } from "@mui/material";

export default function EditDialog({
  name,
  editModalOpen,
  handleEditModalClose,
  inputEdit,
  edit,
}) {
  return (
    <>
      <Dialog
        open={editModalOpen}
        onClose={handleEditModalClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          style={{ fontSize: 20, fontWeight: 700 }}
        >
          Edit {name}
        </DialogTitle>
        <DialogContent>{inputEdit}</DialogContent>
        <DialogActions>
          <Button onClick={handleEditModalClose}>Cancel</Button>
          <Button
            className="!bg-primary"
            onClick={edit}
            size="large"
            variant="contained"
            color="primary"
            autoFocus
          >
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
