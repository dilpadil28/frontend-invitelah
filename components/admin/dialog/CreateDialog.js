import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { useTheme } from "@mui/material/styles";
import { Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";

export default function CreateDialog({
  name,
  openCreate,
  handleCreateModalClose,
  inputCreate,
  create,
}) {
  const theme = useTheme();
  const { user: currentUser } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);
  const dispatch = useDispatch();
  useEffect(() => {}, [message]);

  return (
    <div>
      <Dialog
        open={openCreate}
        onClose={handleCreateModalClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle
          id="responsive-dialog-title"
          style={{ fontSize: 20, fontWeight: 700 }}
        >
          Create {name}
        </DialogTitle>
        <DialogContent>{inputCreate}</DialogContent>
        <DialogActions>
          <Button onClick={handleCreateModalClose} autoFocus>
            Cancel
          </Button>
          <Button
            className="!bg-primary"
            size="large"
            variant="contained"
            color="primary"
            autoFocus
            onClick={create}
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
