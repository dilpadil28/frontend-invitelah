import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { useTheme } from "@mui/material/styles";
import { Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";

export default function CreateDialogInvitation({
  name,
  openCreate,
  handleCreateModalClose,
  inputCreate,
  create,
}) {
  const theme = useTheme();
  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  // const initialValues = {
  //   id: null,
  //   userId: "",
  //   slug: "",
  //   namaPria: "",
  //   namaPendekPria: "",
  //   namaOrangTuaPria: "",
  //   namaWanita: "",
  //   namaPendekWanita: "",
  //   namaOrangTuaWanita: "",
  //   avatar: "",
  //   alamatKado: "",
  //   tanggalNikah: "",
  //   jamNikah: "",
  //   alamatNikah: "",
  //   mapsNikah: "",
  //   tanggalResepsi: "",
  //   jamResepsi: "",
  //   alamatResepsi: "",
  //   mapsResepsi: "",
  //   bissmillah: 0,
  //   salamPembuka: "",
  //   salamPembukaDeskripsi: "",
  //   salamPenutup: "",
  //   salamPenutupDeskripsi: "",
  //   doa: "",
  //   turutMengundang: [],
  // };
  // const validationSchema = Yup.object().shape({
  //   namaPria: Yup.string().required("Nama Pria is required"),
  //   namaPendekPria: Yup.string().required("Nama Pendek Pria is required"),
  //   namaOrangTuaPria: Yup.string().required("Nama Orang Tua Pria is required"),
  //   namaWanita: Yup.string().required("Nama Wanita is required"),
  //   namaPendekWanita: Yup.string().required("Nama Pendek Wanita is required"),
  //   namaOrangTuaWanita: Yup.string().required("Nama Orang Tua Wanita is required"),
  //   tanggalNikah: Yup.string().required("Tanggal Nikah is required"),
  //   jamlNikah: Yup.string().required("Jam Nikah is required"),
  //   alamatlNikah: Yup.string().required("Alamat Nikah is required"),
  //   mapslNikah: Yup.string().required("Maps Nikah is required"),
  //   tanggalResepsi: Yup.string().required("Tanggal Resepsi is required"),
  //   jamlResepsi: Yup.string().required("Jam Resepsi is required"),
  //   alamatlResepsi: Yup.string().required("Alamat Resepsi is required"),
  //   mapslResepsi: Yup.string().required("Maps Resepsi is required"),
  // });

  return (
    <div>
      <Dialog
        open={openCreate}
        onClose={handleCreateModalClose}
        aria-labelledby="responsive-dialog-title"
      >
        {/* <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleRegister}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
          }) =>
            !successful && (
              <form noValidate onSubmit={handleSubmit}> */}
        <DialogTitle
          id="responsive-dialog-title"
          style={{ fontSize: 20, fontWeight: 700 }}
        >
          Create {name}
        </DialogTitle>
        <DialogContent>{inputCreate}</DialogContent>
        <DialogActions>
          <Button onClick={handleCreateModalClose} autoFocus>
            Cancel
          </Button>
          <Button
            className="!bg-primary"
            size="large"
            variant="contained"
            color="primary"
            autoFocus
            onClick={create}
          >
            Create
          </Button>
        </DialogActions>
        {/* </form>
            )
          }
        </Formik> */}
      </Dialog>
    </div>
  );
}
