import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

import moment from 'moment';

export default function InputEdit({
  dataInvitation,
  onValueChangeInvitation,
  user
}) {
  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel required id="demo-simple-select-label">User</InputLabel>
        <Select
          disabled
          required
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="User"
          name="userId"
          onChange={onValueChangeInvitation}
          value={dataInvitation.userId || ""}
        >
          {user?.map((v, i) => (
            <MenuItem key={i} value={v.id}>{v.username}</MenuItem>
          )
          )
          }
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangeInvitation}
        disabled
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Slug"
        name="slug"
        placeholder="ex: putra-putri"
        value={dataInvitation.slug || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        type="file"
        label="Avatar Pria"
        name="avatarPria"
        helperText="max. file size 2Mb" />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Pria"
        name="namaPria"
        placeholder="ex: Putra Pertama"
        value={dataInvitation.namaPria || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Pendek Pria"
        name="namaPendekPria"
        placeholder="ex: Putra"
        value={dataInvitation.namaPendekPria || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Orang Tua Pria"
        name="namaOrangTuaPria"
        placeholder="ex: Bapak Pertama & Ibu Pertama"
        value={dataInvitation.namaOrangTuaPria || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        type="file"
        label="Avatar Wanita"
        name="avatarWanita"
        helperText="max. file size 2Mb" />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Wanita"
        name="namaWanita"
        placeholder="ex: Putri Pertiwi"
        value={dataInvitation.namaWanita || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Pendek Wanita"
        name="namaPendekWanita"
        placeholder="ex: Putri"
        value={dataInvitation.namaPendekWanita || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Orang Tua Wanita"
        name="namaOrangTuaWanita"
        placeholder="ex: Bapak Pertiwi & Ibu Pertiwi"
        value={dataInvitation.namaOrangTuaWanita || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        type="file"
        label="Avatar Pasangan"
        name="avatarPasangan"
        helperText="max. file size 2Mb"
      // value={dataInvitation.avatar || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Alamat Kado"
        multiline
        rows={3}
        name="alamatKado"
        placeholder="ex: Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan Bojonggede, Kabupaten Bogor, Jawa Barat 16920"
        value={dataInvitation.alamatKado || ""}

      />
      <TextField
        margin="dense"
        onChange={onValueChangeInvitation}
        id="datetime-local"
        label="Tanggal Nikah"
        type="datetime-local"
        fullWidth
        InputLabelProps={{
          shrink: true,
        }}
        name="tanggalNikah"
        value={moment(dataInvitation.tanggalNikah).format('YYYY-MM-DDTHH:mm') || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Jam Nikah"
        name="jamNikah"
        placeholder="ex: Pukul 08.00 WIB s/d Selesai"
        value={dataInvitation.jamNikah || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Alamat Nikah"
        multiline
        rows={3}
        name="alamatNikah"
        placeholder="ex: Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan Bojonggede, Kabupaten Bogor, Jawa Barat 16920"
        value={dataInvitation.alamatNikah || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Maps Nikah"
        name="mapsNikah"
        placeholder="ex: https://www.google.com/maps/embed"
        value={dataInvitation.mapsNikah || ""}
      />
      <TextField
        margin="dense"
        onChange={onValueChangeInvitation}
        id="datetime-local"
        label="Tanggal Resepsi"
        type="datetime-local"
        fullWidth
        InputLabelProps={{
          shrink: true,
        }}
        name="tanggalResepsi"
        value={moment(dataInvitation.tanggalResepsi).format('YYYY-MM-DDTHH:mm') || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Jam Resepsi"
        name="jamResepsi"
        placeholder="ex: Pukul 08.00 WIB s/d Selesai"
        value={dataInvitation.jamResepsi || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Alamat Resepsi"
        multiline
        rows={3}
        name="alamatResepsi"
        placeholder="ex: Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan Bojonggede, Kabupaten Bogor, Jawa Barat 16920"
        value={dataInvitation.alamatResepsi || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Maps Resepsi"
        name="mapsResepsi"
        placeholder="ex: https://www.google.com/maps/embed"
        value={dataInvitation.mapsResepsi || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Salam Pembuka"
        name="salamPembuka"
        value={dataInvitation.salamPembuka || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Salam Pembuka Deskripsi"
        multiline
        rows={3}
        name="salamPembukaDeskripsi"
        value={dataInvitation.salamPembukaDeskripsi || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Salam Penutup"
        name="salamPenutup"
        value={dataInvitation.salamPenutup || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Salam Penutup Deskripsi"
        multiline
        rows={3}
        name="salamPenutupDeskripsi"
        value={dataInvitation.salamPenutupDeskripsi || ""}
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Doa"
        name="doa"
        value={dataInvitation.doa || ""}
      />
    </Box>
  );
}
