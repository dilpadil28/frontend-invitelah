import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { useEffect } from "react";
export default function InputCreate({ onValueChangeInvitation, user }) {

  return (
    <Box my={1}>
      <FormControl margin="dense" fullWidth>
        <InputLabel required id="demo-simple-select-label">User</InputLabel>
        <Select
          required
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="User"
          name="userId"
          onChange={onValueChangeInvitation}
          defaultValue={""}
        >
          {user?.map((v, i) => (
            <MenuItem key={i} value={v.id}>{v.username}</MenuItem>
          )
          )
          }
        </Select>
      </FormControl>
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Slug"
        name="slug"
        placeholder="ex: putra-putri"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        type="file"
        label="Avatar Pria"
        name="avatarPria"
        helperText="max. file size 2Mb"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Pria"
        name="namaPria"
        placeholder="ex: Putra Pertama"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Pendek Pria"
        name="namaPendekPria"
        placeholder="ex: Putra"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Orang Tua Pria"
        name="namaOrangTuaPria"
        placeholder="ex: Bapak Pertama & Ibu Pertama"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        type="file"
        label="Avatar Wanita"
        name="avatarWanita"
        helperText="max. file size 2Mb"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Wanita"
        name="namaWanita"
        placeholder="ex: Putri Pertiwi"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Pendek Wanita"
        name="namaPendekWanita"
        placeholder="ex: Putri"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Nama Orang Tua Wanita"
        name="namaOrangTuaWanita"
        placeholder="ex: Bapak Pertiwi & Ibu Pertiwi"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        type="file"
        label="Avatar Pasangan"
        name="avatarPasangan"
        helperText="max. file size 2Mb"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        label="Alamat Kado"
        multiline
        rows={3}
        name="alamatKado"
        placeholder="ex: Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan Bojonggede, Kabupaten Bogor, Jawa Barat 16920"
      />
      <TextField
        required
        margin="dense"
        onChange={onValueChangeInvitation}
        id="datetime-local"
        label="Tanggal Nikah"
        type="datetime-local"
        fullWidth
        InputLabelProps={{
          shrink: true,
        }}
        name="tanggalNikah"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Jam Nikah"
        name="jamNikah"
        placeholder="ex: Pukul 08.00 WIB s/d Selesai"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Alamat Nikah"
        multiline
        rows={3}
        name="alamatNikah"
        placeholder="ex: Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan Bojonggede, Kabupaten Bogor, Jawa Barat 16920"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Maps Nikah"
        name="mapsNikah"
        placeholder="ex: https://www.google.com/maps/embed"
      />
      {/* https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15858.278816888016!2d106.79900288587307!3d-6.449249125712187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e9f194676a43%3A0xb65f455356b0b55d!2sGedung%20Bulu%20Tangkis%20GOR%20Sakinah!5e0!3m2!1sid!2sid!4v1650022589525!5m2!1sid!2sid */}
      <TextField
        margin="dense"
        onChange={onValueChangeInvitation}
        id="datetime-local"
        label="Tanggal Resepsi"
        type="datetime-local"
        fullWidth
        InputLabelProps={{
          shrink: true,
        }}
        name="tanggalResepsi"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Jam Resepsi"
        name="jamResepsi"
        placeholder="ex: Pukul 08.00 WIB s/d Selesai"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Alamat Resepsi"
        multiline
        rows={3}
        name="alamatResepsi"
        placeholder="ex: Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan Bojonggede, Kabupaten Bogor, Jawa Barat 16920"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        required
        margin="dense"
        label="Maps Resepsi"
        name="mapsResepsi"
        placeholder="ex: https://www.google.com/maps/embed"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        label="Salam Pembuka"
        name="salamPembuka"
        placeholder="ex: Assalamualaikum Warahmatullahi Wabarakatuh"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        label="Salam Pembuka Deskripsi"
        multiline
        rows={3}
        name="salamPembukaDeskripsi"
        placeholder="ex: Tanpa mengurangi rasa hormat, Kami mengundang Bapak/Ibu/Saudara/i serta kerabat sekalian untuk menghadiri acara pernikahan kami."
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth

        margin="dense"
        label="Salam Penutup"
        name="salamPenutup"
        placeholder="ex: Wassalamualaikum Warahmatullahi Wabarakatuh"
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        margin="dense"
        label="Salam Penutup Deskripsi"
        multiline
        rows={3}
        name="salamPenutupDeskripsi"
        placeholder="ex: Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila Bapak/Ibu/Saudara/i berkenan hadir dan memberiakn doa restu.Atas Kehadiran dan doa restunya, kami mengucapkan terima kasih."
      />
      <TextField
        onChange={onValueChangeInvitation}
        fullWidth
        multiline
        rows={3}
        margin="dense"
        label="Doa"
        name="doa"
        placeholder="ex: Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir. (Q.S Ar Rum:21)"
      />
    </Box>
  );
}
