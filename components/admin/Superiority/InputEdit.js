import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({
  dataSuperiority,
  onValueChangeSuperiority,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeSuperiority}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataSuperiority.title || ""}
      />
      <TextField
        onChange={onValueChangeSuperiority}
        fullWidth
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
