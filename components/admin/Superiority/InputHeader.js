import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataSuperiorityHeader,
  onValueChangeSuperiorityHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeSuperiorityHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataSuperiorityHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeSuperiorityHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataSuperiorityHeader.description || ""}
      />
    </Box>
  );
}
