import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({
  dataMainLanding,
  onValueChangeMainLanding,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMainLanding}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataMainLanding.title || ""}
      />
      <TextField
        onChange={onValueChangeMainLanding}
        fullWidth
        required
        margin="dense"
        label="Description"
        name="description"
        multiline
        rows={3}
        value={dataMainLanding.description || ""}
      />
      <TextField
        onChange={onValueChangeMainLanding}
        fullWidth
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
