import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeMainLanding }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMainLanding}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
      />
      <TextField
        onChange={onValueChangeMainLanding}
        fullWidth
        required
        margin="dense"
        label="Description"
        name="description"
        multiline
        rows={3}
      />
      <TextField
        onChange={onValueChangeMainLanding}
        fullWidth
        required
        margin="dense"
        type="file"
        helperText="max. file size 2Mb"
        label="Image"
        name="image"
      />
    </Box>
  );
}
