import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({
  dataMainLandingHeader,
  onValueChangeMainLandingHeader,
}) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeMainLandingHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataMainLandingHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeMainLandingHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataMainLandingHeader.description || ""}
      />
    </Box>
  );
}
