import { format } from "date-fns";
import ColumnFilter from "./ColumnFilter";

export const COLUMNS = [
  {
    Header: "Id",
    accessor: "id",
    Footer: "Id",
    disableFilters: true,
  },
  {
    Header: "Name",
    accessor: "name",
    Footer: "Name",
  },
  {
    Header: "Descrtiption",
    accessor: "description",
    Footer: "Descrtiption",
  },
  {
    Header: "Rating",
    accessor: "rating",
    Footer: "Rating",
  },
  {
    Header: "Image",
    accessor: "image",
    Footer: "Image",
    // Cell: ({ value }) => {
    //   return format(new Date(value), "dd/MM/yyyy");
    // },
  },
  // {
  //   Header: "Gender",
  //   accessor: "gender",
  //   Footer: "Gender",
  // },
  // {
  //   Header: "Age",
  //   accessor: "age",
  //   Footer: "Age",
  // },
  // {
  //   Header: "Country",
  //   accessor: "country",
  //   Footer: "Country",
  // },
  // {
  //   Header: "Phone",
  //   accessor: "phone",
  //   Footer: "Phone",
  // },
  {
    Header: "Action",
    accessor: "action",
    Footer: "Action",
    Cell: (row) => (
      <div>
        <button onClick={(e) => console.log(row.row.original)}>Edit</button>
        <button onClick={(e) => console.log(row.row.original)}>Delete</button>
      </div>
    ),
  },
];

// export const GROUPED_COLUMNS = [
//   {
//     Header: "Id",
//     accessor: "id",
//     Footer: "Id",
//   },
//   {
//     Header: "Name",
//     Footer: "Name",
//     columns: [
//       {
//         Header: "First Name",
//         accessor: "first_name",
//         Footer: "First Name",
//       },
//       {
//         Header: "Last Name",
//         accessor: "last_name",
//         Footer: "Last Name",
//       },
//       {
//         Header: "Email",
//         accessor: "email",
//         Footer: "Email",
//       },
//     ],
//   },
//   {
//     Header: "Info",
//     Footer: "Info",
//     columns: [
//       {
//         Header: "Date of Birth",
//         accessor: "date_of_birth",
//         Footer: "Date of Birth",
//       },
//       {
//         Header: "Gender",
//         accessor: "gender",
//         Footer: "Gender",
//       },
//       {
//         Header: "Age",
//         accessor: "age",
//         Footer: "Age",
//       },
//       {
//         Header: "Country",
//         accessor: "country",
//         Footer: "Country",
//       },
//       {
//         Header: "Phone",
//         accessor: "phone",
//         Footer: "Phone",
//       },
//     ],
//   },
// ];
