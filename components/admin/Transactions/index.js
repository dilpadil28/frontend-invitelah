import PageHeader from "./PageHeader";
// import PageTitleWrapper from 'src/components/PageTitleWrapper';
import { Container, Grid } from "@mui/material";
// import Footer from "src/components/Footer";

import RecentOrders from "./RecentOrders";

function ApplicationsTransactions() {
  return (
    <>
      {/* <PageTitleWrapper> */}

      <PageHeader />

      {/* </PageTitleWrapper> */}

      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <RecentOrders />
        </Grid>
      </Grid>

      {/* <Footer /> */}
    </>
  );
}

export default ApplicationsTransactions;
