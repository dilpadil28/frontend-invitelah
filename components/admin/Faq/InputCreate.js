import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeFaq }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeFaq}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Question"
        name="question"
      />
      <TextField
        onChange={onValueChangeFaq}
        fullWidth
        required
        margin="dense"
        label="Answer"
        name="answer"
        multiline
        rows={3}
      />
    </Box>
  );
}
