import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import { api } from "../../../config/api";

export default function InputEdit({ dataFaq, onValueChangeFaq }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeFaq}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Question"
        name="question"
        value={dataFaq.question || ""}
      />
      <TextField
        onChange={onValueChangeFaq}
        fullWidth
        required
        margin="dense"
        label="Answer"
        name="answer"
        multiline
        rows={3}
        value={dataFaq.answer || ""}
      />
    </Box>
  );
}
