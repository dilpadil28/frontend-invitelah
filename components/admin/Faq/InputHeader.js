import { Box, TextField } from "@mui/material";
import React from "react";

export default function InputHeader({ dataFaqHeader, onValueChangeFaqHeader }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeFaqHeader}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        value={dataFaqHeader.title || ""}
      />
      <TextField
        onChange={onValueChangeFaqHeader}
        fullWidth
        required
        margin="dense"
        label="Description"
        multiline
        rows={3}
        name="description"
        value={dataFaqHeader.description || ""}
      />
    </Box>
  );
}
