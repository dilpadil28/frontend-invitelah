import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";

export default function InputCreate({ onValueChangeYoutube }) {
  return (
    <Box my={1}>
      <TextField
        onChange={onValueChangeYoutube}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Title"
        name="title"
        placeholder="ex: Nama Video"
      />
      <TextField
        onChange={onValueChangeYoutube}
        fullWidth
        margin="dense"
        required
        id="outlined-required"
        label="Url"
        name="url"
        placeholder="id youtube ex: QvgnZB0-k4c"
      />
      <FormControl margin="dense" fullWidth>
        <InputLabel id="demo-simple-select-label">Published</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Published"
          name="published"
          onChange={onValueChangeYoutube}
          defaultValue={true}
        >
          <MenuItem value={true}>Published</MenuItem>
          <MenuItem value={false}>Draft</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
