

// log specific events happening.
export const event = ({ action, params }) => {
  typeof window !== "undefined" && window.gtag('event', action, params)

}