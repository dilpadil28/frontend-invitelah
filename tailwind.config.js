/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./pages/**/*.{js,jsx}", "./components/**/*.{js,jsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#006C32",
        "primary-light": "#FBF2EA",
        "primary-gray": "#434f5a",
        secondary: "#6E759F",
        tertiary: "#fe5e02",
        grey: "#38424b",
        "light-black": "#222222",
        "light-grey": "#c0c0c0",
        success: "#44D600",
        "success-light": "#E7F0E1",
        danger: "#FF1943",
        "danger-light": "#EFDEDE",
        info: "#33C2FF",
        warning: "#FFA319",
        white: "#fcfcfc",
        offwhite: "#f5f5f5",
        black: "#222222",
        neutral: "#444649",
        "dark-blue": "#363636",
        "astra-blue": "#005382",
        "astra-blue-light": "#D9E5EC",
        blue: "#377DFF",
      },
    },
    fontFamily: {},
  },
  plugins: [],
};
