import api from "./api";

class MusicService {
  getMusic() {
    return api.get("/music");
  }
  getMusicById(id) {
    return api.get("/music/" + id);
  }
  createMusic(data) {
    return api.post("/music", data).then((response) => {
      return response.data;
    });
  }
  updateMusic(id, result) {
    return api.patch("/music/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMusic(id) {
    return api.delete("/music/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new MusicService();
