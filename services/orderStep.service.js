import api from "./api";

class OrderStepService {
  getOrderStep() {
    return api.get("/orderstep");
  }
  getOrderStepById(id) {
    return api.get("/orderstep/" + id);
  }
  createOrderStep(data) {
    return api.post("/orderstep", data).then((response) => {
      return response.data;
    });
  }
  updateOrderStep(id, result) {
    return api.patch("/orderstep/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteOrderStep(id) {
    return api.delete("/orderstep/" + id).then((response) => {
      return response.data;
    });
  }
  getOrderStepList() {
    return api.get("/ordersteplist");
  }
  getOrderStepListById(id) {
    return api.get("/ordersteplist/" + id);
  }
  createOrderStepList(data) {
    return api.post("/ordersteplist", data).then((response) => {
      return response.data;
    });
  }
  updateOrderStepList(id, result) {
    return api.patch("/ordersteplist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteOrderStepList(id) {
    return api.delete("/ordersteplist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new OrderStepService();
