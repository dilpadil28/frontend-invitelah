import api from "./api";

class TestimonialService {
  getTestimonial() {
    return api.get("/testimonial");
  }
  getTestimonialById(id) {
    return api.get("/testimonial/" + id);
  }
  createTestimonial(data) {
    return api.post("/testimonial", data).then((response) => {
      return response.data;
    });
  }
  updateTestimonial(id, result) {
    return api.patch("/testimonial/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteTestimonial(id) {
    return api.delete("/testimonial/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new TestimonialService();
