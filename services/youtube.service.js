import api from "./api";

class YoutubeService {
  getYoutube() {
    return api.get("/youtube");
  }
  getYoutubeById(id) {
    return api.get("/youtube/" + id);
  }
  getYoutubeByInvitationId(id) {
    return api.get("/youtube-invitation/" + id);
  }
  createYoutube(data) {
    return api.post("/youtube", data).then((response) => {
      return response.data;
    });
  }
  updateYoutube(id, result) {
    return api.patch("/youtube/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteYoutube(id) {
    return api.delete("/youtube/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new YoutubeService();
