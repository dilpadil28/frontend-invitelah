import api from "./api";

class ProkesService {
  getProkes() {
    return api.get("/prokes");
  }
  getProkesById(id) {
    return api.get("/prokes/" + id);
  }
  createProkes(data) {
    return api.post("/prokes", data).then((response) => {
      return response.data;
    });
  }
  updateProkes(id, result) {
    return api.patch("/prokes/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteProkes(id) {
    return api.delete("/prokes/" + id).then((response) => {
      return response.data;
    });
  }
  getProkesList() {
    return api.get("/prokeslist");
  }
  getProkesListById(id) {
    return api.get("/prokeslist/" + id);
  }
  createProkesList(data) {
    return api.post("/prokeslist", data).then((response) => {
      return response.data;
    });
  }
  updateProkesList(id, result) {
    return api.patch("/prokeslist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteProkesList(id) {
    return api.delete("/prokeslist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new ProkesService();
