import api from "./api";

class PriceService {
  getPrice() {
    return api.get("/price");
  }
  getPriceById(id) {
    return api.get("/price/" + id);
  }
  createPrice(data) {
    return api.post("/price", data).then((response) => {
      return response.data;
    });
  }
  updatePrice(id, result) {
    return api.patch("/price/" + id, result).then((response) => {
      return response.data;
    });
  }
  deletePrice(id) {
    return api.delete("/price/" + id).then((response) => {
      return response.data;
    });
  }
  getPriceList() {
    return api.get("/pricelist");
  }
  getPriceListById(id) {
    return api.get("/pricelist/" + id);
  }
  createPriceList(data) {
    return api.post("/pricelist", data).then((response) => {
      return response.data;
    });
  }
  updatePriceList(id, result) {
    return api.patch("/pricelist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deletePriceList(id) {
    return api.delete("/pricelist/" + id).then((response) => {
      return response.data;
    });
  }
  getPriceListUrl() {
    return api.get("/pricelisturl");
  }
  getPriceListUrlById(id) {
    return api.get("/pricelisturl/" + id);
  }
  createPriceListUrl(data) {
    return api.post("/pricelisturl", data).then((response) => {
      return response.data;
    });
  }
  updatePriceListUrl(id, result) {
    return api.patch("/pricelisturl/" + id, result).then((response) => {
      return response.data;
    });
  }
  deletePriceListUrl(id) {
    return api.delete("/pricelisturl/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new PriceService();
