import api from "./api";

class DigitalEnvelopeService {
  getDigitalEnvelope() {
    return api.get("/digital-envelope");
  }
  getDigitalEnvelopeById(id) {
    return api.get("/digital-envelope/" + id);
  }
  getDigitalEnvelopeByInvitationId(id) {
    return api.get("/digital-envelope-invitation/" + id);
  }
  createDigitalEnvelope(data) {
    return api.post("/digital-envelope", data).then((response) => {
      return response.data;
    });
  }
  updateDigitalEnvelope(id, result) {
    return api.patch("/digital-envelope/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteDigitalEnvelope(id) {
    return api.delete("/digital-envelope/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new DigitalEnvelopeService();
