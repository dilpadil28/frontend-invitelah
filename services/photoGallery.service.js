import api from "./api";

class PhotoGalleryService {
  getPhotoGallery() {
    return api.get("/photo-gallery");
  }
  getPhotoGalleryById(id) {
    return api.get("/photo-gallery/" + id);
  }
  getPhotoGalleryByInvitationId(id) {
    return api.get("/photo-gallery-invitation/" + id);
  }
  createPhotoGallery(data) {
    return api.post("/photo-gallery", data).then((response) => {
      return response.data;
    });
  }
  updatePhotoGallery(id, result) {
    return api.patch("/photo-gallery/" + id, result).then((response) => {
      return response.data;
    });
  }
  deletePhotoGallery(id) {
    return api.delete("/photo-gallery/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new PhotoGalleryService();
