import api from "./api";

class PresenceService {
  getPresence() {
    return api.get("/presence");
  }
  getPresenceById(id) {
    return api.get("/presence/" + id);
  }
  getPresenceByInvitationId(id) {
    return api.get("/presence-invitation/" + id);
  }
  createPresence(data) {
    return api.post("/presence", data).then((response) => {
      return response.data;
    });
  }
  updatePresence(id, result) {
    return api.patch("/presence/" + id, result).then((response) => {
      return response.data;
    });
  }
  deletePresence(id) {
    return api.delete("/presence/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new PresenceService();
