import api from "./api";

class LoveStoryService {
  getLoveStory() {
    return api.get("/love-story");
  }
  getLoveStoryById(id) {
    return api.get("/love-story/" + id);
  }
  getLoveStoryByInvitationId(id) {
    return api.get("/love-story-invitation/" + id);
  }
  createLoveStory(data) {
    return api.post("/love-story", data).then((response) => {
      return response.data;
    });
  }
  updateLoveStory(id, result) {
    return api.patch("/love-story/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteLoveStory(id) {
    return api.delete("/love-story/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new LoveStoryService();
