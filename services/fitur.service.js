import api from "./api";

class FiturService {
  getFitur() {
    return api.get("/fitur");
  }
  getFiturById(id) {
    return api.get("/fitur/" + id);
  }
  createFitur(data) {
    return api.post("/fitur", data).then((response) => {
      return response.data;
    });
  }
  updateFitur(id, result) {
    return api.patch("/fitur/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteFitur(id) {
    return api.delete("/fitur/" + id).then((response) => {
      return response.data;
    });
  }
  getFiturList() {
    return api.get("/fiturlist");
  }
  getFiturListById(id) {
    return api.get("/fiturlist/" + id);
  }
  createFiturList(data) {
    return api.post("/fiturlist", data).then((response) => {
      return response.data;
    });
  }
  updateFiturList(id, result) {
    return api.patch("/fiturlist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteFiturList(id) {
    return api.delete("/fiturlist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new FiturService();
