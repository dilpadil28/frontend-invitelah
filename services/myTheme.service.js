import api from "./api";

class MyThemeService {
  getMyTheme() {
    return api.get("/mytheme");
  }
  getMyThemeById(id) {
    return api.get("/mytheme/" + id);
  }
  createMyTheme(data) {
    return api.post("/mytheme", data).then((response) => {
      return response.data;
    });
  }
  updateMyTheme(id, result) {
    return api.patch("/mytheme/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMyTheme(id) {
    return api.delete("/mytheme/" + id).then((response) => {
      return response.data;
    });
  }
  getMyThemeList() {
    return api.get("/mythemelist");
  }
  getMyThemeListById(id) {
    return api.get("/mythemelist/" + id);
  }
  createMyThemeList(data) {
    return api.post("/mythemelist", data).then((response) => {
      return response.data;
    });
  }
  updateMyThemeList(id, result) {
    return api.patch("/mythemelist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMyThemeList(id) {
    return api.delete("/mythemelist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new MyThemeService();
