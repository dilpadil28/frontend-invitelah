import api from "./api";

class InvitationService {
  getInvitation() {
    return api.get("/invitation");
  }
  getInvitationById(id) {
    return api.get("/invitation/" + id);
  }
  createInvitation(data) {
    return api.post("/invitation", data).then((response) => {
      return response.data;
    });
  }
  updateInvitation(id, result) {
    return api.patch("/invitation/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteInvitation(id) {
    return api.delete("/invitation/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new InvitationService();
