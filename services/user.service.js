import api from "./api";
import tokenService from "./token.service";

class UserService {
  getUser() {
    return api.get("/user");
  }
  getUserById(id) {
    return api.get("/user/" + id);
  }
  updateUser(id, fullName, username, phoneNumber, roles, email, password) {
    return api
      .patch("/user/" + id, {
        fullName,
        username,
        phoneNumber,
        roles,
        email,
        password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          tokenService.setUser(response.data);
        }
        return response.data;
      });
  }
  deleteUser(id) {
    return api.delete("/user/" + id).then((response) => {
      if (response.data.accessToken) {
        tokenService.setUser(response.data);
      }
      return response.data;
    });
  }
}

export default new UserService();
