import api from "./api";

class MySocialMediaService {
  getMySocialMedia() {
    return api.get("/mysocialmedia");
  }
  getMySocialMediaById(id) {
    return api.get("/mysocialmedia/" + id);
  }
  createMySocialMedia(data) {
    return api.post("/mysocialmedia", data).then((response) => {
      return response.data;
    });
  }
  updateMySocialMedia(id, result) {
    return api.patch("/mysocialmedia/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMySocialMedia(id) {
    return api.delete("/mysocialmedia/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new MySocialMediaService();
