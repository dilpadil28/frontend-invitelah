import api from "./api";

class SuperiorityService {
  getSuperiority() {
    return api.get("/superiority");
  }
  getSuperiorityById(id) {
    return api.get("/superiority/" + id);
  }
  createSuperiority(data) {
    return api.post("/superiority", data).then((response) => {
      return response.data;
    });
  }
  updateSuperiority(id, result) {
    return api.patch("/superiority/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteSuperiority(id) {
    return api.delete("/superiority/" + id).then((response) => {
      return response.data;
    });
  }
  getSuperiorityList() {
    return api.get("/superioritylist");
  }
  getSuperiorityListById(id) {
    return api.get("/superioritylist/" + id);
  }
  createSuperiorityList(data) {
    return api.post("/superioritylist", data).then((response) => {
      return response.data;
    });
  }
  updateSuperiorityList(id, result) {
    return api.patch("/superioritylist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteSuperiorityList(id) {
    return api.delete("/superioritylist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new SuperiorityService();
