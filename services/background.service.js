import api from "./api";

class BackgroundService {
  getBackground() {
    return api.get("/background");
  }
  getBackgroundById(id) {
    return api.get("/background/" + id);
  }
  getBackgroundByInvitationId(id) {
    return api.get("/background-invitation/" + id);
  }
  createBackground(data) {
    return api.post("/background", data).then((response) => {
      return response.data;
    });
  }
  updateBackground(id, result) {
    return api.patch("/background/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteBackground(id) {
    return api.delete("/background/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new BackgroundService();
