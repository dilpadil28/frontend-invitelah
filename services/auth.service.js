import api from "./api";
import TokenService from "./token.service";
class AuthService {
  login(username, password) {
    return api
      .post("/auth/signin", {
        username,
        password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          TokenService.setUser(response.data);
        }

        return response.data;
      });
  }

  register(fullName, username, phoneNumber, email, roles, password) {
    return api.post("/auth/signup", {
      fullName,
      username,
      phoneNumber,
      email,
      roles,
      password,
    });
  }

  logout() {
    TokenService.removeUser();
  }
}

export default new AuthService();
