import api from "./api";

class FaqService {
  getFaq() {
    return api.get("/faq");
  }
  getFaqById(id) {
    return api.get("/faq/" + id);
  }
  createFaq(data) {
    return api.post("/faq", data).then((response) => {
      return response.data;
    });
  }
  updateFaq(id, result) {
    return api.patch("/faq/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteFaq(id) {
    return api.delete("/faq/" + id).then((response) => {
      return response.data;
    });
  }
  getFaqList() {
    return api.get("/faqlist");
  }
  getFaqListById(id) {
    return api.get("/faqlist/" + id);
  }
  createFaqList(data) {
    return api.post("/faqlist", data).then((response) => {
      return response.data;
    });
  }
  updateFaqList(id, result) {
    return api.patch("/faqlist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteFaqList(id) {
    return api.delete("/faqlist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new FaqService();
