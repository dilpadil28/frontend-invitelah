import api from "./api";

class MessageApiService {
  getMessageApi() {
    return api.get("/message");
  }
  getMessageApiById(id) {
    return api.get("/message/" + id);
  }
  getMessageApiByInvitationId(id) {
    return api.get("/message-invitation/" + id);
  }
  createMessageApi(data) {
    return api.post("/message", data).then((response) => {
      return response.data;
    });
  }
  updateMessageApi(id, result) {
    return api.patch("/message/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMessageApi(id) {
    return api.delete("/message/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new MessageApiService();
