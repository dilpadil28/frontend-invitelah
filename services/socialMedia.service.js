import api from "./api";

class SocialMediaService {
  getSocialMedia() {
    return api.get("/social-media");
  }
  getSocialMediaById(id) {
    return api.get("/social-media/" + id);
  }
  getSocialMediaByInvitationId(id) {
    return api.get("/social-media-invitation/" + id);
  }
  createSocialMedia(data) {
    return api.post("/social-media", data).then((response) => {
      return response.data;
    });
  }
  updateSocialMedia(id, result) {
    return api.patch("/social-media/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteSocialMedia(id) {
    return api.delete("/social-media/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new SocialMediaService();
