import api from "./api";

class MainLandingService {
  getMainLanding() {
    return api.get("/mainlanding");
  }
  getMainLandingById(id) {
    return api.get("/mainlanding/" + id);
  }
  createMainLanding(data) {
    return api.post("/mainlanding", data).then((response) => {
      return response.data;
    });
  }
  updateMainLanding(id, result) {
    return api.patch("/mainlanding/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMainLanding(id) {
    return api.delete("/mainlanding/" + id).then((response) => {
      return response.data;
    });
  }
  getMainLandingList() {
    return api.get("/mainlandinglist");
  }
  getMainLandingListById(id) {
    return api.get("/mainlandinglist/" + id);
  }
  createMainLandingList(data) {
    return api.post("/mainlandinglist", data).then((response) => {
      return response.data;
    });
  }
  updateMainLandingList(id, result) {
    return api.patch("/mainlandinglist/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteMainLandingList(id) {
    return api.delete("/mainlandinglist/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new MainLandingService();
