import api from "./api";

class ThemeService {
  getTheme() {
    return api.get("/theme");
  }
  getThemeById(id) {
    return api.get("/theme/" + id);
  }
  getThemeByInvitationId(id) {
    return api.get("/theme-invitation/" + id);
  }
  createTheme(data) {
    return api.post("/theme", data).then((response) => {
      return response.data;
    });
  }
  updateTheme(id, result) {
    return api.patch("/theme/" + id, result).then((response) => {
      return response.data;
    });
  }
  deleteTheme(id) {
    return api.delete("/theme/" + id).then((response) => {
      return response.data;
    });
  }
}

export default new ThemeService();
