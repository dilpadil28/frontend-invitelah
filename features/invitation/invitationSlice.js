import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import invitationService from "../../services/invitation.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  invitation: null,
};

export const getInvitation = createAsyncThunk(
  "invitation/getInvitation",
  async (_, thunkAPI) => {
    const data = await invitationService.getInvitation().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getInvitationById = createAsyncThunk(
  "invitation/getInvitationById",
  async (id, thunkAPI) => {
    const data = await invitationService.getInvitationById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createInvitation = createAsyncThunk(
  "invitation/createInvitation",
  async (result, thunkAPI) => {
    const data = await invitationService.createInvitation(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getInvitation());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateInvitation = createAsyncThunk(
  "invitation/updateInvitation",
  async ({ id, result }, thunkAPI) => {


    const data = await invitationService.updateInvitation(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getInvitation());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteInvitation = createAsyncThunk(
  "invitation/deleteInvitation",
  async (id, thunkAPI) => {
    const data = await invitationService.deleteInvitation(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getInvitation());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const invitationSlice = createSlice({
  name: "invitation",
  initialState,
  extraReducers: {
    [getInvitation.fulfilled]: (state, action) => {
      state.invitation = action.payload;
    },
    [getInvitation.rejected]: (state, action) => {
      state.invitation = null;
    },
    [getInvitationById.fulfilled]: (state, action) => {
      // state.invitation = action.payload;
    },
    [getInvitationById.rejected]: (state, action) => {
      // state.invitation = null;
    },
    [createInvitation.fulfilled]: (state, action) => {
      // state.invitation = action.payload.invitation;
    },
    [createInvitation.rejected]: (state, action) => {
      // state.invitation = null;
    },
    [updateInvitation.fulfilled]: (state, action) => {
      // state.invitationUpdate = action.payload;
    },
    [updateInvitation.rejected]: (state, action) => {
      // state.invitation = null;
    },
    [deleteInvitation.fulfilled]: (state, action) => {
      // state.invitation = action.payload.invitation;
    },
    [deleteInvitation.rejected]: (state, action) => {
      // state.invitation = null;
    },
  },
});

const { reducer } = invitationSlice;
export default reducer;
