import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import messageApiService from "../../services/messageApi.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  messageApi: null,
};

export const getMessageApi = createAsyncThunk(
  "messageApi/getMessageApi",
  async (_, thunkAPI) => {
    const data = await messageApiService.getMessageApi().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMessageApiById = createAsyncThunk(
  "messageApi/getMessageApiById",
  async (id, thunkAPI) => {
    const data = await messageApiService.getMessageApiById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMessageApiByInvitationId = createAsyncThunk(
  "messageApi/getMessageApiByInvitationId",
  async (id, thunkAPI) => {
    const data = await messageApiService.getMessageApiByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMessageApi = createAsyncThunk(
  "messageApi/createMessageApi",
  async (result, thunkAPI) => {
    const data = await messageApiService.createMessageApi(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMessageApi = createAsyncThunk(
  "messageApi/updateMessageApi",
  async ({ id, result }, thunkAPI) => {


    const data = await messageApiService.updateMessageApi(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteMessageApi = createAsyncThunk(
  "messageApi/deleteMessageApi",
  async (id, thunkAPI) => {
    const data = await messageApiService.deleteMessageApi(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const messageApiSlice = createSlice({
  name: "messageApi",
  initialState,
  extraReducers: {
    [getMessageApi.fulfilled]: (state, action) => {
      // state.messageApi = action.payload;
    },
    [getMessageApi.rejected]: (state, action) => {
      // state.messageApi = null;
    },
    [getMessageApiByInvitationId.fulfilled]: (state, action) => {
      state.messageApi = action.payload;
    },
    [getMessageApiByInvitationId.rejected]: (state, action) => {
      state.messageApi = null;
    },
    [getMessageApiById.fulfilled]: (state, action) => {
      // state.messageApi = action.payload;
    },
    [getMessageApiById.rejected]: (state, action) => {
      // state.messageApi = null;
    },
    [createMessageApi.fulfilled]: (state, action) => {
      // state.messageApi = action.payload.messageApi;
    },
    [createMessageApi.rejected]: (state, action) => {
      // state.messageApi = null;
    },
    [updateMessageApi.fulfilled]: (state, action) => {
      // state.messageApiUpdate = action.payload;
    },
    [updateMessageApi.rejected]: (state, action) => {
      // state.messageApi = null;
    },
    [deleteMessageApi.fulfilled]: (state, action) => {
      // state.messageApi = action.payload.messageApi;
    },
    [deleteMessageApi.rejected]: (state, action) => {
      // state.messageApi = null;
    },
  },
});

const { reducer } = messageApiSlice;
export default reducer;
