import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import userService from "../../services/user.service";
import { setMessage } from "../message/messageSlice";

export const getUser = createAsyncThunk("user/getUser", async (_, thunkAPI) => {
  const data = await userService.getUser().then(
    (response) => {
      return response.data.data;
    },
    (error) => {
      if (
        (error.response && error.response.status === 403) ||
        error.response.status === 500
      ) {
        eventBus.dispatch("logout");
      }
    }
  );
  return data;
});
export const getUserById = createAsyncThunk(
  "user/getUserById",
  async (id, thunkAPI) => {
    const data = await userService.getUserById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateUser = createAsyncThunk(
  "user/updateUser",
  async (
    { id, fullName, username, phoneNumber, roles, email, password },
    thunkAPI
  ) => {
    const data = await userService
      .updateUser(id, fullName, username, phoneNumber, roles, email, password)
      .then(
        (response) => {
          thunkAPI.dispatch(setMessage(response.message));
          thunkAPI.dispatch(getUser());
          return response.data.data;
        },
        (error) => {
          const message =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          thunkAPI.dispatch(
            setMessage(message + " " + error.response.data.error[0].msg)
          );
          if (error.response && error.response.status === 403) {
            eventBus.dispatch("logout");
          }
        }
      );
    return data;
  }
);
export const deleteUser = createAsyncThunk(
  "user/deleteUser",
  async (id, thunkAPI) => {
    const data = await userService.deleteUser(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getUser());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const initialState = {};

const userSlice = createSlice({
  name: "user",
  initialState,
  extraReducers: {
    [getUser.fulfilled]: (state, action) => {
      state.user = action.payload;
    },
    [getUser.rejected]: (state, action) => {
      state.user = null;
    },
    [getUserById.fulfilled]: (state, action) => {
      // state.user = action.payload;
    },
    [getUserById.rejected]: (state, action) => {
      // state.user = null;
    },
    [updateUser.fulfilled]: (state, action) => {
      // state.userUpdate = action.payload;
    },
    [updateUser.rejected]: (state, action) => {
      // state.user = null;
    },
    [deleteUser.fulfilled]: (state, action) => {
      // state.user = action.payload.user;
    },
    [deleteUser.rejected]: (state, action) => {
      // state.user = null;
    },
  },
});

const { reducer } = userSlice;
export default reducer;
