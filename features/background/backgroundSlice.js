import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import backgroundService from "../../services/background.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  background: null,
};

export const getBackground = createAsyncThunk(
  "background/getBackground",
  async (_, thunkAPI) => {
    const data = await backgroundService.getBackground().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getBackgroundById = createAsyncThunk(
  "background/getBackgroundById",
  async (id, thunkAPI) => {
    const data = await backgroundService.getBackgroundById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getBackgroundByInvitationId = createAsyncThunk(
  "background/getBackgroundByInvitationId",
  async (id, thunkAPI) => {
    const data = await backgroundService.getBackgroundByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createBackground = createAsyncThunk(
  "background/createBackground",
  async (result, thunkAPI) => {
    const data = await backgroundService.createBackground(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateBackground = createAsyncThunk(
  "background/updateBackground",
  async ({ id, result }, thunkAPI) => {


    const data = await backgroundService.updateBackground(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteBackground = createAsyncThunk(
  "background/deleteBackground",
  async (id, thunkAPI) => {
    const data = await backgroundService.deleteBackground(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const backgroundSlice = createSlice({
  name: "background",
  initialState,
  extraReducers: {
    [getBackground.fulfilled]: (state, action) => {
      // state.background = action.payload;
    },
    [getBackground.rejected]: (state, action) => {
      // state.background = null;
    },
    [getBackgroundByInvitationId.fulfilled]: (state, action) => {
      state.background = action.payload;
    },
    [getBackgroundByInvitationId.rejected]: (state, action) => {
      state.background = null;
    },
    [getBackgroundById.fulfilled]: (state, action) => {
      // state.background = action.payload;
    },
    [getBackgroundById.rejected]: (state, action) => {
      // state.background = null;
    },
    [createBackground.fulfilled]: (state, action) => {
      // state.background = action.payload.background;
    },
    [createBackground.rejected]: (state, action) => {
      // state.background = null;
    },
    [updateBackground.fulfilled]: (state, action) => {
      // state.backgroundUpdate = action.payload;
    },
    [updateBackground.rejected]: (state, action) => {
      // state.background = null;
    },
    [deleteBackground.fulfilled]: (state, action) => {
      // state.background = action.payload.background;
    },
    [deleteBackground.rejected]: (state, action) => {
      // state.background = null;
    },
  },
});

const { reducer } = backgroundSlice;
export default reducer;
