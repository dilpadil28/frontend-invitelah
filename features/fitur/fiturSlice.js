import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import fiturService from "../../services/fitur.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  fitur: null,
  fiturList: [],
};

export const getFitur = createAsyncThunk(
  "fitur/getFitur",
  async (_, thunkAPI) => {
    const data = await fiturService.getFitur().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getFiturById = createAsyncThunk(
  "fitur/getFiturById",
  async (id, thunkAPI) => {
    const data = await fiturService.getFiturById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createFitur = createAsyncThunk(
  "fitur/createFitur",
  async (result, thunkAPI) => {
    const data = await fiturService.createFitur(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFitur());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateFitur = createAsyncThunk(
  "fitur/updateFitur",
  async ({ id, result }, thunkAPI) => {
    const data = await fiturService.updateFitur(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFitur());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteFitur = createAsyncThunk(
  "fitur/deleteFitur",
  async (id, thunkAPI) => {
    const data = await fiturService.deleteFitur(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFitur());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getFiturList = createAsyncThunk(
  "fiturList/getFiturList",
  async (_, thunkAPI) => {
    const data = await fiturService.getFiturList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getFiturListById = createAsyncThunk(
  "fiturList/getFiturListById",
  async (id, thunkAPI) => {
    const data = await fiturService.getFiturListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createFiturList = createAsyncThunk(
  "fiturList/createFiturList",
  async (result, thunkAPI) => {
    const data = await fiturService.createFiturList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFiturList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateFiturList = createAsyncThunk(
  "fiturList/updateFiturList",
  async ({ id, result }, thunkAPI) => {
    const data = await fiturService.updateFiturList(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFiturList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteFiturList = createAsyncThunk(
  "fiturList/deleteFiturList",
  async (id, thunkAPI) => {
    const data = await fiturService.deleteFiturList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFiturList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const fiturSlice = createSlice({
  name: "fitur",
  initialState,
  extraReducers: {
    [getFitur.fulfilled]: (state, action) => {
      state.fitur = action.payload;
    },
    [getFitur.rejected]: (state, action) => {
      state.fitur = null;
    },
    [getFiturById.fulfilled]: (state, action) => {
      // state.fitur = action.payload;
    },
    [getFiturById.rejected]: (state, action) => {
      // state.fitur = null;
    },
    [createFitur.fulfilled]: (state, action) => {
      // state.fitur = action.payload.fitur;
    },
    [createFitur.rejected]: (state, action) => {
      // state.fitur = null;
    },
    [updateFitur.fulfilled]: (state, action) => {
      // state.fiturUpdate = action.payload;
    },
    [updateFitur.rejected]: (state, action) => {
      // state.fitur = null;
    },
    [deleteFitur.fulfilled]: (state, action) => {
      // state.fitur = action.payload.fitur;
    },
    [deleteFitur.rejected]: (state, action) => {
      // state.fitur = null;
    },
    [getFiturList.fulfilled]: (state, action) => {
      state.fiturList = action.payload;
    },
    [getFiturList.rejected]: (state, action) => {
      state.fiturList = null;
    },
    [getFiturListById.fulfilled]: (state, action) => {
      // state.fiturList = action.payload;
    },
    [getFiturListById.rejected]: (state, action) => {
      // state.fiturList = null;
    },
    [createFiturList.fulfilled]: (state, action) => {
      // state.fiturList = action.payload.fiturList;
    },
    [createFiturList.rejected]: (state, action) => {
      // state.fiturList = null;
    },
    [updateFiturList.fulfilled]: (state, action) => {
      // state.fiturListUpdate = action.payload;
    },
    [updateFiturList.rejected]: (state, action) => {
      // state.fiturList = null;
    },
    [deleteFiturList.fulfilled]: (state, action) => {
      // state.fiturList = action.payload.fiturList;
    },
    [deleteFiturList.rejected]: (state, action) => {
      // state.fiturList = null;
    },
  },
});

const { reducer } = fiturSlice;
export default reducer;
