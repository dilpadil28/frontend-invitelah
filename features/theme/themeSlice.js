import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import backgroundService from "../../services/theme.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  theme: null,
};

export const getTheme = createAsyncThunk(
  "theme/getTheme",
  async (_, thunkAPI) => {
    const data = await backgroundService.getTheme().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getThemeById = createAsyncThunk(
  "theme/getThemeById",
  async (id, thunkAPI) => {
    const data = await backgroundService.getThemeById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getThemeByInvitationId = createAsyncThunk(
  "theme/getThemeByInvitationId",
  async (id, thunkAPI) => {
    const data = await backgroundService.getThemeByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createTheme = createAsyncThunk(
  "theme/createTheme",
  async (result, thunkAPI) => {
    const data = await backgroundService.createTheme(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateTheme = createAsyncThunk(
  "theme/updateTheme",
  async ({ id, result }, thunkAPI) => {


    const data = await backgroundService.updateTheme(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteTheme = createAsyncThunk(
  "theme/deleteTheme",
  async (id, thunkAPI) => {
    const data = await backgroundService.deleteTheme(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const backgroundSlice = createSlice({
  name: "theme",
  initialState,
  extraReducers: {
    [getTheme.fulfilled]: (state, action) => {
      // state.theme = action.payload;
    },
    [getTheme.rejected]: (state, action) => {
      // state.theme = null;
    },
    [getThemeByInvitationId.fulfilled]: (state, action) => {
      state.theme = action.payload;
    },
    [getThemeByInvitationId.rejected]: (state, action) => {
      state.theme = null;
    },
    [getThemeById.fulfilled]: (state, action) => {
      // state.theme = action.payload;
    },
    [getThemeById.rejected]: (state, action) => {
      // state.theme = null;
    },
    [createTheme.fulfilled]: (state, action) => {
      // state.theme = action.payload.theme;
    },
    [createTheme.rejected]: (state, action) => {
      // state.theme = null;
    },
    [updateTheme.fulfilled]: (state, action) => {
      // state.backgroundUpdate = action.payload;
    },
    [updateTheme.rejected]: (state, action) => {
      // state.theme = null;
    },
    [deleteTheme.fulfilled]: (state, action) => {
      // state.theme = action.payload.theme;
    },
    [deleteTheme.rejected]: (state, action) => {
      // state.theme = null;
    },
  },
});

const { reducer } = backgroundSlice;
export default reducer;
