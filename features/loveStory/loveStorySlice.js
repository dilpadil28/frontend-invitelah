import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import backgroundService from "../../services/loveStory.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  loveStory: null,
};

export const getLoveStory = createAsyncThunk(
  "loveStory/getLoveStory",
  async (_, thunkAPI) => {
    const data = await backgroundService.getLoveStory().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getLoveStoryById = createAsyncThunk(
  "loveStory/getLoveStoryById",
  async (id, thunkAPI) => {
    const data = await backgroundService.getLoveStoryById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getLoveStoryByInvitationId = createAsyncThunk(
  "loveStory/getLoveStoryByInvitationId",
  async (id, thunkAPI) => {
    const data = await backgroundService.getLoveStoryByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createLoveStory = createAsyncThunk(
  "loveStory/createLoveStory",
  async (result, thunkAPI) => {
    const data = await backgroundService.createLoveStory(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateLoveStory = createAsyncThunk(
  "loveStory/updateLoveStory",
  async ({ id, result }, thunkAPI) => {


    const data = await backgroundService.updateLoveStory(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteLoveStory = createAsyncThunk(
  "loveStory/deleteLoveStory",
  async (id, thunkAPI) => {
    const data = await backgroundService.deleteLoveStory(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const backgroundSlice = createSlice({
  name: "loveStory",
  initialState,
  extraReducers: {
    [getLoveStory.fulfilled]: (state, action) => {
      // state.loveStory = action.payload;
    },
    [getLoveStory.rejected]: (state, action) => {
      // state.loveStory = null;
    },
    [getLoveStoryByInvitationId.fulfilled]: (state, action) => {
      state.loveStory = action.payload;
    },
    [getLoveStoryByInvitationId.rejected]: (state, action) => {
      state.loveStory = null;
    },
    [getLoveStoryById.fulfilled]: (state, action) => {
      // state.loveStory = action.payload;
    },
    [getLoveStoryById.rejected]: (state, action) => {
      // state.loveStory = null;
    },
    [createLoveStory.fulfilled]: (state, action) => {
      // state.loveStory = action.payload.loveStory;
    },
    [createLoveStory.rejected]: (state, action) => {
      // state.loveStory = null;
    },
    [updateLoveStory.fulfilled]: (state, action) => {
      // state.backgroundUpdate = action.payload;
    },
    [updateLoveStory.rejected]: (state, action) => {
      // state.loveStory = null;
    },
    [deleteLoveStory.fulfilled]: (state, action) => {
      // state.loveStory = action.payload.loveStory;
    },
    [deleteLoveStory.rejected]: (state, action) => {
      // state.loveStory = null;
    },
  },
});

const { reducer } = backgroundSlice;
export default reducer;
