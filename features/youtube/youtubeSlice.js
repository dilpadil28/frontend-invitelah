import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import youtubeService from "../../services/youtube.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  youtube: null,
};

export const getYoutube = createAsyncThunk(
  "youtube/getYoutube",
  async (_, thunkAPI) => {
    const data = await youtubeService.getYoutube().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getYoutubeById = createAsyncThunk(
  "youtube/getYoutubeById",
  async (id, thunkAPI) => {
    const data = await youtubeService.getYoutubeById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getYoutubeByInvitationId = createAsyncThunk(
  "youtube/getYoutubeByInvitationId",
  async (id, thunkAPI) => {
    const data = await youtubeService.getYoutubeByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createYoutube = createAsyncThunk(
  "youtube/createYoutube",
  async (result, thunkAPI) => {
    const data = await youtubeService.createYoutube(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateYoutube = createAsyncThunk(
  "youtube/updateYoutube",
  async ({ id, result }, thunkAPI) => {


    const data = await youtubeService.updateYoutube(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteYoutube = createAsyncThunk(
  "youtube/deleteYoutube",
  async (id, thunkAPI) => {
    const data = await youtubeService.deleteYoutube(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const youtubeSlice = createSlice({
  name: "youtube",
  initialState,
  extraReducers: {
    [getYoutube.fulfilled]: (state, action) => {
      // state.youtube = action.payload;
    },
    [getYoutube.rejected]: (state, action) => {
      // state.youtube = null;
    },
    [getYoutubeByInvitationId.fulfilled]: (state, action) => {
      state.youtube = action.payload;
    },
    [getYoutubeByInvitationId.rejected]: (state, action) => {
      state.youtube = null;
    },
    [getYoutubeById.fulfilled]: (state, action) => {
      // state.youtube = action.payload;
    },
    [getYoutubeById.rejected]: (state, action) => {
      // state.youtube = null;
    },
    [createYoutube.fulfilled]: (state, action) => {
      // state.youtube = action.payload.youtube;
    },
    [createYoutube.rejected]: (state, action) => {
      // state.youtube = null;
    },
    [updateYoutube.fulfilled]: (state, action) => {
      // state.youtubeUpdate = action.payload;
    },
    [updateYoutube.rejected]: (state, action) => {
      // state.youtube = null;
    },
    [deleteYoutube.fulfilled]: (state, action) => {
      // state.youtube = action.payload.youtube;
    },
    [deleteYoutube.rejected]: (state, action) => {
      // state.youtube = null;
    },
  },
});

const { reducer } = youtubeSlice;
export default reducer;
