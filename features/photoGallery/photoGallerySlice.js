import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import photoGalleryService from "../../services/photoGallery.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  photoGallery: null,
};

export const getPhotoGallery = createAsyncThunk(
  "photoGallery/getPhotoGallery",
  async (_, thunkAPI) => {
    const data = await photoGalleryService.getPhotoGallery().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getPhotoGalleryById = createAsyncThunk(
  "photoGallery/getPhotoGalleryById",
  async (id, thunkAPI) => {
    const data = await photoGalleryService.getPhotoGalleryById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getPhotoGalleryByInvitationId = createAsyncThunk(
  "photoGallery/getPhotoGalleryByInvitationId",
  async (id, thunkAPI) => {
    const data = await photoGalleryService.getPhotoGalleryByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createPhotoGallery = createAsyncThunk(
  "photoGallery/createPhotoGallery",
  async (result, thunkAPI) => {
    const data = await photoGalleryService.createPhotoGallery(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updatePhotoGallery = createAsyncThunk(
  "photoGallery/updatePhotoGallery",
  async ({ id, result }, thunkAPI) => {


    const data = await photoGalleryService.updatePhotoGallery(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deletePhotoGallery = createAsyncThunk(
  "photoGallery/deletePhotoGallery",
  async (id, thunkAPI) => {
    const data = await photoGalleryService.deletePhotoGallery(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const photoGallerySlice = createSlice({
  name: "photoGallery",
  initialState,
  extraReducers: {
    [getPhotoGallery.fulfilled]: (state, action) => {
      // state.photoGallery = action.payload;
    },
    [getPhotoGallery.rejected]: (state, action) => {
      // state.photoGallery = null;
    },
    [getPhotoGalleryByInvitationId.fulfilled]: (state, action) => {
      state.photoGallery = action.payload;
    },
    [getPhotoGalleryByInvitationId.rejected]: (state, action) => {
      state.photoGallery = null;
    },
    [getPhotoGalleryById.fulfilled]: (state, action) => {
      // state.photoGallery = action.payload;
    },
    [getPhotoGalleryById.rejected]: (state, action) => {
      // state.photoGallery = null;
    },
    [createPhotoGallery.fulfilled]: (state, action) => {
      // state.photoGallery = action.payload.photoGallery;
    },
    [createPhotoGallery.rejected]: (state, action) => {
      // state.photoGallery = null;
    },
    [updatePhotoGallery.fulfilled]: (state, action) => {
      // state.photoGalleryUpdate = action.payload;
    },
    [updatePhotoGallery.rejected]: (state, action) => {
      // state.photoGallery = null;
    },
    [deletePhotoGallery.fulfilled]: (state, action) => {
      // state.photoGallery = action.payload.photoGallery;
    },
    [deletePhotoGallery.rejected]: (state, action) => {
      // state.photoGallery = null;
    },
  },
});

const { reducer } = photoGallerySlice;
export default reducer;
