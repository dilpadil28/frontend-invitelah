import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import priceService from "../../services/price.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  price: null,
  priceList: [],
};

export const getPrice = createAsyncThunk(
  "price/getPrice",
  async (_, thunkAPI) => {
    const data = await priceService.getPrice().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getPriceById = createAsyncThunk(
  "price/getPriceById",
  async (id, thunkAPI) => {
    const data = await priceService.getPriceById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createPrice = createAsyncThunk(
  "price/createPrice",
  async (result, thunkAPI) => {
    const data = await priceService.createPrice(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getPrice());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updatePrice = createAsyncThunk(
  "price/updatePrice",
  async ({ id, result }, thunkAPI) => {
    const data = await priceService.updatePrice(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getPrice());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deletePrice = createAsyncThunk(
  "price/deletePrice",
  async (id, thunkAPI) => {
    const data = await priceService.deletePrice(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getPrice());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getPriceList = createAsyncThunk(
  "priceList/getPriceList",
  async (_, thunkAPI) => {
    const data = await priceService.getPriceList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getPriceListById = createAsyncThunk(
  "priceList/getPriceListById",
  async (id, thunkAPI) => {
    const data = await priceService.getPriceListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createPriceList = createAsyncThunk(
  "priceList/createPriceList",
  async (result, thunkAPI) => {
    const data = await priceService.createPriceList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getPriceList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updatePriceList = createAsyncThunk(
  "priceList/updatePriceList",
  async ({ id, result }, thunkAPI) => {
    const data = await priceService.updatePriceList(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getPriceList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deletePriceList = createAsyncThunk(
  "priceList/deletePriceList",
  async (id, thunkAPI) => {
    const data = await priceService.deletePriceList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getPriceList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const priceSlice = createSlice({
  name: "price",
  initialState,
  extraReducers: {
    [getPrice.fulfilled]: (state, action) => {
      state.price = action.payload;
    },
    [getPrice.rejected]: (state, action) => {
      state.price = null;
    },
    [getPriceById.fulfilled]: (state, action) => {
      // state.price = action.payload;
    },
    [getPriceById.rejected]: (state, action) => {
      // state.price = null;
    },
    [createPrice.fulfilled]: (state, action) => {
      // state.price = action.payload.price;
    },
    [createPrice.rejected]: (state, action) => {
      // state.price = null;
    },
    [updatePrice.fulfilled]: (state, action) => {
      // state.priceUpdate = action.payload;
    },
    [updatePrice.rejected]: (state, action) => {
      // state.price = null;
    },
    [deletePrice.fulfilled]: (state, action) => {
      // state.price = action.payload.price;
    },
    [deletePrice.rejected]: (state, action) => {
      // state.price = null;
    },
    [getPriceList.fulfilled]: (state, action) => {
      state.priceList = action.payload;
    },
    [getPriceList.rejected]: (state, action) => {
      state.priceList = null;
    },
    [getPriceListById.fulfilled]: (state, action) => {
      // state.priceList = action.payload;
    },
    [getPriceListById.rejected]: (state, action) => {
      // state.priceList = null;
    },
    [createPriceList.fulfilled]: (state, action) => {
      // state.priceList = action.payload.priceList;
    },
    [createPriceList.rejected]: (state, action) => {
      // state.priceList = null;
    },
    [updatePriceList.fulfilled]: (state, action) => {
      // state.priceListUpdate = action.payload;
    },
    [updatePriceList.rejected]: (state, action) => {
      // state.priceList = null;
    },
    [deletePriceList.fulfilled]: (state, action) => {
      // state.priceList = action.payload.priceList;
    },
    [deletePriceList.rejected]: (state, action) => {
      // state.priceList = null;
    },
  },
});

const { reducer } = priceSlice;
export default reducer;
