import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import myThemeService from "../../services/myTheme.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  myTheme: null,
  myThemeList: [],
};

export const getMyTheme = createAsyncThunk(
  "myTheme/getMyTheme",
  async (_, thunkAPI) => {
    const data = await myThemeService.getMyTheme().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMyThemeById = createAsyncThunk(
  "myTheme/getMyThemeById",
  async (id, thunkAPI) => {
    const data = await myThemeService.getMyThemeById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMyTheme = createAsyncThunk(
  "myTheme/createMyTheme",
  async (result, thunkAPI) => {
    const data = await myThemeService.createMyTheme(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMyTheme());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMyTheme = createAsyncThunk(
  "myTheme/updateMyTheme",
  async ({ id, result }, thunkAPI) => {
    const data = await myThemeService.updateMyTheme(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMyTheme());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteMyTheme = createAsyncThunk(
  "myTheme/deleteMyTheme",
  async (id, thunkAPI) => {
    const data = await myThemeService.deleteMyTheme(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMyTheme());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getMyThemeList = createAsyncThunk(
  "myThemeList/getMyThemeList",
  async (_, thunkAPI) => {
    const data = await myThemeService.getMyThemeList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMyThemeListById = createAsyncThunk(
  "myThemeList/getMyThemeListById",
  async (id, thunkAPI) => {
    const data = await myThemeService.getMyThemeListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMyThemeList = createAsyncThunk(
  "myThemeList/createMyThemeList",
  async (result, thunkAPI) => {
    const data = await myThemeService.createMyThemeList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMyThemeList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMyThemeList = createAsyncThunk(
  "myThemeList/updateMyThemeList",
  async ({ id, result }, thunkAPI) => {
    const data = await myThemeService.updateMyThemeList(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMyThemeList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteMyThemeList = createAsyncThunk(
  "myThemeList/deleteMyThemeList",
  async (id, thunkAPI) => {
    const data = await myThemeService.deleteMyThemeList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMyThemeList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const myThemeSlice = createSlice({
  name: "myTheme",
  initialState,
  extraReducers: {
    [getMyTheme.fulfilled]: (state, action) => {
      state.myTheme = action.payload;
    },
    [getMyTheme.rejected]: (state, action) => {
      state.myTheme = null;
    },
    [getMyThemeById.fulfilled]: (state, action) => {
      // state.myTheme = action.payload;
    },
    [getMyThemeById.rejected]: (state, action) => {
      // state.myTheme = null;
    },
    [createMyTheme.fulfilled]: (state, action) => {
      // state.myTheme = action.payload.myTheme;
    },
    [createMyTheme.rejected]: (state, action) => {
      // state.myTheme = null;
    },
    [updateMyTheme.fulfilled]: (state, action) => {
      // state.myThemeUpdate = action.payload;
    },
    [updateMyTheme.rejected]: (state, action) => {
      // state.myTheme = null;
    },
    [deleteMyTheme.fulfilled]: (state, action) => {
      // state.myTheme = action.payload.myTheme;
    },
    [deleteMyTheme.rejected]: (state, action) => {
      // state.myTheme = null;
    },
    [getMyThemeList.fulfilled]: (state, action) => {
      state.myThemeList = action.payload;
    },
    [getMyThemeList.rejected]: (state, action) => {
      state.myThemeList = null;
    },
    [getMyThemeListById.fulfilled]: (state, action) => {
      // state.myThemeList = action.payload;
    },
    [getMyThemeListById.rejected]: (state, action) => {
      // state.myThemeList = null;
    },
    [createMyThemeList.fulfilled]: (state, action) => {
      // state.myThemeList = action.payload.myThemeList;
    },
    [createMyThemeList.rejected]: (state, action) => {
      // state.myThemeList = null;
    },
    [updateMyThemeList.fulfilled]: (state, action) => {
      // state.myThemeListUpdate = action.payload;
    },
    [updateMyThemeList.rejected]: (state, action) => {
      // state.myThemeList = null;
    },
    [deleteMyThemeList.fulfilled]: (state, action) => {
      // state.myThemeList = action.payload.myThemeList;
    },
    [deleteMyThemeList.rejected]: (state, action) => {
      // state.myThemeList = null;
    },
  },
});

const { reducer } = myThemeSlice;
export default reducer;
