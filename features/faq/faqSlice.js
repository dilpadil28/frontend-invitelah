import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import faqService from "../../services/faq.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  faq: null,
  faqList: [],
};

export const getFaq = createAsyncThunk("faq/getFaq", async (_, thunkAPI) => {
  const data = await faqService.getFaq().then(
    (response) => {
      return response.data.data;
    },
    (error) => {
      if (
        (error.response && error.response.status === 403) ||
        error.response.status === 500
      ) {
        eventBus.dispatch("logout");
      }
    }
  );
  return data;
});
export const getFaqById = createAsyncThunk(
  "faq/getFaqById",
  async (id, thunkAPI) => {
    const data = await faqService.getFaqById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createFaq = createAsyncThunk(
  "faq/createFaq",
  async (result, thunkAPI) => {
    const data = await faqService.createFaq(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFaq());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateFaq = createAsyncThunk(
  "faq/updateFaq",
  async ({ id, result }, thunkAPI) => {
    const data = await faqService.updateFaq(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFaq());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteFaq = createAsyncThunk(
  "faq/deleteFaq",
  async (id, thunkAPI) => {
    const data = await faqService.deleteFaq(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFaq());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getFaqList = createAsyncThunk(
  "faqList/getFaqList",
  async (_, thunkAPI) => {
    const data = await faqService.getFaqList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getFaqListById = createAsyncThunk(
  "faqList/getFaqListById",
  async (id, thunkAPI) => {
    const data = await faqService.getFaqListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createFaqList = createAsyncThunk(
  "faqList/createFaqList",
  async (result, thunkAPI) => {
    const data = await faqService.createFaqList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFaqList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateFaqList = createAsyncThunk(
  "faqList/updateFaqList",
  async ({ id, result }, thunkAPI) => {
    const data = await faqService.updateFaqList(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFaqList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteFaqList = createAsyncThunk(
  "faqList/deleteFaqList",
  async (id, thunkAPI) => {
    const data = await faqService.deleteFaqList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getFaqList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const faqSlice = createSlice({
  name: "faq",
  initialState,
  extraReducers: {
    [getFaq.fulfilled]: (state, action) => {
      state.faq = action.payload;
    },
    [getFaq.rejected]: (state, action) => {
      state.faq = null;
    },
    [getFaqById.fulfilled]: (state, action) => {
      // state.faq = action.payload;
    },
    [getFaqById.rejected]: (state, action) => {
      // state.faq = null;
    },
    [createFaq.fulfilled]: (state, action) => {
      // state.faq = action.payload.faq;
    },
    [createFaq.rejected]: (state, action) => {
      // state.faq = null;
    },
    [updateFaq.fulfilled]: (state, action) => {
      // state.faqUpdate = action.payload;
    },
    [updateFaq.rejected]: (state, action) => {
      // state.faq = null;
    },
    [deleteFaq.fulfilled]: (state, action) => {
      // state.faq = action.payload.faq;
    },
    [deleteFaq.rejected]: (state, action) => {
      // state.faq = null;
    },
    [getFaqList.fulfilled]: (state, action) => {
      state.faqList = action.payload;
    },
    [getFaqList.rejected]: (state, action) => {
      state.faqList = null;
    },
    [getFaqListById.fulfilled]: (state, action) => {
      // state.faqList = action.payload;
    },
    [getFaqListById.rejected]: (state, action) => {
      // state.faqList = null;
    },
    [createFaqList.fulfilled]: (state, action) => {
      // state.faqList = action.payload.faqList;
    },
    [createFaqList.rejected]: (state, action) => {
      // state.faqList = null;
    },
    [updateFaqList.fulfilled]: (state, action) => {
      // state.faqListUpdate = action.payload;
    },
    [updateFaqList.rejected]: (state, action) => {
      // state.faqList = null;
    },
    [deleteFaqList.fulfilled]: (state, action) => {
      // state.faqList = action.payload.faqList;
    },
    [deleteFaqList.rejected]: (state, action) => {
      // state.faqList = null;
    },
  },
});

const { reducer } = faqSlice;
export default reducer;
