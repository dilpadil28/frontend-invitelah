import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import musicService from "../../services/music.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  music: null,
};

export const getMusic = createAsyncThunk(
  "music/getMusic",
  async (_, thunkAPI) => {
    const data = await musicService.getMusic().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMusicById = createAsyncThunk(
  "music/getMusicById",
  async (id, thunkAPI) => {
    const data = await musicService.getMusicById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMusic = createAsyncThunk(
  "music/createMusic",
  async (result, thunkAPI) => {
    const data = await musicService.createMusic(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMusic());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMusic = createAsyncThunk(
  "music/updateMusic",
  async ({ id, result }, thunkAPI) => {


    const data = await musicService.updateMusic(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMusic());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteMusic = createAsyncThunk(
  "music/deleteMusic",
  async (id, thunkAPI) => {
    const data = await musicService.deleteMusic(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMusic());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const musicSlice = createSlice({
  name: "music",
  initialState,
  extraReducers: {
    [getMusic.fulfilled]: (state, action) => {
      state.music = action.payload;
    },
    [getMusic.rejected]: (state, action) => {
      state.music = null;
    },
    [getMusicById.fulfilled]: (state, action) => {
      // state.music = action.payload;
    },
    [getMusicById.rejected]: (state, action) => {
      // state.music = null;
    },
    [createMusic.fulfilled]: (state, action) => {
      // state.music = action.payload.music;
    },
    [createMusic.rejected]: (state, action) => {
      // state.music = null;
    },
    [updateMusic.fulfilled]: (state, action) => {
      // state.musicUpdate = action.payload;
    },
    [updateMusic.rejected]: (state, action) => {
      // state.music = null;
    },
    [deleteMusic.fulfilled]: (state, action) => {
      // state.music = action.payload.music;
    },
    [deleteMusic.rejected]: (state, action) => {
      // state.music = null;
    },
  },
});

const { reducer } = musicSlice;
export default reducer;
