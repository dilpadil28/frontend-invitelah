import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import prokesService from "../../services/prokes.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  prokes: null,
  prokesList: [],
};

export const getProkes = createAsyncThunk(
  "prokes/getProkes",
  async (_, thunkAPI) => {
    const data = await prokesService.getProkes().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getProkesById = createAsyncThunk(
  "prokes/getProkesById",
  async (id, thunkAPI) => {
    const data = await prokesService.getProkesById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createProkes = createAsyncThunk(
  "prokes/createProkes",
  async (result, thunkAPI) => {
    const data = await prokesService.createProkes(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getProkes());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateProkes = createAsyncThunk(
  "prokes/updateProkes",
  async ({ id, result }, thunkAPI) => {
    const data = await prokesService.updateProkes(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getProkes());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteProkes = createAsyncThunk(
  "prokes/deleteProkes",
  async (id, thunkAPI) => {
    const data = await prokesService.deleteProkes(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getProkes());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getProkesList = createAsyncThunk(
  "prokesList/getProkesList",
  async (_, thunkAPI) => {
    const data = await prokesService.getProkesList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getProkesListById = createAsyncThunk(
  "prokesList/getProkesListById",
  async (id, thunkAPI) => {
    const data = await prokesService.getProkesListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createProkesList = createAsyncThunk(
  "prokesList/createProkesList",
  async (result, thunkAPI) => {
    const data = await prokesService.createProkesList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getProkesList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateProkesList = createAsyncThunk(
  "prokesList/updateProkesList",
  async ({ id, result }, thunkAPI) => {
    const data = await prokesService.updateProkesList(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getProkesList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteProkesList = createAsyncThunk(
  "prokesList/deleteProkesList",
  async (id, thunkAPI) => {
    const data = await prokesService.deleteProkesList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getProkesList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const prokesSlice = createSlice({
  name: "prokes",
  initialState,
  extraReducers: {
    [getProkes.fulfilled]: (state, action) => {
      state.prokes = action.payload;
    },
    [getProkes.rejected]: (state, action) => {
      state.prokes = null;
    },
    [getProkesById.fulfilled]: (state, action) => {
      // state.prokes = action.payload;
    },
    [getProkesById.rejected]: (state, action) => {
      // state.prokes = null;
    },
    [createProkes.fulfilled]: (state, action) => {
      // state.prokes = action.payload.prokes;
    },
    [createProkes.rejected]: (state, action) => {
      // state.prokes = null;
    },
    [updateProkes.fulfilled]: (state, action) => {
      // state.prokesUpdate = action.payload;
    },
    [updateProkes.rejected]: (state, action) => {
      // state.prokes = null;
    },
    [deleteProkes.fulfilled]: (state, action) => {
      // state.prokes = action.payload.prokes;
    },
    [deleteProkes.rejected]: (state, action) => {
      // state.prokes = null;
    },
    [getProkesList.fulfilled]: (state, action) => {
      state.prokesList = action.payload;
    },
    [getProkesList.rejected]: (state, action) => {
      state.prokesList = null;
    },
    [getProkesListById.fulfilled]: (state, action) => {
      // state.prokesList = action.payload;
    },
    [getProkesListById.rejected]: (state, action) => {
      // state.prokesList = null;
    },
    [createProkesList.fulfilled]: (state, action) => {
      // state.prokesList = action.payload.prokesList;
    },
    [createProkesList.rejected]: (state, action) => {
      // state.prokesList = null;
    },
    [updateProkesList.fulfilled]: (state, action) => {
      // state.prokesListUpdate = action.payload;
    },
    [updateProkesList.rejected]: (state, action) => {
      // state.prokesList = null;
    },
    [deleteProkesList.fulfilled]: (state, action) => {
      // state.prokesList = action.payload.prokesList;
    },
    [deleteProkesList.rejected]: (state, action) => {
      // state.prokesList = null;
    },
  },
});

const { reducer } = prokesSlice;
export default reducer;
