import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import mainLandingService from "../../services/mainLanding.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  mainLanding: null,
  mainLandingList: [],
};

export const getMainLanding = createAsyncThunk(
  "mainLanding/getMainLanding",
  async (_, thunkAPI) => {
    const data = await mainLandingService.getMainLanding().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMainLandingById = createAsyncThunk(
  "mainLanding/getMainLandingById",
  async (id, thunkAPI) => {
    const data = await mainLandingService.getMainLandingById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMainLanding = createAsyncThunk(
  "mainLanding/createMainLanding",
  async (result, thunkAPI) => {
    const data = await mainLandingService.createMainLanding(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMainLanding());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMainLanding = createAsyncThunk(
  "mainLanding/updateMainLanding",
  async ({ id, result }, thunkAPI) => {
    const data = await mainLandingService.updateMainLanding(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMainLanding());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteMainLanding = createAsyncThunk(
  "mainLanding/deleteMainLanding",
  async (id, thunkAPI) => {
    const data = await mainLandingService.deleteMainLanding(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMainLanding());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getMainLandingList = createAsyncThunk(
  "mainLandingList/getMainLandingList",
  async (_, thunkAPI) => {
    const data = await mainLandingService.getMainLandingList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMainLandingListById = createAsyncThunk(
  "mainLandingList/getMainLandingListById",
  async (id, thunkAPI) => {
    const data = await mainLandingService.getMainLandingListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMainLandingList = createAsyncThunk(
  "mainLandingList/createMainLandingList",
  async (result, thunkAPI) => {
    const data = await mainLandingService.createMainLandingList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMainLandingList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMainLandingList = createAsyncThunk(
  "mainLandingList/updateMainLandingList",
  async ({ id, result }, thunkAPI) => {
    const data = await mainLandingService
      .updateMainLandingList(id, result)
      .then(
        (response) => {
          thunkAPI.dispatch(setMessage(response.message));
          thunkAPI.dispatch(getMainLandingList());
          return response.data.data;
        },
        (error) => {
          const message =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          thunkAPI.dispatch(
            setMessage(message + " " + error.response.data.error[0].msg)
          );
          if (error.response && error.response.status === 403) {
            eventBus.dispatch("logout");
          }
        }
      );
    return data;
  }
);
export const deleteMainLandingList = createAsyncThunk(
  "mainLandingList/deleteMainLandingList",
  async (id, thunkAPI) => {
    const data = await mainLandingService.deleteMainLandingList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMainLandingList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const mainLandingSlice = createSlice({
  name: "mainLanding",
  initialState,
  extraReducers: {
    [getMainLanding.fulfilled]: (state, action) => {
      state.mainLanding = action.payload;
    },
    [getMainLanding.rejected]: (state, action) => {
      state.mainLanding = null;
    },
    [getMainLandingById.fulfilled]: (state, action) => {
      // state.mainLanding = action.payload;
    },
    [getMainLandingById.rejected]: (state, action) => {
      // state.mainLanding = null;
    },
    [createMainLanding.fulfilled]: (state, action) => {
      // state.mainLanding = action.payload.mainLanding;
    },
    [createMainLanding.rejected]: (state, action) => {
      // state.mainLanding = null;
    },
    [updateMainLanding.fulfilled]: (state, action) => {
      // state.mainLandingUpdate = action.payload;
    },
    [updateMainLanding.rejected]: (state, action) => {
      // state.mainLanding = null;
    },
    [deleteMainLanding.fulfilled]: (state, action) => {
      // state.mainLanding = action.payload.mainLanding;
    },
    [deleteMainLanding.rejected]: (state, action) => {
      // state.mainLanding = null;
    },
    [getMainLandingList.fulfilled]: (state, action) => {
      state.mainLandingList = action.payload;
    },
    [getMainLandingList.rejected]: (state, action) => {
      state.mainLandingList = null;
    },
    [getMainLandingListById.fulfilled]: (state, action) => {
      // state.mainLandingList = action.payload;
    },
    [getMainLandingListById.rejected]: (state, action) => {
      // state.mainLandingList = null;
    },
    [createMainLandingList.fulfilled]: (state, action) => {
      // state.mainLandingList = action.payload.mainLandingList;
    },
    [createMainLandingList.rejected]: (state, action) => {
      // state.mainLandingList = null;
    },
    [updateMainLandingList.fulfilled]: (state, action) => {
      // state.mainLandingListUpdate = action.payload;
    },
    [updateMainLandingList.rejected]: (state, action) => {
      // state.mainLandingList = null;
    },
    [deleteMainLandingList.fulfilled]: (state, action) => {
      // state.mainLandingList = action.payload.mainLandingList;
    },
    [deleteMainLandingList.rejected]: (state, action) => {
      // state.mainLandingList = null;
    },
  },
});

const { reducer } = mainLandingSlice;
export default reducer;
