import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import presenceService from "../../services/presence.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  presence: null,
};

export const getPresence = createAsyncThunk(
  "presence/getPresence",
  async (_, thunkAPI) => {
    const data = await presenceService.getPresence().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getPresenceById = createAsyncThunk(
  "presence/getPresenceById",
  async (id, thunkAPI) => {
    const data = await presenceService.getPresenceById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getPresenceByInvitationId = createAsyncThunk(
  "presence/getPresenceByInvitationId",
  async (id, thunkAPI) => {
    const data = await presenceService.getPresenceByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createPresence = createAsyncThunk(
  "presence/createPresence",
  async (result, thunkAPI) => {
    const data = await presenceService.createPresence(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updatePresence = createAsyncThunk(
  "presence/updatePresence",
  async ({ id, result }, thunkAPI) => {


    const data = await presenceService.updatePresence(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deletePresence = createAsyncThunk(
  "presence/deletePresence",
  async (id, thunkAPI) => {
    const data = await presenceService.deletePresence(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const presenceSlice = createSlice({
  name: "presence",
  initialState,
  extraReducers: {
    [getPresence.fulfilled]: (state, action) => {
      // state.presence = action.payload;
    },
    [getPresence.rejected]: (state, action) => {
      // state.presence = null;
    },
    [getPresenceByInvitationId.fulfilled]: (state, action) => {
      state.presence = action.payload;
    },
    [getPresenceByInvitationId.rejected]: (state, action) => {
      state.presence = null;
    },
    [getPresenceById.fulfilled]: (state, action) => {
      // state.presence = action.payload;
    },
    [getPresenceById.rejected]: (state, action) => {
      // state.presence = null;
    },
    [createPresence.fulfilled]: (state, action) => {
      // state.presence = action.payload.presence;
    },
    [createPresence.rejected]: (state, action) => {
      // state.presence = null;
    },
    [updatePresence.fulfilled]: (state, action) => {
      // state.presenceUpdate = action.payload;
    },
    [updatePresence.rejected]: (state, action) => {
      // state.presence = null;
    },
    [deletePresence.fulfilled]: (state, action) => {
      // state.presence = action.payload.presence;
    },
    [deletePresence.rejected]: (state, action) => {
      // state.presence = null;
    },
  },
});

const { reducer } = presenceSlice;
export default reducer;
