import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import orderStepService from "../../services/orderStep.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  orderStep: null,
  orderStepList: [],
};

export const getOrderStep = createAsyncThunk(
  "orderStep/getOrderStep",
  async (_, thunkAPI) => {
    const data = await orderStepService.getOrderStep().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getOrderStepById = createAsyncThunk(
  "orderStep/getOrderStepById",
  async (id, thunkAPI) => {
    const data = await orderStepService.getOrderStepById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createOrderStep = createAsyncThunk(
  "orderStep/createOrderStep",
  async (result, thunkAPI) => {
    const data = await orderStepService.createOrderStep(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getOrderStep());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateOrderStep = createAsyncThunk(
  "orderStep/updateOrderStep",
  async ({ id, result }, thunkAPI) => {
    const data = await orderStepService.updateOrderStep(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getOrderStep());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteOrderStep = createAsyncThunk(
  "orderStep/deleteOrderStep",
  async (id, thunkAPI) => {
    const data = await orderStepService.deleteOrderStep(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getOrderStep());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getOrderStepList = createAsyncThunk(
  "orderStepList/getOrderStepList",
  async (_, thunkAPI) => {
    const data = await orderStepService.getOrderStepList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getOrderStepListById = createAsyncThunk(
  "orderStepList/getOrderStepListById",
  async (id, thunkAPI) => {
    const data = await orderStepService.getOrderStepListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createOrderStepList = createAsyncThunk(
  "orderStepList/createOrderStepList",
  async (result, thunkAPI) => {
    const data = await orderStepService.createOrderStepList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getOrderStepList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateOrderStepList = createAsyncThunk(
  "orderStepList/updateOrderStepList",
  async ({ id, result }, thunkAPI) => {
    const data = await orderStepService.updateOrderStepList(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getOrderStepList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteOrderStepList = createAsyncThunk(
  "orderStepList/deleteOrderStepList",
  async (id, thunkAPI) => {
    const data = await orderStepService.deleteOrderStepList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getOrderStepList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const orderStepSlice = createSlice({
  name: "orderStep",
  initialState,
  extraReducers: {
    [getOrderStep.fulfilled]: (state, action) => {
      state.orderStep = action.payload;
    },
    [getOrderStep.rejected]: (state, action) => {
      state.orderStep = null;
    },
    [getOrderStepById.fulfilled]: (state, action) => {
      // state.orderStep = action.payload;
    },
    [getOrderStepById.rejected]: (state, action) => {
      // state.orderStep = null;
    },
    [createOrderStep.fulfilled]: (state, action) => {
      // state.orderStep = action.payload.orderStep;
    },
    [createOrderStep.rejected]: (state, action) => {
      // state.orderStep = null;
    },
    [updateOrderStep.fulfilled]: (state, action) => {
      // state.orderStepUpdate = action.payload;
    },
    [updateOrderStep.rejected]: (state, action) => {
      // state.orderStep = null;
    },
    [deleteOrderStep.fulfilled]: (state, action) => {
      // state.orderStep = action.payload.orderStep;
    },
    [deleteOrderStep.rejected]: (state, action) => {
      // state.orderStep = null;
    },
    [getOrderStepList.fulfilled]: (state, action) => {
      state.orderStepList = action.payload;
    },
    [getOrderStepList.rejected]: (state, action) => {
      state.orderStepList = null;
    },
    [getOrderStepListById.fulfilled]: (state, action) => {
      // state.orderStepList = action.payload;
    },
    [getOrderStepListById.rejected]: (state, action) => {
      // state.orderStepList = null;
    },
    [createOrderStepList.fulfilled]: (state, action) => {
      // state.orderStepList = action.payload.orderStepList;
    },
    [createOrderStepList.rejected]: (state, action) => {
      // state.orderStepList = null;
    },
    [updateOrderStepList.fulfilled]: (state, action) => {
      // state.orderStepListUpdate = action.payload;
    },
    [updateOrderStepList.rejected]: (state, action) => {
      // state.orderStepList = null;
    },
    [deleteOrderStepList.fulfilled]: (state, action) => {
      // state.orderStepList = action.payload.orderStepList;
    },
    [deleteOrderStepList.rejected]: (state, action) => {
      // state.orderStepList = null;
    },
  },
});

const { reducer } = orderStepSlice;
export default reducer;
