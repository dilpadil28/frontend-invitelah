import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import testimonialService from "../../services/testimonial.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  testimonial: null,
};

export const getTestimonial = createAsyncThunk(
  "testimonial/getTestimonial",
  async (_, thunkAPI) => {
    const data = await testimonialService.getTestimonial().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getTestimonialById = createAsyncThunk(
  "testimonial/getTestimonialById",
  async (id, thunkAPI) => {
    const data = await testimonialService.getTestimonialById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createTestimonial = createAsyncThunk(
  "testimonial/createTestimonial",
  async (result, thunkAPI) => {
    const data = await testimonialService.createTestimonial(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getTestimonial());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateTestimonial = createAsyncThunk(
  "testimonial/updateTestimonial",
  async ({ id, result }, thunkAPI) => {


    const data = await testimonialService.updateTestimonial(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getTestimonial());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteTestimonial = createAsyncThunk(
  "testimonial/deleteTestimonial",
  async (id, thunkAPI) => {
    const data = await testimonialService.deleteTestimonial(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getTestimonial());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const testimonialSlice = createSlice({
  name: "testimonial",
  initialState,
  extraReducers: {
    [getTestimonial.fulfilled]: (state, action) => {
      state.testimonial = action.payload;
    },
    [getTestimonial.rejected]: (state, action) => {
      state.testimonial = null;
    },
    [getTestimonialById.fulfilled]: (state, action) => {
      // state.testimonial = action.payload;
    },
    [getTestimonialById.rejected]: (state, action) => {
      // state.testimonial = null;
    },
    [createTestimonial.fulfilled]: (state, action) => {
      // state.testimonial = action.payload.testimonial;
    },
    [createTestimonial.rejected]: (state, action) => {
      // state.testimonial = null;
    },
    [updateTestimonial.fulfilled]: (state, action) => {
      // state.testimonialUpdate = action.payload;
    },
    [updateTestimonial.rejected]: (state, action) => {
      // state.testimonial = null;
    },
    [deleteTestimonial.fulfilled]: (state, action) => {
      // state.testimonial = action.payload.testimonial;
    },
    [deleteTestimonial.rejected]: (state, action) => {
      // state.testimonial = null;
    },
  },
});

const { reducer } = testimonialSlice;
export default reducer;
