import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import mySocialMediaService from "../../services/mySocialMedia.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  mySocialMedia: null,
};

export const getMySocialMedia = createAsyncThunk(
  "mySocialMedia/getMySocialMedia",
  async (_, thunkAPI) => {
    const data = await mySocialMediaService.getMySocialMedia().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getMySocialMediaById = createAsyncThunk(
  "mySocialMedia/getMySocialMediaById",
  async (id, thunkAPI) => {
    const data = await mySocialMediaService.getMySocialMediaById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createMySocialMedia = createAsyncThunk(
  "mySocialMedia/createMySocialMedia",
  async (result, thunkAPI) => {
    const data = await mySocialMediaService.createMySocialMedia(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMySocialMedia());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateMySocialMedia = createAsyncThunk(
  "mySocialMedia/updateMySocialMedia",
  async ({ id, result }, thunkAPI) => {


    const data = await mySocialMediaService
      .updateMySocialMedia(id, result)
      .then(
        (response) => {
          thunkAPI.dispatch(setMessage(response.message));
          thunkAPI.dispatch(getMySocialMedia());
          return response.data.data;
        },
        (error) => {
          const message =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          thunkAPI.dispatch(
            setMessage(message + " " + error.response.data.error[0].msg)
          );
          if (error.response && error.response.status === 403) {
            eventBus.dispatch("logout");
          }
        }
      );
    return data;
  }
);
export const deleteMySocialMedia = createAsyncThunk(
  "mySocialMedia/deleteMySocialMedia",
  async (id, thunkAPI) => {
    const data = await mySocialMediaService.deleteMySocialMedia(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getMySocialMedia());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const mySocialMediaSlice = createSlice({
  name: "mySocialMedia",
  initialState,
  extraReducers: {
    [getMySocialMedia.fulfilled]: (state, action) => {
      state.mySocialMedia = action.payload;
    },
    [getMySocialMedia.rejected]: (state, action) => {
      state.mySocialMedia = null;
    },
    [getMySocialMediaById.fulfilled]: (state, action) => {
      // state.mySocialMedia = action.payload;
    },
    [getMySocialMediaById.rejected]: (state, action) => {
      // state.mySocialMedia = null;
    },
    [createMySocialMedia.fulfilled]: (state, action) => {
      // state.mySocialMedia = action.payload.mySocialMedia;
    },
    [createMySocialMedia.rejected]: (state, action) => {
      // state.mySocialMedia = null;
    },
    [updateMySocialMedia.fulfilled]: (state, action) => {
      // state.mySocialMediaUpdate = action.payload;
    },
    [updateMySocialMedia.rejected]: (state, action) => {
      // state.mySocialMedia = null;
    },
    [deleteMySocialMedia.fulfilled]: (state, action) => {
      // state.mySocialMedia = action.payload.mySocialMedia;
    },
    [deleteMySocialMedia.rejected]: (state, action) => {
      // state.mySocialMedia = null;
    },
  },
});

const { reducer } = mySocialMediaSlice;
export default reducer;
