import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import socialMediaService from "../../services/socialMedia.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  socialMedia: null,
};

export const getSocialMedia = createAsyncThunk(
  "socialMedia/getSocialMedia",
  async (_, thunkAPI) => {
    const data = await socialMediaService.getSocialMedia().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getSocialMediaById = createAsyncThunk(
  "socialMedia/getSocialMediaById",
  async (id, thunkAPI) => {
    const data = await socialMediaService.getSocialMediaById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getSocialMediaByInvitationId = createAsyncThunk(
  "socialMedia/getSocialMediaByInvitationId",
  async (id, thunkAPI) => {
    const data = await socialMediaService.getSocialMediaByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createSocialMedia = createAsyncThunk(
  "socialMedia/createSocialMedia",
  async (result, thunkAPI) => {
    const data = await socialMediaService.createSocialMedia(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateSocialMedia = createAsyncThunk(
  "socialMedia/updateSocialMedia",
  async ({ id, result }, thunkAPI) => {


    const data = await socialMediaService.updateSocialMedia(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteSocialMedia = createAsyncThunk(
  "socialMedia/deleteSocialMedia",
  async (id, thunkAPI) => {
    const data = await socialMediaService.deleteSocialMedia(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const socialMediaSlice = createSlice({
  name: "socialMedia",
  initialState,
  extraReducers: {
    [getSocialMedia.fulfilled]: (state, action) => {
      // state.socialMedia = action.payload;
    },
    [getSocialMedia.rejected]: (state, action) => {
      // state.socialMedia = null;
    },
    [getSocialMediaByInvitationId.fulfilled]: (state, action) => {
      state.socialMedia = action.payload;
    },
    [getSocialMediaByInvitationId.rejected]: (state, action) => {
      state.socialMedia = null;
    },
    [getSocialMediaById.fulfilled]: (state, action) => {
      // state.socialMedia = action.payload;
    },
    [getSocialMediaById.rejected]: (state, action) => {
      // state.socialMedia = null;
    },
    [createSocialMedia.fulfilled]: (state, action) => {
      // state.socialMedia = action.payload.socialMedia;
    },
    [createSocialMedia.rejected]: (state, action) => {
      // state.socialMedia = null;
    },
    [updateSocialMedia.fulfilled]: (state, action) => {
      // state.socialMediaUpdate = action.payload;
    },
    [updateSocialMedia.rejected]: (state, action) => {
      // state.socialMedia = null;
    },
    [deleteSocialMedia.fulfilled]: (state, action) => {
      // state.socialMedia = action.payload.socialMedia;
    },
    [deleteSocialMedia.rejected]: (state, action) => {
      // state.socialMedia = null;
    },
  },
});

const { reducer } = socialMediaSlice;
export default reducer;
