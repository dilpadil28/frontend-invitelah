import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  invitationSlug: null,
};

const invitationSlugSlice = createSlice({
  name: "invitationSlug",
  initialState,
  reducers: {
    slugReducer: (state, action) => {
      state.invitationSlug = action.payload
    }
  },
  extraReducers: {
  },
});

const { reducer, actions } = invitationSlugSlice;
export const { slugReducer } = actions
export default reducer;
