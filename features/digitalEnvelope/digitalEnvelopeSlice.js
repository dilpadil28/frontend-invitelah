import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import digitalEnvelopeService from "../../services/digitalEnvelope.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  digitalEnvelope: null,
};

export const getDigitalEnvelope = createAsyncThunk(
  "digitalEnvelope/getDigitalEnvelope",
  async (_, thunkAPI) => {
    const data = await digitalEnvelopeService.getDigitalEnvelope().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getDigitalEnvelopeById = createAsyncThunk(
  "digitalEnvelope/getDigitalEnvelopeById",
  async (id, thunkAPI) => {
    const data = await digitalEnvelopeService.getDigitalEnvelopeById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getDigitalEnvelopeByInvitationId = createAsyncThunk(
  "digitalEnvelope/getDigitalEnvelopeByInvitationId",
  async (id, thunkAPI) => {
    const data = await digitalEnvelopeService.getDigitalEnvelopeByInvitationId(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createDigitalEnvelope = createAsyncThunk(
  "digitalEnvelope/createDigitalEnvelope",
  async (result, thunkAPI) => {
    const data = await digitalEnvelopeService.createDigitalEnvelope(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateDigitalEnvelope = createAsyncThunk(
  "digitalEnvelope/updateDigitalEnvelope",
  async ({ id, result }, thunkAPI) => {


    const data = await digitalEnvelopeService.updateDigitalEnvelope(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteDigitalEnvelope = createAsyncThunk(
  "digitalEnvelope/deleteDigitalEnvelope",
  async (id, thunkAPI) => {
    const data = await digitalEnvelopeService.deleteDigitalEnvelope(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const digitalEnvelopeSlice = createSlice({
  name: "digitalEnvelope",
  initialState,
  extraReducers: {
    [getDigitalEnvelope.fulfilled]: (state, action) => {
      // state.digitalEnvelope = action.payload;
    },
    [getDigitalEnvelope.rejected]: (state, action) => {
      // state.digitalEnvelope = null;
    },
    [getDigitalEnvelopeByInvitationId.fulfilled]: (state, action) => {
      state.digitalEnvelope = action.payload;
    },
    [getDigitalEnvelopeByInvitationId.rejected]: (state, action) => {
      state.digitalEnvelope = null;
    },
    [getDigitalEnvelopeById.fulfilled]: (state, action) => {
      // state.digitalEnvelope = action.payload;
    },
    [getDigitalEnvelopeById.rejected]: (state, action) => {
      // state.digitalEnvelope = null;
    },
    [createDigitalEnvelope.fulfilled]: (state, action) => {
      // state.digitalEnvelope = action.payload.digitalEnvelope;
    },
    [createDigitalEnvelope.rejected]: (state, action) => {
      // state.digitalEnvelope = null;
    },
    [updateDigitalEnvelope.fulfilled]: (state, action) => {
      // state.digitalEnvelopeUpdate = action.payload;
    },
    [updateDigitalEnvelope.rejected]: (state, action) => {
      // state.digitalEnvelope = null;
    },
    [deleteDigitalEnvelope.fulfilled]: (state, action) => {
      // state.digitalEnvelope = action.payload.digitalEnvelope;
    },
    [deleteDigitalEnvelope.rejected]: (state, action) => {
      // state.digitalEnvelope = null;
    },
  },
});

const { reducer } = digitalEnvelopeSlice;
export default reducer;
