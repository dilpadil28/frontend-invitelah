import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import eventBus from "../../common/EventBus";
import superiorityService from "../../services/superiority.service";
import { setMessage } from "../message/messageSlice";

const initialState = {
  superiority: null,
  superiorityList: [],
};

export const getSuperiority = createAsyncThunk(
  "superiority/getSuperiority",
  async (_, thunkAPI) => {
    const data = await superiorityService.getSuperiority().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getSuperiorityById = createAsyncThunk(
  "superiority/getSuperiorityById",
  async (id, thunkAPI) => {
    const data = await superiorityService.getSuperiorityById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createSuperiority = createAsyncThunk(
  "superiority/createSuperiority",
  async (result, thunkAPI) => {
    const data = await superiorityService.createSuperiority(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getSuperiority());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateSuperiority = createAsyncThunk(
  "superiority/updateSuperiority",
  async ({ id, result }, thunkAPI) => {
    const data = await superiorityService.updateSuperiority(id, result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getSuperiority());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const deleteSuperiority = createAsyncThunk(
  "superiority/deleteSuperiority",
  async (id, thunkAPI) => {
    const data = await superiorityService.deleteSuperiority(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getSuperiority());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

export const getSuperiorityList = createAsyncThunk(
  "superiorityList/getSuperiorityList",
  async (_, thunkAPI) => {
    const data = await superiorityService.getSuperiorityList().then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const getSuperiorityListById = createAsyncThunk(
  "superiorityList/getSuperiorityListById",
  async (id, thunkAPI) => {
    const data = await superiorityService.getSuperiorityListById(id).then(
      (response) => {
        return response.data.data;
      },
      (error) => {
        if (
          (error.response && error.response.status === 403) ||
          error.response.status === 500
        ) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const createSuperiorityList = createAsyncThunk(
  "superiorityList/createSuperiorityList",
  async (result, thunkAPI) => {
    const data = await superiorityService.createSuperiorityList(result).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getSuperiorityList());
        return response.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(
          setMessage(message + " " + error.response.data.error[0].msg)
        );
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);
export const updateSuperiorityList = createAsyncThunk(
  "superiorityList/updateSuperiorityList",
  async ({ id, result }, thunkAPI) => {
    const data = await superiorityService
      .updateSuperiorityList(id, result)
      .then(
        (response) => {
          thunkAPI.dispatch(setMessage(response.message));
          thunkAPI.dispatch(getSuperiorityList());
          return response.data.data;
        },
        (error) => {
          const message =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          thunkAPI.dispatch(
            setMessage(message + " " + error.response.data.error[0].msg)
          );
          if (error.response && error.response.status === 403) {
            eventBus.dispatch("logout");
          }
        }
      );
    return data;
  }
);
export const deleteSuperiorityList = createAsyncThunk(
  "superiorityList/deleteSuperiorityList",
  async (id, thunkAPI) => {
    const data = await superiorityService.deleteSuperiorityList(id).then(
      (response) => {
        thunkAPI.dispatch(setMessage(response.message));
        thunkAPI.dispatch(getSuperiorityList());
        return response.data.data;
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        thunkAPI.dispatch(setMessage(message));
        if (error.response && error.response.status === 403) {
          eventBus.dispatch("logout");
        }
      }
    );
    return data;
  }
);

const superioritySlice = createSlice({
  name: "superiority",
  initialState,
  extraReducers: {
    [getSuperiority.fulfilled]: (state, action) => {
      state.superiority = action.payload;
    },
    [getSuperiority.rejected]: (state, action) => {
      state.superiority = null;
    },
    [getSuperiorityById.fulfilled]: (state, action) => {
      // state.superiority = action.payload;
    },
    [getSuperiorityById.rejected]: (state, action) => {
      // state.superiority = null;
    },
    [createSuperiority.fulfilled]: (state, action) => {
      // state.superiority = action.payload.superiority;
    },
    [createSuperiority.rejected]: (state, action) => {
      // state.superiority = null;
    },
    [updateSuperiority.fulfilled]: (state, action) => {
      // state.superiorityUpdate = action.payload;
    },
    [updateSuperiority.rejected]: (state, action) => {
      // state.superiority = null;
    },
    [deleteSuperiority.fulfilled]: (state, action) => {
      // state.superiority = action.payload.superiority;
    },
    [deleteSuperiority.rejected]: (state, action) => {
      // state.superiority = null;
    },
    [getSuperiorityList.fulfilled]: (state, action) => {
      state.superiorityList = action.payload;
    },
    [getSuperiorityList.rejected]: (state, action) => {
      state.superiorityList = null;
    },
    [getSuperiorityListById.fulfilled]: (state, action) => {
      // state.superiorityList = action.payload;
    },
    [getSuperiorityListById.rejected]: (state, action) => {
      // state.superiorityList = null;
    },
    [createSuperiorityList.fulfilled]: (state, action) => {
      // state.superiorityList = action.payload.superiorityList;
    },
    [createSuperiorityList.rejected]: (state, action) => {
      // state.superiorityList = null;
    },
    [updateSuperiorityList.fulfilled]: (state, action) => {
      // state.superiorityListUpdate = action.payload;
    },
    [updateSuperiorityList.rejected]: (state, action) => {
      // state.superiorityList = null;
    },
    [deleteSuperiorityList.fulfilled]: (state, action) => {
      // state.superiorityList = action.payload.superiorityList;
    },
    [deleteSuperiorityList.rejected]: (state, action) => {
      // state.superiorityList = null;
    },
  },
});

const { reducer } = superioritySlice;
export default reducer;
