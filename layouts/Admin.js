import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import MailIcon from "@mui/icons-material/Mail";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MoreIcon from "@mui/icons-material/MoreVert";
import DashboardIcon from "@mui/icons-material/Dashboard";
import AnalyticsIcon from "@mui/icons-material/Analytics";
import { Collapse, ListItemButton, ListSubheader } from "@mui/material";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import StarBorder from "@mui/icons-material/StarBorder";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import EngineeringIcon from "@mui/icons-material/Engineering";
import { logout } from "../features/auth/authSlice";
import { useDispatch, useSelector } from "react-redux";
import eventBus from "../common/EventBus";
import Link from "next/link";
import { useRouter } from "next/router";
import GroupIcon from "@mui/icons-material/Group";
import SettingsIcon from "@mui/icons-material/Settings";
import ConnectWithoutContactIcon from "@mui/icons-material/ConnectWithoutContact";
import ColorLensIcon from "@mui/icons-material/ColorLens";
import QuizIcon from "@mui/icons-material/Quiz";
import HomeIcon from "@mui/icons-material/Home";
import AutoFixHighIcon from "@mui/icons-material/AutoFixHigh";
import LibraryMusicIcon from "@mui/icons-material/LibraryMusic";
import TimelineIcon from "@mui/icons-material/Timeline";
import HealthAndSafetyIcon from "@mui/icons-material/HealthAndSafety";
import AutoAwesomeIcon from "@mui/icons-material/AutoAwesome";
import PaymentsIcon from "@mui/icons-material/Payments";
import EmailIcon from "@mui/icons-material/Email";
import Loading from "../components/Loading";

const drawerWidth = 240;

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

function ResponsiveDrawer(props) {
  const { window, children } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const [open, setOpen] = React.useState(false);
  const [openConfig, setOpenConfig] = React.useState(false);

  const [showModeratorBoard, setShowModeratorBoard] = React.useState(false);
  const [showAdminBoard, setShowAdminBoard] = React.useState(false);

  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const router = useRouter();

  const logOut = React.useCallback(() => {
    dispatch(logout());
  }, [dispatch]);

  React.useEffect(() => {
    if (currentUser) {
      setShowModeratorBoard(currentUser.roles.includes("ROLE_MODERATOR"));
      setShowAdminBoard(currentUser.roles.includes("ROLE_ADMIN"));
    } else {
      setShowModeratorBoard(false);
      setShowAdminBoard(false);
    }

    eventBus.on("logout", () => {
      logOut();
    });

    return () => {
      eventBus.remove("logout");
    };
  }, [currentUser, logOut]);

  const handleClick = () => {
    setOpen(!open);
  };
  const handleClickConfig = () => {
    setOpenConfig(!openConfig);
  };

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      <MenuItem onClick={logOut}>Logout</MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      {/* <MenuItem>
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="error">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton
          size="large"
          aria-label="show 17 new notifications"
          color="inherit"
        >
          <Badge badgeContent={17} color="error">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem> */}
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <Toolbar style={{ minHeight: 80 }} />
      <List
        sx={{
          width: "100%",
          maxWidth: 360,
          bgcolor: "background.paper",
        }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            Dashboard
          </ListSubheader>
        }
      >
        <Link passHref href={`/admin/dashboard`}>
          <ListItemButton
            selected={router.pathname.indexOf("/admin/dashboard") !== -1}
          >
            <ListItemIcon>
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItemButton>
        </Link>
        <Link passHref href={`/admin/analytics`}>
          <ListItemButton
            selected={router.pathname.indexOf("/admin/analytics") !== -1}
          >
            <ListItemIcon>
              <AnalyticsIcon />
            </ListItemIcon>
            <ListItemText primary="Analytics" />
          </ListItemButton>
        </Link>

        {showAdminBoard || showModeratorBoard ? (
          <>
            <ListItemButton
              selected={
                router.pathname.indexOf("/admin/users") !== -1 ||
                router.pathname.indexOf("/admin/musics") !== -1 ||
                router.pathname.indexOf("/admin/invitation") !== -1
              }
              onClick={handleClick}
            >
              <ListItemIcon>
                <EngineeringIcon />
              </ListItemIcon>
              <ListItemText primary="Manage" />
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse
              in={
                router.pathname.indexOf("/admin/users") !== -1 ||
                router.pathname.indexOf("/admin/musics") !== -1 ||
                router.pathname.indexOf("/admin/invitation") !== -1 ||
                open
                  ? true
                  : false
              }
              timeout="auto"
              unmountOnExit
            >
              <List component="div" disablePadding>
                {showAdminBoard ? (
                  <Link passHref href={`/admin/users`}>
                    <ListItemButton
                      sx={{ pl: 4 }}
                      selected={router.pathname.indexOf("/admin/users") !== -1}
                    >
                      <ListItemIcon>
                        <AccountBoxIcon />
                      </ListItemIcon>
                      <ListItemText primary="Users" />
                    </ListItemButton>
                  </Link>
                ) : (
                  <></>
                )}
                <Link passHref href={`/admin/invitation`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={
                      router.pathname.indexOf("/admin/invitation") !== -1
                    }
                  >
                    <ListItemIcon>
                      <EmailIcon />
                    </ListItemIcon>
                    <ListItemText primary="Invitation" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/musics`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={router.pathname.indexOf("/admin/musics") !== -1}
                  >
                    <ListItemIcon>
                      <LibraryMusicIcon />
                    </ListItemIcon>
                    <ListItemText primary="Musics" />
                  </ListItemButton>
                </Link>
              </List>
            </Collapse>
          </>
        ) : (
          <></>
        )}

        {showAdminBoard ? (
          <>
            <ListItemButton
              selected={
                router.pathname.indexOf("/admin/testimonials") !== -1 ||
                router.pathname.indexOf("/admin/mysocialmedia") !== -1 ||
                router.pathname.indexOf("/admin/mythemes") !== -1 ||
                router.pathname.indexOf("/admin/faqs") !== -1 ||
                router.pathname.indexOf("/admin/mainlanding") !== -1 ||
                router.pathname.indexOf("/admin/fitur") !== -1 ||
                router.pathname.indexOf("/admin/prokes") !== -1 ||
                router.pathname.indexOf("/admin/orderstep") !== -1 ||
                router.pathname.indexOf("/admin/superiority") !== -1 ||
                router.pathname.indexOf("/admin/price") !== -1
              }
              onClick={handleClickConfig}
            >
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>
              <ListItemText primary="Config" />
              {openConfig ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse
              in={
                router.pathname.indexOf("/admin/testimonials") !== -1 ||
                router.pathname.indexOf("/admin/mysocialmedia") !== -1 ||
                router.pathname.indexOf("/admin/mythemes") !== -1 ||
                router.pathname.indexOf("/admin/faqs") !== -1 ||
                router.pathname.indexOf("/admin/mainlanding") !== -1 ||
                router.pathname.indexOf("/admin/fitur") !== -1 ||
                router.pathname.indexOf("/admin/prokes") !== -1 ||
                router.pathname.indexOf("/admin/orderstep") !== -1 ||
                router.pathname.indexOf("/admin/superiority") !== -1 ||
                router.pathname.indexOf("/admin/price") !== -1 ||
                openConfig
                  ? true
                  : false
              }
              timeout="auto"
              unmountOnExit
            >
              <List component="div" disablePadding>
                <Link passHref href={`/admin/mainlanding`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={
                      router.pathname.indexOf("/admin/mainlanding") !== -1
                    }
                  >
                    <ListItemIcon>
                      <HomeIcon />
                    </ListItemIcon>
                    <ListItemText primary="Main Landing" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/fitur`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={router.pathname.indexOf("/admin/fitur") !== -1}
                  >
                    <ListItemIcon>
                      <AutoFixHighIcon />
                    </ListItemIcon>
                    <ListItemText primary="Fitur" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/testimonials`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={
                      router.pathname.indexOf("/admin/testimonials") !== -1
                    }
                  >
                    <ListItemIcon>
                      <GroupIcon />
                    </ListItemIcon>
                    <ListItemText primary="Testimonials" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/mysocialmedia`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={
                      router.pathname.indexOf("/admin/mysocialmedia") !== -1
                    }
                  >
                    <ListItemIcon>
                      <ConnectWithoutContactIcon />
                    </ListItemIcon>
                    <ListItemText primary="Social Media" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/mythemes`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={router.pathname.indexOf("/admin/mythemes") !== -1}
                  >
                    <ListItemIcon>
                      <ColorLensIcon />
                    </ListItemIcon>
                    <ListItemText primary="Themes" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/faqs`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={router.pathname.indexOf("/admin/faqs") !== -1}
                  >
                    <ListItemIcon>
                      <QuizIcon />
                    </ListItemIcon>
                    <ListItemText primary="Faq" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/orderstep`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={
                      router.pathname.indexOf("/admin/orderstep") !== -1
                    }
                  >
                    <ListItemIcon>
                      <TimelineIcon />
                    </ListItemIcon>
                    <ListItemText primary="Order Step" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/prokes`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={router.pathname.indexOf("/admin/prokes") !== -1}
                  >
                    <ListItemIcon>
                      <HealthAndSafetyIcon />
                    </ListItemIcon>
                    <ListItemText primary="Prokes" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/superiority`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={
                      router.pathname.indexOf("/admin/superiority") !== -1
                    }
                  >
                    <ListItemIcon>
                      <AutoAwesomeIcon />
                    </ListItemIcon>
                    <ListItemText primary="Superiority" />
                  </ListItemButton>
                </Link>
                <Link passHref href={`/admin/price`}>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    selected={router.pathname.indexOf("/admin/price") !== -1}
                  >
                    <ListItemIcon>
                      <PaymentsIcon />
                    </ListItemIcon>
                    <ListItemText primary="Price" />
                  </ListItemButton>
                </Link>
              </List>
            </Collapse>
          </>
        ) : (
          <></>
        )}
      </List>
      {/* 
      <Divider />
      <List>
        {["All mail", "Trash", "Spam"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List> */}
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        color="inherit"
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
          boxShadow: "none",
        }}
      >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ display: { xs: "none", sm: "block" } }}
          >
            Invitelah
          </Typography>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
            />
          </Search>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            {/* <IconButton
              size="large"
              aria-label="show 4 new mails"
              color="inherit"
            >
              <Badge badgeContent={4} color="error">
                <MailIcon />
              </Badge>
            </IconButton>
            <IconButton
              size="large"
              aria-label="show 17 new notifications"
              color="inherit"
            >
              <Badge badgeContent={17} color="error">
                <NotificationsIcon />
              </Badge>
            </IconButton> */}
            <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </Box>
          <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>

      {renderMobileMenu}
      {renderMenu}

      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
              borderRight: "0px solid #FFFFFF",
            },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
              borderRight: "0px solid #FFFFFF",
            },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          mt: 5,
          width: { sm: `calc(100% - ${drawerWidth}px)` },
        }}
      >
        <Toolbar />
        <Box
          component="div"
          sx={{
            padding: 3,
            backgroundColor: "#D7FFF1",
            color: "black",
            marginRight: 3,
            marginLeft: { xs: 3, sm: 0, md: 0, lg: 0, xl: 0 },
            borderRadius: 4,
          }}
        >
          {children}
        </Box>
      </Box>
    </Box>
  );
}

ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default ResponsiveDrawer;
