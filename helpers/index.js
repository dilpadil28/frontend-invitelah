export const convertDate = (date) => {
  if (date) {
    const bln = {};
    bln["01"] = "Januari";
    bln["02"] = "Februari";
    bln["03"] = "Maret";
    bln["04"] = "April";
    bln["05"] = "Mei";
    bln["06"] = "Juni";
    bln["07"] = "Juli";
    bln["08"] = "Agustus";
    bln["09"] = "September";
    bln["10"] = "Oktober";
    bln["11"] = "November";
    bln["12"] = "Desember";
    let res = date.split("-");
    return res[2].replace(/\b0/g, '') + " " + bln[res[1]] + " " + res[0];
  } else {
    return "";
  }
}