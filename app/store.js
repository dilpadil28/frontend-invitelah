import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/auth/authSlice";
import messageReducer from "../features/message/messageSlice";
import userReducer from "../features/user/userSlice";
import testimonialReducer from "../features/testimonial/testimonialSlice";
import mySocialMediaReducer from "../features/mySocialMedia/mySocialMediaSlice";
import myThemeReducer from "../features/myTheme/myThemeSlice";
import faqReducer from "../features/faq/faqSlice";
import mainLandingReducer from "../features/mainLanding/mainLandingSlice";
import fiturReducer from "../features/fitur/fiturSlice";
import musicReducer from "../features/music/musicSlice";
import orderStepReducer from "../features/orderStep/orderStepSlice";
import prokesReducer from "../features/prokes/prokesSlice";
import superiorityReducer from "../features/superiority/superioritySlice";
import priceReducer from "../features/price/priceSlice";
import invitationReducer from "../features/invitation/invitationSlice";
import invitationSlugReducer from "../features/invitationSlug/invitationSlugSlice";
import backgroundReducer from "../features/background/backgroundSlice";
import digitalEnvelopeReducer from "../features/digitalEnvelope/digitalEnvelopeSlice";
import loveStoryReducer from "../features/loveStory/loveStorySlice";
import photoGalleryReducer from "../features/photoGallery/photoGallerySlice";
import presenceReducer from "../features/presence/presenceSlice";
import socialMediaReducer from "../features/socialMedia/socialMediaSlice";
import themeReducer from "../features/theme/themeSlice";
import youtubeReducer from "../features/youtube/youtubeSlice";
import messageApiReducer from "../features/messageApi/messageApiSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    message: messageReducer,
    messageApi: messageApiReducer,
    youtube: youtubeReducer,
    theme: themeReducer,
    socialMedia: socialMediaReducer,
    presence: presenceReducer,
    photoGallery: photoGalleryReducer,
    loveStory: loveStoryReducer,
    digitalEnvelope: digitalEnvelopeReducer,
    background: backgroundReducer,
    invitationSlug: invitationSlugReducer,
    invitation: invitationReducer,
    price: priceReducer,
    superiority: superiorityReducer,
    prokes: prokesReducer,
    orderStep: orderStepReducer,
    music: musicReducer,
    fitur: fiturReducer,
    mainLanding: mainLandingReducer,
    faq: faqReducer,
    myTheme: myThemeReducer,
    mySocialMedia: mySocialMediaReducer,
    testimonial: testimonialReducer,
    user: userReducer,
  },
  devTools: true,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
