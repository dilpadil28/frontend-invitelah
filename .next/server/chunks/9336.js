"use strict";
exports.id = 9336;
exports.ids = [9336];
exports.modules = {

/***/ 9336:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "ZP": () => (/* binding */ auth_authSlice),
  "x4": () => (/* binding */ login),
  "kS": () => (/* binding */ logout),
  "g$": () => (/* binding */ refreshToken),
  "z2": () => (/* binding */ register)
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
// EXTERNAL MODULE: ./services/token.service.js
var token_service = __webpack_require__(3775);
;// CONCATENATED MODULE: ./services/auth.service.js



class AuthService {
  login(username, password) {
    return api/* default.post */.Z.post("/auth/signin", {
      username,
      password
    }).then(response => {
      if (response.data.accessToken) {
        token_service/* default.setUser */.Z.setUser(response.data);
      }

      return response.data;
    });
  }

  register(fullName, username, phoneNumber, email, roles, password) {
    return api/* default.post */.Z.post("/auth/signup", {
      fullName,
      username,
      phoneNumber,
      email,
      roles,
      password
    });
  }

  logout() {
    token_service/* default.removeUser */.Z.removeUser();
  }

}

/* harmony default export */ const auth_service = (new AuthService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/auth/authSlice.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




let user = "";

if (false) {}

const register = (0,toolkit_.createAsyncThunk)("auth/register", async ({
  fullName,
  username,
  phoneNumber,
  roles,
  email,
  password
}, thunkAPI) => {
  try {
    const response = await auth_service.register(fullName, username, phoneNumber, email, roles, password);
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.data.message));
    return response.data;
  } catch (error) {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));
    return thunkAPI.rejectWithValue();
  }
});
const login = (0,toolkit_.createAsyncThunk)("auth/login", async ({
  username,
  password
}, thunkAPI) => {
  try {
    const data = await auth_service.login(username, password);
    return {
      user: data
    };
  } catch (error) {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));
    return thunkAPI.rejectWithValue();
  }
});
const logout = (0,toolkit_.createAsyncThunk)("auth/logout", async () => {
  await auth_service.logout();
});
const initialState = user ? {
  isLoggedIn: true,
  user
} : {
  isLoggedIn: false,
  user: null
};
const authSlice = (0,toolkit_.createSlice)({
  name: "auth",
  initialState,
  reducers: {
    refreshToken: (state, action) => {
      state.user = _objectSpread(_objectSpread({}, user), {}, {
        accessToken: action.payload
      });
    }
  },
  extraReducers: {
    [register.fulfilled]: (state, action) => {
      state.isLoggedIn = false;
    },
    [register.rejected]: (state, action) => {
      state.isLoggedIn = false;
    },
    [login.fulfilled]: (state, action) => {
      state.isLoggedIn = true;
      state.user = action.payload.user;
    },
    [login.rejected]: (state, action) => {
      state.isLoggedIn = false;
      state.user = null;
    },
    [logout.fulfilled]: (state, action) => {
      state.isLoggedIn = false;
      state.user = null;
    }
  }
});
const {
  reducer,
  actions
} = authSlice;
const {
  refreshToken
} = actions;
/* harmony default export */ const auth_authSlice = (reducer);

/***/ }),

/***/ 2841:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PJ": () => (/* binding */ setMessage),
/* harmony export */   "c4": () => (/* binding */ clearMessage),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const initialState = {};
const messageSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "message",
  initialState,
  reducers: {
    setMessage: (state, action) => {
      return {
        message: action.payload
      };
    },
    clearMessage: () => {
      return {
        message: ""
      };
    }
  }
});
const {
  reducer,
  actions
} = messageSlice;
const {
  setMessage,
  clearMessage
} = actions;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (reducer);

/***/ }),

/***/ 7751:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

const instance = axios__WEBPACK_IMPORTED_MODULE_0___default().create({
  // baseURL: "http://localhost:8080/api/v1",
  baseURL: "https://api.invitelah.com/api/v1",
  headers: {
    "Content-Type": "application/json"
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (instance);

/***/ }),

/***/ 3775:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
class TokenService {
  getLocalRefreshToken() {
    const user = JSON.parse(localStorage.getItem("user"));
    return user === null || user === void 0 ? void 0 : user.refreshToken;
  }

  getLocalAccessToken() {
    const user = JSON.parse(localStorage.getItem("user"));
    return user === null || user === void 0 ? void 0 : user.accessToken;
  }

  updateLocalAccessToken(token) {
    let user = JSON.parse(localStorage.getItem("user"));
    user.accessToken = token;
    localStorage.setItem("user", JSON.stringify(user));
  }

  getUser() {
    return JSON.parse(localStorage.getItem("user"));
  }

  setUser(user) {
    localStorage.setItem("user", JSON.stringify(user));
  }

  removeUser() {
    localStorage.removeItem("user");
  }

}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (new TokenService());

/***/ })

};
;