"use strict";
exports.id = 2623;
exports.ids = [2623];
exports.modules = {

/***/ 2623:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "i6": () => (/* binding */ createTestimonial),
  "ZP": () => (/* binding */ testimonial_testimonialSlice),
  "IF": () => (/* binding */ deleteTestimonial),
  "Ov": () => (/* binding */ getTestimonial),
  "zm": () => (/* binding */ getTestimonialById),
  "fN": () => (/* binding */ updateTestimonial)
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/testimonial.service.js


class TestimonialService {
  getTestimonial() {
    return api/* default.get */.Z.get("/testimonial");
  }

  getTestimonialById(id) {
    return api/* default.get */.Z.get("/testimonial/" + id);
  }

  createTestimonial(data) {
    return api/* default.post */.Z.post("/testimonial", data).then(response => {
      return response.data;
    });
  }

  updateTestimonial(id, result) {
    return api/* default.patch */.Z.patch("/testimonial/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteTestimonial(id) {
    return api/* default.delete */.Z["delete"]("/testimonial/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const testimonial_service = (new TestimonialService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/testimonial/testimonialSlice.js




const initialState = {
  testimonial: null
};
const getTestimonial = (0,toolkit_.createAsyncThunk)("testimonial/getTestimonial", async (_, thunkAPI) => {
  const data = await testimonial_service.getTestimonial().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getTestimonialById = (0,toolkit_.createAsyncThunk)("testimonial/getTestimonialById", async (id, thunkAPI) => {
  const data = await testimonial_service.getTestimonialById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createTestimonial = (0,toolkit_.createAsyncThunk)("testimonial/createTestimonial", async (result, thunkAPI) => {
  const data = await testimonial_service.createTestimonial(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getTestimonial());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateTestimonial = (0,toolkit_.createAsyncThunk)("testimonial/updateTestimonial", async ({
  id,
  result
}, thunkAPI) => {
  const data = await testimonial_service.updateTestimonial(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getTestimonial());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteTestimonial = (0,toolkit_.createAsyncThunk)("testimonial/deleteTestimonial", async (id, thunkAPI) => {
  const data = await testimonial_service.deleteTestimonial(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getTestimonial());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const testimonialSlice = (0,toolkit_.createSlice)({
  name: "testimonial",
  initialState,
  extraReducers: {
    [getTestimonial.fulfilled]: (state, action) => {
      state.testimonial = action.payload;
    },
    [getTestimonial.rejected]: (state, action) => {
      state.testimonial = null;
    },
    [getTestimonialById.fulfilled]: (state, action) => {// state.testimonial = action.payload;
    },
    [getTestimonialById.rejected]: (state, action) => {// state.testimonial = null;
    },
    [createTestimonial.fulfilled]: (state, action) => {// state.testimonial = action.payload.testimonial;
    },
    [createTestimonial.rejected]: (state, action) => {// state.testimonial = null;
    },
    [updateTestimonial.fulfilled]: (state, action) => {// state.testimonialUpdate = action.payload;
    },
    [updateTestimonial.rejected]: (state, action) => {// state.testimonial = null;
    },
    [deleteTestimonial.fulfilled]: (state, action) => {// state.testimonial = action.payload.testimonial;
    },
    [deleteTestimonial.rejected]: (state, action) => {// state.testimonial = null;
    }
  }
});
const {
  reducer
} = testimonialSlice;
/* harmony default export */ const testimonial_testimonialSlice = (reducer);

/***/ })

};
;