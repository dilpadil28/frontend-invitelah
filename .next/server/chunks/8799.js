"use strict";
exports.id = 8799;
exports.ids = [8799];
exports.modules = {

/***/ 8799:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "hs": () => (/* binding */ createPresence),
  "ZP": () => (/* binding */ presence_presenceSlice),
  "OL": () => (/* binding */ deletePresence),
  "Z0": () => (/* binding */ getPresenceById),
  "_L": () => (/* binding */ getPresenceByInvitationId),
  "Kv": () => (/* binding */ updatePresence)
});

// UNUSED EXPORTS: getPresence

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/presence.service.js


class PresenceService {
  getPresence() {
    return api/* default.get */.Z.get("/presence");
  }

  getPresenceById(id) {
    return api/* default.get */.Z.get("/presence/" + id);
  }

  getPresenceByInvitationId(id) {
    return api/* default.get */.Z.get("/presence-invitation/" + id);
  }

  createPresence(data) {
    return api/* default.post */.Z.post("/presence", data).then(response => {
      return response.data;
    });
  }

  updatePresence(id, result) {
    return api/* default.patch */.Z.patch("/presence/" + id, result).then(response => {
      return response.data;
    });
  }

  deletePresence(id) {
    return api/* default.delete */.Z["delete"]("/presence/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const presence_service = (new PresenceService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/presence/presenceSlice.js




const initialState = {
  presence: null
};
const getPresence = (0,toolkit_.createAsyncThunk)("presence/getPresence", async (_, thunkAPI) => {
  const data = await presence_service.getPresence().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPresenceById = (0,toolkit_.createAsyncThunk)("presence/getPresenceById", async (id, thunkAPI) => {
  const data = await presence_service.getPresenceById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPresenceByInvitationId = (0,toolkit_.createAsyncThunk)("presence/getPresenceByInvitationId", async (id, thunkAPI) => {
  const data = await presence_service.getPresenceByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createPresence = (0,toolkit_.createAsyncThunk)("presence/createPresence", async (result, thunkAPI) => {
  const data = await presence_service.createPresence(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updatePresence = (0,toolkit_.createAsyncThunk)("presence/updatePresence", async ({
  id,
  result
}, thunkAPI) => {
  const data = await presence_service.updatePresence(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deletePresence = (0,toolkit_.createAsyncThunk)("presence/deletePresence", async (id, thunkAPI) => {
  const data = await presence_service.deletePresence(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const presenceSlice = (0,toolkit_.createSlice)({
  name: "presence",
  initialState,
  extraReducers: {
    [getPresence.fulfilled]: (state, action) => {// state.presence = action.payload;
    },
    [getPresence.rejected]: (state, action) => {// state.presence = null;
    },
    [getPresenceByInvitationId.fulfilled]: (state, action) => {
      state.presence = action.payload;
    },
    [getPresenceByInvitationId.rejected]: (state, action) => {
      state.presence = null;
    },
    [getPresenceById.fulfilled]: (state, action) => {// state.presence = action.payload;
    },
    [getPresenceById.rejected]: (state, action) => {// state.presence = null;
    },
    [createPresence.fulfilled]: (state, action) => {// state.presence = action.payload.presence;
    },
    [createPresence.rejected]: (state, action) => {// state.presence = null;
    },
    [updatePresence.fulfilled]: (state, action) => {// state.presenceUpdate = action.payload;
    },
    [updatePresence.rejected]: (state, action) => {// state.presence = null;
    },
    [deletePresence.fulfilled]: (state, action) => {// state.presence = action.payload.presence;
    },
    [deletePresence.rejected]: (state, action) => {// state.presence = null;
    }
  }
});
const {
  reducer
} = presenceSlice;
/* harmony default export */ const presence_presenceSlice = (reducer);

/***/ })

};
;