"use strict";
exports.id = 3974;
exports.ids = [3974];
exports.modules = {

/***/ 3974:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "KS": () => (/* binding */ createMySocialMedia),
  "ZP": () => (/* binding */ mySocialMedia_mySocialMediaSlice),
  "YO": () => (/* binding */ deleteMySocialMedia),
  "Q6": () => (/* binding */ getMySocialMedia),
  "sg": () => (/* binding */ getMySocialMediaById),
  "BS": () => (/* binding */ updateMySocialMedia)
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/mySocialMedia.service.js


class MySocialMediaService {
  getMySocialMedia() {
    return api/* default.get */.Z.get("/mysocialmedia");
  }

  getMySocialMediaById(id) {
    return api/* default.get */.Z.get("/mysocialmedia/" + id);
  }

  createMySocialMedia(data) {
    return api/* default.post */.Z.post("/mysocialmedia", data).then(response => {
      return response.data;
    });
  }

  updateMySocialMedia(id, result) {
    return api/* default.patch */.Z.patch("/mysocialmedia/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMySocialMedia(id) {
    return api/* default.delete */.Z["delete"]("/mysocialmedia/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const mySocialMedia_service = (new MySocialMediaService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/mySocialMedia/mySocialMediaSlice.js




const initialState = {
  mySocialMedia: null
};
const getMySocialMedia = (0,toolkit_.createAsyncThunk)("mySocialMedia/getMySocialMedia", async (_, thunkAPI) => {
  const data = await mySocialMedia_service.getMySocialMedia().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMySocialMediaById = (0,toolkit_.createAsyncThunk)("mySocialMedia/getMySocialMediaById", async (id, thunkAPI) => {
  const data = await mySocialMedia_service.getMySocialMediaById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMySocialMedia = (0,toolkit_.createAsyncThunk)("mySocialMedia/createMySocialMedia", async (result, thunkAPI) => {
  const data = await mySocialMedia_service.createMySocialMedia(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMySocialMedia());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMySocialMedia = (0,toolkit_.createAsyncThunk)("mySocialMedia/updateMySocialMedia", async ({
  id,
  result
}, thunkAPI) => {
  const data = await mySocialMedia_service.updateMySocialMedia(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMySocialMedia());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMySocialMedia = (0,toolkit_.createAsyncThunk)("mySocialMedia/deleteMySocialMedia", async (id, thunkAPI) => {
  const data = await mySocialMedia_service.deleteMySocialMedia(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMySocialMedia());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const mySocialMediaSlice = (0,toolkit_.createSlice)({
  name: "mySocialMedia",
  initialState,
  extraReducers: {
    [getMySocialMedia.fulfilled]: (state, action) => {
      state.mySocialMedia = action.payload;
    },
    [getMySocialMedia.rejected]: (state, action) => {
      state.mySocialMedia = null;
    },
    [getMySocialMediaById.fulfilled]: (state, action) => {// state.mySocialMedia = action.payload;
    },
    [getMySocialMediaById.rejected]: (state, action) => {// state.mySocialMedia = null;
    },
    [createMySocialMedia.fulfilled]: (state, action) => {// state.mySocialMedia = action.payload.mySocialMedia;
    },
    [createMySocialMedia.rejected]: (state, action) => {// state.mySocialMedia = null;
    },
    [updateMySocialMedia.fulfilled]: (state, action) => {// state.mySocialMediaUpdate = action.payload;
    },
    [updateMySocialMedia.rejected]: (state, action) => {// state.mySocialMedia = null;
    },
    [deleteMySocialMedia.fulfilled]: (state, action) => {// state.mySocialMedia = action.payload.mySocialMedia;
    },
    [deleteMySocialMedia.rejected]: (state, action) => {// state.mySocialMedia = null;
    }
  }
});
const {
  reducer
} = mySocialMediaSlice;
/* harmony default export */ const mySocialMedia_mySocialMediaSlice = (reducer);

/***/ })

};
;