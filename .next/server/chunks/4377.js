"use strict";
exports.id = 4377;
exports.ids = [4377];
exports.modules = {

/***/ 994:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const eventBus = {
  on(event, callback) {
    document.addEventListener(event, e => callback(e.detail));
  },

  dispatch(event, data) {
    document.dispatchEvent(new CustomEvent(event, {
      detail: data
    }));
  },

  remove(event, callback) {
    document.removeEventListener(event, callback);
  }

};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (eventBus);

/***/ }),

/***/ 2215:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "M": () => (/* binding */ slugReducer),
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const initialState = {
  invitationSlug: null
};
const invitationSlugSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "invitationSlug",
  initialState,
  reducers: {
    slugReducer: (state, action) => {
      state.invitationSlug = action.payload;
    }
  },
  extraReducers: {}
});
const {
  reducer,
  actions
} = invitationSlugSlice;
const {
  slugReducer
} = actions;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (reducer);

/***/ }),

/***/ 3188:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Yd": () => (/* binding */ createMessageApi),
  "ZP": () => (/* binding */ messageApi_messageApiSlice),
  "cu": () => (/* binding */ getMessageApiByInvitationId)
});

// UNUSED EXPORTS: deleteMessageApi, getMessageApi, getMessageApiById, updateMessageApi

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/messageApi.service.js


class MessageApiService {
  getMessageApi() {
    return api/* default.get */.Z.get("/message");
  }

  getMessageApiById(id) {
    return api/* default.get */.Z.get("/message/" + id);
  }

  getMessageApiByInvitationId(id) {
    return api/* default.get */.Z.get("/message-invitation/" + id);
  }

  createMessageApi(data) {
    return api/* default.post */.Z.post("/message", data).then(response => {
      return response.data;
    });
  }

  updateMessageApi(id, result) {
    return api/* default.patch */.Z.patch("/message/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMessageApi(id) {
    return api/* default.delete */.Z["delete"]("/message/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const messageApi_service = (new MessageApiService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/messageApi/messageApiSlice.js




const initialState = {
  messageApi: null
};
const getMessageApi = (0,toolkit_.createAsyncThunk)("messageApi/getMessageApi", async (_, thunkAPI) => {
  const data = await messageApi_service.getMessageApi().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMessageApiById = (0,toolkit_.createAsyncThunk)("messageApi/getMessageApiById", async (id, thunkAPI) => {
  const data = await messageApi_service.getMessageApiById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMessageApiByInvitationId = (0,toolkit_.createAsyncThunk)("messageApi/getMessageApiByInvitationId", async (id, thunkAPI) => {
  const data = await messageApi_service.getMessageApiByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMessageApi = (0,toolkit_.createAsyncThunk)("messageApi/createMessageApi", async (result, thunkAPI) => {
  const data = await messageApi_service.createMessageApi(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMessageApi = (0,toolkit_.createAsyncThunk)("messageApi/updateMessageApi", async ({
  id,
  result
}, thunkAPI) => {
  const data = await messageApi_service.updateMessageApi(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMessageApi = (0,toolkit_.createAsyncThunk)("messageApi/deleteMessageApi", async (id, thunkAPI) => {
  const data = await messageApi_service.deleteMessageApi(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const messageApiSlice = (0,toolkit_.createSlice)({
  name: "messageApi",
  initialState,
  extraReducers: {
    [getMessageApi.fulfilled]: (state, action) => {// state.messageApi = action.payload;
    },
    [getMessageApi.rejected]: (state, action) => {// state.messageApi = null;
    },
    [getMessageApiByInvitationId.fulfilled]: (state, action) => {
      state.messageApi = action.payload;
    },
    [getMessageApiByInvitationId.rejected]: (state, action) => {
      state.messageApi = null;
    },
    [getMessageApiById.fulfilled]: (state, action) => {// state.messageApi = action.payload;
    },
    [getMessageApiById.rejected]: (state, action) => {// state.messageApi = null;
    },
    [createMessageApi.fulfilled]: (state, action) => {// state.messageApi = action.payload.messageApi;
    },
    [createMessageApi.rejected]: (state, action) => {// state.messageApi = null;
    },
    [updateMessageApi.fulfilled]: (state, action) => {// state.messageApiUpdate = action.payload;
    },
    [updateMessageApi.rejected]: (state, action) => {// state.messageApi = null;
    },
    [deleteMessageApi.fulfilled]: (state, action) => {// state.messageApi = action.payload.messageApi;
    },
    [deleteMessageApi.rejected]: (state, action) => {// state.messageApi = null;
    }
  }
});
const {
  reducer
} = messageApiSlice;
/* harmony default export */ const messageApi_messageApiSlice = (reducer);

/***/ })

};
;