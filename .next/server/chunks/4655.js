"use strict";
exports.id = 4655;
exports.ids = [4655];
exports.modules = {

/***/ 4655:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "ZH": () => (/* binding */ createYoutube),
  "ZP": () => (/* binding */ youtube_youtubeSlice),
  "l$": () => (/* binding */ deleteYoutube),
  "Tq": () => (/* binding */ getYoutubeById),
  "Db": () => (/* binding */ getYoutubeByInvitationId),
  "KU": () => (/* binding */ updateYoutube)
});

// UNUSED EXPORTS: getYoutube

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/youtube.service.js


class YoutubeService {
  getYoutube() {
    return api/* default.get */.Z.get("/youtube");
  }

  getYoutubeById(id) {
    return api/* default.get */.Z.get("/youtube/" + id);
  }

  getYoutubeByInvitationId(id) {
    return api/* default.get */.Z.get("/youtube-invitation/" + id);
  }

  createYoutube(data) {
    return api/* default.post */.Z.post("/youtube", data).then(response => {
      return response.data;
    });
  }

  updateYoutube(id, result) {
    return api/* default.patch */.Z.patch("/youtube/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteYoutube(id) {
    return api/* default.delete */.Z["delete"]("/youtube/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const youtube_service = (new YoutubeService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/youtube/youtubeSlice.js




const initialState = {
  youtube: null
};
const getYoutube = (0,toolkit_.createAsyncThunk)("youtube/getYoutube", async (_, thunkAPI) => {
  const data = await youtube_service.getYoutube().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getYoutubeById = (0,toolkit_.createAsyncThunk)("youtube/getYoutubeById", async (id, thunkAPI) => {
  const data = await youtube_service.getYoutubeById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getYoutubeByInvitationId = (0,toolkit_.createAsyncThunk)("youtube/getYoutubeByInvitationId", async (id, thunkAPI) => {
  const data = await youtube_service.getYoutubeByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createYoutube = (0,toolkit_.createAsyncThunk)("youtube/createYoutube", async (result, thunkAPI) => {
  const data = await youtube_service.createYoutube(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateYoutube = (0,toolkit_.createAsyncThunk)("youtube/updateYoutube", async ({
  id,
  result
}, thunkAPI) => {
  const data = await youtube_service.updateYoutube(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteYoutube = (0,toolkit_.createAsyncThunk)("youtube/deleteYoutube", async (id, thunkAPI) => {
  const data = await youtube_service.deleteYoutube(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const youtubeSlice = (0,toolkit_.createSlice)({
  name: "youtube",
  initialState,
  extraReducers: {
    [getYoutube.fulfilled]: (state, action) => {// state.youtube = action.payload;
    },
    [getYoutube.rejected]: (state, action) => {// state.youtube = null;
    },
    [getYoutubeByInvitationId.fulfilled]: (state, action) => {
      state.youtube = action.payload;
    },
    [getYoutubeByInvitationId.rejected]: (state, action) => {
      state.youtube = null;
    },
    [getYoutubeById.fulfilled]: (state, action) => {// state.youtube = action.payload;
    },
    [getYoutubeById.rejected]: (state, action) => {// state.youtube = null;
    },
    [createYoutube.fulfilled]: (state, action) => {// state.youtube = action.payload.youtube;
    },
    [createYoutube.rejected]: (state, action) => {// state.youtube = null;
    },
    [updateYoutube.fulfilled]: (state, action) => {// state.youtubeUpdate = action.payload;
    },
    [updateYoutube.rejected]: (state, action) => {// state.youtube = null;
    },
    [deleteYoutube.fulfilled]: (state, action) => {// state.youtube = action.payload.youtube;
    },
    [deleteYoutube.rejected]: (state, action) => {// state.youtube = null;
    }
  }
});
const {
  reducer
} = youtubeSlice;
/* harmony default export */ const youtube_youtubeSlice = (reducer);

/***/ })

};
;