"use strict";
exports.id = 8748;
exports.ids = [8748];
exports.modules = {

/***/ 8748:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "yH": () => (/* binding */ createPhotoGallery),
  "ZP": () => (/* binding */ photoGallery_photoGallerySlice),
  "RR": () => (/* binding */ deletePhotoGallery),
  "FE": () => (/* binding */ getPhotoGalleryById),
  "Uw": () => (/* binding */ getPhotoGalleryByInvitationId),
  "vr": () => (/* binding */ updatePhotoGallery)
});

// UNUSED EXPORTS: getPhotoGallery

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/photoGallery.service.js


class PhotoGalleryService {
  getPhotoGallery() {
    return api/* default.get */.Z.get("/photo-gallery");
  }

  getPhotoGalleryById(id) {
    return api/* default.get */.Z.get("/photo-gallery/" + id);
  }

  getPhotoGalleryByInvitationId(id) {
    return api/* default.get */.Z.get("/photo-gallery-invitation/" + id);
  }

  createPhotoGallery(data) {
    return api/* default.post */.Z.post("/photo-gallery", data).then(response => {
      return response.data;
    });
  }

  updatePhotoGallery(id, result) {
    return api/* default.patch */.Z.patch("/photo-gallery/" + id, result).then(response => {
      return response.data;
    });
  }

  deletePhotoGallery(id) {
    return api/* default.delete */.Z["delete"]("/photo-gallery/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const photoGallery_service = (new PhotoGalleryService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/photoGallery/photoGallerySlice.js




const initialState = {
  photoGallery: null
};
const getPhotoGallery = (0,toolkit_.createAsyncThunk)("photoGallery/getPhotoGallery", async (_, thunkAPI) => {
  const data = await photoGallery_service.getPhotoGallery().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPhotoGalleryById = (0,toolkit_.createAsyncThunk)("photoGallery/getPhotoGalleryById", async (id, thunkAPI) => {
  const data = await photoGallery_service.getPhotoGalleryById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPhotoGalleryByInvitationId = (0,toolkit_.createAsyncThunk)("photoGallery/getPhotoGalleryByInvitationId", async (id, thunkAPI) => {
  const data = await photoGallery_service.getPhotoGalleryByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createPhotoGallery = (0,toolkit_.createAsyncThunk)("photoGallery/createPhotoGallery", async (result, thunkAPI) => {
  const data = await photoGallery_service.createPhotoGallery(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updatePhotoGallery = (0,toolkit_.createAsyncThunk)("photoGallery/updatePhotoGallery", async ({
  id,
  result
}, thunkAPI) => {
  const data = await photoGallery_service.updatePhotoGallery(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deletePhotoGallery = (0,toolkit_.createAsyncThunk)("photoGallery/deletePhotoGallery", async (id, thunkAPI) => {
  const data = await photoGallery_service.deletePhotoGallery(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const photoGallerySlice = (0,toolkit_.createSlice)({
  name: "photoGallery",
  initialState,
  extraReducers: {
    [getPhotoGallery.fulfilled]: (state, action) => {// state.photoGallery = action.payload;
    },
    [getPhotoGallery.rejected]: (state, action) => {// state.photoGallery = null;
    },
    [getPhotoGalleryByInvitationId.fulfilled]: (state, action) => {
      state.photoGallery = action.payload;
    },
    [getPhotoGalleryByInvitationId.rejected]: (state, action) => {
      state.photoGallery = null;
    },
    [getPhotoGalleryById.fulfilled]: (state, action) => {// state.photoGallery = action.payload;
    },
    [getPhotoGalleryById.rejected]: (state, action) => {// state.photoGallery = null;
    },
    [createPhotoGallery.fulfilled]: (state, action) => {// state.photoGallery = action.payload.photoGallery;
    },
    [createPhotoGallery.rejected]: (state, action) => {// state.photoGallery = null;
    },
    [updatePhotoGallery.fulfilled]: (state, action) => {// state.photoGalleryUpdate = action.payload;
    },
    [updatePhotoGallery.rejected]: (state, action) => {// state.photoGallery = null;
    },
    [deletePhotoGallery.fulfilled]: (state, action) => {// state.photoGallery = action.payload.photoGallery;
    },
    [deletePhotoGallery.rejected]: (state, action) => {// state.photoGallery = null;
    }
  }
});
const {
  reducer
} = photoGallerySlice;
/* harmony default export */ const photoGallery_photoGallerySlice = (reducer);

/***/ })

};
;