"use strict";
exports.id = 7987;
exports.ids = [7987];
exports.modules = {

/***/ 7987:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PR": () => (/* binding */ getUser),
/* harmony export */   "GA": () => (/* binding */ getUserById),
/* harmony export */   "Nq": () => (/* binding */ updateUser),
/* harmony export */   "h8": () => (/* binding */ deleteUser),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_EventBus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(994);
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6728);
/* harmony import */ var _message_messageSlice__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2841);




const getUser = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)("user/getUser", async (_, thunkAPI) => {
  const data = await _services_user_service__WEBPACK_IMPORTED_MODULE_2__/* ["default"].getUser */ .Z.getUser().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      _common_EventBus__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dispatch */ .Z.dispatch("logout");
    }
  });
  return data;
});
const getUserById = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)("user/getUserById", async (id, thunkAPI) => {
  const data = await _services_user_service__WEBPACK_IMPORTED_MODULE_2__/* ["default"].getUserById */ .Z.getUserById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      _common_EventBus__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dispatch */ .Z.dispatch("logout");
    }
  });
  return data;
});
const updateUser = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)("user/updateUser", async ({
  id,
  fullName,
  username,
  phoneNumber,
  roles,
  email,
  password
}, thunkAPI) => {
  const data = await _services_user_service__WEBPACK_IMPORTED_MODULE_2__/* ["default"].updateUser */ .Z.updateUser(id, fullName, username, phoneNumber, roles, email, password).then(response => {
    thunkAPI.dispatch((0,_message_messageSlice__WEBPACK_IMPORTED_MODULE_3__/* .setMessage */ .PJ)(response.message));
    thunkAPI.dispatch(getUser());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,_message_messageSlice__WEBPACK_IMPORTED_MODULE_3__/* .setMessage */ .PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      _common_EventBus__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dispatch */ .Z.dispatch("logout");
    }
  });
  return data;
});
const deleteUser = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)("user/deleteUser", async (id, thunkAPI) => {
  const data = await _services_user_service__WEBPACK_IMPORTED_MODULE_2__/* ["default"].deleteUser */ .Z.deleteUser(id).then(response => {
    thunkAPI.dispatch((0,_message_messageSlice__WEBPACK_IMPORTED_MODULE_3__/* .setMessage */ .PJ)(response.message));
    thunkAPI.dispatch(getUser());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,_message_messageSlice__WEBPACK_IMPORTED_MODULE_3__/* .setMessage */ .PJ)(message));

    if (error.response && error.response.status === 403) {
      _common_EventBus__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dispatch */ .Z.dispatch("logout");
    }
  });
  return data;
});
const initialState = {};
const userSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "user",
  initialState,
  extraReducers: {
    [getUser.fulfilled]: (state, action) => {
      state.user = action.payload;
    },
    [getUser.rejected]: (state, action) => {
      state.user = null;
    },
    [getUserById.fulfilled]: (state, action) => {// state.user = action.payload;
    },
    [getUserById.rejected]: (state, action) => {// state.user = null;
    },
    [updateUser.fulfilled]: (state, action) => {// state.userUpdate = action.payload;
    },
    [updateUser.rejected]: (state, action) => {// state.user = null;
    },
    [deleteUser.fulfilled]: (state, action) => {// state.user = action.payload.user;
    },
    [deleteUser.rejected]: (state, action) => {// state.user = null;
    }
  }
});
const {
  reducer
} = userSlice;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (reducer);

/***/ }),

/***/ 6728:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7751);
/* harmony import */ var _token_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3775);



class UserService {
  getUser() {
    return _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get("/user");
  }

  getUserById(id) {
    return _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].get */ .Z.get("/user/" + id);
  }

  updateUser(id, fullName, username, phoneNumber, roles, email, password) {
    return _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].patch */ .Z.patch("/user/" + id, {
      fullName,
      username,
      phoneNumber,
      roles,
      email,
      password
    }).then(response => {
      if (response.data.accessToken) {
        _token_service__WEBPACK_IMPORTED_MODULE_1__/* ["default"].setUser */ .Z.setUser(response.data);
      }

      return response.data;
    });
  }

  deleteUser(id) {
    return _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"]["delete"] */ .Z["delete"]("/user/" + id).then(response => {
      if (response.data.accessToken) {
        _token_service__WEBPACK_IMPORTED_MODULE_1__/* ["default"].setUser */ .Z.setUser(response.data);
      }

      return response.data;
    });
  }

}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (new UserService());

/***/ })

};
;