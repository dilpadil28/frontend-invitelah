"use strict";
exports.id = 175;
exports.ids = [175];
exports.modules = {

/***/ 175:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "ch": () => (/* binding */ createOrderStepList),
  "ZP": () => (/* binding */ orderStep_orderStepSlice),
  "Nh": () => (/* binding */ deleteOrderStepList),
  "$u": () => (/* binding */ getOrderStep),
  "Id": () => (/* binding */ getOrderStepById),
  "VW": () => (/* binding */ getOrderStepList),
  "HA": () => (/* binding */ getOrderStepListById),
  "Wy": () => (/* binding */ updateOrderStep),
  "qr": () => (/* binding */ updateOrderStepList)
});

// UNUSED EXPORTS: createOrderStep, deleteOrderStep

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/orderStep.service.js


class OrderStepService {
  getOrderStep() {
    return api/* default.get */.Z.get("/orderstep");
  }

  getOrderStepById(id) {
    return api/* default.get */.Z.get("/orderstep/" + id);
  }

  createOrderStep(data) {
    return api/* default.post */.Z.post("/orderstep", data).then(response => {
      return response.data;
    });
  }

  updateOrderStep(id, result) {
    return api/* default.patch */.Z.patch("/orderstep/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteOrderStep(id) {
    return api/* default.delete */.Z["delete"]("/orderstep/" + id).then(response => {
      return response.data;
    });
  }

  getOrderStepList() {
    return api/* default.get */.Z.get("/ordersteplist");
  }

  getOrderStepListById(id) {
    return api/* default.get */.Z.get("/ordersteplist/" + id);
  }

  createOrderStepList(data) {
    return api/* default.post */.Z.post("/ordersteplist", data).then(response => {
      return response.data;
    });
  }

  updateOrderStepList(id, result) {
    return api/* default.patch */.Z.patch("/ordersteplist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteOrderStepList(id) {
    return api/* default.delete */.Z["delete"]("/ordersteplist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const orderStep_service = (new OrderStepService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/orderStep/orderStepSlice.js




const initialState = {
  orderStep: null,
  orderStepList: []
};
const getOrderStep = (0,toolkit_.createAsyncThunk)("orderStep/getOrderStep", async (_, thunkAPI) => {
  const data = await orderStep_service.getOrderStep().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getOrderStepById = (0,toolkit_.createAsyncThunk)("orderStep/getOrderStepById", async (id, thunkAPI) => {
  const data = await orderStep_service.getOrderStepById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createOrderStep = (0,toolkit_.createAsyncThunk)("orderStep/createOrderStep", async (result, thunkAPI) => {
  const data = await orderStep_service.createOrderStep(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getOrderStep());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateOrderStep = (0,toolkit_.createAsyncThunk)("orderStep/updateOrderStep", async ({
  id,
  result
}, thunkAPI) => {
  const data = await orderStep_service.updateOrderStep(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getOrderStep());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteOrderStep = (0,toolkit_.createAsyncThunk)("orderStep/deleteOrderStep", async (id, thunkAPI) => {
  const data = await orderStep_service.deleteOrderStep(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getOrderStep());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getOrderStepList = (0,toolkit_.createAsyncThunk)("orderStepList/getOrderStepList", async (_, thunkAPI) => {
  const data = await orderStep_service.getOrderStepList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getOrderStepListById = (0,toolkit_.createAsyncThunk)("orderStepList/getOrderStepListById", async (id, thunkAPI) => {
  const data = await orderStep_service.getOrderStepListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createOrderStepList = (0,toolkit_.createAsyncThunk)("orderStepList/createOrderStepList", async (result, thunkAPI) => {
  const data = await orderStep_service.createOrderStepList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getOrderStepList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateOrderStepList = (0,toolkit_.createAsyncThunk)("orderStepList/updateOrderStepList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await orderStep_service.updateOrderStepList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getOrderStepList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteOrderStepList = (0,toolkit_.createAsyncThunk)("orderStepList/deleteOrderStepList", async (id, thunkAPI) => {
  const data = await orderStep_service.deleteOrderStepList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getOrderStepList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const orderStepSlice = (0,toolkit_.createSlice)({
  name: "orderStep",
  initialState,
  extraReducers: {
    [getOrderStep.fulfilled]: (state, action) => {
      state.orderStep = action.payload;
    },
    [getOrderStep.rejected]: (state, action) => {
      state.orderStep = null;
    },
    [getOrderStepById.fulfilled]: (state, action) => {// state.orderStep = action.payload;
    },
    [getOrderStepById.rejected]: (state, action) => {// state.orderStep = null;
    },
    [createOrderStep.fulfilled]: (state, action) => {// state.orderStep = action.payload.orderStep;
    },
    [createOrderStep.rejected]: (state, action) => {// state.orderStep = null;
    },
    [updateOrderStep.fulfilled]: (state, action) => {// state.orderStepUpdate = action.payload;
    },
    [updateOrderStep.rejected]: (state, action) => {// state.orderStep = null;
    },
    [deleteOrderStep.fulfilled]: (state, action) => {// state.orderStep = action.payload.orderStep;
    },
    [deleteOrderStep.rejected]: (state, action) => {// state.orderStep = null;
    },
    [getOrderStepList.fulfilled]: (state, action) => {
      state.orderStepList = action.payload;
    },
    [getOrderStepList.rejected]: (state, action) => {
      state.orderStepList = null;
    },
    [getOrderStepListById.fulfilled]: (state, action) => {// state.orderStepList = action.payload;
    },
    [getOrderStepListById.rejected]: (state, action) => {// state.orderStepList = null;
    },
    [createOrderStepList.fulfilled]: (state, action) => {// state.orderStepList = action.payload.orderStepList;
    },
    [createOrderStepList.rejected]: (state, action) => {// state.orderStepList = null;
    },
    [updateOrderStepList.fulfilled]: (state, action) => {// state.orderStepListUpdate = action.payload;
    },
    [updateOrderStepList.rejected]: (state, action) => {// state.orderStepList = null;
    },
    [deleteOrderStepList.fulfilled]: (state, action) => {// state.orderStepList = action.payload.orderStepList;
    },
    [deleteOrderStepList.rejected]: (state, action) => {// state.orderStepList = null;
    }
  }
});
const {
  reducer
} = orderStepSlice;
/* harmony default export */ const orderStep_orderStepSlice = (reducer);

/***/ })

};
;