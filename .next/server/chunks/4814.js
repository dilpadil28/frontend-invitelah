"use strict";
exports.id = 4814;
exports.ids = [4814];
exports.modules = {

/***/ 4814:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "qC": () => (/* binding */ createSuperiorityList),
  "ZP": () => (/* binding */ superiority_superioritySlice),
  "cu": () => (/* binding */ deleteSuperiorityList),
  "Fy": () => (/* binding */ getSuperiority),
  "Zg": () => (/* binding */ getSuperiorityById),
  "Wd": () => (/* binding */ getSuperiorityList),
  "wn": () => (/* binding */ getSuperiorityListById),
  "Ez": () => (/* binding */ updateSuperiority),
  "qg": () => (/* binding */ updateSuperiorityList)
});

// UNUSED EXPORTS: createSuperiority, deleteSuperiority

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/superiority.service.js


class SuperiorityService {
  getSuperiority() {
    return api/* default.get */.Z.get("/superiority");
  }

  getSuperiorityById(id) {
    return api/* default.get */.Z.get("/superiority/" + id);
  }

  createSuperiority(data) {
    return api/* default.post */.Z.post("/superiority", data).then(response => {
      return response.data;
    });
  }

  updateSuperiority(id, result) {
    return api/* default.patch */.Z.patch("/superiority/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteSuperiority(id) {
    return api/* default.delete */.Z["delete"]("/superiority/" + id).then(response => {
      return response.data;
    });
  }

  getSuperiorityList() {
    return api/* default.get */.Z.get("/superioritylist");
  }

  getSuperiorityListById(id) {
    return api/* default.get */.Z.get("/superioritylist/" + id);
  }

  createSuperiorityList(data) {
    return api/* default.post */.Z.post("/superioritylist", data).then(response => {
      return response.data;
    });
  }

  updateSuperiorityList(id, result) {
    return api/* default.patch */.Z.patch("/superioritylist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteSuperiorityList(id) {
    return api/* default.delete */.Z["delete"]("/superioritylist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const superiority_service = (new SuperiorityService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/superiority/superioritySlice.js




const initialState = {
  superiority: null,
  superiorityList: []
};
const getSuperiority = (0,toolkit_.createAsyncThunk)("superiority/getSuperiority", async (_, thunkAPI) => {
  const data = await superiority_service.getSuperiority().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getSuperiorityById = (0,toolkit_.createAsyncThunk)("superiority/getSuperiorityById", async (id, thunkAPI) => {
  const data = await superiority_service.getSuperiorityById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createSuperiority = (0,toolkit_.createAsyncThunk)("superiority/createSuperiority", async (result, thunkAPI) => {
  const data = await superiority_service.createSuperiority(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getSuperiority());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateSuperiority = (0,toolkit_.createAsyncThunk)("superiority/updateSuperiority", async ({
  id,
  result
}, thunkAPI) => {
  const data = await superiority_service.updateSuperiority(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getSuperiority());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteSuperiority = (0,toolkit_.createAsyncThunk)("superiority/deleteSuperiority", async (id, thunkAPI) => {
  const data = await superiority_service.deleteSuperiority(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getSuperiority());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getSuperiorityList = (0,toolkit_.createAsyncThunk)("superiorityList/getSuperiorityList", async (_, thunkAPI) => {
  const data = await superiority_service.getSuperiorityList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getSuperiorityListById = (0,toolkit_.createAsyncThunk)("superiorityList/getSuperiorityListById", async (id, thunkAPI) => {
  const data = await superiority_service.getSuperiorityListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createSuperiorityList = (0,toolkit_.createAsyncThunk)("superiorityList/createSuperiorityList", async (result, thunkAPI) => {
  const data = await superiority_service.createSuperiorityList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getSuperiorityList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateSuperiorityList = (0,toolkit_.createAsyncThunk)("superiorityList/updateSuperiorityList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await superiority_service.updateSuperiorityList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getSuperiorityList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteSuperiorityList = (0,toolkit_.createAsyncThunk)("superiorityList/deleteSuperiorityList", async (id, thunkAPI) => {
  const data = await superiority_service.deleteSuperiorityList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getSuperiorityList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const superioritySlice = (0,toolkit_.createSlice)({
  name: "superiority",
  initialState,
  extraReducers: {
    [getSuperiority.fulfilled]: (state, action) => {
      state.superiority = action.payload;
    },
    [getSuperiority.rejected]: (state, action) => {
      state.superiority = null;
    },
    [getSuperiorityById.fulfilled]: (state, action) => {// state.superiority = action.payload;
    },
    [getSuperiorityById.rejected]: (state, action) => {// state.superiority = null;
    },
    [createSuperiority.fulfilled]: (state, action) => {// state.superiority = action.payload.superiority;
    },
    [createSuperiority.rejected]: (state, action) => {// state.superiority = null;
    },
    [updateSuperiority.fulfilled]: (state, action) => {// state.superiorityUpdate = action.payload;
    },
    [updateSuperiority.rejected]: (state, action) => {// state.superiority = null;
    },
    [deleteSuperiority.fulfilled]: (state, action) => {// state.superiority = action.payload.superiority;
    },
    [deleteSuperiority.rejected]: (state, action) => {// state.superiority = null;
    },
    [getSuperiorityList.fulfilled]: (state, action) => {
      state.superiorityList = action.payload;
    },
    [getSuperiorityList.rejected]: (state, action) => {
      state.superiorityList = null;
    },
    [getSuperiorityListById.fulfilled]: (state, action) => {// state.superiorityList = action.payload;
    },
    [getSuperiorityListById.rejected]: (state, action) => {// state.superiorityList = null;
    },
    [createSuperiorityList.fulfilled]: (state, action) => {// state.superiorityList = action.payload.superiorityList;
    },
    [createSuperiorityList.rejected]: (state, action) => {// state.superiorityList = null;
    },
    [updateSuperiorityList.fulfilled]: (state, action) => {// state.superiorityListUpdate = action.payload;
    },
    [updateSuperiorityList.rejected]: (state, action) => {// state.superiorityList = null;
    },
    [deleteSuperiorityList.fulfilled]: (state, action) => {// state.superiorityList = action.payload.superiorityList;
    },
    [deleteSuperiorityList.rejected]: (state, action) => {// state.superiorityList = null;
    }
  }
});
const {
  reducer
} = superioritySlice;
/* harmony default export */ const superiority_superioritySlice = (reducer);

/***/ })

};
;