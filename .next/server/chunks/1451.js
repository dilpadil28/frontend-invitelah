"use strict";
exports.id = 1451;
exports.ids = [1451];
exports.modules = {

/***/ 1451:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "pM": () => (/* binding */ createLoveStory),
  "ZP": () => (/* binding */ loveStorySlice),
  "ot": () => (/* binding */ deleteLoveStory),
  "Qi": () => (/* binding */ getLoveStoryById),
  "fB": () => (/* binding */ getLoveStoryByInvitationId),
  "UH": () => (/* binding */ updateLoveStory)
});

// UNUSED EXPORTS: getLoveStory

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/loveStory.service.js


class LoveStoryService {
  getLoveStory() {
    return api/* default.get */.Z.get("/love-story");
  }

  getLoveStoryById(id) {
    return api/* default.get */.Z.get("/love-story/" + id);
  }

  getLoveStoryByInvitationId(id) {
    return api/* default.get */.Z.get("/love-story-invitation/" + id);
  }

  createLoveStory(data) {
    return api/* default.post */.Z.post("/love-story", data).then(response => {
      return response.data;
    });
  }

  updateLoveStory(id, result) {
    return api/* default.patch */.Z.patch("/love-story/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteLoveStory(id) {
    return api/* default.delete */.Z["delete"]("/love-story/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const loveStory_service = (new LoveStoryService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/loveStory/loveStorySlice.js




const initialState = {
  loveStory: null
};
const getLoveStory = (0,toolkit_.createAsyncThunk)("loveStory/getLoveStory", async (_, thunkAPI) => {
  const data = await loveStory_service.getLoveStory().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getLoveStoryById = (0,toolkit_.createAsyncThunk)("loveStory/getLoveStoryById", async (id, thunkAPI) => {
  const data = await loveStory_service.getLoveStoryById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getLoveStoryByInvitationId = (0,toolkit_.createAsyncThunk)("loveStory/getLoveStoryByInvitationId", async (id, thunkAPI) => {
  const data = await loveStory_service.getLoveStoryByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createLoveStory = (0,toolkit_.createAsyncThunk)("loveStory/createLoveStory", async (result, thunkAPI) => {
  const data = await loveStory_service.createLoveStory(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateLoveStory = (0,toolkit_.createAsyncThunk)("loveStory/updateLoveStory", async ({
  id,
  result
}, thunkAPI) => {
  const data = await loveStory_service.updateLoveStory(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteLoveStory = (0,toolkit_.createAsyncThunk)("loveStory/deleteLoveStory", async (id, thunkAPI) => {
  const data = await loveStory_service.deleteLoveStory(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const backgroundSlice = (0,toolkit_.createSlice)({
  name: "loveStory",
  initialState,
  extraReducers: {
    [getLoveStory.fulfilled]: (state, action) => {// state.loveStory = action.payload;
    },
    [getLoveStory.rejected]: (state, action) => {// state.loveStory = null;
    },
    [getLoveStoryByInvitationId.fulfilled]: (state, action) => {
      state.loveStory = action.payload;
    },
    [getLoveStoryByInvitationId.rejected]: (state, action) => {
      state.loveStory = null;
    },
    [getLoveStoryById.fulfilled]: (state, action) => {// state.loveStory = action.payload;
    },
    [getLoveStoryById.rejected]: (state, action) => {// state.loveStory = null;
    },
    [createLoveStory.fulfilled]: (state, action) => {// state.loveStory = action.payload.loveStory;
    },
    [createLoveStory.rejected]: (state, action) => {// state.loveStory = null;
    },
    [updateLoveStory.fulfilled]: (state, action) => {// state.backgroundUpdate = action.payload;
    },
    [updateLoveStory.rejected]: (state, action) => {// state.loveStory = null;
    },
    [deleteLoveStory.fulfilled]: (state, action) => {// state.loveStory = action.payload.loveStory;
    },
    [deleteLoveStory.rejected]: (state, action) => {// state.loveStory = null;
    }
  }
});
const {
  reducer
} = backgroundSlice;
/* harmony default export */ const loveStorySlice = (reducer);

/***/ })

};
;