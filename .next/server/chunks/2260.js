"use strict";
exports.id = 2260;
exports.ids = [2260];
exports.modules = {

/***/ 2260:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Tl": () => (/* binding */ createFiturList),
  "ZP": () => (/* binding */ fitur_fiturSlice),
  "Dd": () => (/* binding */ deleteFiturList),
  "QO": () => (/* binding */ getFitur),
  "BE": () => (/* binding */ getFiturById),
  "Db": () => (/* binding */ getFiturList),
  "zb": () => (/* binding */ getFiturListById),
  "GE": () => (/* binding */ updateFitur),
  "CL": () => (/* binding */ updateFiturList)
});

// UNUSED EXPORTS: createFitur, deleteFitur

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/fitur.service.js


class FiturService {
  getFitur() {
    return api/* default.get */.Z.get("/fitur");
  }

  getFiturById(id) {
    return api/* default.get */.Z.get("/fitur/" + id);
  }

  createFitur(data) {
    return api/* default.post */.Z.post("/fitur", data).then(response => {
      return response.data;
    });
  }

  updateFitur(id, result) {
    return api/* default.patch */.Z.patch("/fitur/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteFitur(id) {
    return api/* default.delete */.Z["delete"]("/fitur/" + id).then(response => {
      return response.data;
    });
  }

  getFiturList() {
    return api/* default.get */.Z.get("/fiturlist");
  }

  getFiturListById(id) {
    return api/* default.get */.Z.get("/fiturlist/" + id);
  }

  createFiturList(data) {
    return api/* default.post */.Z.post("/fiturlist", data).then(response => {
      return response.data;
    });
  }

  updateFiturList(id, result) {
    return api/* default.patch */.Z.patch("/fiturlist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteFiturList(id) {
    return api/* default.delete */.Z["delete"]("/fiturlist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const fitur_service = (new FiturService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/fitur/fiturSlice.js




const initialState = {
  fitur: null,
  fiturList: []
};
const getFitur = (0,toolkit_.createAsyncThunk)("fitur/getFitur", async (_, thunkAPI) => {
  const data = await fitur_service.getFitur().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getFiturById = (0,toolkit_.createAsyncThunk)("fitur/getFiturById", async (id, thunkAPI) => {
  const data = await fitur_service.getFiturById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createFitur = (0,toolkit_.createAsyncThunk)("fitur/createFitur", async (result, thunkAPI) => {
  const data = await fitur_service.createFitur(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFitur());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateFitur = (0,toolkit_.createAsyncThunk)("fitur/updateFitur", async ({
  id,
  result
}, thunkAPI) => {
  const data = await fitur_service.updateFitur(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFitur());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteFitur = (0,toolkit_.createAsyncThunk)("fitur/deleteFitur", async (id, thunkAPI) => {
  const data = await fitur_service.deleteFitur(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFitur());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getFiturList = (0,toolkit_.createAsyncThunk)("fiturList/getFiturList", async (_, thunkAPI) => {
  const data = await fitur_service.getFiturList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getFiturListById = (0,toolkit_.createAsyncThunk)("fiturList/getFiturListById", async (id, thunkAPI) => {
  const data = await fitur_service.getFiturListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createFiturList = (0,toolkit_.createAsyncThunk)("fiturList/createFiturList", async (result, thunkAPI) => {
  const data = await fitur_service.createFiturList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFiturList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateFiturList = (0,toolkit_.createAsyncThunk)("fiturList/updateFiturList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await fitur_service.updateFiturList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFiturList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteFiturList = (0,toolkit_.createAsyncThunk)("fiturList/deleteFiturList", async (id, thunkAPI) => {
  const data = await fitur_service.deleteFiturList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFiturList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const fiturSlice = (0,toolkit_.createSlice)({
  name: "fitur",
  initialState,
  extraReducers: {
    [getFitur.fulfilled]: (state, action) => {
      state.fitur = action.payload;
    },
    [getFitur.rejected]: (state, action) => {
      state.fitur = null;
    },
    [getFiturById.fulfilled]: (state, action) => {// state.fitur = action.payload;
    },
    [getFiturById.rejected]: (state, action) => {// state.fitur = null;
    },
    [createFitur.fulfilled]: (state, action) => {// state.fitur = action.payload.fitur;
    },
    [createFitur.rejected]: (state, action) => {// state.fitur = null;
    },
    [updateFitur.fulfilled]: (state, action) => {// state.fiturUpdate = action.payload;
    },
    [updateFitur.rejected]: (state, action) => {// state.fitur = null;
    },
    [deleteFitur.fulfilled]: (state, action) => {// state.fitur = action.payload.fitur;
    },
    [deleteFitur.rejected]: (state, action) => {// state.fitur = null;
    },
    [getFiturList.fulfilled]: (state, action) => {
      state.fiturList = action.payload;
    },
    [getFiturList.rejected]: (state, action) => {
      state.fiturList = null;
    },
    [getFiturListById.fulfilled]: (state, action) => {// state.fiturList = action.payload;
    },
    [getFiturListById.rejected]: (state, action) => {// state.fiturList = null;
    },
    [createFiturList.fulfilled]: (state, action) => {// state.fiturList = action.payload.fiturList;
    },
    [createFiturList.rejected]: (state, action) => {// state.fiturList = null;
    },
    [updateFiturList.fulfilled]: (state, action) => {// state.fiturListUpdate = action.payload;
    },
    [updateFiturList.rejected]: (state, action) => {// state.fiturList = null;
    },
    [deleteFiturList.fulfilled]: (state, action) => {// state.fiturList = action.payload.fiturList;
    },
    [deleteFiturList.rejected]: (state, action) => {// state.fiturList = null;
    }
  }
});
const {
  reducer
} = fiturSlice;
/* harmony default export */ const fitur_fiturSlice = (reducer);

/***/ })

};
;