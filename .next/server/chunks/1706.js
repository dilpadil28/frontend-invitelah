"use strict";
exports.id = 1706;
exports.ids = [1706];
exports.modules = {

/***/ 1706:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Fs": () => (/* binding */ createMainLandingList),
  "ZP": () => (/* binding */ mainLanding_mainLandingSlice),
  "aG": () => (/* binding */ deleteMainLandingList),
  "GJ": () => (/* binding */ getMainLanding),
  "sN": () => (/* binding */ getMainLandingById),
  "gL": () => (/* binding */ getMainLandingList),
  "Sw": () => (/* binding */ getMainLandingListById),
  "Ly": () => (/* binding */ updateMainLanding),
  "SB": () => (/* binding */ updateMainLandingList)
});

// UNUSED EXPORTS: createMainLanding, deleteMainLanding

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/mainLanding.service.js


class MainLandingService {
  getMainLanding() {
    return api/* default.get */.Z.get("/mainlanding");
  }

  getMainLandingById(id) {
    return api/* default.get */.Z.get("/mainlanding/" + id);
  }

  createMainLanding(data) {
    return api/* default.post */.Z.post("/mainlanding", data).then(response => {
      return response.data;
    });
  }

  updateMainLanding(id, result) {
    return api/* default.patch */.Z.patch("/mainlanding/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMainLanding(id) {
    return api/* default.delete */.Z["delete"]("/mainlanding/" + id).then(response => {
      return response.data;
    });
  }

  getMainLandingList() {
    return api/* default.get */.Z.get("/mainlandinglist");
  }

  getMainLandingListById(id) {
    return api/* default.get */.Z.get("/mainlandinglist/" + id);
  }

  createMainLandingList(data) {
    return api/* default.post */.Z.post("/mainlandinglist", data).then(response => {
      return response.data;
    });
  }

  updateMainLandingList(id, result) {
    return api/* default.patch */.Z.patch("/mainlandinglist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMainLandingList(id) {
    return api/* default.delete */.Z["delete"]("/mainlandinglist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const mainLanding_service = (new MainLandingService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/mainLanding/mainLandingSlice.js




const initialState = {
  mainLanding: null,
  mainLandingList: []
};
const getMainLanding = (0,toolkit_.createAsyncThunk)("mainLanding/getMainLanding", async (_, thunkAPI) => {
  const data = await mainLanding_service.getMainLanding().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMainLandingById = (0,toolkit_.createAsyncThunk)("mainLanding/getMainLandingById", async (id, thunkAPI) => {
  const data = await mainLanding_service.getMainLandingById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMainLanding = (0,toolkit_.createAsyncThunk)("mainLanding/createMainLanding", async (result, thunkAPI) => {
  const data = await mainLanding_service.createMainLanding(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMainLanding());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMainLanding = (0,toolkit_.createAsyncThunk)("mainLanding/updateMainLanding", async ({
  id,
  result
}, thunkAPI) => {
  const data = await mainLanding_service.updateMainLanding(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMainLanding());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMainLanding = (0,toolkit_.createAsyncThunk)("mainLanding/deleteMainLanding", async (id, thunkAPI) => {
  const data = await mainLanding_service.deleteMainLanding(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMainLanding());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMainLandingList = (0,toolkit_.createAsyncThunk)("mainLandingList/getMainLandingList", async (_, thunkAPI) => {
  const data = await mainLanding_service.getMainLandingList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMainLandingListById = (0,toolkit_.createAsyncThunk)("mainLandingList/getMainLandingListById", async (id, thunkAPI) => {
  const data = await mainLanding_service.getMainLandingListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMainLandingList = (0,toolkit_.createAsyncThunk)("mainLandingList/createMainLandingList", async (result, thunkAPI) => {
  const data = await mainLanding_service.createMainLandingList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMainLandingList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMainLandingList = (0,toolkit_.createAsyncThunk)("mainLandingList/updateMainLandingList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await mainLanding_service.updateMainLandingList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMainLandingList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMainLandingList = (0,toolkit_.createAsyncThunk)("mainLandingList/deleteMainLandingList", async (id, thunkAPI) => {
  const data = await mainLanding_service.deleteMainLandingList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMainLandingList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const mainLandingSlice = (0,toolkit_.createSlice)({
  name: "mainLanding",
  initialState,
  extraReducers: {
    [getMainLanding.fulfilled]: (state, action) => {
      state.mainLanding = action.payload;
    },
    [getMainLanding.rejected]: (state, action) => {
      state.mainLanding = null;
    },
    [getMainLandingById.fulfilled]: (state, action) => {// state.mainLanding = action.payload;
    },
    [getMainLandingById.rejected]: (state, action) => {// state.mainLanding = null;
    },
    [createMainLanding.fulfilled]: (state, action) => {// state.mainLanding = action.payload.mainLanding;
    },
    [createMainLanding.rejected]: (state, action) => {// state.mainLanding = null;
    },
    [updateMainLanding.fulfilled]: (state, action) => {// state.mainLandingUpdate = action.payload;
    },
    [updateMainLanding.rejected]: (state, action) => {// state.mainLanding = null;
    },
    [deleteMainLanding.fulfilled]: (state, action) => {// state.mainLanding = action.payload.mainLanding;
    },
    [deleteMainLanding.rejected]: (state, action) => {// state.mainLanding = null;
    },
    [getMainLandingList.fulfilled]: (state, action) => {
      state.mainLandingList = action.payload;
    },
    [getMainLandingList.rejected]: (state, action) => {
      state.mainLandingList = null;
    },
    [getMainLandingListById.fulfilled]: (state, action) => {// state.mainLandingList = action.payload;
    },
    [getMainLandingListById.rejected]: (state, action) => {// state.mainLandingList = null;
    },
    [createMainLandingList.fulfilled]: (state, action) => {// state.mainLandingList = action.payload.mainLandingList;
    },
    [createMainLandingList.rejected]: (state, action) => {// state.mainLandingList = null;
    },
    [updateMainLandingList.fulfilled]: (state, action) => {// state.mainLandingListUpdate = action.payload;
    },
    [updateMainLandingList.rejected]: (state, action) => {// state.mainLandingList = null;
    },
    [deleteMainLandingList.fulfilled]: (state, action) => {// state.mainLandingList = action.payload.mainLandingList;
    },
    [deleteMainLandingList.rejected]: (state, action) => {// state.mainLandingList = null;
    }
  }
});
const {
  reducer
} = mainLandingSlice;
/* harmony default export */ const mainLanding_mainLandingSlice = (reducer);

/***/ })

};
;