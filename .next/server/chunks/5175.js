"use strict";
exports.id = 5175;
exports.ids = [5175];
exports.modules = {

/***/ 994:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const eventBus = {
  on(event, callback) {
    document.addEventListener(event, e => callback(e.detail));
  },

  dispatch(event, data) {
    document.dispatchEvent(new CustomEvent(event, {
      detail: data
    }));
  },

  remove(event, callback) {
    document.removeEventListener(event, callback);
  }

};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (eventBus);

/***/ }),

/***/ 5175:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mui_material_AppBar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3882);
/* harmony import */ var _mui_material_AppBar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_material_AppBar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(19);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Box__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material_CssBaseline__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4960);
/* harmony import */ var _mui_material_CssBaseline__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_material_CssBaseline__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _mui_material_Divider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3646);
/* harmony import */ var _mui_material_Divider__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Divider__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _mui_material_Drawer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7898);
/* harmony import */ var _mui_material_Drawer__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Drawer__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7934);
/* harmony import */ var _mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _mui_icons_material_MoveToInbox__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8307);
/* harmony import */ var _mui_icons_material_MoveToInbox__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_MoveToInbox__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _mui_material_List__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4192);
/* harmony import */ var _mui_material_List__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_mui_material_List__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _mui_material_ListItem__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(834);
/* harmony import */ var _mui_material_ListItem__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_mui_material_ListItem__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3787);
/* harmony import */ var _mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(8315);
/* harmony import */ var _mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _mui_icons_material_Mail__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(9026);
/* harmony import */ var _mui_icons_material_Mail__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Mail__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _mui_icons_material_Menu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3365);
/* harmony import */ var _mui_icons_material_Menu__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Menu__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _mui_material_Toolbar__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(1431);
/* harmony import */ var _mui_material_Toolbar__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Toolbar__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(7163);
/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Typography__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _mui_material_InputBase__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(8855);
/* harmony import */ var _mui_material_InputBase__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_mui_material_InputBase__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _mui_material_Badge__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(5168);
/* harmony import */ var _mui_material_Badge__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Badge__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(9271);
/* harmony import */ var _mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(_mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _mui_material_Menu__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(5486);
/* harmony import */ var _mui_material_Menu__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Menu__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _mui_icons_material_Search__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(8017);
/* harmony import */ var _mui_icons_material_Search__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Search__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var _mui_icons_material_AccountCircle__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(1883);
/* harmony import */ var _mui_icons_material_AccountCircle__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_AccountCircle__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var _mui_icons_material_MoreVert__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(6952);
/* harmony import */ var _mui_icons_material_MoreVert__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_MoreVert__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var _mui_icons_material_Dashboard__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(7235);
/* harmony import */ var _mui_icons_material_Dashboard__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Dashboard__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var _mui_icons_material_Analytics__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(5041);
/* harmony import */ var _mui_icons_material_Analytics__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Analytics__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_26__);
/* harmony import */ var _mui_icons_material_ExpandLess__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(6174);
/* harmony import */ var _mui_icons_material_ExpandLess__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_ExpandLess__WEBPACK_IMPORTED_MODULE_27__);
/* harmony import */ var _mui_icons_material_ExpandMore__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(8148);
/* harmony import */ var _mui_icons_material_ExpandMore__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_ExpandMore__WEBPACK_IMPORTED_MODULE_28__);
/* harmony import */ var _mui_icons_material_StarBorder__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(5327);
/* harmony import */ var _mui_icons_material_StarBorder__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_StarBorder__WEBPACK_IMPORTED_MODULE_29__);
/* harmony import */ var _mui_icons_material_AccountBox__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(3329);
/* harmony import */ var _mui_icons_material_AccountBox__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_AccountBox__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var _mui_icons_material_Engineering__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(9777);
/* harmony import */ var _mui_icons_material_Engineering__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Engineering__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var _features_auth_authSlice__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(9336);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_33___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_33__);
/* harmony import */ var _common_EventBus__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(994);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(1664);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_36___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_36__);
/* harmony import */ var _mui_icons_material_Group__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(7549);
/* harmony import */ var _mui_icons_material_Group__WEBPACK_IMPORTED_MODULE_37___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Group__WEBPACK_IMPORTED_MODULE_37__);
/* harmony import */ var _mui_icons_material_Settings__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(32);
/* harmony import */ var _mui_icons_material_Settings__WEBPACK_IMPORTED_MODULE_38___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Settings__WEBPACK_IMPORTED_MODULE_38__);
/* harmony import */ var _mui_icons_material_ConnectWithoutContact__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(2289);
/* harmony import */ var _mui_icons_material_ConnectWithoutContact__WEBPACK_IMPORTED_MODULE_39___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_ConnectWithoutContact__WEBPACK_IMPORTED_MODULE_39__);
/* harmony import */ var _mui_icons_material_ColorLens__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(8507);
/* harmony import */ var _mui_icons_material_ColorLens__WEBPACK_IMPORTED_MODULE_40___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_ColorLens__WEBPACK_IMPORTED_MODULE_40__);
/* harmony import */ var _mui_icons_material_Quiz__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(3407);
/* harmony import */ var _mui_icons_material_Quiz__WEBPACK_IMPORTED_MODULE_41___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Quiz__WEBPACK_IMPORTED_MODULE_41__);
/* harmony import */ var _mui_icons_material_Home__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(3467);
/* harmony import */ var _mui_icons_material_Home__WEBPACK_IMPORTED_MODULE_42___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Home__WEBPACK_IMPORTED_MODULE_42__);
/* harmony import */ var _mui_icons_material_AutoFixHigh__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(3765);
/* harmony import */ var _mui_icons_material_AutoFixHigh__WEBPACK_IMPORTED_MODULE_43___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_AutoFixHigh__WEBPACK_IMPORTED_MODULE_43__);
/* harmony import */ var _mui_icons_material_LibraryMusic__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(8582);
/* harmony import */ var _mui_icons_material_LibraryMusic__WEBPACK_IMPORTED_MODULE_44___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_LibraryMusic__WEBPACK_IMPORTED_MODULE_44__);
/* harmony import */ var _mui_icons_material_Timeline__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(5481);
/* harmony import */ var _mui_icons_material_Timeline__WEBPACK_IMPORTED_MODULE_45___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Timeline__WEBPACK_IMPORTED_MODULE_45__);
/* harmony import */ var _mui_icons_material_HealthAndSafety__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(3017);
/* harmony import */ var _mui_icons_material_HealthAndSafety__WEBPACK_IMPORTED_MODULE_46___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_HealthAndSafety__WEBPACK_IMPORTED_MODULE_46__);
/* harmony import */ var _mui_icons_material_AutoAwesome__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(2395);
/* harmony import */ var _mui_icons_material_AutoAwesome__WEBPACK_IMPORTED_MODULE_47___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_AutoAwesome__WEBPACK_IMPORTED_MODULE_47__);
/* harmony import */ var _mui_icons_material_Payments__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(3679);
/* harmony import */ var _mui_icons_material_Payments__WEBPACK_IMPORTED_MODULE_48___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Payments__WEBPACK_IMPORTED_MODULE_48__);
/* harmony import */ var _mui_icons_material_Email__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(9226);
/* harmony import */ var _mui_icons_material_Email__WEBPACK_IMPORTED_MODULE_49___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Email__WEBPACK_IMPORTED_MODULE_49__);
/* harmony import */ var _components_Loading__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(7402);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__);






















































const drawerWidth = 240;
const Search = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_16__.styled)("div")(({
  theme
}) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_16__.alpha)(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_16__.alpha)(theme.palette.common.white, 0.25)
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto"
  }
}));
const SearchIconWrapper = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_16__.styled)("div")(({
  theme
}) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
}));
const StyledInputBase = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_16__.styled)((_mui_material_InputBase__WEBPACK_IMPORTED_MODULE_17___default()))(({
  theme
}) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch"
    }
  }
}));

function ResponsiveDrawer(props) {
  const {
    window,
    children
  } = props;
  const [mobileOpen, setMobileOpen] = react__WEBPACK_IMPORTED_MODULE_0__.useState(false);
  const [anchorEl, setAnchorEl] = react__WEBPACK_IMPORTED_MODULE_0__.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = react__WEBPACK_IMPORTED_MODULE_0__.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const [open, setOpen] = react__WEBPACK_IMPORTED_MODULE_0__.useState(false);
  const [openConfig, setOpenConfig] = react__WEBPACK_IMPORTED_MODULE_0__.useState(false);
  const [showModeratorBoard, setShowModeratorBoard] = react__WEBPACK_IMPORTED_MODULE_0__.useState(false);
  const [showAdminBoard, setShowAdminBoard] = react__WEBPACK_IMPORTED_MODULE_0__.useState(false);
  const {
    user: currentUser
  } = (0,react_redux__WEBPACK_IMPORTED_MODULE_33__.useSelector)(state => state.auth);
  const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_33__.useDispatch)();
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_36__.useRouter)();
  const logOut = react__WEBPACK_IMPORTED_MODULE_0__.useCallback(() => {
    dispatch((0,_features_auth_authSlice__WEBPACK_IMPORTED_MODULE_32__/* .logout */ .kS)());
  }, [dispatch]);
  react__WEBPACK_IMPORTED_MODULE_0__.useEffect(() => {
    if (currentUser) {
      setShowModeratorBoard(currentUser.roles.includes("ROLE_MODERATOR"));
      setShowAdminBoard(currentUser.roles.includes("ROLE_ADMIN"));
    } else {
      setShowModeratorBoard(false);
      setShowAdminBoard(false);
    }

    _common_EventBus__WEBPACK_IMPORTED_MODULE_34__/* ["default"].on */ .Z.on("logout", () => {
      logOut();
    });
    return () => {
      _common_EventBus__WEBPACK_IMPORTED_MODULE_34__/* ["default"].remove */ .Z.remove("logout");
    };
  }, [currentUser, logOut]);

  const handleClick = () => {
    setOpen(!open);
  };

  const handleClickConfig = () => {
    setOpenConfig(!openConfig);
  };

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";

  const renderMenu = /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_Menu__WEBPACK_IMPORTED_MODULE_20___default()), {
    anchorEl: anchorEl,
    anchorOrigin: {
      vertical: "top",
      horizontal: "right"
    },
    id: menuId,
    keepMounted: true,
    transformOrigin: {
      vertical: "top",
      horizontal: "right"
    },
    open: isMenuOpen,
    onClose: handleMenuClose,
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19___default()), {
      onClick: handleMenuClose,
      children: "Profile"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19___default()), {
      onClick: handleMenuClose,
      children: "My account"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19___default()), {
      onClick: logOut,
      children: "Logout"
    })]
  });

  const mobileMenuId = "primary-search-account-menu-mobile";

  const renderMobileMenu = /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Menu__WEBPACK_IMPORTED_MODULE_20___default()), {
    anchorEl: mobileMoreAnchorEl,
    anchorOrigin: {
      vertical: "top",
      horizontal: "right"
    },
    id: mobileMenuId,
    keepMounted: true,
    transformOrigin: {
      vertical: "top",
      horizontal: "right"
    },
    open: isMobileMenuOpen,
    onClose: handleMobileMenuClose,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_MenuItem__WEBPACK_IMPORTED_MODULE_19___default()), {
      onClick: handleProfileMenuOpen,
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6___default()), {
        size: "large",
        "aria-label": "account of current user",
        "aria-controls": "primary-search-account-menu",
        "aria-haspopup": "true",
        color: "inherit",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_AccountCircle__WEBPACK_IMPORTED_MODULE_22___default()), {})
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx("p", {
        children: "Profile"
      })]
    })
  });

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)("div", {
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Toolbar__WEBPACK_IMPORTED_MODULE_14___default()), {
      style: {
        minHeight: 80
      }
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_List__WEBPACK_IMPORTED_MODULE_8___default()), {
      sx: {
        width: "100%",
        maxWidth: 360,
        bgcolor: "background.paper"
      },
      component: "nav",
      "aria-labelledby": "nested-list-subheader",
      subheader: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListSubheader, {
        component: "div",
        id: "nested-list-subheader",
        children: "Dashboard"
      }),
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
        passHref: true,
        href: `/admin/dashboard`,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
          selected: router.pathname.indexOf("/admin/dashboard") !== -1,
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Dashboard__WEBPACK_IMPORTED_MODULE_24___default()), {})
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
            primary: "Dashboard"
          })]
        })
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
        passHref: true,
        href: `/admin/analytics`,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
          selected: router.pathname.indexOf("/admin/analytics") !== -1,
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Analytics__WEBPACK_IMPORTED_MODULE_25___default()), {})
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
            primary: "Analytics"
          })]
        })
      }), showAdminBoard || showModeratorBoard ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.Fragment, {
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
          selected: router.pathname.indexOf("/admin/users") !== -1 || router.pathname.indexOf("/admin/musics") !== -1 || router.pathname.indexOf("/admin/invitation") !== -1,
          onClick: handleClick,
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Engineering__WEBPACK_IMPORTED_MODULE_31___default()), {})
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
            primary: "Manage"
          }), open ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_ExpandLess__WEBPACK_IMPORTED_MODULE_27___default()), {}) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_ExpandMore__WEBPACK_IMPORTED_MODULE_28___default()), {})]
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_26__.Collapse, {
          in: router.pathname.indexOf("/admin/users") !== -1 || router.pathname.indexOf("/admin/musics") !== -1 || router.pathname.indexOf("/admin/invitation") !== -1 || open ? true : false,
          timeout: "auto",
          unmountOnExit: true,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_List__WEBPACK_IMPORTED_MODULE_8___default()), {
            component: "div",
            disablePadding: true,
            children: [showAdminBoard ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/users`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/users") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_AccountBox__WEBPACK_IMPORTED_MODULE_30___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Users"
                })]
              })
            }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.Fragment, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/invitation`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/invitation") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Email__WEBPACK_IMPORTED_MODULE_49___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Invitation"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/musics`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/musics") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_LibraryMusic__WEBPACK_IMPORTED_MODULE_44___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Musics"
                })]
              })
            })]
          })
        })]
      }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.Fragment, {}), showAdminBoard ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.Fragment, {
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
          selected: router.pathname.indexOf("/admin/testimonials") !== -1 || router.pathname.indexOf("/admin/mysocialmedia") !== -1 || router.pathname.indexOf("/admin/mythemes") !== -1 || router.pathname.indexOf("/admin/faqs") !== -1 || router.pathname.indexOf("/admin/mainlanding") !== -1 || router.pathname.indexOf("/admin/fitur") !== -1 || router.pathname.indexOf("/admin/prokes") !== -1 || router.pathname.indexOf("/admin/orderstep") !== -1 || router.pathname.indexOf("/admin/superiority") !== -1 || router.pathname.indexOf("/admin/price") !== -1,
          onClick: handleClickConfig,
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Settings__WEBPACK_IMPORTED_MODULE_38___default()), {})
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
            primary: "Config"
          }), openConfig ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_ExpandLess__WEBPACK_IMPORTED_MODULE_27___default()), {}) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_ExpandMore__WEBPACK_IMPORTED_MODULE_28___default()), {})]
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_26__.Collapse, {
          in: router.pathname.indexOf("/admin/testimonials") !== -1 || router.pathname.indexOf("/admin/mysocialmedia") !== -1 || router.pathname.indexOf("/admin/mythemes") !== -1 || router.pathname.indexOf("/admin/faqs") !== -1 || router.pathname.indexOf("/admin/mainlanding") !== -1 || router.pathname.indexOf("/admin/fitur") !== -1 || router.pathname.indexOf("/admin/prokes") !== -1 || router.pathname.indexOf("/admin/orderstep") !== -1 || router.pathname.indexOf("/admin/superiority") !== -1 || router.pathname.indexOf("/admin/price") !== -1 || openConfig ? true : false,
          timeout: "auto",
          unmountOnExit: true,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_List__WEBPACK_IMPORTED_MODULE_8___default()), {
            component: "div",
            disablePadding: true,
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/mainlanding`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/mainlanding") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Home__WEBPACK_IMPORTED_MODULE_42___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Main Landing"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/fitur`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/fitur") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_AutoFixHigh__WEBPACK_IMPORTED_MODULE_43___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Fitur"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/testimonials`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/testimonials") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Group__WEBPACK_IMPORTED_MODULE_37___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Testimonials"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/mysocialmedia`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/mysocialmedia") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_ConnectWithoutContact__WEBPACK_IMPORTED_MODULE_39___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Social Media"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/mythemes`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/mythemes") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_ColorLens__WEBPACK_IMPORTED_MODULE_40___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Themes"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/faqs`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/faqs") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Quiz__WEBPACK_IMPORTED_MODULE_41___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Faq"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/orderstep`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/orderstep") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Timeline__WEBPACK_IMPORTED_MODULE_45___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Order Step"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/prokes`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/prokes") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_HealthAndSafety__WEBPACK_IMPORTED_MODULE_46___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Prokes"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/superiority`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/superiority") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_AutoAwesome__WEBPACK_IMPORTED_MODULE_47___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Superiority"
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(next_link__WEBPACK_IMPORTED_MODULE_35__["default"], {
              passHref: true,
              href: `/admin/price`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_26__.ListItemButton, {
                sx: {
                  pl: 4
                },
                selected: router.pathname.indexOf("/admin/price") !== -1,
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemIcon__WEBPACK_IMPORTED_MODULE_10___default()), {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Payments__WEBPACK_IMPORTED_MODULE_48___default()), {})
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_ListItemText__WEBPACK_IMPORTED_MODULE_11___default()), {
                  primary: "Price"
                })]
              })
            })]
          })
        })]
      }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.Fragment, {})]
    })]
  });

  const container = window !== undefined ? () => window().document.body : undefined;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
    sx: {
      display: "flex"
    },
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_CssBaseline__WEBPACK_IMPORTED_MODULE_3___default()), {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_AppBar__WEBPACK_IMPORTED_MODULE_1___default()), {
      color: "inherit",
      position: "fixed",
      sx: {
        width: {
          sm: `calc(100% - ${drawerWidth}px)`
        },
        ml: {
          sm: `${drawerWidth}px`
        },
        boxShadow: "none"
      },
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_Toolbar__WEBPACK_IMPORTED_MODULE_14___default()), {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6___default()), {
          size: "large",
          edge: "start",
          color: "inherit",
          "aria-label": "open drawer",
          onClick: handleDrawerToggle,
          sx: {
            mr: 2,
            display: {
              sm: "none"
            }
          },
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Menu__WEBPACK_IMPORTED_MODULE_13___default()), {})
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Typography__WEBPACK_IMPORTED_MODULE_15___default()), {
          variant: "h6",
          noWrap: true,
          component: "div",
          sx: {
            display: {
              xs: "none",
              sm: "block"
            }
          },
          children: "Invitelah"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)(Search, {
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(SearchIconWrapper, {
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_Search__WEBPACK_IMPORTED_MODULE_21___default()), {})
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx(StyledInputBase, {
            placeholder: "Search\u2026",
            inputProps: {
              "aria-label": "search"
            }
          })]
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
          sx: {
            flexGrow: 1
          }
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
          sx: {
            display: {
              xs: "none",
              md: "flex"
            }
          },
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6___default()), {
            size: "large",
            edge: "end",
            "aria-label": "account of current user",
            "aria-controls": menuId,
            "aria-haspopup": "true",
            onClick: handleProfileMenuOpen,
            color: "inherit",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_AccountCircle__WEBPACK_IMPORTED_MODULE_22___default()), {})
          })
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
          sx: {
            display: {
              xs: "flex",
              md: "none"
            }
          },
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_IconButton__WEBPACK_IMPORTED_MODULE_6___default()), {
            size: "large",
            "aria-label": "show more",
            "aria-controls": mobileMenuId,
            "aria-haspopup": "true",
            onClick: handleMobileMenuOpen,
            color: "inherit",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_icons_material_MoreVert__WEBPACK_IMPORTED_MODULE_23___default()), {})
          })
        })]
      })
    }), renderMobileMenu, renderMenu, /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
      component: "nav",
      sx: {
        width: {
          sm: drawerWidth
        },
        flexShrink: {
          sm: 0
        }
      },
      "aria-label": "mailbox folders",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Drawer__WEBPACK_IMPORTED_MODULE_5___default()), {
        container: container,
        variant: "temporary",
        open: mobileOpen,
        onClose: handleDrawerToggle,
        ModalProps: {
          keepMounted: true // Better open performance on mobile.

        },
        sx: {
          display: {
            xs: "block",
            sm: "none"
          },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
            borderRight: "0px solid #FFFFFF"
          }
        },
        children: drawer
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Drawer__WEBPACK_IMPORTED_MODULE_5___default()), {
        variant: "permanent",
        sx: {
          display: {
            xs: "none",
            sm: "block"
          },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
            borderRight: "0px solid #FFFFFF"
          }
        },
        open: true,
        children: drawer
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
      component: "main",
      sx: {
        flexGrow: 1,
        mt: 5,
        width: {
          sm: `calc(100% - ${drawerWidth}px)`
        }
      },
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Toolbar__WEBPACK_IMPORTED_MODULE_14___default()), {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_51__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
        component: "div",
        sx: {
          padding: 3,
          backgroundColor: "#D7FFF1",
          color: "black",
          marginRight: 3,
          marginLeft: {
            xs: 3,
            sm: 0,
            md: 0,
            lg: 0,
            xl: 0
          },
          borderRadius: 4
        },
        children: children
      })]
    })]
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ResponsiveDrawer);

/***/ })

};
;