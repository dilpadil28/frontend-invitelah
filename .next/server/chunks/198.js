"use strict";
exports.id = 198;
exports.ids = [198];
exports.modules = {

/***/ 198:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "IW": () => (/* binding */ createPriceList),
  "ZP": () => (/* binding */ price_priceSlice),
  "Rr": () => (/* binding */ deletePriceList),
  "aS": () => (/* binding */ getPrice),
  "Ns": () => (/* binding */ getPriceById),
  "D7": () => (/* binding */ getPriceList),
  "VT": () => (/* binding */ getPriceListById),
  "F5": () => (/* binding */ updatePrice),
  "ND": () => (/* binding */ updatePriceList)
});

// UNUSED EXPORTS: createPrice, deletePrice

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/price.service.js


class PriceService {
  getPrice() {
    return api/* default.get */.Z.get("/price");
  }

  getPriceById(id) {
    return api/* default.get */.Z.get("/price/" + id);
  }

  createPrice(data) {
    return api/* default.post */.Z.post("/price", data).then(response => {
      return response.data;
    });
  }

  updatePrice(id, result) {
    return api/* default.patch */.Z.patch("/price/" + id, result).then(response => {
      return response.data;
    });
  }

  deletePrice(id) {
    return api/* default.delete */.Z["delete"]("/price/" + id).then(response => {
      return response.data;
    });
  }

  getPriceList() {
    return api/* default.get */.Z.get("/pricelist");
  }

  getPriceListById(id) {
    return api/* default.get */.Z.get("/pricelist/" + id);
  }

  createPriceList(data) {
    return api/* default.post */.Z.post("/pricelist", data).then(response => {
      return response.data;
    });
  }

  updatePriceList(id, result) {
    return api/* default.patch */.Z.patch("/pricelist/" + id, result).then(response => {
      return response.data;
    });
  }

  deletePriceList(id) {
    return api/* default.delete */.Z["delete"]("/pricelist/" + id).then(response => {
      return response.data;
    });
  }

  getPriceListUrl() {
    return api/* default.get */.Z.get("/pricelisturl");
  }

  getPriceListUrlById(id) {
    return api/* default.get */.Z.get("/pricelisturl/" + id);
  }

  createPriceListUrl(data) {
    return api/* default.post */.Z.post("/pricelisturl", data).then(response => {
      return response.data;
    });
  }

  updatePriceListUrl(id, result) {
    return api/* default.patch */.Z.patch("/pricelisturl/" + id, result).then(response => {
      return response.data;
    });
  }

  deletePriceListUrl(id) {
    return api/* default.delete */.Z["delete"]("/pricelisturl/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const price_service = (new PriceService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/price/priceSlice.js




const initialState = {
  price: null,
  priceList: []
};
const getPrice = (0,toolkit_.createAsyncThunk)("price/getPrice", async (_, thunkAPI) => {
  const data = await price_service.getPrice().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPriceById = (0,toolkit_.createAsyncThunk)("price/getPriceById", async (id, thunkAPI) => {
  const data = await price_service.getPriceById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createPrice = (0,toolkit_.createAsyncThunk)("price/createPrice", async (result, thunkAPI) => {
  const data = await price_service.createPrice(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getPrice());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updatePrice = (0,toolkit_.createAsyncThunk)("price/updatePrice", async ({
  id,
  result
}, thunkAPI) => {
  const data = await price_service.updatePrice(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getPrice());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deletePrice = (0,toolkit_.createAsyncThunk)("price/deletePrice", async (id, thunkAPI) => {
  const data = await price_service.deletePrice(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getPrice());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPriceList = (0,toolkit_.createAsyncThunk)("priceList/getPriceList", async (_, thunkAPI) => {
  const data = await price_service.getPriceList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getPriceListById = (0,toolkit_.createAsyncThunk)("priceList/getPriceListById", async (id, thunkAPI) => {
  const data = await price_service.getPriceListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createPriceList = (0,toolkit_.createAsyncThunk)("priceList/createPriceList", async (result, thunkAPI) => {
  const data = await price_service.createPriceList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getPriceList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updatePriceList = (0,toolkit_.createAsyncThunk)("priceList/updatePriceList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await price_service.updatePriceList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getPriceList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deletePriceList = (0,toolkit_.createAsyncThunk)("priceList/deletePriceList", async (id, thunkAPI) => {
  const data = await price_service.deletePriceList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getPriceList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const priceSlice = (0,toolkit_.createSlice)({
  name: "price",
  initialState,
  extraReducers: {
    [getPrice.fulfilled]: (state, action) => {
      state.price = action.payload;
    },
    [getPrice.rejected]: (state, action) => {
      state.price = null;
    },
    [getPriceById.fulfilled]: (state, action) => {// state.price = action.payload;
    },
    [getPriceById.rejected]: (state, action) => {// state.price = null;
    },
    [createPrice.fulfilled]: (state, action) => {// state.price = action.payload.price;
    },
    [createPrice.rejected]: (state, action) => {// state.price = null;
    },
    [updatePrice.fulfilled]: (state, action) => {// state.priceUpdate = action.payload;
    },
    [updatePrice.rejected]: (state, action) => {// state.price = null;
    },
    [deletePrice.fulfilled]: (state, action) => {// state.price = action.payload.price;
    },
    [deletePrice.rejected]: (state, action) => {// state.price = null;
    },
    [getPriceList.fulfilled]: (state, action) => {
      state.priceList = action.payload;
    },
    [getPriceList.rejected]: (state, action) => {
      state.priceList = null;
    },
    [getPriceListById.fulfilled]: (state, action) => {// state.priceList = action.payload;
    },
    [getPriceListById.rejected]: (state, action) => {// state.priceList = null;
    },
    [createPriceList.fulfilled]: (state, action) => {// state.priceList = action.payload.priceList;
    },
    [createPriceList.rejected]: (state, action) => {// state.priceList = null;
    },
    [updatePriceList.fulfilled]: (state, action) => {// state.priceListUpdate = action.payload;
    },
    [updatePriceList.rejected]: (state, action) => {// state.priceList = null;
    },
    [deletePriceList.fulfilled]: (state, action) => {// state.priceList = action.payload.priceList;
    },
    [deletePriceList.rejected]: (state, action) => {// state.priceList = null;
    }
  }
});
const {
  reducer
} = priceSlice;
/* harmony default export */ const price_priceSlice = (reducer);

/***/ })

};
;