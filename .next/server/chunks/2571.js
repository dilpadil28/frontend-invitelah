"use strict";
exports.id = 2571;
exports.ids = [2571];
exports.modules = {

/***/ 2571:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "WQ": () => (/* binding */ createFaqList),
  "ZP": () => (/* binding */ faq_faqSlice),
  "ou": () => (/* binding */ deleteFaqList),
  "QR": () => (/* binding */ getFaq),
  "oE": () => (/* binding */ getFaqById),
  "Yv": () => (/* binding */ getFaqList),
  "FI": () => (/* binding */ getFaqListById),
  "y": () => (/* binding */ updateFaq),
  "mp": () => (/* binding */ updateFaqList)
});

// UNUSED EXPORTS: createFaq, deleteFaq

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/faq.service.js


class FaqService {
  getFaq() {
    return api/* default.get */.Z.get("/faq");
  }

  getFaqById(id) {
    return api/* default.get */.Z.get("/faq/" + id);
  }

  createFaq(data) {
    return api/* default.post */.Z.post("/faq", data).then(response => {
      return response.data;
    });
  }

  updateFaq(id, result) {
    return api/* default.patch */.Z.patch("/faq/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteFaq(id) {
    return api/* default.delete */.Z["delete"]("/faq/" + id).then(response => {
      return response.data;
    });
  }

  getFaqList() {
    return api/* default.get */.Z.get("/faqlist");
  }

  getFaqListById(id) {
    return api/* default.get */.Z.get("/faqlist/" + id);
  }

  createFaqList(data) {
    return api/* default.post */.Z.post("/faqlist", data).then(response => {
      return response.data;
    });
  }

  updateFaqList(id, result) {
    return api/* default.patch */.Z.patch("/faqlist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteFaqList(id) {
    return api/* default.delete */.Z["delete"]("/faqlist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const faq_service = (new FaqService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/faq/faqSlice.js




const initialState = {
  faq: null,
  faqList: []
};
const getFaq = (0,toolkit_.createAsyncThunk)("faq/getFaq", async (_, thunkAPI) => {
  const data = await faq_service.getFaq().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getFaqById = (0,toolkit_.createAsyncThunk)("faq/getFaqById", async (id, thunkAPI) => {
  const data = await faq_service.getFaqById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createFaq = (0,toolkit_.createAsyncThunk)("faq/createFaq", async (result, thunkAPI) => {
  const data = await faq_service.createFaq(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFaq());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateFaq = (0,toolkit_.createAsyncThunk)("faq/updateFaq", async ({
  id,
  result
}, thunkAPI) => {
  const data = await faq_service.updateFaq(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFaq());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteFaq = (0,toolkit_.createAsyncThunk)("faq/deleteFaq", async (id, thunkAPI) => {
  const data = await faq_service.deleteFaq(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFaq());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getFaqList = (0,toolkit_.createAsyncThunk)("faqList/getFaqList", async (_, thunkAPI) => {
  const data = await faq_service.getFaqList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getFaqListById = (0,toolkit_.createAsyncThunk)("faqList/getFaqListById", async (id, thunkAPI) => {
  const data = await faq_service.getFaqListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createFaqList = (0,toolkit_.createAsyncThunk)("faqList/createFaqList", async (result, thunkAPI) => {
  const data = await faq_service.createFaqList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFaqList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateFaqList = (0,toolkit_.createAsyncThunk)("faqList/updateFaqList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await faq_service.updateFaqList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFaqList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteFaqList = (0,toolkit_.createAsyncThunk)("faqList/deleteFaqList", async (id, thunkAPI) => {
  const data = await faq_service.deleteFaqList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getFaqList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const faqSlice = (0,toolkit_.createSlice)({
  name: "faq",
  initialState,
  extraReducers: {
    [getFaq.fulfilled]: (state, action) => {
      state.faq = action.payload;
    },
    [getFaq.rejected]: (state, action) => {
      state.faq = null;
    },
    [getFaqById.fulfilled]: (state, action) => {// state.faq = action.payload;
    },
    [getFaqById.rejected]: (state, action) => {// state.faq = null;
    },
    [createFaq.fulfilled]: (state, action) => {// state.faq = action.payload.faq;
    },
    [createFaq.rejected]: (state, action) => {// state.faq = null;
    },
    [updateFaq.fulfilled]: (state, action) => {// state.faqUpdate = action.payload;
    },
    [updateFaq.rejected]: (state, action) => {// state.faq = null;
    },
    [deleteFaq.fulfilled]: (state, action) => {// state.faq = action.payload.faq;
    },
    [deleteFaq.rejected]: (state, action) => {// state.faq = null;
    },
    [getFaqList.fulfilled]: (state, action) => {
      state.faqList = action.payload;
    },
    [getFaqList.rejected]: (state, action) => {
      state.faqList = null;
    },
    [getFaqListById.fulfilled]: (state, action) => {// state.faqList = action.payload;
    },
    [getFaqListById.rejected]: (state, action) => {// state.faqList = null;
    },
    [createFaqList.fulfilled]: (state, action) => {// state.faqList = action.payload.faqList;
    },
    [createFaqList.rejected]: (state, action) => {// state.faqList = null;
    },
    [updateFaqList.fulfilled]: (state, action) => {// state.faqListUpdate = action.payload;
    },
    [updateFaqList.rejected]: (state, action) => {// state.faqList = null;
    },
    [deleteFaqList.fulfilled]: (state, action) => {// state.faqList = action.payload.faqList;
    },
    [deleteFaqList.rejected]: (state, action) => {// state.faqList = null;
    }
  }
});
const {
  reducer
} = faqSlice;
/* harmony default export */ const faq_faqSlice = (reducer);

/***/ })

};
;