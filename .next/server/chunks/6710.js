"use strict";
exports.id = 6710;
exports.ids = [6710];
exports.modules = {

/***/ 6710:
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {


// UNUSED EXPORTS: default

// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/icons-material/AddTwoTone"
var AddTwoTone_ = __webpack_require__(1750);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/admin/Transactions/PageHeader.js





function PageHeader_PageHeader() {
  const user = {
    name: "Catherine Pike",
    avatar: "/static/images/avatars/1.jpg"
  };
  return /*#__PURE__*/_jsxs(Grid, {
    container: true,
    justifyContent: "space-between",
    alignItems: "center",
    children: [/*#__PURE__*/_jsxs(Grid, {
      item: true,
      children: [/*#__PURE__*/_jsx(Typography, {
        variant: "h5",
        gutterBottom: true,
        children: "Transactions"
      }), /*#__PURE__*/_jsxs(Typography, {
        variant: "subtitle2",
        children: [user.name, ", these are your recent transactions"]
      })]
    }), /*#__PURE__*/_jsx(Grid, {
      item: true,
      children: /*#__PURE__*/_jsx(Button, {
        className: "!bg-primary",
        sx: {
          mt: {
            xs: 2,
            md: 0
          }
        },
        variant: "contained",
        startIcon: /*#__PURE__*/_jsx(AddTwoToneIcon, {
          fontSize: "small"
        }),
        children: "Create transaction"
      })
    })]
  });
}

/* harmony default export */ const Transactions_PageHeader = ((/* unused pure expression or super */ null && (PageHeader_PageHeader)));
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "date-fns"
var external_date_fns_ = __webpack_require__(4146);
// EXTERNAL MODULE: external "numeral"
var external_numeral_ = __webpack_require__(8032);
var external_numeral_default = /*#__PURE__*/__webpack_require__.n(external_numeral_);
// EXTERNAL MODULE: ./components/admin/Label/index.js
var Label = __webpack_require__(5743);
// EXTERNAL MODULE: external "@mui/icons-material/EditTwoTone"
var EditTwoTone_ = __webpack_require__(6544);
var EditTwoTone_default = /*#__PURE__*/__webpack_require__.n(EditTwoTone_);
// EXTERNAL MODULE: external "@mui/icons-material/DeleteTwoTone"
var DeleteTwoTone_ = __webpack_require__(6502);
var DeleteTwoTone_default = /*#__PURE__*/__webpack_require__.n(DeleteTwoTone_);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
// EXTERNAL MODULE: external "@mui/icons-material/MoreVertTwoTone"
var MoreVertTwoTone_ = __webpack_require__(9790);
var MoreVertTwoTone_default = /*#__PURE__*/__webpack_require__.n(MoreVertTwoTone_);
;// CONCATENATED MODULE: ./components/admin/Transactions/BulkActions.js








const ButtonError = (0,styles_.styled)(material_.Button)(({
  theme
}) => `
     background: ${theme.palette.error.main};
     color: ${theme.palette.error.contrastText};

     &:hover {
        background: ${theme.palette.error.dark};
     }
    `);

function BulkActions() {
  const {
    0: onMenuOpen,
    1: menuOpen
  } = (0,external_react_.useState)(false);
  const moreRef = (0,external_react_.useRef)(null);

  const openMenu = () => {
    menuOpen(true);
  };

  const closeMenu = () => {
    menuOpen(false);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
        display: "flex",
        alignItems: "center",
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
          variant: "h5",
          color: "text.secondary",
          children: "Bulk actions:"
        }), /*#__PURE__*/jsx_runtime_.jsx(ButtonError, {
          sx: {
            ml: 1
          },
          startIcon: /*#__PURE__*/jsx_runtime_.jsx((DeleteTwoTone_default()), {}),
          variant: "contained",
          children: "Delete"
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.IconButton, {
        color: "primary",
        onClick: openMenu,
        ref: moreRef,
        sx: {
          ml: 2,
          p: 1
        },
        children: /*#__PURE__*/jsx_runtime_.jsx((MoreVertTwoTone_default()), {})
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(material_.Menu, {
      keepMounted: true,
      anchorEl: moreRef.current,
      open: onMenuOpen,
      onClose: closeMenu,
      anchorOrigin: {
        vertical: "center",
        horizontal: "center"
      },
      transformOrigin: {
        vertical: "center",
        horizontal: "center"
      },
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.List, {
        sx: {
          p: 1
        },
        component: "nav",
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItem, {
          button: true,
          children: /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
            primary: "Bulk delete selected"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItem, {
          button: true,
          children: /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
            primary: "Bulk edit selected"
          })
        })]
      })
    })]
  });
}

/* harmony default export */ const Transactions_BulkActions = (BulkActions);
;// CONCATENATED MODULE: ./components/admin/Transactions/RecentOrdersTable.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












const getStatusLabel = cryptoOrderStatus => {
  const map = {
    failed: {
      text: "Failed",
      color: "error"
    },
    completed: {
      text: "Completed",
      color: "success"
    },
    pending: {
      text: "Pending",
      color: "warning"
    }
  };
  const {
    text,
    color
  } = map[cryptoOrderStatus];
  return /*#__PURE__*/jsx_runtime_.jsx(Label/* default */.Z, {
    color: color,
    children: text
  });
};

const applyFilters = (cryptoOrders, filters) => {
  return cryptoOrders.filter(cryptoOrder => {
    let matches = true;

    if (filters.status && cryptoOrder.status !== filters.status) {
      matches = false;
    }

    return matches;
  });
};

const applyPagination = (cryptoOrders, page, limit) => {
  return cryptoOrders.slice(page * limit, page * limit + limit);
};

const RecentOrdersTable_RecentOrdersTable = ({
  cryptoOrders
}) => {
  const {
    0: selectedCryptoOrders,
    1: setSelectedCryptoOrders
  } = (0,external_react_.useState)([]);
  const selectedBulkActions = selectedCryptoOrders.length > 0;
  const {
    0: page,
    1: setPage
  } = (0,external_react_.useState)(0);
  const {
    0: limit,
    1: setLimit
  } = (0,external_react_.useState)(5);
  const {
    0: filters,
    1: setFilters
  } = (0,external_react_.useState)({
    status: null
  });
  const statusOptions = [{
    id: "all",
    name: "All"
  }, {
    id: "completed",
    name: "Completed"
  }, {
    id: "pending",
    name: "Pending"
  }, {
    id: "failed",
    name: "Failed"
  }];

  const handleStatusChange = e => {
    let value = null;

    if (e.target.value !== "all") {
      value = e.target.value;
    }

    setFilters(prevFilters => _objectSpread(_objectSpread({}, prevFilters), {}, {
      status: value
    }));
  };

  const handleSelectAllCryptoOrders = event => {
    setSelectedCryptoOrders(event.target.checked ? cryptoOrders.map(cryptoOrder => cryptoOrder.id) : []);
  };

  const handleSelectOneCryptoOrder = (event, cryptoOrderId) => {
    if (!selectedCryptoOrders.includes(cryptoOrderId)) {
      setSelectedCryptoOrders(prevSelected => [...prevSelected, cryptoOrderId]);
    } else {
      setSelectedCryptoOrders(prevSelected => prevSelected.filter(id => id !== cryptoOrderId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = event => {
    setLimit(parseInt(event.target.value));
  };

  const filteredCryptoOrders = applyFilters(cryptoOrders, filters);
  const paginatedCryptoOrders = applyPagination(filteredCryptoOrders, page, limit);
  const selectedSomeCryptoOrders = selectedCryptoOrders.length > 0 && selectedCryptoOrders.length < cryptoOrders.length;
  const selectedAllCryptoOrders = selectedCryptoOrders.length === cryptoOrders.length;
  const theme = (0,material_.useTheme)();
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Card, {
    children: [selectedBulkActions && /*#__PURE__*/jsx_runtime_.jsx(material_.Box, {
      flex: 1,
      p: 2,
      children: /*#__PURE__*/jsx_runtime_.jsx(Transactions_BulkActions, {})
    }), !selectedBulkActions && /*#__PURE__*/jsx_runtime_.jsx(material_.CardHeader, {
      action: /*#__PURE__*/jsx_runtime_.jsx(material_.Box, {
        width: 150,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.FormControl, {
          fullWidth: true,
          variant: "outlined",
          children: [/*#__PURE__*/jsx_runtime_.jsx(material_.InputLabel, {
            children: "Status"
          }), /*#__PURE__*/jsx_runtime_.jsx(material_.Select, {
            value: filters.status || "all",
            onChange: handleStatusChange,
            label: "Status",
            autoWidth: true,
            children: statusOptions.map(statusOption => /*#__PURE__*/jsx_runtime_.jsx(material_.MenuItem, {
              value: statusOption.id,
              children: statusOption.name
            }, statusOption.id))
          })]
        })
      }),
      title: "Recent Orders"
    }), /*#__PURE__*/jsx_runtime_.jsx(material_.Divider, {}), /*#__PURE__*/jsx_runtime_.jsx(material_.TableContainer, {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Table, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.TableHead, {
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.TableRow, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              padding: "checkbox",
              children: /*#__PURE__*/jsx_runtime_.jsx(material_.Checkbox, {
                color: "primary",
                checked: selectedAllCryptoOrders,
                indeterminate: selectedSomeCryptoOrders,
                onChange: handleSelectAllCryptoOrders
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              children: "Order Details"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              children: "Order ID"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              children: "Source"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              align: "right",
              children: "Amount"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              align: "right",
              children: "Status"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
              align: "right",
              children: "Actions"
            })]
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableBody, {
          children: paginatedCryptoOrders.map(cryptoOrder => {
            const isCryptoOrderSelected = selectedCryptoOrders.includes(cryptoOrder.id);
            return /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.TableRow, {
              hover: true,
              selected: isCryptoOrderSelected,
              children: [/*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
                padding: "checkbox",
                children: /*#__PURE__*/jsx_runtime_.jsx(material_.Checkbox, {
                  color: "primary",
                  checked: isCryptoOrderSelected,
                  onChange: event => handleSelectOneCryptoOrder(event, cryptoOrder.id),
                  value: isCryptoOrderSelected
                })
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.TableCell, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "body1",
                  fontWeight: "bold",
                  color: "text.primary",
                  gutterBottom: true,
                  noWrap: true,
                  children: cryptoOrder.orderDetails
                }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "body2",
                  color: "text.secondary",
                  noWrap: true,
                  children: (0,external_date_fns_.format)(cryptoOrder.orderDate, "MMMM dd yyyy")
                })]
              }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
                children: /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "body1",
                  fontWeight: "bold",
                  color: "text.primary",
                  gutterBottom: true,
                  noWrap: true,
                  children: cryptoOrder.orderID
                })
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.TableCell, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "body1",
                  fontWeight: "bold",
                  color: "text.primary",
                  gutterBottom: true,
                  noWrap: true,
                  children: cryptoOrder.sourceName
                }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "body2",
                  color: "text.secondary",
                  noWrap: true,
                  children: cryptoOrder.sourceDesc
                })]
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.TableCell, {
                align: "right",
                children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Typography, {
                  variant: "body1",
                  fontWeight: "bold",
                  color: "text.primary",
                  gutterBottom: true,
                  noWrap: true,
                  children: [cryptoOrder.amountCrypto, cryptoOrder.cryptoCurrency]
                }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "body2",
                  color: "text.secondary",
                  noWrap: true,
                  children: external_numeral_default()(cryptoOrder.amount).format(`${cryptoOrder.currency}0,0.00`)
                })]
              }), /*#__PURE__*/jsx_runtime_.jsx(material_.TableCell, {
                align: "right",
                children: getStatusLabel(cryptoOrder.status)
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.TableCell, {
                align: "right",
                children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Tooltip, {
                  title: "Edit Order",
                  arrow: true,
                  children: /*#__PURE__*/jsx_runtime_.jsx(material_.IconButton, {
                    sx: {
                      "&:hover": {
                        background: theme.palette.primary.light
                      },
                      color: theme.palette.primary.main
                    },
                    color: "inherit",
                    size: "small",
                    children: /*#__PURE__*/jsx_runtime_.jsx((EditTwoTone_default()), {
                      fontSize: "small"
                    })
                  })
                }), /*#__PURE__*/jsx_runtime_.jsx(material_.Tooltip, {
                  title: "Delete Order",
                  arrow: true,
                  children: /*#__PURE__*/jsx_runtime_.jsx(material_.IconButton, {
                    sx: {
                      "&:hover": {
                        background: theme.palette.error.light
                      },
                      color: theme.palette.error.main
                    },
                    color: "inherit",
                    size: "small",
                    children: /*#__PURE__*/jsx_runtime_.jsx((DeleteTwoTone_default()), {
                      fontSize: "small"
                    })
                  })
                })]
              })]
            }, cryptoOrder.id);
          })
        })]
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(material_.Box, {
      p: 2,
      children: /*#__PURE__*/jsx_runtime_.jsx(material_.TablePagination, {
        component: "div",
        count: filteredCryptoOrders.length,
        onPageChange: handlePageChange,
        onRowsPerPageChange: handleLimitChange,
        page: page,
        rowsPerPage: limit,
        rowsPerPageOptions: [5, 10, 25, 30]
      })
    })]
  });
};

RecentOrdersTable_RecentOrdersTable.defaultProps = {
  cryptoOrders: []
};
/* harmony default export */ const Transactions_RecentOrdersTable = ((/* unused pure expression or super */ null && (RecentOrdersTable_RecentOrdersTable)));
;// CONCATENATED MODULE: ./components/admin/Transactions/RecentOrders.js





function RecentOrders_RecentOrders() {
  const cryptoOrders = [{
    id: '1',
    orderDetails: 'Fiat Deposit',
    orderDate: new Date().getTime(),
    status: 'completed',
    orderID: 'VUVX709ET7BY',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1111',
    amountCrypto: 34.4565,
    amount: 56787,
    cryptoCurrency: 'ETH',
    currency: '$'
  }, {
    id: '2',
    orderDetails: 'Fiat Deposit',
    orderDate: subDays(new Date(), 1).getTime(),
    status: 'completed',
    orderID: '23M3UOG65G8K',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1111',
    amountCrypto: 6.58454334,
    amount: 8734587,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '3',
    orderDetails: 'Fiat Deposit',
    orderDate: subDays(new Date(), 5).getTime(),
    status: 'failed',
    orderID: 'F6JHK65MS818',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1111',
    amountCrypto: 6.58454334,
    amount: 8734587,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '4',
    orderDetails: 'Fiat Deposit',
    orderDate: subDays(new Date(), 55).getTime(),
    status: 'completed',
    orderID: 'QJFAI7N84LGM',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1111',
    amountCrypto: 6.58454334,
    amount: 8734587,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '5',
    orderDetails: 'Fiat Deposit',
    orderDate: subDays(new Date(), 56).getTime(),
    status: 'pending',
    orderID: 'BO5KFSYGC0YW',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1111',
    amountCrypto: 6.58454334,
    amount: 8734587,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '6',
    orderDetails: 'Fiat Deposit',
    orderDate: subDays(new Date(), 33).getTime(),
    status: 'completed',
    orderID: '6RS606CBMKVQ',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1111',
    amountCrypto: 6.58454334,
    amount: 8734587,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '7',
    orderDetails: 'Fiat Deposit',
    orderDate: new Date().getTime(),
    status: 'pending',
    orderID: '479KUYHOBMJS',
    sourceName: 'Bank Account',
    sourceDesc: '*** 1212',
    amountCrypto: 2.346546,
    amount: 234234,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '8',
    orderDetails: 'Paypal Withdraw',
    orderDate: subDays(new Date(), 22).getTime(),
    status: 'completed',
    orderID: 'W67CFZNT71KR',
    sourceName: 'Paypal Account',
    sourceDesc: '*** 1111',
    amountCrypto: 3.345456,
    amount: 34544,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '9',
    orderDetails: 'Fiat Deposit',
    orderDate: subDays(new Date(), 11).getTime(),
    status: 'completed',
    orderID: '63GJ5DJFKS4H',
    sourceName: 'Bank Account',
    sourceDesc: '*** 2222',
    amountCrypto: 1.4389567945,
    amount: 123843,
    cryptoCurrency: 'BTC',
    currency: '$'
  }, {
    id: '10',
    orderDetails: 'Wallet Transfer',
    orderDate: subDays(new Date(), 123).getTime(),
    status: 'failed',
    orderID: '17KRZHY8T05M',
    sourceName: 'Wallet Transfer',
    sourceDesc: "John's Cardano Wallet",
    amountCrypto: 765.5695,
    amount: 7567,
    cryptoCurrency: 'ADA',
    currency: '$'
  }];
  return /*#__PURE__*/_jsx(Card, {
    children: /*#__PURE__*/_jsx(RecentOrdersTable, {
      cryptoOrders: cryptoOrders
    })
  });
}

/* harmony default export */ const Transactions_RecentOrders = ((/* unused pure expression or super */ null && (RecentOrders_RecentOrders)));
;// CONCATENATED MODULE: ./components/admin/Transactions/index.js
 // import PageTitleWrapper from 'src/components/PageTitleWrapper';

 // import Footer from "src/components/Footer";






function ApplicationsTransactions() {
  return /*#__PURE__*/_jsxs(_Fragment, {
    children: [/*#__PURE__*/_jsx(PageHeader, {}), /*#__PURE__*/_jsx(Grid, {
      container: true,
      direction: "row",
      justifyContent: "center",
      alignItems: "stretch",
      spacing: 3,
      children: /*#__PURE__*/_jsx(Grid, {
        item: true,
        xs: 12,
        children: /*#__PURE__*/_jsx(RecentOrders, {})
      })
    })]
  });
}

/* harmony default export */ const Transactions = ((/* unused pure expression or super */ null && (ApplicationsTransactions)));

/***/ })

};
;