"use strict";
exports.id = 2205;
exports.ids = [2205];
exports.modules = {

/***/ 2205:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "x7": () => (/* binding */ createInvitation),
  "ZP": () => (/* binding */ invitation_invitationSlice),
  "ZH": () => (/* binding */ deleteInvitation),
  "KO": () => (/* binding */ getInvitation),
  "GF": () => (/* binding */ getInvitationById),
  "_V": () => (/* binding */ updateInvitation)
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/invitation.service.js


class InvitationService {
  getInvitation() {
    return api/* default.get */.Z.get("/invitation");
  }

  getInvitationById(id) {
    return api/* default.get */.Z.get("/invitation/" + id);
  }

  createInvitation(data) {
    return api/* default.post */.Z.post("/invitation", data).then(response => {
      return response.data;
    });
  }

  updateInvitation(id, result) {
    return api/* default.patch */.Z.patch("/invitation/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteInvitation(id) {
    return api/* default.delete */.Z["delete"]("/invitation/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const invitation_service = (new InvitationService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/invitation/invitationSlice.js




const initialState = {
  invitation: null
};
const getInvitation = (0,toolkit_.createAsyncThunk)("invitation/getInvitation", async (_, thunkAPI) => {
  const data = await invitation_service.getInvitation().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getInvitationById = (0,toolkit_.createAsyncThunk)("invitation/getInvitationById", async (id, thunkAPI) => {
  const data = await invitation_service.getInvitationById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createInvitation = (0,toolkit_.createAsyncThunk)("invitation/createInvitation", async (result, thunkAPI) => {
  const data = await invitation_service.createInvitation(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getInvitation());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateInvitation = (0,toolkit_.createAsyncThunk)("invitation/updateInvitation", async ({
  id,
  result
}, thunkAPI) => {
  const data = await invitation_service.updateInvitation(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getInvitation());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteInvitation = (0,toolkit_.createAsyncThunk)("invitation/deleteInvitation", async (id, thunkAPI) => {
  const data = await invitation_service.deleteInvitation(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getInvitation());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const invitationSlice = (0,toolkit_.createSlice)({
  name: "invitation",
  initialState,
  extraReducers: {
    [getInvitation.fulfilled]: (state, action) => {
      state.invitation = action.payload;
    },
    [getInvitation.rejected]: (state, action) => {
      state.invitation = null;
    },
    [getInvitationById.fulfilled]: (state, action) => {// state.invitation = action.payload;
    },
    [getInvitationById.rejected]: (state, action) => {// state.invitation = null;
    },
    [createInvitation.fulfilled]: (state, action) => {// state.invitation = action.payload.invitation;
    },
    [createInvitation.rejected]: (state, action) => {// state.invitation = null;
    },
    [updateInvitation.fulfilled]: (state, action) => {// state.invitationUpdate = action.payload;
    },
    [updateInvitation.rejected]: (state, action) => {// state.invitation = null;
    },
    [deleteInvitation.fulfilled]: (state, action) => {// state.invitation = action.payload.invitation;
    },
    [deleteInvitation.rejected]: (state, action) => {// state.invitation = null;
    }
  }
});
const {
  reducer
} = invitationSlice;
/* harmony default export */ const invitation_invitationSlice = (reducer);

/***/ })

};
;