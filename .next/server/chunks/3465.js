"use strict";
exports.id = 3465;
exports.ids = [3465];
exports.modules = {

/***/ 3465:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "tj": () => (/* binding */ createProkesList),
  "ZP": () => (/* binding */ prokes_prokesSlice),
  "mH": () => (/* binding */ deleteProkesList),
  "nx": () => (/* binding */ getProkes),
  "ZE": () => (/* binding */ getProkesById),
  "Gl": () => (/* binding */ getProkesList),
  "A4": () => (/* binding */ getProkesListById),
  "nq": () => (/* binding */ updateProkes),
  "mD": () => (/* binding */ updateProkesList)
});

// UNUSED EXPORTS: createProkes, deleteProkes

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/prokes.service.js


class ProkesService {
  getProkes() {
    return api/* default.get */.Z.get("/prokes");
  }

  getProkesById(id) {
    return api/* default.get */.Z.get("/prokes/" + id);
  }

  createProkes(data) {
    return api/* default.post */.Z.post("/prokes", data).then(response => {
      return response.data;
    });
  }

  updateProkes(id, result) {
    return api/* default.patch */.Z.patch("/prokes/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteProkes(id) {
    return api/* default.delete */.Z["delete"]("/prokes/" + id).then(response => {
      return response.data;
    });
  }

  getProkesList() {
    return api/* default.get */.Z.get("/prokeslist");
  }

  getProkesListById(id) {
    return api/* default.get */.Z.get("/prokeslist/" + id);
  }

  createProkesList(data) {
    return api/* default.post */.Z.post("/prokeslist", data).then(response => {
      return response.data;
    });
  }

  updateProkesList(id, result) {
    return api/* default.patch */.Z.patch("/prokeslist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteProkesList(id) {
    return api/* default.delete */.Z["delete"]("/prokeslist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const prokes_service = (new ProkesService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/prokes/prokesSlice.js




const initialState = {
  prokes: null,
  prokesList: []
};
const getProkes = (0,toolkit_.createAsyncThunk)("prokes/getProkes", async (_, thunkAPI) => {
  const data = await prokes_service.getProkes().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getProkesById = (0,toolkit_.createAsyncThunk)("prokes/getProkesById", async (id, thunkAPI) => {
  const data = await prokes_service.getProkesById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createProkes = (0,toolkit_.createAsyncThunk)("prokes/createProkes", async (result, thunkAPI) => {
  const data = await prokes_service.createProkes(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getProkes());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateProkes = (0,toolkit_.createAsyncThunk)("prokes/updateProkes", async ({
  id,
  result
}, thunkAPI) => {
  const data = await prokes_service.updateProkes(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getProkes());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteProkes = (0,toolkit_.createAsyncThunk)("prokes/deleteProkes", async (id, thunkAPI) => {
  const data = await prokes_service.deleteProkes(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getProkes());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getProkesList = (0,toolkit_.createAsyncThunk)("prokesList/getProkesList", async (_, thunkAPI) => {
  const data = await prokes_service.getProkesList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getProkesListById = (0,toolkit_.createAsyncThunk)("prokesList/getProkesListById", async (id, thunkAPI) => {
  const data = await prokes_service.getProkesListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createProkesList = (0,toolkit_.createAsyncThunk)("prokesList/createProkesList", async (result, thunkAPI) => {
  const data = await prokes_service.createProkesList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getProkesList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateProkesList = (0,toolkit_.createAsyncThunk)("prokesList/updateProkesList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await prokes_service.updateProkesList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getProkesList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteProkesList = (0,toolkit_.createAsyncThunk)("prokesList/deleteProkesList", async (id, thunkAPI) => {
  const data = await prokes_service.deleteProkesList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getProkesList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const prokesSlice = (0,toolkit_.createSlice)({
  name: "prokes",
  initialState,
  extraReducers: {
    [getProkes.fulfilled]: (state, action) => {
      state.prokes = action.payload;
    },
    [getProkes.rejected]: (state, action) => {
      state.prokes = null;
    },
    [getProkesById.fulfilled]: (state, action) => {// state.prokes = action.payload;
    },
    [getProkesById.rejected]: (state, action) => {// state.prokes = null;
    },
    [createProkes.fulfilled]: (state, action) => {// state.prokes = action.payload.prokes;
    },
    [createProkes.rejected]: (state, action) => {// state.prokes = null;
    },
    [updateProkes.fulfilled]: (state, action) => {// state.prokesUpdate = action.payload;
    },
    [updateProkes.rejected]: (state, action) => {// state.prokes = null;
    },
    [deleteProkes.fulfilled]: (state, action) => {// state.prokes = action.payload.prokes;
    },
    [deleteProkes.rejected]: (state, action) => {// state.prokes = null;
    },
    [getProkesList.fulfilled]: (state, action) => {
      state.prokesList = action.payload;
    },
    [getProkesList.rejected]: (state, action) => {
      state.prokesList = null;
    },
    [getProkesListById.fulfilled]: (state, action) => {// state.prokesList = action.payload;
    },
    [getProkesListById.rejected]: (state, action) => {// state.prokesList = null;
    },
    [createProkesList.fulfilled]: (state, action) => {// state.prokesList = action.payload.prokesList;
    },
    [createProkesList.rejected]: (state, action) => {// state.prokesList = null;
    },
    [updateProkesList.fulfilled]: (state, action) => {// state.prokesListUpdate = action.payload;
    },
    [updateProkesList.rejected]: (state, action) => {// state.prokesList = null;
    },
    [deleteProkesList.fulfilled]: (state, action) => {// state.prokesList = action.payload.prokesList;
    },
    [deleteProkesList.rejected]: (state, action) => {// state.prokesList = null;
    }
  }
});
const {
  reducer
} = prokesSlice;
/* harmony default export */ const prokes_prokesSlice = (reducer);

/***/ })

};
;