"use strict";
exports.id = 7336;
exports.ids = [7336];
exports.modules = {

/***/ 7336:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "qr": () => (/* binding */ createBackground),
  "ZP": () => (/* binding */ background_backgroundSlice),
  "e2": () => (/* binding */ deleteBackground),
  "p5": () => (/* binding */ getBackgroundById),
  "Ne": () => (/* binding */ getBackgroundByInvitationId),
  "Io": () => (/* binding */ updateBackground)
});

// UNUSED EXPORTS: getBackground

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/background.service.js


class BackgroundService {
  getBackground() {
    return api/* default.get */.Z.get("/background");
  }

  getBackgroundById(id) {
    return api/* default.get */.Z.get("/background/" + id);
  }

  getBackgroundByInvitationId(id) {
    return api/* default.get */.Z.get("/background-invitation/" + id);
  }

  createBackground(data) {
    return api/* default.post */.Z.post("/background", data).then(response => {
      return response.data;
    });
  }

  updateBackground(id, result) {
    return api/* default.patch */.Z.patch("/background/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteBackground(id) {
    return api/* default.delete */.Z["delete"]("/background/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const background_service = (new BackgroundService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/background/backgroundSlice.js




const initialState = {
  background: null
};
const getBackground = (0,toolkit_.createAsyncThunk)("background/getBackground", async (_, thunkAPI) => {
  const data = await background_service.getBackground().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getBackgroundById = (0,toolkit_.createAsyncThunk)("background/getBackgroundById", async (id, thunkAPI) => {
  const data = await background_service.getBackgroundById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getBackgroundByInvitationId = (0,toolkit_.createAsyncThunk)("background/getBackgroundByInvitationId", async (id, thunkAPI) => {
  const data = await background_service.getBackgroundByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createBackground = (0,toolkit_.createAsyncThunk)("background/createBackground", async (result, thunkAPI) => {
  const data = await background_service.createBackground(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateBackground = (0,toolkit_.createAsyncThunk)("background/updateBackground", async ({
  id,
  result
}, thunkAPI) => {
  const data = await background_service.updateBackground(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteBackground = (0,toolkit_.createAsyncThunk)("background/deleteBackground", async (id, thunkAPI) => {
  const data = await background_service.deleteBackground(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const backgroundSlice = (0,toolkit_.createSlice)({
  name: "background",
  initialState,
  extraReducers: {
    [getBackground.fulfilled]: (state, action) => {// state.background = action.payload;
    },
    [getBackground.rejected]: (state, action) => {// state.background = null;
    },
    [getBackgroundByInvitationId.fulfilled]: (state, action) => {
      state.background = action.payload;
    },
    [getBackgroundByInvitationId.rejected]: (state, action) => {
      state.background = null;
    },
    [getBackgroundById.fulfilled]: (state, action) => {// state.background = action.payload;
    },
    [getBackgroundById.rejected]: (state, action) => {// state.background = null;
    },
    [createBackground.fulfilled]: (state, action) => {// state.background = action.payload.background;
    },
    [createBackground.rejected]: (state, action) => {// state.background = null;
    },
    [updateBackground.fulfilled]: (state, action) => {// state.backgroundUpdate = action.payload;
    },
    [updateBackground.rejected]: (state, action) => {// state.background = null;
    },
    [deleteBackground.fulfilled]: (state, action) => {// state.background = action.payload.background;
    },
    [deleteBackground.rejected]: (state, action) => {// state.background = null;
    }
  }
});
const {
  reducer
} = backgroundSlice;
/* harmony default export */ const background_backgroundSlice = (reducer);

/***/ })

};
;