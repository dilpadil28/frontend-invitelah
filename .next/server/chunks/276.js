"use strict";
exports.id = 276;
exports.ids = [276];
exports.modules = {

/***/ 276:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "FH": () => (/* binding */ createMyThemeList),
  "ZP": () => (/* binding */ myTheme_myThemeSlice),
  "XY": () => (/* binding */ deleteMyThemeList),
  "_Z": () => (/* binding */ getMyTheme),
  "$D": () => (/* binding */ getMyThemeById),
  "x4": () => (/* binding */ getMyThemeList),
  "Mz": () => (/* binding */ getMyThemeListById),
  "Py": () => (/* binding */ updateMyTheme),
  "vo": () => (/* binding */ updateMyThemeList)
});

// UNUSED EXPORTS: createMyTheme, deleteMyTheme

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/myTheme.service.js


class MyThemeService {
  getMyTheme() {
    return api/* default.get */.Z.get("/mytheme");
  }

  getMyThemeById(id) {
    return api/* default.get */.Z.get("/mytheme/" + id);
  }

  createMyTheme(data) {
    return api/* default.post */.Z.post("/mytheme", data).then(response => {
      return response.data;
    });
  }

  updateMyTheme(id, result) {
    return api/* default.patch */.Z.patch("/mytheme/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMyTheme(id) {
    return api/* default.delete */.Z["delete"]("/mytheme/" + id).then(response => {
      return response.data;
    });
  }

  getMyThemeList() {
    return api/* default.get */.Z.get("/mythemelist");
  }

  getMyThemeListById(id) {
    return api/* default.get */.Z.get("/mythemelist/" + id);
  }

  createMyThemeList(data) {
    return api/* default.post */.Z.post("/mythemelist", data).then(response => {
      return response.data;
    });
  }

  updateMyThemeList(id, result) {
    return api/* default.patch */.Z.patch("/mythemelist/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMyThemeList(id) {
    return api/* default.delete */.Z["delete"]("/mythemelist/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const myTheme_service = (new MyThemeService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/myTheme/myThemeSlice.js




const initialState = {
  myTheme: null,
  myThemeList: []
};
const getMyTheme = (0,toolkit_.createAsyncThunk)("myTheme/getMyTheme", async (_, thunkAPI) => {
  const data = await myTheme_service.getMyTheme().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMyThemeById = (0,toolkit_.createAsyncThunk)("myTheme/getMyThemeById", async (id, thunkAPI) => {
  const data = await myTheme_service.getMyThemeById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMyTheme = (0,toolkit_.createAsyncThunk)("myTheme/createMyTheme", async (result, thunkAPI) => {
  const data = await myTheme_service.createMyTheme(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMyTheme());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMyTheme = (0,toolkit_.createAsyncThunk)("myTheme/updateMyTheme", async ({
  id,
  result
}, thunkAPI) => {
  const data = await myTheme_service.updateMyTheme(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMyTheme());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMyTheme = (0,toolkit_.createAsyncThunk)("myTheme/deleteMyTheme", async (id, thunkAPI) => {
  const data = await myTheme_service.deleteMyTheme(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMyTheme());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMyThemeList = (0,toolkit_.createAsyncThunk)("myThemeList/getMyThemeList", async (_, thunkAPI) => {
  const data = await myTheme_service.getMyThemeList().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMyThemeListById = (0,toolkit_.createAsyncThunk)("myThemeList/getMyThemeListById", async (id, thunkAPI) => {
  const data = await myTheme_service.getMyThemeListById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMyThemeList = (0,toolkit_.createAsyncThunk)("myThemeList/createMyThemeList", async (result, thunkAPI) => {
  const data = await myTheme_service.createMyThemeList(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMyThemeList());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMyThemeList = (0,toolkit_.createAsyncThunk)("myThemeList/updateMyThemeList", async ({
  id,
  result
}, thunkAPI) => {
  const data = await myTheme_service.updateMyThemeList(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMyThemeList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMyThemeList = (0,toolkit_.createAsyncThunk)("myThemeList/deleteMyThemeList", async (id, thunkAPI) => {
  const data = await myTheme_service.deleteMyThemeList(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMyThemeList());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const myThemeSlice = (0,toolkit_.createSlice)({
  name: "myTheme",
  initialState,
  extraReducers: {
    [getMyTheme.fulfilled]: (state, action) => {
      state.myTheme = action.payload;
    },
    [getMyTheme.rejected]: (state, action) => {
      state.myTheme = null;
    },
    [getMyThemeById.fulfilled]: (state, action) => {// state.myTheme = action.payload;
    },
    [getMyThemeById.rejected]: (state, action) => {// state.myTheme = null;
    },
    [createMyTheme.fulfilled]: (state, action) => {// state.myTheme = action.payload.myTheme;
    },
    [createMyTheme.rejected]: (state, action) => {// state.myTheme = null;
    },
    [updateMyTheme.fulfilled]: (state, action) => {// state.myThemeUpdate = action.payload;
    },
    [updateMyTheme.rejected]: (state, action) => {// state.myTheme = null;
    },
    [deleteMyTheme.fulfilled]: (state, action) => {// state.myTheme = action.payload.myTheme;
    },
    [deleteMyTheme.rejected]: (state, action) => {// state.myTheme = null;
    },
    [getMyThemeList.fulfilled]: (state, action) => {
      state.myThemeList = action.payload;
    },
    [getMyThemeList.rejected]: (state, action) => {
      state.myThemeList = null;
    },
    [getMyThemeListById.fulfilled]: (state, action) => {// state.myThemeList = action.payload;
    },
    [getMyThemeListById.rejected]: (state, action) => {// state.myThemeList = null;
    },
    [createMyThemeList.fulfilled]: (state, action) => {// state.myThemeList = action.payload.myThemeList;
    },
    [createMyThemeList.rejected]: (state, action) => {// state.myThemeList = null;
    },
    [updateMyThemeList.fulfilled]: (state, action) => {// state.myThemeListUpdate = action.payload;
    },
    [updateMyThemeList.rejected]: (state, action) => {// state.myThemeList = null;
    },
    [deleteMyThemeList.fulfilled]: (state, action) => {// state.myThemeList = action.payload.myThemeList;
    },
    [deleteMyThemeList.rejected]: (state, action) => {// state.myThemeList = null;
    }
  }
});
const {
  reducer
} = myThemeSlice;
/* harmony default export */ const myTheme_myThemeSlice = (reducer);

/***/ })

};
;