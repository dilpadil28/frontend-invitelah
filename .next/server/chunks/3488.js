"use strict";
exports.id = 3488;
exports.ids = [3488];
exports.modules = {

/***/ 3488:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "jG": () => (/* binding */ createTheme),
  "ZP": () => (/* binding */ themeSlice),
  "fR": () => (/* binding */ deleteTheme),
  "jY": () => (/* binding */ getThemeById),
  "ey": () => (/* binding */ getThemeByInvitationId),
  "xz": () => (/* binding */ updateTheme)
});

// UNUSED EXPORTS: getTheme

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/theme.service.js


class ThemeService {
  getTheme() {
    return api/* default.get */.Z.get("/theme");
  }

  getThemeById(id) {
    return api/* default.get */.Z.get("/theme/" + id);
  }

  getThemeByInvitationId(id) {
    return api/* default.get */.Z.get("/theme-invitation/" + id);
  }

  createTheme(data) {
    return api/* default.post */.Z.post("/theme", data).then(response => {
      return response.data;
    });
  }

  updateTheme(id, result) {
    return api/* default.patch */.Z.patch("/theme/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteTheme(id) {
    return api/* default.delete */.Z["delete"]("/theme/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const theme_service = (new ThemeService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/theme/themeSlice.js




const initialState = {
  theme: null
};
const getTheme = (0,toolkit_.createAsyncThunk)("theme/getTheme", async (_, thunkAPI) => {
  const data = await theme_service.getTheme().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getThemeById = (0,toolkit_.createAsyncThunk)("theme/getThemeById", async (id, thunkAPI) => {
  const data = await theme_service.getThemeById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getThemeByInvitationId = (0,toolkit_.createAsyncThunk)("theme/getThemeByInvitationId", async (id, thunkAPI) => {
  const data = await theme_service.getThemeByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createTheme = (0,toolkit_.createAsyncThunk)("theme/createTheme", async (result, thunkAPI) => {
  const data = await theme_service.createTheme(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateTheme = (0,toolkit_.createAsyncThunk)("theme/updateTheme", async ({
  id,
  result
}, thunkAPI) => {
  const data = await theme_service.updateTheme(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteTheme = (0,toolkit_.createAsyncThunk)("theme/deleteTheme", async (id, thunkAPI) => {
  const data = await theme_service.deleteTheme(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const backgroundSlice = (0,toolkit_.createSlice)({
  name: "theme",
  initialState,
  extraReducers: {
    [getTheme.fulfilled]: (state, action) => {// state.theme = action.payload;
    },
    [getTheme.rejected]: (state, action) => {// state.theme = null;
    },
    [getThemeByInvitationId.fulfilled]: (state, action) => {
      state.theme = action.payload;
    },
    [getThemeByInvitationId.rejected]: (state, action) => {
      state.theme = null;
    },
    [getThemeById.fulfilled]: (state, action) => {// state.theme = action.payload;
    },
    [getThemeById.rejected]: (state, action) => {// state.theme = null;
    },
    [createTheme.fulfilled]: (state, action) => {// state.theme = action.payload.theme;
    },
    [createTheme.rejected]: (state, action) => {// state.theme = null;
    },
    [updateTheme.fulfilled]: (state, action) => {// state.backgroundUpdate = action.payload;
    },
    [updateTheme.rejected]: (state, action) => {// state.theme = null;
    },
    [deleteTheme.fulfilled]: (state, action) => {// state.theme = action.payload.theme;
    },
    [deleteTheme.rejected]: (state, action) => {// state.theme = null;
    }
  }
});
const {
  reducer
} = backgroundSlice;
/* harmony default export */ const themeSlice = (reducer);

/***/ })

};
;