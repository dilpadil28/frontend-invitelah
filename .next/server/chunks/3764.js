"use strict";
exports.id = 3764;
exports.ids = [3764];
exports.modules = {

/***/ 3764:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "mf": () => (/* binding */ createSocialMedia),
  "ZP": () => (/* binding */ socialMedia_socialMediaSlice),
  "er": () => (/* binding */ deleteSocialMedia),
  "Lv": () => (/* binding */ getSocialMediaById),
  "wM": () => (/* binding */ getSocialMediaByInvitationId),
  "tu": () => (/* binding */ updateSocialMedia)
});

// UNUSED EXPORTS: getSocialMedia

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/socialMedia.service.js


class SocialMediaService {
  getSocialMedia() {
    return api/* default.get */.Z.get("/social-media");
  }

  getSocialMediaById(id) {
    return api/* default.get */.Z.get("/social-media/" + id);
  }

  getSocialMediaByInvitationId(id) {
    return api/* default.get */.Z.get("/social-media-invitation/" + id);
  }

  createSocialMedia(data) {
    return api/* default.post */.Z.post("/social-media", data).then(response => {
      return response.data;
    });
  }

  updateSocialMedia(id, result) {
    return api/* default.patch */.Z.patch("/social-media/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteSocialMedia(id) {
    return api/* default.delete */.Z["delete"]("/social-media/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const socialMedia_service = (new SocialMediaService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/socialMedia/socialMediaSlice.js




const initialState = {
  socialMedia: null
};
const getSocialMedia = (0,toolkit_.createAsyncThunk)("socialMedia/getSocialMedia", async (_, thunkAPI) => {
  const data = await socialMedia_service.getSocialMedia().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getSocialMediaById = (0,toolkit_.createAsyncThunk)("socialMedia/getSocialMediaById", async (id, thunkAPI) => {
  const data = await socialMedia_service.getSocialMediaById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getSocialMediaByInvitationId = (0,toolkit_.createAsyncThunk)("socialMedia/getSocialMediaByInvitationId", async (id, thunkAPI) => {
  const data = await socialMedia_service.getSocialMediaByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createSocialMedia = (0,toolkit_.createAsyncThunk)("socialMedia/createSocialMedia", async (result, thunkAPI) => {
  const data = await socialMedia_service.createSocialMedia(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateSocialMedia = (0,toolkit_.createAsyncThunk)("socialMedia/updateSocialMedia", async ({
  id,
  result
}, thunkAPI) => {
  const data = await socialMedia_service.updateSocialMedia(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteSocialMedia = (0,toolkit_.createAsyncThunk)("socialMedia/deleteSocialMedia", async (id, thunkAPI) => {
  const data = await socialMedia_service.deleteSocialMedia(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const socialMediaSlice = (0,toolkit_.createSlice)({
  name: "socialMedia",
  initialState,
  extraReducers: {
    [getSocialMedia.fulfilled]: (state, action) => {// state.socialMedia = action.payload;
    },
    [getSocialMedia.rejected]: (state, action) => {// state.socialMedia = null;
    },
    [getSocialMediaByInvitationId.fulfilled]: (state, action) => {
      state.socialMedia = action.payload;
    },
    [getSocialMediaByInvitationId.rejected]: (state, action) => {
      state.socialMedia = null;
    },
    [getSocialMediaById.fulfilled]: (state, action) => {// state.socialMedia = action.payload;
    },
    [getSocialMediaById.rejected]: (state, action) => {// state.socialMedia = null;
    },
    [createSocialMedia.fulfilled]: (state, action) => {// state.socialMedia = action.payload.socialMedia;
    },
    [createSocialMedia.rejected]: (state, action) => {// state.socialMedia = null;
    },
    [updateSocialMedia.fulfilled]: (state, action) => {// state.socialMediaUpdate = action.payload;
    },
    [updateSocialMedia.rejected]: (state, action) => {// state.socialMedia = null;
    },
    [deleteSocialMedia.fulfilled]: (state, action) => {// state.socialMedia = action.payload.socialMedia;
    },
    [deleteSocialMedia.rejected]: (state, action) => {// state.socialMedia = null;
    }
  }
});
const {
  reducer
} = socialMediaSlice;
/* harmony default export */ const socialMedia_socialMediaSlice = (reducer);

/***/ })

};
;