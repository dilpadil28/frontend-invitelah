"use strict";
exports.id = 8922;
exports.ids = [8922];
exports.modules = {

/***/ 8922:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "lk": () => (/* binding */ createMusic),
  "ZP": () => (/* binding */ music_musicSlice),
  "_B": () => (/* binding */ deleteMusic),
  "dZ": () => (/* binding */ getMusic),
  "eU": () => (/* binding */ getMusicById),
  "RC": () => (/* binding */ updateMusic)
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/music.service.js


class MusicService {
  getMusic() {
    return api/* default.get */.Z.get("/music");
  }

  getMusicById(id) {
    return api/* default.get */.Z.get("/music/" + id);
  }

  createMusic(data) {
    return api/* default.post */.Z.post("/music", data).then(response => {
      return response.data;
    });
  }

  updateMusic(id, result) {
    return api/* default.patch */.Z.patch("/music/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteMusic(id) {
    return api/* default.delete */.Z["delete"]("/music/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const music_service = (new MusicService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/music/musicSlice.js




const initialState = {
  music: null
};
const getMusic = (0,toolkit_.createAsyncThunk)("music/getMusic", async (_, thunkAPI) => {
  const data = await music_service.getMusic().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getMusicById = (0,toolkit_.createAsyncThunk)("music/getMusicById", async (id, thunkAPI) => {
  const data = await music_service.getMusicById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createMusic = (0,toolkit_.createAsyncThunk)("music/createMusic", async (result, thunkAPI) => {
  const data = await music_service.createMusic(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMusic());
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateMusic = (0,toolkit_.createAsyncThunk)("music/updateMusic", async ({
  id,
  result
}, thunkAPI) => {
  const data = await music_service.updateMusic(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMusic());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteMusic = (0,toolkit_.createAsyncThunk)("music/deleteMusic", async (id, thunkAPI) => {
  const data = await music_service.deleteMusic(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    thunkAPI.dispatch(getMusic());
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const musicSlice = (0,toolkit_.createSlice)({
  name: "music",
  initialState,
  extraReducers: {
    [getMusic.fulfilled]: (state, action) => {
      state.music = action.payload;
    },
    [getMusic.rejected]: (state, action) => {
      state.music = null;
    },
    [getMusicById.fulfilled]: (state, action) => {// state.music = action.payload;
    },
    [getMusicById.rejected]: (state, action) => {// state.music = null;
    },
    [createMusic.fulfilled]: (state, action) => {// state.music = action.payload.music;
    },
    [createMusic.rejected]: (state, action) => {// state.music = null;
    },
    [updateMusic.fulfilled]: (state, action) => {// state.musicUpdate = action.payload;
    },
    [updateMusic.rejected]: (state, action) => {// state.music = null;
    },
    [deleteMusic.fulfilled]: (state, action) => {// state.music = action.payload.music;
    },
    [deleteMusic.rejected]: (state, action) => {// state.music = null;
    }
  }
});
const {
  reducer
} = musicSlice;
/* harmony default export */ const music_musicSlice = (reducer);

/***/ })

};
;