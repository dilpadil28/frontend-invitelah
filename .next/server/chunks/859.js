"use strict";
exports.id = 859;
exports.ids = [859];
exports.modules = {

/***/ 859:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Crypto)
});

// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/admin/Crypto/PageHeader.js





function PageHeader() {
  const user = {
    name: "Catherine Pike",
    avatar: "/static/images/avatars/1.jpg"
  };
  const theme = (0,styles_.useTheme)();
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
    container: true,
    alignItems: "center",
    children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
      item: true,
      children: /*#__PURE__*/jsx_runtime_.jsx(material_.Avatar, {
        sx: {
          mr: 2,
          width: theme.spacing(8),
          height: theme.spacing(8)
        },
        variant: "rounded",
        alt: user.name,
        src: user.avatar
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
      item: true,
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Typography, {
        variant: "h5",
        gutterBottom: true,
        children: ["Welcome, ", user.name, "!"]
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
        variant: "subtitle2",
        children: "Today is a good day to start trading crypto assets!"
      })]
    })]
  });
}

/* harmony default export */ const Crypto_PageHeader = (PageHeader);
// EXTERNAL MODULE: ./components/admin/PageTitleWrapper/index.js
var PageTitleWrapper = __webpack_require__(3358);
// EXTERNAL MODULE: ./components/admin/Footer/index.js
var Footer = __webpack_require__(5376);
// EXTERNAL MODULE: external "@mui/icons-material/TrendingUp"
var TrendingUp_ = __webpack_require__(7067);
var TrendingUp_default = /*#__PURE__*/__webpack_require__.n(TrendingUp_);
// EXTERNAL MODULE: external "react-chartjs-2"
var external_react_chartjs_2_ = __webpack_require__(7051);
;// CONCATENATED MODULE: ./components/admin/Crypto/AccountBalanceChart.js
const _excluded = ["data"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const AccountBalanceChart = _ref => {
  let {
    data: dataProp
  } = _ref,
      rest = _objectWithoutProperties(_ref, _excluded);

  const theme = (0,material_.useTheme)();
  const data = {
    datasets: dataProp.datasets.map(dataset => _objectSpread(_objectSpread({}, dataset), {}, {
      borderWidth: 10,
      borderColor: theme.palette.alpha.white[100],
      hoverBorderColor: theme.palette.alpha.white[30]
    })),
    labels: dataProp.labels
  };
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    animation: false,
    cutoutPercentage: 60,
    legend: {
      display: false
    },
    layout: {
      padding: 0
    },
    tooltips: {
      enabled: true,
      caretSize: 6,
      displayColors: false,
      mode: "label",
      intersect: true,
      yPadding: 8,
      xPadding: 16,
      borderWidth: 2,
      bodySpacing: 10,
      borderColor: theme.palette.alpha.black[30],
      backgroundColor: theme.palette.common.white,
      titleFontColor: theme.palette.common.black,
      bodyFontColor: theme.palette.common.black,
      footerFontColor: theme.palette.common.black,
      callbacks: {
        label(tooltipItem, _data) {
          const label = _data.labels[tooltipItem.index];
          const value = _data.datasets[0].data[tooltipItem.index];
          return `${label}: ${value}%`;
        }

      }
    }
  };
  return /*#__PURE__*/jsx_runtime_.jsx(external_react_chartjs_2_.Doughnut, _objectSpread({
    data: data,
    options: options
  }, rest));
};

/* harmony default export */ const Crypto_AccountBalanceChart = (AccountBalanceChart);
// EXTERNAL MODULE: ./components/admin/Text/index.js
var admin_Text = __webpack_require__(9297);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
;// CONCATENATED MODULE: ./components/admin/Crypto/AccountBalance.js








const AccountBalanceChartWrapper = (0,styles_.styled)(Crypto_AccountBalanceChart)(() => `
      width: 100%;
      height: 100%;
`);
const AvatarSuccess = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
      background-color: ${theme.palette.success.main};
      color: ${theme.palette.success.contrastText};
      width: ${theme.spacing(8)};
      height: ${theme.spacing(8)};
      box-shadow: ${theme.palette.shadows.success};
`);

function AccountBalance() {
  const cryptoBalance = {
    datasets: [{
      data: [20, 10, 40, 30],
      backgroundColor: ["#ff9900", "#1c81c2", "#333", "#5c6ac0"]
    }],
    labels: ["Bitcoin", "Ripple", "Cardano", "Ethereum"]
  };
  return /*#__PURE__*/jsx_runtime_.jsx(material_.Card, {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
      spacing: 0,
      container: true,
      children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        item: true,
        xs: 12,
        md: 6,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
          p: 4,
          children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
            sx: {
              pb: 3
            },
            variant: "h4",
            children: "Account Balance"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "h1",
              gutterBottom: true,
              children: "$54,584.23"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "h4",
              fontWeight: "normal",
              color: "text.secondary",
              children: "1.0045983485234 BTC"
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
              display: "flex",
              sx: {
                py: 4
              },
              alignItems: "center",
              children: [/*#__PURE__*/jsx_runtime_.jsx(AvatarSuccess, {
                sx: {
                  mr: 2
                },
                variant: "rounded",
                children: /*#__PURE__*/jsx_runtime_.jsx((TrendingUp_default()), {
                  fontSize: "large"
                })
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "h4",
                  children: "+ $3,594.00"
                }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                  variant: "subtitle2",
                  noWrap: true,
                  children: "this month"
                })]
              })]
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
            container: true,
            spacing: 3,
            children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
              sm: true,
              item: true,
              children: /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
                fullWidth: true,
                variant: "outlined",
                children: "Send"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
              sm: true,
              item: true,
              children: /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
                className: "!bg-primary",
                fullWidth: true,
                variant: "contained",
                children: "Receive"
              })
            })]
          })]
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
        sx: {
          position: "relative"
        },
        display: "flex",
        alignItems: "center",
        item: true,
        xs: 12,
        md: 6,
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Hidden, {
          mdDown: true,
          children: /*#__PURE__*/jsx_runtime_.jsx(material_.Divider, {
            absolute: true,
            orientation: "vertical"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.Box, {
          p: 4,
          flex: 1,
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
            container: true,
            spacing: 0,
            children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
              xs: 12,
              sm: 5,
              item: true,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              children: /*#__PURE__*/jsx_runtime_.jsx(material_.Box, {
                style: {
                  height: "160px"
                },
                children: /*#__PURE__*/jsx_runtime_.jsx(AccountBalanceChartWrapper, {
                  data: cryptoBalance
                })
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
              xs: 12,
              sm: 7,
              item: true,
              display: "flex",
              alignItems: "center",
              children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.List, {
                disablePadding: true,
                sx: {
                  width: "100%"
                },
                children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
                  disableGutters: true,
                  children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
                    sx: {
                      minWidth: "46px",
                      display: "flex",
                      alignItems: "center"
                    },
                    children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                      width: 42,
                      height: 42,
                      alt: "BTC",
                      src: "/static/images/placeholders/logo/bitcoin.png"
                    })
                  }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
                    primary: "BTC",
                    primaryTypographyProps: {
                      variant: "h5",
                      noWrap: true
                    },
                    secondary: "Bitcoin",
                    secondaryTypographyProps: {
                      variant: "subtitle2",
                      noWrap: true
                    }
                  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
                    children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                      align: "right",
                      variant: "h4",
                      noWrap: true,
                      children: "20%"
                    }), /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
                      color: "success",
                      children: "+2.54%"
                    })]
                  })]
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
                  disableGutters: true,
                  children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
                    sx: {
                      minWidth: "46px",
                      display: "flex",
                      alignItems: "center"
                    },
                    children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                      width: 42,
                      height: 42,
                      alt: "XRP",
                      src: "/static/images/placeholders/logo/ripple.png"
                    })
                  }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
                    primary: "XRP",
                    primaryTypographyProps: {
                      variant: "h5",
                      noWrap: true
                    },
                    secondary: "Ripple",
                    secondaryTypographyProps: {
                      variant: "subtitle2",
                      noWrap: true
                    }
                  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
                    children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                      align: "right",
                      variant: "h4",
                      noWrap: true,
                      children: "10%"
                    }), /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
                      color: "error",
                      children: "-1.22%"
                    })]
                  })]
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
                  disableGutters: true,
                  children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
                    sx: {
                      minWidth: "46px",
                      display: "flex",
                      alignItems: "center"
                    },
                    children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                      width: 42,
                      height: 42,
                      alt: "ADA",
                      src: "/static/images/placeholders/logo/cardano.png"
                    })
                  }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
                    primary: "ADA",
                    primaryTypographyProps: {
                      variant: "h5",
                      noWrap: true
                    },
                    secondary: "Cardano",
                    secondaryTypographyProps: {
                      variant: "subtitle2",
                      noWrap: true
                    }
                  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
                    children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                      align: "right",
                      variant: "h4",
                      noWrap: true,
                      children: "40%"
                    }), /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
                      color: "success",
                      children: "+10.50%"
                    })]
                  })]
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
                  disableGutters: true,
                  children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
                    sx: {
                      minWidth: "46px",
                      display: "flex",
                      alignItems: "center"
                    },
                    children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                      width: 42,
                      height: 42,
                      alt: "ETH",
                      src: "/static/images/placeholders/logo/ethereum.png"
                    })
                  }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
                    primary: "ETH",
                    primaryTypographyProps: {
                      variant: "h5",
                      noWrap: true
                    },
                    secondary: "Ethereum",
                    secondaryTypographyProps: {
                      variant: "subtitle2",
                      noWrap: true
                    }
                  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
                    children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                      align: "right",
                      variant: "h4",
                      noWrap: true,
                      children: "30%"
                    }), /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
                      color: "error",
                      children: "-12.38%"
                    })]
                  })]
                })]
              })
            })]
          })
        })]
      })]
    })
  });
}

/* harmony default export */ const Crypto_AccountBalance = (AccountBalance);
// EXTERNAL MODULE: external "@mui/icons-material/AddTwoTone"
var AddTwoTone_ = __webpack_require__(1750);
var AddTwoTone_default = /*#__PURE__*/__webpack_require__.n(AddTwoTone_);
;// CONCATENATED MODULE: ./components/admin/Crypto/Wallets.js







const AvatarWrapper = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
        background: transparent;
        margin-left: -${theme.spacing(0.5)};
        margin-bottom: ${theme.spacing(1)};
        margin-top: ${theme.spacing(2)};
`);
const AvatarAddWrapper = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
        background: ${theme.palette.alpha.black[5]};
        color: ${theme.palette.primary.main};
        width: ${theme.spacing(8)};
        height: ${theme.spacing(8)};
`);
const CardAddAction = (0,styles_.styled)(material_.Card)(({
  theme
}) => `
        border: ${theme.palette.primary.main} dashed 1px;
        height: 100%;
        color: ${theme.palette.primary.main};
        
        .MuiCardActionArea-root {
          height: 100%;
          justify-content: center;
          align-items: center;
          display: flex;
        }
        
        .MuiTouchRipple-root {
          opacity: .2;
        }
        
        &:hover {
          border-color: ${theme.palette.alpha.black[100]};
        }
`);

function Wallets() {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      sx: {
        pb: 3
      },
      children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
        variant: "h3",
        children: "Wallets"
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
        size: "small",
        variant: "outlined",
        startIcon: /*#__PURE__*/jsx_runtime_.jsx((AddTwoTone_default()), {
          fontSize: "small"
        }),
        children: "Add new wallet"
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
      container: true,
      spacing: 3,
      children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        xs: 12,
        sm: 6,
        md: 3,
        item: true,
        children: /*#__PURE__*/jsx_runtime_.jsx(material_.Card, {
          sx: {
            px: 1
          },
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.CardContent, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(AvatarWrapper, {
              children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                width: 42,
                height: 42,
                alt: "BTC",
                src: "/static/images/placeholders/logo/bitcoin.png"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "h5",
              noWrap: true,
              children: "Bitcoin"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "subtitle1",
              noWrap: true,
              children: "BTC"
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
              sx: {
                pt: 3
              },
              children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                variant: "h5",
                gutterBottom: true,
                noWrap: true,
                children: "$3,586.22"
              }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                variant: "subtitle2",
                noWrap: true,
                children: "1.25843 BTC"
              })]
            })]
          })
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        xs: 12,
        sm: 6,
        md: 3,
        item: true,
        children: /*#__PURE__*/jsx_runtime_.jsx(material_.Card, {
          sx: {
            px: 1
          },
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.CardContent, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(AvatarWrapper, {
              children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                width: 42,
                height: 42,
                alt: "Ripple",
                src: "/static/images/placeholders/logo/ripple.png"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "h5",
              noWrap: true,
              children: "Ripple"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "subtitle1",
              noWrap: true,
              children: "XRP"
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
              sx: {
                pt: 3
              },
              children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                variant: "h5",
                gutterBottom: true,
                noWrap: true,
                children: "$586.83"
              }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                variant: "subtitle2",
                noWrap: true,
                children: "5,783 XRP"
              })]
            })]
          })
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        xs: 12,
        sm: 6,
        md: 3,
        item: true,
        children: /*#__PURE__*/jsx_runtime_.jsx(material_.Card, {
          sx: {
            px: 1
          },
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.CardContent, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(AvatarWrapper, {
              children: /*#__PURE__*/jsx_runtime_.jsx(next_image["default"], {
                width: 42,
                height: 42,
                alt: "Cardano",
                src: "/static/images/placeholders/logo/cardano.png"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "h5",
              noWrap: true,
              children: "Cardano"
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              variant: "subtitle1",
              noWrap: true,
              children: "ADA"
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Box, {
              sx: {
                pt: 3
              },
              children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                variant: "h5",
                gutterBottom: true,
                noWrap: true,
                children: "$54,985.00"
              }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
                variant: "subtitle2",
                noWrap: true,
                children: "34,985 ADA"
              })]
            })]
          })
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        xs: 12,
        sm: 6,
        md: 3,
        item: true,
        children: /*#__PURE__*/jsx_runtime_.jsx(material_.Tooltip, {
          arrow: true,
          title: "Click to add a new wallet",
          children: /*#__PURE__*/jsx_runtime_.jsx(CardAddAction, {
            children: /*#__PURE__*/jsx_runtime_.jsx(material_.CardActionArea, {
              sx: {
                px: 1
              },
              children: /*#__PURE__*/jsx_runtime_.jsx(material_.CardContent, {
                children: /*#__PURE__*/jsx_runtime_.jsx(AvatarAddWrapper, {
                  children: /*#__PURE__*/jsx_runtime_.jsx((AddTwoTone_default()), {
                    fontSize: "large"
                  })
                })
              })
            })
          })
        })
      })]
    })]
  });
}

/* harmony default export */ const Crypto_Wallets = (Wallets);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@mui/icons-material/LockTwoTone"
var LockTwoTone_ = __webpack_require__(2814);
var LockTwoTone_default = /*#__PURE__*/__webpack_require__.n(LockTwoTone_);
// EXTERNAL MODULE: external "@mui/icons-material/PhoneLockedTwoTone"
var PhoneLockedTwoTone_ = __webpack_require__(8696);
var PhoneLockedTwoTone_default = /*#__PURE__*/__webpack_require__.n(PhoneLockedTwoTone_);
// EXTERNAL MODULE: external "@mui/icons-material/EmailTwoTone"
var EmailTwoTone_ = __webpack_require__(9714);
var EmailTwoTone_default = /*#__PURE__*/__webpack_require__.n(EmailTwoTone_);
;// CONCATENATED MODULE: ./components/admin/Crypto/AccountSecurity.js









const AvatarWrapperError = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
      background-color: ${theme.palette.error.lighter};
      color:  ${theme.palette.error.main};
`);
const AvatarWrapperSuccess = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
      background-color: ${theme.palette.success.lighter};
      color:  ${theme.palette.success.main};
`);
const AvatarWrapperWarning = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
      background-color: ${theme.palette.warning.lighter};
      color:  ${theme.palette.warning.main};
`);

function AccountSecurity() {
  const {
    0: checked,
    1: setChecked
  } = (0,external_react_.useState)(["phone_verification"]);

  const handleToggle = value => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Card, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(material_.CardHeader, {
      title: "Account Security"
    }), /*#__PURE__*/jsx_runtime_.jsx(material_.Divider, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.List, {
      disablePadding: true,
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
        sx: {
          py: 2
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
          children: /*#__PURE__*/jsx_runtime_.jsx(AvatarWrapperError, {
            children: /*#__PURE__*/jsx_runtime_.jsx((LockTwoTone_default()), {
              fontSize: "medium"
            })
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
          primary: /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
            color: "black",
            children: "2FA Authentication"
          }),
          primaryTypographyProps: {
            variant: "body1",
            fontWeight: "bold",
            color: "textPrimary",
            gutterBottom: true,
            noWrap: true
          },
          secondary: /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
            color: "error",
            children: "Disabled"
          }),
          secondaryTypographyProps: {
            variant: "body2",
            noWrap: true
          }
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.Switch, {
          edge: "end",
          color: "primary",
          onChange: handleToggle("2fa"),
          checked: checked.indexOf("2fa") !== -1
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Divider, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
        sx: {
          py: 2
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
          children: /*#__PURE__*/jsx_runtime_.jsx(AvatarWrapperSuccess, {
            children: /*#__PURE__*/jsx_runtime_.jsx((PhoneLockedTwoTone_default()), {
              fontSize: "medium"
            })
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
          primary: /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
            color: "black",
            children: "Phone Verification"
          }),
          primaryTypographyProps: {
            variant: "body1",
            fontWeight: "bold",
            color: "textPrimary",
            gutterBottom: true,
            noWrap: true
          },
          secondary: /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
            color: "success",
            children: "Active"
          }),
          secondaryTypographyProps: {
            variant: "body2",
            noWrap: true
          }
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.Switch, {
          edge: "end",
          color: "primary",
          onChange: handleToggle("phone_verification"),
          checked: checked.indexOf("phone_verification") !== -1
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Divider, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
        sx: {
          py: 2
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemAvatar, {
          children: /*#__PURE__*/jsx_runtime_.jsx(AvatarWrapperWarning, {
            children: /*#__PURE__*/jsx_runtime_.jsx((EmailTwoTone_default()), {
              fontSize: "medium"
            })
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
          primary: /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
            color: "black",
            children: "Recovery Email"
          }),
          primaryTypographyProps: {
            variant: "body1",
            fontWeight: "bold",
            color: "textPrimary",
            gutterBottom: true,
            noWrap: true
          },
          secondary: /*#__PURE__*/jsx_runtime_.jsx(admin_Text/* default */.Z, {
            color: "warning",
            children: "Not completed"
          }),
          secondaryTypographyProps: {
            variant: "body2",
            noWrap: true
          }
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.Switch, {
          edge: "end",
          color: "primary",
          onChange: handleToggle("recovery_email"),
          checked: checked.indexOf("recovery_email") !== -1
        })]
      })]
    })]
  });
}

/* harmony default export */ const Crypto_AccountSecurity = (AccountSecurity);
// EXTERNAL MODULE: external "@mui/icons-material/ViewWeekTwoTone"
var ViewWeekTwoTone_ = __webpack_require__(4732);
// EXTERNAL MODULE: external "@mui/icons-material/TableRowsTwoTone"
var TableRowsTwoTone_ = __webpack_require__(2923);
// EXTERNAL MODULE: ./components/admin/Label/index.js
var admin_Label = __webpack_require__(5743);
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchListColumn1Chart.js
const WatchListColumn1Chart_excluded = ["data", "labels"];

function WatchListColumn1Chart_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function WatchListColumn1Chart_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { WatchListColumn1Chart_ownKeys(Object(source), true).forEach(function (key) { WatchListColumn1Chart_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { WatchListColumn1Chart_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function WatchListColumn1Chart_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function WatchListColumn1Chart_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = WatchListColumn1Chart_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function WatchListColumn1Chart_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const WatchListColumn1Chart = _ref => {
  let {
    data: dataProp,
    labels
  } = _ref,
      rest = WatchListColumn1Chart_objectWithoutProperties(_ref, WatchListColumn1Chart_excluded);

  const theme = (0,material_.useTheme)();

  const data = canvas => {
    const ctx = canvas.getContext('2d');
    const primaryGradient = ctx.createLinearGradient(6, 6, 6, 150);
    primaryGradient.addColorStop(0, (0,material_.alpha)(theme.colors.primary.light, 0.8));
    primaryGradient.addColorStop(0.8, theme.colors.alpha.white[10]);
    primaryGradient.addColorStop(1, theme.colors.alpha.white[100]);
    return {
      datasets: [{
        data: dataProp,
        borderWidth: 1,
        backgroundColor: primaryGradient,
        borderColor: theme.colors.primary.main,
        pointBorderWidth: 0,
        pointRadius: 0,
        pointHoverRadius: 0
      }],
      labels
    };
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    layout: {
      padding: 0
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          display: false
        }
      }],
      yAxes: [{
        gridLines: {
          display: false
        },
        ticks: {
          display: false
        }
      }]
    },
    tooltips: {
      enabled: true,
      mode: 'nearest',
      intersect: false,
      caretSize: 6,
      displayColors: false,
      yPadding: 8,
      xPadding: 16,
      borderWidth: 4,
      borderColor: theme.palette.common.black,
      backgroundColor: theme.palette.common.black,
      titleFontColor: theme.palette.common.white,
      bodyFontColor: theme.palette.common.white,
      footerFontColor: theme.palette.common.white,
      callbacks: {
        title: () => {},
        label: tooltipItem => {
          return `Price: $${tooltipItem.yLabel}`;
        }
      }
    }
  };
  return /*#__PURE__*/jsx_runtime_.jsx("div", WatchListColumn1Chart_objectSpread(WatchListColumn1Chart_objectSpread({}, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx(external_react_chartjs_2_.Line, {
      data: data,
      options: options
    })
  }));
};

/* harmony default export */ const Crypto_WatchListColumn1Chart = (WatchListColumn1Chart);
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchListColumn1.js







const WatchListColumn1_AvatarWrapper = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
        background: transparent;
        margin-right: ${theme.spacing(0.5)};
`);
const WatchListColumn1ChartWrapper = (0,styles_.styled)(Crypto_WatchListColumn1Chart)(({
  theme
}) => `
        height: 130px;
`);

function WatchListColumn1_WatchListColumn1() {
  const price = {
    week: {
      labels: ["Monday", "Tueday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
      data: [55.701, 57.598, 48.607, 46.439, 58.755, 46.978, 58.16]
    }
  };
  return /*#__PURE__*/_jsxs(Card, {
    children: [/*#__PURE__*/_jsxs(Box, {
      sx: {
        p: 3
      },
      children: [/*#__PURE__*/_jsxs(Box, {
        display: "flex",
        alignItems: "center",
        children: [/*#__PURE__*/_jsx(WatchListColumn1_AvatarWrapper, {
          children: /*#__PURE__*/_jsx("img", {
            alt: "BTC",
            src: "/static/images/placeholders/logo/bitcoin.png"
          })
        }), /*#__PURE__*/_jsxs(Box, {
          children: [/*#__PURE__*/_jsx(Typography, {
            variant: "h4",
            noWrap: true,
            children: "Bitcoin"
          }), /*#__PURE__*/_jsx(Typography, {
            variant: "subtitle1",
            noWrap: true,
            children: "BTC"
          })]
        })]
      }), /*#__PURE__*/_jsxs(Box, {
        sx: {
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          pt: 3
        },
        children: [/*#__PURE__*/_jsx(Typography, {
          variant: "h2",
          sx: {
            pr: 1,
            mb: 1
          },
          children: "$56,475.99"
        }), /*#__PURE__*/_jsx(Text, {
          color: "success",
          children: /*#__PURE__*/_jsx("b", {
            children: "+12.5%"
          })
        })]
      }), /*#__PURE__*/_jsxs(Box, {
        sx: {
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start"
        },
        children: [/*#__PURE__*/_jsx(Label, {
          color: "success",
          children: "+$500"
        }), /*#__PURE__*/_jsx(Typography, {
          variant: "body2",
          color: "text.secondary",
          sx: {
            pl: 1
          },
          children: "last 24h"
        })]
      })]
    }), /*#__PURE__*/_jsx(Box, {
      height: 130,
      sx: {
        ml: -1.5
      },
      children: /*#__PURE__*/_jsx(WatchListColumn1ChartWrapper, {
        data: price.week.data,
        labels: price.week.labels
      })
    })]
  });
}

/* harmony default export */ const Crypto_WatchListColumn1 = ((/* unused pure expression or super */ null && (WatchListColumn1_WatchListColumn1)));
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchListColumn2.js







const WatchListColumn2_AvatarWrapper = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
        background: transparent;
        margin-right: ${theme.spacing(0.5)};
`);
const WatchListColumn2_WatchListColumn1ChartWrapper = (0,styles_.styled)(Crypto_WatchListColumn1Chart)(() => `
        height: 130px;
`);

function WatchListColumn2_WatchListColumn2() {
  const price = {
    week: {
      labels: ["Monday", "Tueday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
      data: [1.854, 1.773, 2.092, 2.009, 1.909, 1.842, 1.884]
    }
  };
  return /*#__PURE__*/_jsxs(Card, {
    children: [/*#__PURE__*/_jsxs(Box, {
      sx: {
        p: 3
      },
      children: [/*#__PURE__*/_jsxs(Box, {
        display: "flex",
        alignItems: "center",
        children: [/*#__PURE__*/_jsx(WatchListColumn2_AvatarWrapper, {
          children: /*#__PURE__*/_jsx("img", {
            alt: "ETH",
            src: "/static/images/placeholders/logo/ethereum.png"
          })
        }), /*#__PURE__*/_jsxs(Box, {
          children: [/*#__PURE__*/_jsx(Typography, {
            variant: "h4",
            noWrap: true,
            children: "Ethereum"
          }), /*#__PURE__*/_jsx(Typography, {
            variant: "subtitle1",
            noWrap: true,
            children: "ETH"
          })]
        })]
      }), /*#__PURE__*/_jsxs(Box, {
        sx: {
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          pt: 3
        },
        children: [/*#__PURE__*/_jsx(Typography, {
          variant: "h2",
          sx: {
            pr: 1,
            mb: 1
          },
          children: "$1,968.00"
        }), /*#__PURE__*/_jsx(Text, {
          color: "error",
          children: /*#__PURE__*/_jsx("b", {
            children: "-3.24%"
          })
        })]
      }), /*#__PURE__*/_jsxs(Box, {
        sx: {
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start"
        },
        children: [/*#__PURE__*/_jsx(Label, {
          color: "error",
          children: "-$90"
        }), /*#__PURE__*/_jsx(Typography, {
          variant: "body2",
          color: "text.secondary",
          sx: {
            pl: 1
          },
          children: "last 24h"
        })]
      })]
    }), /*#__PURE__*/_jsx(Box, {
      height: 130,
      sx: {
        ml: -1.5
      },
      children: /*#__PURE__*/_jsx(WatchListColumn2_WatchListColumn1ChartWrapper, {
        data: price.week.data,
        labels: price.week.labels
      })
    })]
  });
}

/* harmony default export */ const Crypto_WatchListColumn2 = ((/* unused pure expression or super */ null && (WatchListColumn2_WatchListColumn2)));
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchListColumn3.js







const WatchListColumn3_AvatarWrapper = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
        background: transparent;
        margin-right: ${theme.spacing(0.5)};
`);
const WatchListColumn3_WatchListColumn1ChartWrapper = (0,styles_.styled)(Crypto_WatchListColumn1Chart)(({
  theme
}) => `
        height: 130px;
`);

function WatchListColumn3_WatchListColumn3() {
  const price = {
    week: {
      labels: ["Monday", "Tueday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
      data: [13, 16, 14, 21, 8, 11, 20]
    }
  };
  return /*#__PURE__*/_jsxs(Card, {
    children: [/*#__PURE__*/_jsxs(Box, {
      sx: {
        p: 3
      },
      children: [/*#__PURE__*/_jsxs(Box, {
        display: "flex",
        alignItems: "center",
        children: [/*#__PURE__*/_jsx(WatchListColumn3_AvatarWrapper, {
          children: /*#__PURE__*/_jsx("img", {
            alt: "ADA",
            src: "/static/images/placeholders/logo/cardano.png"
          })
        }), /*#__PURE__*/_jsxs(Box, {
          children: [/*#__PURE__*/_jsx(Typography, {
            variant: "h4",
            noWrap: true,
            children: "Cardano"
          }), /*#__PURE__*/_jsx(Typography, {
            variant: "subtitle1",
            noWrap: true,
            children: "ADA"
          })]
        })]
      }), /*#__PURE__*/_jsxs(Box, {
        sx: {
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          pt: 3
        },
        children: [/*#__PURE__*/_jsx(Typography, {
          variant: "h2",
          sx: {
            pr: 1,
            mb: 1
          },
          children: "$23.00"
        }), /*#__PURE__*/_jsx(Text, {
          color: "error",
          children: /*#__PURE__*/_jsx("b", {
            children: "-0.33%"
          })
        })]
      }), /*#__PURE__*/_jsxs(Box, {
        sx: {
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start"
        },
        children: [/*#__PURE__*/_jsx(Label, {
          color: "error",
          children: "-$5"
        }), /*#__PURE__*/_jsx(Typography, {
          variant: "body2",
          color: "text.secondary",
          sx: {
            pl: 1
          },
          children: "last 24h"
        })]
      })]
    }), /*#__PURE__*/_jsx(Box, {
      height: 130,
      sx: {
        ml: -1.5
      },
      children: /*#__PURE__*/_jsx(WatchListColumn3_WatchListColumn1ChartWrapper, {
        data: price.week.data,
        labels: price.week.labels
      })
    })]
  });
}

/* harmony default export */ const Crypto_WatchListColumn3 = ((/* unused pure expression or super */ null && (WatchListColumn3_WatchListColumn3)));
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchListRowChart.js
const WatchListRowChart_excluded = ["data", "labels"];

function WatchListRowChart_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function WatchListRowChart_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { WatchListRowChart_ownKeys(Object(source), true).forEach(function (key) { WatchListRowChart_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { WatchListRowChart_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function WatchListRowChart_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function WatchListRowChart_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = WatchListRowChart_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function WatchListRowChart_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const WatchListRowChart = _ref => {
  let {
    data: dataProp,
    labels
  } = _ref,
      rest = WatchListRowChart_objectWithoutProperties(_ref, WatchListRowChart_excluded);

  const theme = (0,material_.useTheme)();

  const data = () => {
    return {
      datasets: [{
        data: dataProp,
        borderWidth: 3,
        backgroundColor: 'transparent',
        borderColor: theme.colors.primary.main,
        pointBorderWidth: 0,
        pointRadius: 0,
        pointHoverRadius: 0
      }],
      labels
    };
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    layout: {
      padding: 5
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          display: false
        }
      }],
      yAxes: [{
        gridLines: {
          display: false
        },
        ticks: {
          display: false
        }
      }]
    },
    tooltips: {
      enabled: true,
      mode: 'nearest',
      intersect: false,
      caretSize: 6,
      displayColors: false,
      yPadding: 8,
      xPadding: 16,
      borderWidth: 4,
      borderColor: theme.palette.common.black,
      backgroundColor: theme.palette.common.black,
      titleFontColor: theme.palette.common.white,
      bodyFontColor: theme.palette.common.white,
      footerFontColor: theme.palette.common.white,
      callbacks: {
        title: () => {},
        label: tooltipItem => {
          return `Price: $${tooltipItem.yLabel}`;
        }
      }
    }
  };
  return /*#__PURE__*/jsx_runtime_.jsx("div", WatchListRowChart_objectSpread(WatchListRowChart_objectSpread({}, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx(external_react_chartjs_2_.Line, {
      data: data,
      options: options
    })
  }));
};

/* harmony default export */ const Crypto_WatchListRowChart = (WatchListRowChart);
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchListRow.js







const WatchListRow_AvatarWrapper = (0,styles_.styled)(material_.Avatar)(({
  theme
}) => `
        background: transparent;
        margin-right: ${theme.spacing(0.5)};
`);
const LabelWrapper = (0,styles_.styled)(material_.Box)(({
  theme
}) => `
        position: absolute;
        right: ${theme.spacing(2)};
        top: ${theme.spacing(2)};
`);
const WatchListRowChartWrapper = (0,styles_.styled)(Crypto_WatchListRowChart)(({
  theme
}) => `
        height: 100px;
`);

function WatchListRow_WatchListRow() {
  const price = {
    week: {
      labels: ["Monday", "Tueday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
      bitcoin: [55.701, 57.598, 48.607, 46.439, 58.755, 46.978, 58.16],
      ethereum: [1.854, 1.773, 2.092, 2.009, 1.909, 1.842, 1.884],
      cardano: [13, 16, 14, 21, 8, 11, 20]
    }
  };
  return /*#__PURE__*/_jsxs(Card, {
    children: [/*#__PURE__*/_jsxs(Grid, {
      container: true,
      spacing: 0,
      alignItems: "center",
      children: [/*#__PURE__*/_jsx(Grid, {
        xs: 12,
        md: true,
        item: true,
        sx: {
          position: "relative"
        },
        children: /*#__PURE__*/_jsxs(Box, {
          sx: {
            px: 3,
            pt: 3
          },
          children: [/*#__PURE__*/_jsx(LabelWrapper, {
            children: /*#__PURE__*/_jsx(Label, {
              color: "secondary",
              children: "24h"
            })
          }), /*#__PURE__*/_jsxs(Box, {
            display: "flex",
            alignItems: "center",
            children: [/*#__PURE__*/_jsx(WatchListRow_AvatarWrapper, {
              children: /*#__PURE__*/_jsx("img", {
                alt: "BTC",
                src: "/static/images/placeholders/logo/bitcoin.png"
              })
            }), /*#__PURE__*/_jsxs(Box, {
              children: [/*#__PURE__*/_jsx(Typography, {
                variant: "h4",
                noWrap: true,
                children: "Bitcoin"
              }), /*#__PURE__*/_jsx(Typography, {
                variant: "subtitle1",
                noWrap: true,
                children: "BTC"
              })]
            })]
          }), /*#__PURE__*/_jsxs(Box, {
            sx: {
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-start",
              pt: 3
            },
            children: [/*#__PURE__*/_jsx(Typography, {
              variant: "h2",
              sx: {
                pr: 1,
                mb: 1
              },
              children: "$56,475.99"
            }), /*#__PURE__*/_jsx(Text, {
              color: "success",
              children: /*#__PURE__*/_jsx("b", {
                children: "+12.5%"
              })
            })]
          }), /*#__PURE__*/_jsx(Box, {
            height: 100,
            sx: {
              ml: -1.5
            },
            children: /*#__PURE__*/_jsx(WatchListRowChartWrapper, {
              data: price.week.bitcoin,
              labels: price.week.labels
            })
          })]
        })
      }), /*#__PURE__*/_jsx(Divider, {
        orientation: "vertical",
        flexItem: true
      }), /*#__PURE__*/_jsx(Grid, {
        xs: 12,
        md: true,
        item: true,
        sx: {
          position: "relative"
        },
        children: /*#__PURE__*/_jsxs(Box, {
          sx: {
            px: 3,
            pt: 3
          },
          children: [/*#__PURE__*/_jsx(LabelWrapper, {
            children: /*#__PURE__*/_jsx(Label, {
              color: "secondary",
              children: "24h"
            })
          }), /*#__PURE__*/_jsxs(Box, {
            display: "flex",
            alignItems: "center",
            children: [/*#__PURE__*/_jsx(WatchListRow_AvatarWrapper, {
              children: /*#__PURE__*/_jsx("img", {
                alt: "ETH",
                src: "/static/images/placeholders/logo/ethereum.png"
              })
            }), /*#__PURE__*/_jsxs(Box, {
              children: [/*#__PURE__*/_jsx(Typography, {
                variant: "h4",
                noWrap: true,
                children: "Ethereum"
              }), /*#__PURE__*/_jsx(Typography, {
                variant: "subtitle1",
                noWrap: true,
                children: "ETH"
              })]
            })]
          }), /*#__PURE__*/_jsxs(Box, {
            sx: {
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-start",
              pt: 3
            },
            children: [/*#__PURE__*/_jsx(Typography, {
              variant: "h2",
              sx: {
                pr: 1,
                mb: 1
              },
              children: "$1,968.00"
            }), /*#__PURE__*/_jsx(Text, {
              color: "error",
              children: /*#__PURE__*/_jsx("b", {
                children: "-3.24%"
              })
            })]
          }), /*#__PURE__*/_jsx(Box, {
            height: 100,
            sx: {
              ml: -1.5
            },
            children: /*#__PURE__*/_jsx(WatchListRowChartWrapper, {
              data: price.week.ethereum,
              labels: price.week.labels
            })
          })]
        })
      }), /*#__PURE__*/_jsx(Divider, {
        orientation: "vertical",
        flexItem: true
      }), /*#__PURE__*/_jsx(Grid, {
        xs: 12,
        md: true,
        item: true,
        sx: {
          position: "relative"
        },
        children: /*#__PURE__*/_jsxs(Box, {
          sx: {
            px: 3,
            pt: 3
          },
          children: [/*#__PURE__*/_jsx(LabelWrapper, {
            children: /*#__PURE__*/_jsx(Label, {
              color: "secondary",
              children: "24h"
            })
          }), /*#__PURE__*/_jsxs(Box, {
            display: "flex",
            alignItems: "center",
            children: [/*#__PURE__*/_jsx(WatchListRow_AvatarWrapper, {
              children: /*#__PURE__*/_jsx("img", {
                alt: "ADA",
                src: "/static/images/placeholders/logo/cardano.png"
              })
            }), /*#__PURE__*/_jsxs(Box, {
              children: [/*#__PURE__*/_jsx(Typography, {
                variant: "h4",
                noWrap: true,
                children: "Cardano"
              }), /*#__PURE__*/_jsx(Typography, {
                variant: "subtitle1",
                noWrap: true,
                children: "ADA"
              })]
            })]
          }), /*#__PURE__*/_jsxs(Box, {
            sx: {
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-start",
              pt: 3
            },
            children: [/*#__PURE__*/_jsx(Typography, {
              variant: "h2",
              sx: {
                pr: 1,
                mb: 1
              },
              children: "$23.00"
            }), /*#__PURE__*/_jsx(Text, {
              color: "error",
              children: /*#__PURE__*/_jsx("b", {
                children: "-0.33%"
              })
            })]
          }), /*#__PURE__*/_jsx(Box, {
            height: 100,
            sx: {
              ml: -1.5
            },
            children: /*#__PURE__*/_jsx(WatchListRowChartWrapper, {
              data: price.week.cardano,
              labels: price.week.labels
            })
          })]
        })
      })]
    }), /*#__PURE__*/_jsx(Divider, {}), /*#__PURE__*/_jsx(CardActions, {
      disableSpacing: true,
      sx: {
        p: 3,
        display: "flex",
        justifyContent: "center"
      },
      children: /*#__PURE__*/_jsx(Button, {
        variant: "outlined",
        children: "View more assets"
      })
    })]
  });
}

/* harmony default export */ const Crypto_WatchListRow = ((/* unused pure expression or super */ null && (WatchListRow_WatchListRow)));
;// CONCATENATED MODULE: ./components/admin/Crypto/WatchList.js












const EmptyResultsWrapper = (0,styles_.styled)('img')(({
  theme
}) => `
      max-width: 100%;
      width: ${theme.spacing(66)};
      height: ${theme.spacing(34)};
`);

function WatchList() {
  const {
    0: tabs,
    1: setTab
  } = useState('watch_list_columns');

  const handleViewOrientation = (event, newValue) => {
    setTab(newValue);
  };

  return /*#__PURE__*/_jsxs(_Fragment, {
    children: [/*#__PURE__*/_jsxs(Box, {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      sx: {
        pb: 3
      },
      children: [/*#__PURE__*/_jsx(Typography, {
        variant: "h3",
        children: "Watch List"
      }), /*#__PURE__*/_jsxs(ToggleButtonGroup, {
        value: tabs,
        exclusive: true,
        onChange: handleViewOrientation,
        children: [/*#__PURE__*/_jsx(ToggleButton, {
          disableRipple: true,
          value: "watch_list_columns",
          children: /*#__PURE__*/_jsx(ViewWeekTwoToneIcon, {})
        }), /*#__PURE__*/_jsx(ToggleButton, {
          disableRipple: true,
          value: "watch_list_rows",
          children: /*#__PURE__*/_jsx(TableRowsTwoToneIcon, {})
        })]
      })]
    }), /*#__PURE__*/_jsxs(Grid, {
      container: true,
      direction: "row",
      justifyContent: "center",
      alignItems: "stretch",
      spacing: 3,
      children: [tabs === 'watch_list_columns' && /*#__PURE__*/_jsxs(_Fragment, {
        children: [/*#__PURE__*/_jsx(Grid, {
          item: true,
          lg: 4,
          xs: 12,
          children: /*#__PURE__*/_jsx(WatchListColumn1, {})
        }), /*#__PURE__*/_jsx(Grid, {
          item: true,
          lg: 4,
          xs: 12,
          children: /*#__PURE__*/_jsx(WatchListColumn2, {})
        }), /*#__PURE__*/_jsx(Grid, {
          item: true,
          lg: 4,
          xs: 12,
          children: /*#__PURE__*/_jsx(WatchListColumn3, {})
        })]
      }), tabs === 'watch_list_rows' && /*#__PURE__*/_jsx(Grid, {
        item: true,
        xs: 12,
        children: /*#__PURE__*/_jsx(WatchListRow, {})
      }), !tabs && /*#__PURE__*/_jsx(Grid, {
        item: true,
        xs: 12,
        children: /*#__PURE__*/_jsxs(Card, {
          sx: {
            textAlign: 'center',
            p: 3
          },
          children: [/*#__PURE__*/_jsx(EmptyResultsWrapper, {
            src: "/static/images/placeholders/illustrations/1.svg"
          }), /*#__PURE__*/_jsx(Typography, {
            align: "center",
            variant: "h2",
            fontWeight: "normal",
            color: "text.secondary",
            sx: {
              mt: 3
            },
            gutterBottom: true,
            children: "Click something, anything!"
          }), /*#__PURE__*/_jsx(Button, {
            variant: "contained",
            size: "large",
            sx: {
              mt: 4
            },
            children: "Maybe, a button?"
          })]
        })
      })]
    })]
  });
}

/* harmony default export */ const Crypto_WatchList = ((/* unused pure expression or super */ null && (WatchList)));
// EXTERNAL MODULE: external "chart.js"
var external_chart_js_ = __webpack_require__(4467);
;// CONCATENATED MODULE: ./components/admin/Crypto/index.js












external_chart_js_.Chart.register(external_chart_js_.ArcElement);

function DashboardCrypto() {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(PageTitleWrapper/* default */.Z, {
      children: /*#__PURE__*/jsx_runtime_.jsx(Crypto_PageHeader, {})
    }), /*#__PURE__*/jsx_runtime_.jsx(material_.Container, {
      maxWidth: "lg",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
        container: true,
        direction: "row",
        justifyContent: "center",
        alignItems: "stretch",
        spacing: 3,
        children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
          item: true,
          xs: 12,
          children: /*#__PURE__*/jsx_runtime_.jsx(Crypto_AccountBalance, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
          item: true,
          lg: 8,
          xs: 12,
          children: /*#__PURE__*/jsx_runtime_.jsx(Crypto_Wallets, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
          item: true,
          lg: 4,
          xs: 12,
          children: /*#__PURE__*/jsx_runtime_.jsx(Crypto_AccountSecurity, {})
        })]
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(Footer/* default */.Z, {})]
  });
}

/* harmony default export */ const Crypto = (DashboardCrypto);

/***/ }),

/***/ 5376:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__);




const FooterWrapper = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_1__.styled)(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box)(({
  theme
}) => `
        border-radius: 0;
        margin: ${theme.spacing(3)} 0;
`);

function Footer() {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(FooterWrapper, {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Container, {
      maxWidth: "lg",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box, {
        py: 3,
        display: {
          xs: 'block',
          md: 'flex'
        },
        alignItems: "center",
        textAlign: {
          xs: 'center',
          md: 'left'
        },
        justifyContent: "space-between",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box, {
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Typography, {
            variant: "subtitle1",
            children: "\xA9 2021 - Tokyo Free White React Javascript Admin Dashboard"
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Typography, {
          sx: {
            pt: {
              xs: 2,
              md: 0
            }
          },
          variant: "subtitle1",
          children: ["Crafted by ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Link, {
            href: "https://bloomui.com",
            target: "_blank",
            rel: "noopener noreferrer",
            children: "BloomUI.com"
          })]
        })]
      })
    })
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Footer);

/***/ }),

/***/ 3358:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__);





const PageTitle = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_1__.styled)(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box)(({
  theme
}) => `
        padding: ${theme.spacing(4, 0)};
`);

const PageTitleWrapper = ({
  children
}) => {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.Fragment, {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(PageTitle, {
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Container, {
        maxWidth: "lg",
        children: children
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PageTitleWrapper);

/***/ }),

/***/ 9297:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);
const _excluded = ["className", "color", "children"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



const TextWrapper = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__.styled)('span')(({
  theme
}) => `
      
      &.MuiText {

        &-black {
          color: ${theme.palette.common.black}
        }

        &-primary {
          color: ${theme.palette.primary.main}
        }
        
        &-secondary {
          color: ${theme.palette.secondary.main}
        }
        
        &-success {
          color: ${theme.palette.success.main}
        }
        
        &-warning {
          color: ${theme.palette.warning.main}
        }
              
        &-error {
          color: ${theme.palette.error.main}
        }
        
        &-info {
          color: ${theme.palette.info.main}
        }
      }
`);

const Text = _ref => {
  let {
    className = '',
    color = 'secondary',
    children
  } = _ref,
      rest = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx(TextWrapper, _objectSpread(_objectSpread({
    className: 'MuiText-' + color
  }, rest), {}, {
    children: children
  }));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Text);

/***/ })

};
;