"use strict";
exports.id = 5642;
exports.ids = [5642];
exports.modules = {

/***/ 5642:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "h": () => (/* binding */ api)
/* harmony export */ });
const api = {
  home: "https://invitelah.com",
  // baseUrl: "http://localhost:8080/api/v1",
  baseUrl: "https://api.invitelah.com/api/v1",
  fileUrl: "https://api.invitelah.com/",
  // fileUrl: "http://localhost:8080/",
  facebook: "https://www.facebook.com/Invitelah-110379041532885",
  instagram: "https://www.instagram.com/invitelah",
  tiktok: "https://www.tiktok.com/@invitelah.com",
  youtube: "https://www.youtube.com/channel/UCyPaVUABfAzH1-bDqUvDkhw",
  bukalapak: "https://www.bukalapak.com/u/invitelah_475467",
  tokopedia: "https://www.tokopedia.com/invitelah",
  shopee: "https://shopee.co.id/invitelah",
  whatsappGold: "https://api.whatsapp.com/send?phone=6285171113314&text=Assalamualaikum%20warahmatullahi%20wabarakatuh%20Saya%20ingin%20memesan%20undangan%20digital%20yang%20paket%20Gold.%20Terimakasih",
  whatsappSilver: "https://api.whatsapp.com/send?phone=6285171113314&text=Assalamualaikum%20warahmatullahi%20wabarakatuh%20Saya%20ingin%20memesan%20undangan%20digital%20yang%20paket%20Silver.%20Terimakasih",
  whatsappDiamond: "https://api.whatsapp.com/send?phone=6285171113314&text=Assalamualaikum%20warahmatullahi%20wabarakatuh%20Saya%20ingin%20memesan%20undangan%20digital%20yang%20paket%20Diamond.%20Terimakasih"
};

/***/ })

};
;