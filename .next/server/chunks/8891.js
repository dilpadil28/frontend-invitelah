"use strict";
exports.id = 8891;
exports.ids = [8891];
exports.modules = {

/***/ 8891:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "V": () => (/* binding */ strengthColor),
/* harmony export */   "X": () => (/* binding */ strengthIndicator)
/* harmony export */ });
/* harmony import */ var _assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4317);
/* harmony import */ var _assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0__);
/**
 * Password validator for login pages
 */
 // has number

const hasNumber = number => new RegExp(/[0-9]/).test(number); // has mix of small and capitals


const hasMixed = number => new RegExp(/[a-z]/).test(number) && new RegExp(/[A-Z]/).test(number); // has special chars


const hasSpecial = number => new RegExp(/[!#@$%^&*)(+=._-]/).test(number); // set color based on password strength


const strengthColor = count => {
  if (count < 2) return {
    label: "Poor",
    color: (_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default().errorMain)
  };
  if (count < 3) return {
    label: "Weak",
    color: (_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default().warningDark)
  };
  if (count < 4) return {
    label: "Normal",
    color: (_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default().orangeMain)
  };
  if (count < 5) return {
    label: "Good",
    color: (_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default().successMain)
  };
  if (count < 6) return {
    label: "Strong",
    color: (_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default().successDark)
  };
  return {
    label: "Poor",
    color: (_assets_scss_themes_vars_module_scss__WEBPACK_IMPORTED_MODULE_0___default().errorMain)
  };
}; // password strength indicator

const strengthIndicator = number => {
  let strengths = 0;
  if (number.length > 5) strengths += 1;
  if (number.length > 7) strengths += 1;
  if (hasNumber(number)) strengths += 1;
  if (hasSpecial(number)) strengths += 1;
  if (hasMixed(number)) strengths += 1;
  return strengths;
};

/***/ })

};
;