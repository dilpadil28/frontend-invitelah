"use strict";
exports.id = 350;
exports.ids = [350];
exports.modules = {

/***/ 350:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "$h": () => (/* binding */ createDigitalEnvelope),
  "ZP": () => (/* binding */ digitalEnvelope_digitalEnvelopeSlice),
  "_n": () => (/* binding */ deleteDigitalEnvelope),
  "mr": () => (/* binding */ getDigitalEnvelopeById),
  "Nq": () => (/* binding */ getDigitalEnvelopeByInvitationId),
  "kz": () => (/* binding */ updateDigitalEnvelope)
});

// UNUSED EXPORTS: getDigitalEnvelope

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./common/EventBus.js
var EventBus = __webpack_require__(994);
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
;// CONCATENATED MODULE: ./services/digitalEnvelope.service.js


class DigitalEnvelopeService {
  getDigitalEnvelope() {
    return api/* default.get */.Z.get("/digital-envelope");
  }

  getDigitalEnvelopeById(id) {
    return api/* default.get */.Z.get("/digital-envelope/" + id);
  }

  getDigitalEnvelopeByInvitationId(id) {
    return api/* default.get */.Z.get("/digital-envelope-invitation/" + id);
  }

  createDigitalEnvelope(data) {
    return api/* default.post */.Z.post("/digital-envelope", data).then(response => {
      return response.data;
    });
  }

  updateDigitalEnvelope(id, result) {
    return api/* default.patch */.Z.patch("/digital-envelope/" + id, result).then(response => {
      return response.data;
    });
  }

  deleteDigitalEnvelope(id) {
    return api/* default.delete */.Z["delete"]("/digital-envelope/" + id).then(response => {
      return response.data;
    });
  }

}

/* harmony default export */ const digitalEnvelope_service = (new DigitalEnvelopeService());
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
;// CONCATENATED MODULE: ./features/digitalEnvelope/digitalEnvelopeSlice.js




const initialState = {
  digitalEnvelope: null
};
const getDigitalEnvelope = (0,toolkit_.createAsyncThunk)("digitalEnvelope/getDigitalEnvelope", async (_, thunkAPI) => {
  const data = await digitalEnvelope_service.getDigitalEnvelope().then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getDigitalEnvelopeById = (0,toolkit_.createAsyncThunk)("digitalEnvelope/getDigitalEnvelopeById", async (id, thunkAPI) => {
  const data = await digitalEnvelope_service.getDigitalEnvelopeById(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const getDigitalEnvelopeByInvitationId = (0,toolkit_.createAsyncThunk)("digitalEnvelope/getDigitalEnvelopeByInvitationId", async (id, thunkAPI) => {
  const data = await digitalEnvelope_service.getDigitalEnvelopeByInvitationId(id).then(response => {
    return response.data.data;
  }, error => {
    if (error.response && error.response.status === 403 || error.response.status === 500) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const createDigitalEnvelope = (0,toolkit_.createAsyncThunk)("digitalEnvelope/createDigitalEnvelope", async (result, thunkAPI) => {
  const data = await digitalEnvelope_service.createDigitalEnvelope(result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const updateDigitalEnvelope = (0,toolkit_.createAsyncThunk)("digitalEnvelope/updateDigitalEnvelope", async ({
  id,
  result
}, thunkAPI) => {
  const data = await digitalEnvelope_service.updateDigitalEnvelope(id, result).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message + " " + error.response.data.error[0].msg));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const deleteDigitalEnvelope = (0,toolkit_.createAsyncThunk)("digitalEnvelope/deleteDigitalEnvelope", async (id, thunkAPI) => {
  const data = await digitalEnvelope_service.deleteDigitalEnvelope(id).then(response => {
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(response.message));
    return response.data.data;
  }, error => {
    const message = error.response && error.response.data && error.response.data.message || error.message || error.toString();
    thunkAPI.dispatch((0,messageSlice/* setMessage */.PJ)(message));

    if (error.response && error.response.status === 403) {
      EventBus/* default.dispatch */.Z.dispatch("logout");
    }
  });
  return data;
});
const digitalEnvelopeSlice = (0,toolkit_.createSlice)({
  name: "digitalEnvelope",
  initialState,
  extraReducers: {
    [getDigitalEnvelope.fulfilled]: (state, action) => {// state.digitalEnvelope = action.payload;
    },
    [getDigitalEnvelope.rejected]: (state, action) => {// state.digitalEnvelope = null;
    },
    [getDigitalEnvelopeByInvitationId.fulfilled]: (state, action) => {
      state.digitalEnvelope = action.payload;
    },
    [getDigitalEnvelopeByInvitationId.rejected]: (state, action) => {
      state.digitalEnvelope = null;
    },
    [getDigitalEnvelopeById.fulfilled]: (state, action) => {// state.digitalEnvelope = action.payload;
    },
    [getDigitalEnvelopeById.rejected]: (state, action) => {// state.digitalEnvelope = null;
    },
    [createDigitalEnvelope.fulfilled]: (state, action) => {// state.digitalEnvelope = action.payload.digitalEnvelope;
    },
    [createDigitalEnvelope.rejected]: (state, action) => {// state.digitalEnvelope = null;
    },
    [updateDigitalEnvelope.fulfilled]: (state, action) => {// state.digitalEnvelopeUpdate = action.payload;
    },
    [updateDigitalEnvelope.rejected]: (state, action) => {// state.digitalEnvelope = null;
    },
    [deleteDigitalEnvelope.fulfilled]: (state, action) => {// state.digitalEnvelope = action.payload.digitalEnvelope;
    },
    [deleteDigitalEnvelope.rejected]: (state, action) => {// state.digitalEnvelope = null;
    }
  }
});
const {
  reducer
} = digitalEnvelopeSlice;
/* harmony default export */ const digitalEnvelope_digitalEnvelopeSlice = (reducer);

/***/ })

};
;