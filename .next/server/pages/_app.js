"use strict";
(() => {
var exports = {};
exports.id = 2888;
exports.ids = [2888];
exports.modules = {

/***/ 8793:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/system"
var system_ = __webpack_require__(7986);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
// EXTERNAL MODULE: ./assets/scss/_themes-vars.module.scss
var _themes_vars_module = __webpack_require__(4317);
var _themes_vars_module_default = /*#__PURE__*/__webpack_require__.n(_themes_vars_module);
;// CONCATENATED MODULE: ./themes/compStyleOverride.js
function componentStyleOverrides(theme) {
  var _theme$customization, _theme$colors, _theme$colors2, _theme$colors3, _theme$colors4, _theme$colors5, _theme$colors6;

  // const bgColor = theme.colors?.grey50;
  return {
    MuiButton: {
      styleOverrides: {
        root: {
          fontWeight: 500,
          borderRadius: "4px",
          fontFamily: "Quicksand"
        }
      }
    },
    MuiPaper: {
      defaultProps: {
        elevation: 0
      },
      styleOverrides: {
        root: {
          backgroundImage: "none"
        },
        rounded: {
          borderRadius: `${theme === null || theme === void 0 ? void 0 : (_theme$customization = theme.customization) === null || _theme$customization === void 0 ? void 0 : _theme$customization.borderRadius}px`
        }
      }
    },
    MuiCardHeader: {
      styleOverrides: {
        root: {
          color: (_theme$colors = theme.colors) === null || _theme$colors === void 0 ? void 0 : _theme$colors.textDark,
          padding: "24px"
        },
        title: {
          fontSize: "1.125rem"
        }
      }
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: "24px"
        }
      }
    },
    MuiCardActions: {
      styleOverrides: {
        root: {
          padding: "24px"
        }
      }
    },
    MuiListItemButton: {
      styleOverrides: {
        root: {
          color: theme.darkTextPrimary,
          paddingTop: "10px",
          paddingBottom: "10px",
          "&.Mui-selected": {
            color: theme.menuSelected,
            backgroundColor: theme.menuSelectedBack,
            "&:hover": {
              backgroundColor: theme.menuSelectedBack
            },
            "& .MuiListItemIcon-root": {
              color: theme.menuSelected
            }
          },
          "&:hover": {
            backgroundColor: theme.menuSelectedBack,
            color: theme.menuSelected,
            "& .MuiListItemIcon-root": {
              color: theme.menuSelected
            }
          }
        }
      }
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: {
          color: theme.darkTextPrimary,
          minWidth: "36px"
        }
      }
    },
    MuiListItemText: {
      styleOverrides: {
        primary: {
          color: theme.textDark
        }
      }
    },
    MuiSlider: {
      styleOverrides: {
        root: {
          "&.Mui-disabled": {
            color: (_theme$colors2 = theme.colors) === null || _theme$colors2 === void 0 ? void 0 : _theme$colors2.grey300
          }
        },
        mark: {
          backgroundColor: theme.paper,
          width: "4px"
        },
        valueLabel: {
          color: theme === null || theme === void 0 ? void 0 : (_theme$colors3 = theme.colors) === null || _theme$colors3 === void 0 ? void 0 : _theme$colors3.primaryLight
        }
      }
    },
    MuiDivider: {
      styleOverrides: {
        root: {
          borderColor: theme.divider,
          opacity: 1
        }
      }
    },
    MuiAvatar: {
      styleOverrides: {
        root: {
          color: (_theme$colors4 = theme.colors) === null || _theme$colors4 === void 0 ? void 0 : _theme$colors4.primaryDark,
          background: (_theme$colors5 = theme.colors) === null || _theme$colors5 === void 0 ? void 0 : _theme$colors5.primary200
        }
      }
    },
    MuiChip: {
      styleOverrides: {
        root: {
          "&.MuiChip-deletable .MuiChip-deleteIcon": {
            color: "inherit"
          }
        }
      }
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          color: theme.paper,
          background: (_theme$colors6 = theme.colors) === null || _theme$colors6 === void 0 ? void 0 : _theme$colors6.grey700
        }
      }
    }
  };
}
;// CONCATENATED MODULE: ./themes/palette.js
/**
 * Color intention that you want to used in your theme
 * @param {JsonObject} theme Theme customization object
 */

const themeColors = {
  primary: "#5569ff",
  secondary: "#6E759F",
  success: "#44D600",
  warning: "#FFA319",
  error: "#FF1943",
  info: "#33C2FF",
  black: "#223354",
  white: "#ffffff",
  primaryAlt: "#000C57"
};
function themePalette(theme) {
  var _theme$customization, _theme$colors, _theme$colors2, _theme$colors3, _theme$colors4, _theme$colors5, _theme$colors6, _theme$colors7, _theme$colors8, _theme$colors9, _theme$colors10, _theme$colors11, _theme$colors12, _theme$colors13, _theme$colors14, _theme$colors15, _theme$colors16, _theme$colors17, _theme$colors18, _theme$colors19, _theme$colors20, _theme$colors21, _theme$colors22, _theme$colors23, _theme$colors24, _theme$colors25, _theme$colors26, _theme$colors27, _theme$colors28, _theme$colors29, _theme$colors30, _theme$colors31, _theme$colors32;

  return {
    mode: theme === null || theme === void 0 ? void 0 : (_theme$customization = theme.customization) === null || _theme$customization === void 0 ? void 0 : _theme$customization.navType,
    common: {
      black: (_theme$colors = theme.colors) === null || _theme$colors === void 0 ? void 0 : _theme$colors.darkPaper
    },
    primary: {
      light: (_theme$colors2 = theme.colors) === null || _theme$colors2 === void 0 ? void 0 : _theme$colors2.primaryLight,
      main: (_theme$colors3 = theme.colors) === null || _theme$colors3 === void 0 ? void 0 : _theme$colors3.primaryMain,
      dark: (_theme$colors4 = theme.colors) === null || _theme$colors4 === void 0 ? void 0 : _theme$colors4.primaryDark,
      200: (_theme$colors5 = theme.colors) === null || _theme$colors5 === void 0 ? void 0 : _theme$colors5.primary200,
      800: (_theme$colors6 = theme.colors) === null || _theme$colors6 === void 0 ? void 0 : _theme$colors6.primary800
    },
    secondary: {
      light: (_theme$colors7 = theme.colors) === null || _theme$colors7 === void 0 ? void 0 : _theme$colors7.secondaryLight,
      main: (_theme$colors8 = theme.colors) === null || _theme$colors8 === void 0 ? void 0 : _theme$colors8.secondaryMain,
      dark: (_theme$colors9 = theme.colors) === null || _theme$colors9 === void 0 ? void 0 : _theme$colors9.secondaryDark,
      200: (_theme$colors10 = theme.colors) === null || _theme$colors10 === void 0 ? void 0 : _theme$colors10.secondary200,
      800: (_theme$colors11 = theme.colors) === null || _theme$colors11 === void 0 ? void 0 : _theme$colors11.secondary800
    },
    error: {
      light: (_theme$colors12 = theme.colors) === null || _theme$colors12 === void 0 ? void 0 : _theme$colors12.errorLight,
      main: (_theme$colors13 = theme.colors) === null || _theme$colors13 === void 0 ? void 0 : _theme$colors13.errorMain,
      dark: (_theme$colors14 = theme.colors) === null || _theme$colors14 === void 0 ? void 0 : _theme$colors14.errorDark
    },
    orange: {
      light: (_theme$colors15 = theme.colors) === null || _theme$colors15 === void 0 ? void 0 : _theme$colors15.orangeLight,
      main: (_theme$colors16 = theme.colors) === null || _theme$colors16 === void 0 ? void 0 : _theme$colors16.orangeMain,
      dark: (_theme$colors17 = theme.colors) === null || _theme$colors17 === void 0 ? void 0 : _theme$colors17.orangeDark
    },
    warning: {
      light: (_theme$colors18 = theme.colors) === null || _theme$colors18 === void 0 ? void 0 : _theme$colors18.warningLight,
      main: (_theme$colors19 = theme.colors) === null || _theme$colors19 === void 0 ? void 0 : _theme$colors19.warningMain,
      dark: (_theme$colors20 = theme.colors) === null || _theme$colors20 === void 0 ? void 0 : _theme$colors20.warningDark
    },
    success: {
      light: (_theme$colors21 = theme.colors) === null || _theme$colors21 === void 0 ? void 0 : _theme$colors21.successLight,
      200: (_theme$colors22 = theme.colors) === null || _theme$colors22 === void 0 ? void 0 : _theme$colors22.success200,
      main: (_theme$colors23 = theme.colors) === null || _theme$colors23 === void 0 ? void 0 : _theme$colors23.successMain,
      dark: (_theme$colors24 = theme.colors) === null || _theme$colors24 === void 0 ? void 0 : _theme$colors24.successDark
    },
    grey: {
      50: (_theme$colors25 = theme.colors) === null || _theme$colors25 === void 0 ? void 0 : _theme$colors25.grey50,
      100: (_theme$colors26 = theme.colors) === null || _theme$colors26 === void 0 ? void 0 : _theme$colors26.grey100,
      500: theme.darkTextSecondary,
      600: theme.heading,
      700: theme.darkTextPrimary,
      900: theme.textDark
    },
    dark: {
      light: (_theme$colors27 = theme.colors) === null || _theme$colors27 === void 0 ? void 0 : _theme$colors27.darkTextPrimary,
      main: (_theme$colors28 = theme.colors) === null || _theme$colors28 === void 0 ? void 0 : _theme$colors28.darkLevel1,
      dark: (_theme$colors29 = theme.colors) === null || _theme$colors29 === void 0 ? void 0 : _theme$colors29.darkLevel2,
      800: (_theme$colors30 = theme.colors) === null || _theme$colors30 === void 0 ? void 0 : _theme$colors30.darkBackground,
      900: (_theme$colors31 = theme.colors) === null || _theme$colors31 === void 0 ? void 0 : _theme$colors31.darkPaper
    },
    text: {
      primary: theme.darkTextPrimary,
      secondary: theme.darkTextSecondary,
      dark: theme.textDark,
      hint: (_theme$colors32 = theme.colors) === null || _theme$colors32 === void 0 ? void 0 : _theme$colors32.grey100
    },
    background: {
      paper: theme.paper,
      default: theme.backgroundDefault
    },
    shadows: {
      success: "0px 1px 4px rgba(68, 214, 0, 0.25), 0px 3px 12px 2px rgba(68, 214, 0, 0.35)",
      error: "0px 1px 4px rgba(255, 25, 67, 0.25), 0px 3px 12px 2px rgba(255, 25, 67, 0.35)",
      info: "0px 1px 4px rgba(51, 194, 255, 0.25), 0px 3px 12px 2px rgba(51, 194, 255, 0.35)",
      primary: "0px 1px 4px rgba(85, 105, 255, 0.25), 0px 3px 12px 2px rgba(85, 105, 255, 0.35)",
      warning: "0px 1px 4px rgba(255, 163, 25, 0.25), 0px 3px 12px 2px rgba(255, 163, 25, 0.35)",
      card: "0px 9px 16px rgba(159, 162, 191, 0.18), 0px 2px 2px rgba(159, 162, 191, 0.32)",
      cardSm: "0px 2px 3px rgba(159, 162, 191, 0.18), 0px 1px 1px rgba(159, 162, 191, 0.32)",
      cardLg: "0 5rem 14rem 0 rgb(255 255 255 / 30%), 0 0.8rem 2.3rem rgb(0 0 0 / 60%), 0 0.2rem 0.3rem rgb(0 0 0 / 45%)"
    },
    alpha: {
      white: {
        5: (0,material_.alpha)(themeColors.white, 0.02),
        10: (0,material_.alpha)(themeColors.white, 0.1),
        30: (0,material_.alpha)(themeColors.white, 0.3),
        50: (0,material_.alpha)(themeColors.white, 0.5),
        70: (0,material_.alpha)(themeColors.white, 0.7),
        100: themeColors.white
      },
      trueWhite: {
        5: (0,material_.alpha)(themeColors.white, 0.02),
        10: (0,material_.alpha)(themeColors.white, 0.1),
        30: (0,material_.alpha)(themeColors.white, 0.3),
        50: (0,material_.alpha)(themeColors.white, 0.5),
        70: (0,material_.alpha)(themeColors.white, 0.7),
        100: themeColors.white
      },
      black: {
        5: (0,material_.alpha)(themeColors.black, 0.02),
        10: (0,material_.alpha)(themeColors.black, 0.1),
        30: (0,material_.alpha)(themeColors.black, 0.3),
        50: (0,material_.alpha)(themeColors.black, 0.5),
        70: (0,material_.alpha)(themeColors.black, 0.7),
        100: themeColors.black
      },
      general: {
        reactFrameworkColor: "#00D8FF",
        borderRadiusSm: "4px",
        borderRadius: "6px",
        borderRadiusLg: "10px",
        borderRadiusXl: "18px"
      }
    }
  };
}
;// CONCATENATED MODULE: ./themes/typography.js
/**
 * Typography used in theme
 * @param {JsonObject} theme theme customization object
 */
function themeTypography(theme) {
  var _theme$customization;

  return {
    fontFamily: ["Quicksand", "Alex Brush", "Anonymous Pro", "Bebas Neue", "Forum", "Great Vibes", "Josefin Sans", "Lora", "Montserrat", "Mr Dafoe", "Niconne", "Noto Serif", "Open Sans", "Pacifico", "Petit Formal Script", "Pinyon Script", "Playball", "Playfair Display", "Parisienne", "Allison", "Cabin", "Clicker Script", "Cookie", "Dancing Script", "Euphoria Script", "Hurricane", "Montez", "Petemoss", "Rouge Script", "Sacramento", "Satisfy", "Smooch", "Style Script"].join(","),
    button: {
      textTransform: "capitalize"
    },
    customInput: {
      marginTop: 1,
      marginBottom: 1,
      "& > label": {
        top: 23,
        left: 0,
        color: theme.grey500,
        '&[data-shrink="false"]': {
          top: 5
        }
      },
      "& > div > input": {
        padding: "30.5px 14px 11.5px !important"
      },
      "& legend": {
        display: "none"
      },
      "& fieldset": {
        top: 0
      }
    },
    mainContent: {
      backgroundColor: theme.background,
      width: "100%",
      minHeight: "calc(100vh - 88px)",
      flexGrow: 1,
      padding: "20px",
      marginTop: "88px",
      marginRight: "20px",
      borderRadius: `${theme === null || theme === void 0 ? void 0 : (_theme$customization = theme.customization) === null || _theme$customization === void 0 ? void 0 : _theme$customization.borderRadius}px`
    },
    menuCaption: {
      fontSize: "0.875rem",
      fontWeight: 500,
      color: theme.heading,
      padding: "6px",
      textTransform: "capitalize",
      marginTop: "10px"
    },
    subMenuCaption: {
      fontSize: "0.6875rem",
      fontWeight: 500,
      color: theme.darkTextSecondary,
      textTransform: "capitalize"
    },
    commonAvatar: {
      cursor: "pointer",
      borderRadius: "8px"
    },
    smallAvatar: {
      width: "22px",
      height: "22px",
      fontSize: "1rem"
    },
    mediumAvatar: {
      width: "34px",
      height: "34px",
      fontSize: "1.2rem"
    },
    largeAvatar: {
      width: "44px",
      height: "44px",
      fontSize: "1.5rem"
    }
  };
}
;// CONCATENATED MODULE: ./themes/index.js
 // assets

 // project imports




/**
 * Represent theme style and structure as per Material-UI
 * @param {JsonObject} customization customization parameter object
 */

const theme = () => {
  const color = (_themes_vars_module_default());
  const themeOption = {
    colors: color,
    heading: color.grey900,
    paper: color.paper,
    backgroundDefault: color.paper,
    background: color.primaryLight,
    darkTextPrimary: color.grey700,
    darkTextSecondary: color.grey500,
    textDark: color.grey900,
    menuSelected: color.secondaryDark,
    menuSelectedBack: color.secondaryLight,
    divider: color.grey200
  };
  const themeOptions = {
    direction: "ltr",
    palette: themePalette(themeOption),
    mixins: {
      toolbar: {
        minHeight: "48px",
        padding: "16px",
        "@media (min-width: 600px)": {
          minHeight: "48px"
        }
      }
    },
    typography: themeTypography(themeOption)
  };
  const themes = (0,styles_.createTheme)(themeOptions);
  themes.components = componentStyleOverrides(themeOption);
  return themes;
};
/* harmony default export */ const themes = (theme);
// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: ./features/auth/authSlice.js + 1 modules
var authSlice = __webpack_require__(9336);
// EXTERNAL MODULE: ./features/message/messageSlice.js
var messageSlice = __webpack_require__(2841);
// EXTERNAL MODULE: ./features/user/userSlice.js
var userSlice = __webpack_require__(7987);
// EXTERNAL MODULE: ./features/testimonial/testimonialSlice.js + 1 modules
var testimonialSlice = __webpack_require__(2623);
// EXTERNAL MODULE: ./features/mySocialMedia/mySocialMediaSlice.js + 1 modules
var mySocialMediaSlice = __webpack_require__(3974);
// EXTERNAL MODULE: ./features/myTheme/myThemeSlice.js + 1 modules
var myThemeSlice = __webpack_require__(276);
// EXTERNAL MODULE: ./features/faq/faqSlice.js + 1 modules
var faqSlice = __webpack_require__(2571);
// EXTERNAL MODULE: ./features/mainLanding/mainLandingSlice.js + 1 modules
var mainLandingSlice = __webpack_require__(1706);
// EXTERNAL MODULE: ./features/fitur/fiturSlice.js + 1 modules
var fiturSlice = __webpack_require__(2260);
// EXTERNAL MODULE: ./features/music/musicSlice.js + 1 modules
var musicSlice = __webpack_require__(8922);
// EXTERNAL MODULE: ./features/orderStep/orderStepSlice.js + 1 modules
var orderStepSlice = __webpack_require__(175);
// EXTERNAL MODULE: ./features/prokes/prokesSlice.js + 1 modules
var prokesSlice = __webpack_require__(3465);
// EXTERNAL MODULE: ./features/superiority/superioritySlice.js + 1 modules
var superioritySlice = __webpack_require__(4814);
// EXTERNAL MODULE: ./features/price/priceSlice.js + 1 modules
var priceSlice = __webpack_require__(198);
// EXTERNAL MODULE: ./features/invitation/invitationSlice.js + 1 modules
var invitationSlice = __webpack_require__(2205);
// EXTERNAL MODULE: ./features/invitationSlug/invitationSlugSlice.js
var invitationSlugSlice = __webpack_require__(2215);
// EXTERNAL MODULE: ./features/background/backgroundSlice.js + 1 modules
var backgroundSlice = __webpack_require__(7336);
// EXTERNAL MODULE: ./features/digitalEnvelope/digitalEnvelopeSlice.js + 1 modules
var digitalEnvelopeSlice = __webpack_require__(350);
// EXTERNAL MODULE: ./features/loveStory/loveStorySlice.js + 1 modules
var loveStorySlice = __webpack_require__(1451);
// EXTERNAL MODULE: ./features/photoGallery/photoGallerySlice.js + 1 modules
var photoGallerySlice = __webpack_require__(8748);
// EXTERNAL MODULE: ./features/presence/presenceSlice.js + 1 modules
var presenceSlice = __webpack_require__(8799);
// EXTERNAL MODULE: ./features/socialMedia/socialMediaSlice.js + 1 modules
var socialMediaSlice = __webpack_require__(3764);
// EXTERNAL MODULE: ./features/theme/themeSlice.js + 1 modules
var themeSlice = __webpack_require__(3488);
// EXTERNAL MODULE: ./features/youtube/youtubeSlice.js + 1 modules
var youtubeSlice = __webpack_require__(4655);
// EXTERNAL MODULE: ./features/messageApi/messageApiSlice.js + 1 modules
var messageApiSlice = __webpack_require__(3188);
;// CONCATENATED MODULE: ./app/store.js


























const store = (0,toolkit_.configureStore)({
  reducer: {
    auth: authSlice/* default */.ZP,
    message: messageSlice/* default */.ZP,
    messageApi: messageApiSlice/* default */.ZP,
    youtube: youtubeSlice/* default */.ZP,
    theme: themeSlice/* default */.ZP,
    socialMedia: socialMediaSlice/* default */.ZP,
    presence: presenceSlice/* default */.ZP,
    photoGallery: photoGallerySlice/* default */.ZP,
    loveStory: loveStorySlice/* default */.ZP,
    digitalEnvelope: digitalEnvelopeSlice/* default */.ZP,
    background: backgroundSlice/* default */.ZP,
    invitationSlug: invitationSlugSlice/* default */.Z,
    invitation: invitationSlice/* default */.ZP,
    price: priceSlice/* default */.ZP,
    superiority: superioritySlice/* default */.ZP,
    prokes: prokesSlice/* default */.ZP,
    orderStep: orderStepSlice/* default */.ZP,
    music: musicSlice/* default */.ZP,
    fitur: fiturSlice/* default */.ZP,
    mainLanding: mainLandingSlice/* default */.ZP,
    faq: faqSlice/* default */.ZP,
    myTheme: myThemeSlice/* default */.ZP,
    mySocialMedia: mySocialMediaSlice/* default */.ZP,
    testimonial: testimonialSlice/* default */.ZP,
    user: userSlice/* default */.ZP
  },
  devTools: true,
  middleware: getDefaultMiddleware => getDefaultMiddleware({
    serializableCheck: false
  })
});
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
;// CONCATENATED MODULE: external "notistack"
const external_notistack_namespaceObject = require("notistack");
// EXTERNAL MODULE: ./services/api.js
var api = __webpack_require__(7751);
// EXTERNAL MODULE: ./services/token.service.js
var token_service = __webpack_require__(3775);
;// CONCATENATED MODULE: ./services/setupInterceptors.js




const setup = store => {
  api/* default.interceptors.request.use */.Z.interceptors.request.use(config => {
    const token = token_service/* default.getLocalAccessToken */.Z.getLocalAccessToken();

    if (token) {
      // config.headers["Authorization"] = 'Bearer ' + token;  // for Spring Boot back-end
      config.headers["x-access-token"] = token; // for Node.js Express back-end
    }

    return config;
  }, error => {
    return Promise.reject(error);
  });
  const {
    dispatch
  } = store;
  api/* default.interceptors.response.use */.Z.interceptors.response.use(res => {
    return res;
  }, async err => {
    const originalConfig = err.config;

    if (originalConfig.url !== "/auth/signin" && err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;

        try {
          const rs = await api/* default.post */.Z.post("/auth/refreshtoken", {
            refreshToken: token_service/* default.getLocalRefreshToken */.Z.getLocalRefreshToken()
          });
          const {
            accessToken
          } = rs.data;
          dispatch((0,authSlice/* refreshToken */.g$)(accessToken));
          token_service/* default.updateLocalAccessToken */.Z.updateLocalAccessToken(accessToken);
          return (0,api/* default */.Z)(originalConfig);
        } catch (_error) {
          return Promise.reject(_error);
        }
      }
    }

    return Promise.reject(err);
  });
};

/* harmony default export */ const setupInterceptors = (setup);
;// CONCATENATED MODULE: external "moment/locale/id"
const id_namespaceObject = require("moment/locale/id");
;// CONCATENATED MODULE: external "aos"
const external_aos_namespaceObject = require("aos");
var external_aos_default = /*#__PURE__*/__webpack_require__.n(external_aos_namespaceObject);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
;// CONCATENATED MODULE: external "nprogress"
const external_nprogress_namespaceObject = require("nprogress");
var external_nprogress_default = /*#__PURE__*/__webpack_require__.n(external_nprogress_namespaceObject);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./pages/_app.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable @next/next/no-script-in-head */

/* eslint-disable @next/next/inline-script-id */





















function MyApp({
  Component,
  pageProps
}) {
  const router = (0,router_.useRouter)();
  external_nprogress_default().configure({
    template: `<div class="preloader flex justify-center h-screen" role="bar">
        <img
          class="animate__animated animate__bounce animate__infinite"
          src="/static/images/logo/invitelah-left-green.svg"
          alt="invitelah.com"
          width="200"
        />
    </div>`
  });
  (0,external_react_.useEffect)(() => {
    const handleStart = () => {
      external_nprogress_default().start();
    };

    const handleStop = () => {
      external_nprogress_default().done();
    };

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleStop);
    router.events.on("routeChangeError", handleStop);
    return () => {
      router.events.off("routeChangeStart", handleStart);
      router.events.off("routeChangeComplete", handleStop);
      router.events.off("routeChangeError", handleStop);
    };
  }, [router.events]);
  (0,external_react_.useEffect)(() => {
    external_aos_default().init({
      easing: "ease-out-cubic",
      once: true,
      offset: 50
    });
  }, []);
  return /*#__PURE__*/jsx_runtime_.jsx(material_.ThemeProvider, {
    theme: themes(),
    children: /*#__PURE__*/jsx_runtime_.jsx(external_react_redux_.Provider, {
      store: store,
      children: /*#__PURE__*/jsx_runtime_.jsx(external_notistack_namespaceObject.SnackbarProvider, {
        maxSnack: 3,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(system_.StyledEngineProvider, {
          injectFirst: true,
          children: [/*#__PURE__*/jsx_runtime_.jsx(material_.CssBaseline, {}), /*#__PURE__*/jsx_runtime_.jsx(Component, _objectSpread({}, pageProps))]
        })
      })
    })
  });
}

setupInterceptors(store);
/* harmony default export */ const _app = (MyApp);

/***/ }),

/***/ 5692:
/***/ ((module) => {

module.exports = require("@mui/material");

/***/ }),

/***/ 8442:
/***/ ((module) => {

module.exports = require("@mui/material/styles");

/***/ }),

/***/ 7986:
/***/ ((module) => {

module.exports = require("@mui/system");

/***/ }),

/***/ 5184:
/***/ ((module) => {

module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [9336,2205,8799,8922,7987,4317,4814,1706,198,175,276,3465,2260,2571,4377,350,8748,3764,7336,1451,4655,3488,3974,2623], () => (__webpack_exec__(8793)));
module.exports = __webpack_exports__;

})();