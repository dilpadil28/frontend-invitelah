"use strict";
(() => {
var exports = {};
exports.id = 5405;
exports.ids = [5405];
exports.modules = {

/***/ 5967:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Beranda)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mui_material_Grid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5612);
/* harmony import */ var _mui_material_Grid__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Grid__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(19);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Box__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Custom_BtnCustom_BtnCustom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2184);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3015);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3877);
/* harmony import */ var react_typed__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1070);
/* harmony import */ var react_typed__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_typed__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _mui_material_colors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5574);
/* harmony import */ var _mui_material_colors__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_mui_material_colors__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_scroll__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3094);
/* harmony import */ var react_scroll__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_scroll__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper__WEBPACK_IMPORTED_MODULE_6__, swiper_react__WEBPACK_IMPORTED_MODULE_5__]);
([swiper__WEBPACK_IMPORTED_MODULE_6__, swiper_react__WEBPACK_IMPORTED_MODULE_5__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);




 // Import Swiper React components

 // import Swiper core and required modules


 // install Swiper modules

swiper__WEBPACK_IMPORTED_MODULE_6__["default"].use([swiper__WEBPACK_IMPORTED_MODULE_6__.EffectFade, swiper__WEBPACK_IMPORTED_MODULE_6__.Navigation, swiper__WEBPACK_IMPORTED_MODULE_6__.Pagination]);
 // Import Swiper styles









function Beranda() {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_3__.Container, {
    maxWidth: "xl",
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
      sx: {
        width: "100%"
      },
      pt: 10,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)((_mui_material_Grid__WEBPACK_IMPORTED_MODULE_1___default()), {
        container: true,
        direction: {
          xs: "column-reverse",
          md: "row"
        },
        justifyContent: "center",
        alignItems: "center",
        spacing: 1,
        rowSpacing: 3,
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Grid__WEBPACK_IMPORTED_MODULE_1___default()), {
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          xs: 12,
          md: 6,
          item: true,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
            component: "div",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_3__.Typography, {
              "data-aos": "fade-left",
              "data-aos-duration": "200",
              component: "h1",
              fontWeight: "bold",
              gutterBottom: true,
              fontSize: {
                xs: 24,
                sm: 48
              },
              children: "Undangan Pernikahan Digital"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
              component: "div",
              "data-aos": "fade-left",
              "data-aos-duration": "400",
              children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_3__.Typography, {
                gutterBottom: true,
                component: "h6",
                color: _mui_material_colors__WEBPACK_IMPORTED_MODULE_8__.grey[500],
                fontSize: {
                  xs: 14,
                  sm: 22
                },
                className: "inline",
                children: "berupa"
              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_3__.Typography, {
                gutterBottom: true,
                component: "h6",
                className: " inline",
                ml: 2,
                color: "primary",
                fontWeight: "bold",
                fontSize: {
                  xs: 16,
                  md: 36
                },
                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((react_typed__WEBPACK_IMPORTED_MODULE_7___default()), {
                  strings: ["Website", "Video", "Image"],
                  loop: true,
                  backSpeed: 80,
                  typeSpeed: 100
                })
              })]
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
              my: 2
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(react_scroll__WEBPACK_IMPORTED_MODULE_9__.Link, {
              to: "Harga",
              spy: true,
              smooth: true,
              duration: 500,
              offset: -20,
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_3__.Button, {
                variant: "contained",
                size: "large",
                className: "!bg-primary",
                sx: {
                  fontSize: {
                    xs: 12,
                    sm: 14,
                    md: 18
                  }
                },
                children: "Buat Undangan"
              })
            })]
          })
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Grid__WEBPACK_IMPORTED_MODULE_1___default()), {
          "data-aos": "zoom-in",
          "data-aos-duration": "200",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          item: true,
          xs: 12,
          md: 6,
          mb: 5,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
            position: "relative",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
              sx: {
                width: {
                  xs: 40,
                  sm: 70
                },
                position: "absolute",
                right: {
                  xs: 20,
                  sm: 95
                },
                top: {
                  xs: 60,
                  sm: 185
                }
              },
              alt: "instagram invitelah",
              className: " animate__animated animate__infinite animate__slower animate__shakeY animate__delay-2s ",
              component: "img",
              src: "/static/images/icons/3d-ig.png"
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
              sx: {
                width: {
                  xs: 40,
                  sm: 70
                },
                position: "absolute",
                top: {
                  xs: 212,
                  sm: 330
                }
              },
              alt: "facebook invitelah",
              className: " animate__animated animate__infinite animate__slower animate__shakeY animate__delay-3s ",
              component: "img",
              src: "/static/images/icons/3d-fb.png"
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
              sx: {
                width: {
                  xs: 40,
                  sm: 70
                },
                position: "absolute",
                left: {
                  xs: 33,
                  sm: 100
                }
              },
              alt: "whatsapp invitelah",
              className: " animate__animated animate__infinite animate__slower animate__shakeY animate__delay-5s ",
              component: "img",
              src: "/static/images/icons/3d-wa.png"
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_2___default()), {
              alt: "invitelah",
              component: "img",
              src: "/static/images/bg/banner.png",
              sx: {
                width: {
                  xs: 304,
                  sm: 700
                }
              }
            })]
          })
        })]
      })
    })
  });
}
});

/***/ }),

/***/ 2184:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* unused harmony export default */
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




function BtnCustom(props) {
  return /*#__PURE__*/_jsx(Button, _objectSpread(_objectSpread({
    className: "!bg-primary",
    variant: "contained"
  }, props), {}, {
    style: {
      borderRadius: 20,
      textTransform: "none"
    }
  }));
}

/***/ }),

/***/ 5666:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Keunggulan)
});

// EXTERNAL MODULE: external "@mui/icons-material"
var icons_material_ = __webpack_require__(7915);
// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/system"
var system_ = __webpack_require__(7986);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@mui/icons-material/ColorLens"
var ColorLens_ = __webpack_require__(8507);
var ColorLens_default = /*#__PURE__*/__webpack_require__.n(ColorLens_);
;// CONCATENATED MODULE: external "@mui/icons-material/Timer"
const Timer_namespaceObject = require("@mui/icons-material/Timer");
var Timer_default = /*#__PURE__*/__webpack_require__.n(Timer_namespaceObject);
;// CONCATENATED MODULE: external "@mui/icons-material/PhoneAndroid"
const PhoneAndroid_namespaceObject = require("@mui/icons-material/PhoneAndroid");
var PhoneAndroid_default = /*#__PURE__*/__webpack_require__.n(PhoneAndroid_namespaceObject);
// EXTERNAL MODULE: external "@mui/icons-material/Collections"
var Collections_ = __webpack_require__(9193);
var Collections_default = /*#__PURE__*/__webpack_require__.n(Collections_);
;// CONCATENATED MODULE: external "@mui/icons-material/VideoCameraBack"
const VideoCameraBack_namespaceObject = require("@mui/icons-material/VideoCameraBack");
var VideoCameraBack_default = /*#__PURE__*/__webpack_require__.n(VideoCameraBack_namespaceObject);
// EXTERNAL MODULE: external "@mui/icons-material/MyLocation"
var MyLocation_ = __webpack_require__(5924);
var MyLocation_default = /*#__PURE__*/__webpack_require__.n(MyLocation_);
;// CONCATENATED MODULE: external "@mui/icons-material/MusicNote"
const MusicNote_namespaceObject = require("@mui/icons-material/MusicNote");
var MusicNote_default = /*#__PURE__*/__webpack_require__.n(MusicNote_namespaceObject);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/Fitur/Fitur.js













const keunggulan = [{
  icon: /*#__PURE__*/jsx_runtime_.jsx((ColorLens_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Tema",
  desc: "Tersedia tema-tema undangan yang menarik dan elegan"
}, {
  icon: /*#__PURE__*/jsx_runtime_.jsx((Timer_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Waktu Mundur",
  desc: "Waktu mundur samapai hari H kamu resepsi "
}, {
  icon: /*#__PURE__*/jsx_runtime_.jsx((PhoneAndroid_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Responsive",
  desc: "Bisa dibuka semua perangkat, seperti handphone, tablet, laptop"
}, {
  icon: /*#__PURE__*/jsx_runtime_.jsx((Collections_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Galeri Foto",
  desc: "Unggah foto-foto kamu dan pasanganmu"
}, {
  icon: /*#__PURE__*/jsx_runtime_.jsx((VideoCameraBack_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Video",
  desc: "Tampilkan video-video kamu dan pasanganmu"
}, {
  icon: /*#__PURE__*/jsx_runtime_.jsx((MyLocation_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Lokasi",
  desc: "Menampilkan lokasi resepsi kamu dan pasanganmu"
}, {
  icon: /*#__PURE__*/jsx_runtime_.jsx((MusicNote_default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-up"
  }),
  title: "Musik",
  desc: "Undangan online akan lebih romantis sengan adanya backsound musik"
}];
function Keunggulan() {
  return /*#__PURE__*/jsx_runtime_.jsx(material_.Container, {
    id: "Fitur",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(system_.Box, {
      pt: 10,
      textAlign: "center",
      children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
        mb: 5,
        fontWeight: 500,
        component: "h1",
        fontSize: {
          xs: 24,
          sm: 40
        },
        "data-aos": "fade-up",
        children: "Fitur Undangan Invitelah"
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        py: 5,
        container: true,
        justifyContent: "center",
        spacing: {
          xs: 2,
          md: 3
        },
        columns: {
          xs: 4,
          sm: 8,
          md: 12
        },
        children: keunggulan === null || keunggulan === void 0 ? void 0 : keunggulan.map((v, i) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Grid, {
          item: true,
          xs: 2,
          sm: 3,
          md: 3,
          mb: 5,
          children: [v.icon, /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
            mt: 3,
            mb: 3,
            fontWeight: "bold",
            component: "h6",
            fontSize: {
              xs: 18,
              md: 24
            },
            "data-aos-delay": "75",
            "data-aos": "fade-up",
            children: v.title
          }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
            component: "p",
            fontSize: {
              xs: 12,
              md: 14
            },
            "data-aos-delay": "100",
            "data-aos": "fade-up",
            children: v.desc
          })]
        }, i))
      })]
    })
  });
}

/***/ }),

/***/ 3978:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Harga)
});

// EXTERNAL MODULE: external "@mui/icons-material"
var icons_material_ = __webpack_require__(7915);
// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/system"
var system_ = __webpack_require__(7986);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./config/api.js
var api = __webpack_require__(5642);
;// CONCATENATED MODULE: ./lib/ga/index.js
// log specific events happening.
const ga_event = ({
  action,
  params
}) => {
   false && 0;
};
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/Harga/Harga.js
/* eslint-disable react/jsx-key */








const harga = [{
  image: "",
  harga_diskon: "Rp 120.000",
  harga: "Rp 40.000",
  title: "Gold",
  fitur: ["Aktif Seumur Hidup Website", "Amplop Digital", "Autoplay Music", "Cerita Cinta", "Custom Music", "Download Data RVSP(Konfirmasi Kehadiran Tamu)", "Google Maps(Lokasi Acara)", "Google Calendar(Pengingat Tanggal)", "Hitung Mudur", "Kirim Kado", "Kolom Ucapan", "Sosial Media", "Live Streameing", "Protokol Kesehatan", "Scan Barcode Tamu", "Max 20 Photo", "Max 20 Video", "Unlimited Revisi", "Unlimited Nama Tamu", "Ucapan & RVSP(Konfirmasi Kehadiran Tamu)", /*#__PURE__*/jsx_runtime_.jsx("b", {
    children: " Undangan Image 4 buah "
  }), /*#__PURE__*/jsx_runtime_.jsx("b", {
    children: "Undangan Video "
  }), /*#__PURE__*/jsx_runtime_.jsx("div", {
    style: {
      textDecoration: "line-through"
    },
    children: " Custom Theme "
  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    style: {
      textDecoration: "line-through"
    },
    children: [" ", "Domain/Subdoamin invitelah.com", " "]
  })],
  whatsapp: api/* api.whatsappGold */.h.whatsappGold,
  bukalapak: api/* api.bukalapak */.h.bukalapak,
  tokopedia: api/* api.tokopedia */.h.tokopedia,
  shopee: api/* api.shopee */.h.shopee
}, {
  image: "",
  harga_diskon: "Rp 100.000",
  harga: "Rp 30.000",
  title: "Silver",
  fitur: ["Aktif Seumur Hidup Website", "Amplop Digital", "Autoplay Music", "Cerita Cinta", "Custom Music", "Download Data RVSP(Konfirmasi Kehadiran Tamu)", "Google Maps(Lokasi Acara)", "Google Calendar(Pengingat Tanggal)", "Hitung Mudur", "Kirim Kado", "Kolom Ucapan", "Sosial Media", "Live Streameing", "Protokol Kesehatan", "Scan Barcode Tamu", "Max 20 Photo", "Max 20 Video", "Unlimited Revisi", "Unlimited Nama Tamu", "Ucapan & RVSP(Konfirmasi Kehadiran Tamu)", /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    style: {
      textDecoration: "line-through"
    },
    children: [" ", "Undangan Image 4 buah", " "]
  }), /*#__PURE__*/jsx_runtime_.jsx("div", {
    style: {
      textDecoration: "line-through"
    },
    children: " Undangan Video "
  }), /*#__PURE__*/jsx_runtime_.jsx("div", {
    style: {
      textDecoration: "line-through"
    },
    children: " Custom Theme "
  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    style: {
      textDecoration: "line-through"
    },
    children: [" ", "Domain/Subdoamin invitelah.com", " "]
  })],
  whatsapp: api/* api.whatsappSilver */.h.whatsappSilver,
  bukalapak: api/* api.bukalapak */.h.bukalapak,
  tokopedia: api/* api.tokopedia */.h.tokopedia,
  shopee: api/* api.shopee */.h.shopee
}, {
  image: "",
  harga_diskon: "Rp 500.000",
  harga: "Rp 400.000",
  title: "Diamond",
  fitur: ["Aktif Seumur Hidup Website", "Amplop Digital", "Autoplay Music", "Cerita Cinta", "Custom Music", "Download Data RVSP(Konfirmasi Kehadiran Tamu)", "Google Maps(Lokasi Acara)", "Google Calendar(Pengingat Tanggal)", "Hitung Mudur", "Kirim Kado", "Kolom Ucapan", "Sosial Media", "Live Streameing", "Protokol Kesehatan", "Scan Barcode Tamu", "Max 20 Photo", "Max 20 Video", "Unlimited Revisi", "Unlimited Nama Tamu", "Ucapan & RVSP(Konfirmasi Kehadiran Tamu)", /*#__PURE__*/jsx_runtime_.jsx("b", {
    children: " Undangan Image 4 buah "
  }), /*#__PURE__*/jsx_runtime_.jsx("b", {
    children: "Undangan Video "
  }), /*#__PURE__*/jsx_runtime_.jsx("b", {
    children: " Custom Theme "
  }), /*#__PURE__*/jsx_runtime_.jsx("b", {
    children: " Domain/Subdoamin invitelah.com "
  })],
  whatsapp: api/* api.whatsappDiamond */.h.whatsappDiamond,
  bukalapak: api/* api.bukalapak */.h.bukalapak,
  tokopedia: api/* api.tokopedia */.h.tokopedia,
  shopee: api/* api.shopee */.h.shopee
}];
function Harga() {
  const handlePrice = param => {
    ga_event({
      action: "price",
      params: {
        order: param
      }
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx(material_.Container, {
    id: "Harga",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(system_.Box, {
      pt: 10,
      pb: 10,
      textAlign: "center",
      children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
        component: "h1",
        mb: 5,
        fontSize: {
          xs: 24,
          sm: 40
        },
        fontWeight: 500,
        "data-aos": "zoom-in",
        children: "Harga Undangan"
      }), /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
        container: true,
        justifyContent: "center",
        spacing: {
          xs: 2,
          md: 3
        },
        columns: {
          xs: 4,
          sm: 8,
          md: 12
        },
        children: harga === null || harga === void 0 ? void 0 : harga.map((v, i) => /*#__PURE__*/jsx_runtime_.jsx(material_.Grid, {
          item: true,
          xs: 12,
          sm: 4,
          md: 4,
          mb: 5,
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.Paper, {
            "data-aos-delay": "50",
            "data-aos": "zoom-in",
            elevation: 3,
            style: {
              padding: 30,
              borderRadius: 20
            },
            children: [/*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              "data-aos-delay": "55",
              "data-aos": "zoom-in",
              mb: 2,
              fontSize: 18,
              component: "p",
              style: {
                textDecoration: "line-through"
              },
              color: "lightgrey",
              children: v.harga_diskon
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              mb: 2,
              color: "#006C32",
              fontSize: 28,
              fontWeight: "bold",
              "data-aos-delay": "60",
              "data-aos": "zoom-in",
              component: "p",
              children: v.harga
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Typography, {
              mb: 2,
              fontWeight: "bold",
              fontSize: 18,
              "data-aos-delay": "65",
              "data-aos": "zoom-in",
              component: "h1",
              children: v.title
            }), v.fitur.map((v, i) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(material_.ListItem, {
              sx: {
                p: 0
              },
              "data-aos-delay": "70",
              "data-aos": "zoom-in",
              children: [/*#__PURE__*/jsx_runtime_.jsx(material_.ListItemIcon, {
                children: /*#__PURE__*/jsx_runtime_.jsx(icons_material_.CheckRounded, {
                  color: "success"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx(material_.ListItemText, {
                children: v
              })]
            }, i)), /*#__PURE__*/jsx_runtime_.jsx(system_.Box, {
              my: 2
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
              onClick: () => handlePrice("whatsapp"),
              target: "_blank",
              href: v.whatsapp,
              fullWidth: true,
              variant: "contained",
              "data-aos-delay": "65",
              "data-aos": "zoom-in",
              children: "Pesan Whatsapp"
            }), /*#__PURE__*/jsx_runtime_.jsx(system_.Box, {
              mb: 1
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
              onClick: () => handlePrice("Bukalapak"),
              target: "_blank",
              sx: {
                backgroundColor: "#E31E52"
              },
              href: v.bukalapak,
              fullWidth: true,
              variant: "contained",
              "data-aos-delay": "66",
              "data-aos": "zoom-in",
              children: "Bukalapak"
            }), /*#__PURE__*/jsx_runtime_.jsx(system_.Box, {
              mb: 1
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
              onClick: () => handlePrice("Tokopedia"),
              target: "_blank",
              sx: {
                backgroundColor: "#03AC0E"
              },
              href: v.tokopedia,
              fullWidth: true,
              variant: "contained",
              "data-aos-delay": "67",
              "data-aos": "zoom-in",
              children: "Tokopedia"
            }), /*#__PURE__*/jsx_runtime_.jsx(system_.Box, {
              mb: 1
            }), /*#__PURE__*/jsx_runtime_.jsx(material_.Button, {
              onClick: () => handlePrice("Shopee"),
              target: "_blank",
              sx: {
                backgroundColor: "#EE4D2D"
              },
              href: v.shopee,
              fullWidth: true,
              variant: "contained",
              "data-aos-delay": "68",
              "data-aos": "zoom-in",
              children: "Shopee"
            })]
          })
        }, i))
      })]
    })
  });
}

/***/ }),

/***/ 5032:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Keunggulan)
/* harmony export */ });
/* harmony import */ var _mui_icons_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7915);
/* harmony import */ var _mui_icons_material__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_material_colors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5574);
/* harmony import */ var _mui_material_colors__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material_colors__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_system__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7986);
/* harmony import */ var _mui_system__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_system__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _mui_icons_material_Money__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5638);
/* harmony import */ var _mui_icons_material_Money__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Money__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__);








const keunggulan = [{
  icon: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_icons_material__WEBPACK_IMPORTED_MODULE_0__.FlashOnRounded, {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-right"
  }),
  title: "Cepat",
  desc: "Proses pembuatan undangan hanya membutuhkan beberapa jam, bahkan menit saja,tidak seperti menunggu chat gebetan :D"
}, {
  icon: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx((_mui_icons_material_Money__WEBPACK_IMPORTED_MODULE_5___default()), {
    color: "primary",
    sx: {
      fontSize: {
        xs: 40,
        md: 54
      }
    },
    "data-aos-delay": "75",
    "data-aos": "fade-right"
  }),
  title: "Murah",
  desc: "Harga yang di tawarkan relatif murah dibanding dengan undangan kertas, karena tidak dihitung perlembar :D"
}];
function Keunggulan() {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Container, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)(_mui_system__WEBPACK_IMPORTED_MODULE_3__.Box, {
      pt: 10,
      pb: 10,
      textAlign: "center",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Typography, {
        fontSize: {
          xs: 24,
          sm: 40
        },
        fontWeight: 500,
        "data-aos": "fade-right",
        component: "h1",
        children: "Mengapa harus menggunkaan Undangan Invitelah ?"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Typography, {
        component: "p",
        mt: 3,
        mb: 5,
        fontSize: {
          xs: 12,
          md: 18
        },
        color: _mui_material_colors__WEBPACK_IMPORTED_MODULE_2__.grey[500],
        "data-aos": "fade-right",
        children: "Dari pada harus membeli undangan kertas yang di asumsikan harga perlembarnya Rp 1.000 x 500 orang = Rp 500.000, Lebih baik mengguankan invitelah.com hanya membayar mulai dari 20 ribu saja bisa disebar sebanyak-banyaknya hingga ratusan bahkan ribuan orang."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Grid, {
        justifyContent: "center",
        container: true,
        spacing: {
          xs: 2,
          md: 3
        },
        columns: {
          xs: 4,
          sm: 8,
          md: 12
        },
        children: keunggulan === null || keunggulan === void 0 ? void 0 : keunggulan.map((v, i) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Grid, {
          item: true,
          xs: 4,
          md: 4,
          mb: 5,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Paper, {
            elevation: 3,
            "data-aos-delay": "50",
            "data-aos": "fade-right",
            style: {
              padding: 20,
              borderRadius: 20
            },
            children: [v.icon, /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Typography, {
              mt: 3,
              mb: 5,
              fontSize: {
                xs: 18,
                md: 24
              },
              component: "h1",
              fontWeight: "bold",
              "data-aos-delay": "100",
              "data-aos": "fade-right",
              children: v.title
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_1__.Typography, {
              fontSize: {
                xs: 12,
                md: 14
              },
              component: "p",
              "data-aos-delay": "125",
              "data-aos": "fade-right",
              children: v.desc
            })]
          })
        }, i))
      })]
    })
  });
}

/***/ }),

/***/ 9724:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Sosmed)
/* harmony export */ });
/* harmony import */ var _mui_system__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7986);
/* harmony import */ var _mui_system__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_system__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8096);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const images = [// 'static/images/sosmed/Blue-Flower-1.png',
// 'static/images/sosmed/Blue-Flower-2.png',
// 'static/images/sosmed/Blue-Flower-3.png',
// 'static/images/sosmed/Brown-Crystal-1.png',
// 'static/images/sosmed/Brown-Crystal-2.png',
// 'static/images/sosmed/Brown-Crystal-3.png',
// 'static/images/sosmed/Brown-Frame-1.png',
// 'static/images/sosmed/Brown-Frame-2.png',
// 'static/images/sosmed/Brown-Frame-3.png',
// 'static/images/sosmed/Gold-Flower-1.png',
// 'static/images/sosmed/Gold-Flower-2.png',
// 'static/images/sosmed/Gold-Flower-3.png',
// 'static/images/sosmed/Gold-Frame-1.png',
// 'static/images/sosmed/Gold-Frame-2.png',
// 'static/images/sosmed/Gold-Frame-3.png',
"static/images/sosmed/Pink-Flower-1.png", "static/images/sosmed/Pink-Flower-2.png", "static/images/sosmed/Pink-Flower-3.png", // 'static/images/sosmed/Red-Flower-1.png',
// 'static/images/sosmed/Red-Flower-2.png',
// 'static/images/sosmed/Red-Flower-3.png',
// 'static/images/sosmed/Triangle-Gold-1.png',
// 'static/images/sosmed/Triangle-Gold-2.png',
// 'static/images/sosmed/Triangle-Gold-3.png',
"static/images/sosmed/White-Flower-1.png", "static/images/sosmed/White-Flower-2.png", "static/images/sosmed/White-Flower-3.png" // 'static/images/sosmed/Yellow-Flower-1.png',
// 'static/images/sosmed/Yellow-Flower-2.png',
// 'static/images/sosmed/Yellow-Flower-3.png',
];
function Sosmed() {
  const settings = {
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    loop: true,
    autoplay: true,
    speed: 1000,
    cssEase: "linear",
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  };
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Container, {
    maxWidth: "md",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Grid, {
      container: true,
      spacing: 2,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Grid, {
        item: true,
        xs: 12,
        md: 6,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Typography, {
            my: 2,
            fontWeight: "bold",
            fontSize: {
              xs: 18,
              md: 24
            },
            "data-aos-delay": "75",
            "data-aos": "fade-up",
            textAlign: "center",
            component: "h1",
            children: "Undangan Video"
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("div", {
            "data-aos-delay": "200",
            "data-aos": "fade-up",
            style: {
              position: "relative"
            },
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("div", {
              className: "iphone-frame",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("video", {
                loop: true,
                autoPlay: true,
                muted: true,
                playsInline: true,
                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("source", {
                  src: "/static/images/video/Brown-Crystal.mp4"
                })
              }, "/static/images/video/Brown-Crystal.mp4")
            })
          })]
        })
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Grid, {
        item: true,
        xs: 12,
        md: 6,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_2__.Typography, {
            my: 2,
            fontWeight: "bold",
            fontSize: {
              xs: 18,
              md: 24
            },
            "data-aos-delay": "75",
            "data-aos": "fade-up",
            textAlign: "center",
            component: "h1",
            children: "Post Intagram"
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_system__WEBPACK_IMPORTED_MODULE_0__.Box, {
            component: "div",
            position: "relative",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("div", {
              "data-aos-delay": "200",
              "data-aos": "fade-up",
              className: "ig-frame",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx((react_slick__WEBPACK_IMPORTED_MODULE_3___default()), _objectSpread(_objectSpread({}, settings), {}, {
                children: images.map((v, i) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("div", {
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_system__WEBPACK_IMPORTED_MODULE_0__.Box, {
                    component: "img",
                    style: {
                      marginLeft: 19,
                      marginTop: 44
                    },
                    height: {
                      xs: 273
                    },
                    src: v,
                    alt: "ig"
                  })
                }, i))
              }))
            })
          })]
        })
      })]
    })
  });
}

/***/ }),

/***/ 2056:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Tema)
/* harmony export */ });
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mui_system__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7986);
/* harmony import */ var _mui_system__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_mui_system__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8096);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable @next/next/link-passhref */







function CardTheme({
  img,
  name
}) {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Card, {
    sx: {
      maxWidth: 345,
      borderRadius: 5,
      m: 2
    },
    elevation: 3,
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_system__WEBPACK_IMPORTED_MODULE_1__.Box, {
      component: "a",
      href: `/${name}?to=didi+dan+partner`,
      target: "_blank",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_0__.CardActionArea, {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.CardMedia, {
          component: "img",
          height: "250",
          image: img,
          alt: name + " Invitelah"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.CardContent, {
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Typography, {
            gutterBottom: true,
            fontWeight: "bold",
            fontSize: {
              xs: 18,
              sm: 24
            },
            component: "div",
            children: name
          })
        })]
      })
    })
  });
}

const themes = [{
  img: '/static/images/cards/Modern-Gold.jpeg',
  name: 'modern-gold'
}, {
  img: '/static/images/cards/Modern-Black.jpeg',
  name: 'raja-ratu'
}, {
  img: '/static/images/cards/Modern-Blue.jpeg',
  name: 'modern-blue'
}, {
  img: '/static/images/cards/Modern-Brown.jpeg',
  name: 'modern-brown'
}, {
  img: '/static/images/cards/Modern-Green.jpeg',
  name: 'modern-green'
}, {
  img: '/static/images/cards/Modern-Pink.jpeg',
  name: 'modern-pink'
}, {
  img: '/static/images/cards/Modern-Red.jpeg',
  name: 'modern-red'
}, {
  img: '/static/images/cards/Minimalis.jpeg',
  name: 'minimalis'
}, {
  img: '/static/images/cards/White-Flower.png',
  name: 'white-flower'
}, {
  img: '/static/images/cards/Blue-Flower.png',
  name: 'blue-flower'
}, {
  img: '/static/images/cards/Brown-Crystal.png',
  name: 'brown-crystal'
}, {
  img: '/static/images/cards/Brown-Frame.png',
  name: 'brown-frame'
}, {
  img: '/static/images/cards/Gold-Flower.png',
  name: 'gold-flower'
}, {
  img: '/static/images/cards/Gold-Frame.png',
  name: 'gold-frame'
}, {
  img: '/static/images/cards/Pink-Flower.png',
  name: 'pink-flower'
}, {
  img: '/static/images/cards/Red-Flower.png',
  name: 'red-flower'
}, {
  img: '/static/images/cards/Triangle-Gold.png',
  name: 'triangle-gold'
}, {
  img: '/static/images/cards/Yellow-Flower.png',
  name: 'yellow-flower'
}];
function Tema() {
  const settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  };
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Container, {
    id: "Tema",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_mui_system__WEBPACK_IMPORTED_MODULE_1__.Box, {
      pt: 10,
      pb: 10,
      textAlign: "center",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Typography, {
        mb: 5,
        fontWeight: 500,
        fontSize: {
          xs: 24,
          sm: 40
        },
        "data-aos": "zoom-in",
        component: "h1",
        children: "Tema"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Grid, {
        container: true,
        spacing: 1,
        direction: "row",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Grid, {
          item: true,
          xs: 12,
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx((react_slick__WEBPACK_IMPORTED_MODULE_3___default()), _objectSpread(_objectSpread({}, settings), {}, {
            children: themes.map((v, i) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(CardTheme, {
              img: v.img,
              name: v.name
            }, i))
          }))
        })
      })]
    })
  });
}

/***/ }),

/***/ 127:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ TopBar)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@mui/material/AppBar"
var AppBar_ = __webpack_require__(3882);
var AppBar_default = /*#__PURE__*/__webpack_require__.n(AppBar_);
// EXTERNAL MODULE: external "@mui/material/Toolbar"
var Toolbar_ = __webpack_require__(1431);
var Toolbar_default = /*#__PURE__*/__webpack_require__.n(Toolbar_);
// EXTERNAL MODULE: external "@mui/material/Typography"
var Typography_ = __webpack_require__(7163);
var Typography_default = /*#__PURE__*/__webpack_require__.n(Typography_);
// EXTERNAL MODULE: external "@mui/material/CssBaseline"
var CssBaseline_ = __webpack_require__(4960);
var CssBaseline_default = /*#__PURE__*/__webpack_require__.n(CssBaseline_);
;// CONCATENATED MODULE: external "@mui/material/useScrollTrigger"
const useScrollTrigger_namespaceObject = require("@mui/material/useScrollTrigger");
var useScrollTrigger_default = /*#__PURE__*/__webpack_require__.n(useScrollTrigger_namespaceObject);
// EXTERNAL MODULE: external "@mui/material/Box"
var Box_ = __webpack_require__(19);
var Box_default = /*#__PURE__*/__webpack_require__.n(Box_);
;// CONCATENATED MODULE: external "@mui/material/Container"
const Container_namespaceObject = require("@mui/material/Container");
var Container_default = /*#__PURE__*/__webpack_require__.n(Container_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/Slide"
const Slide_namespaceObject = require("@mui/material/Slide");
var Slide_default = /*#__PURE__*/__webpack_require__.n(Slide_namespaceObject);
// EXTERNAL MODULE: external "@mui/material/IconButton"
var IconButton_ = __webpack_require__(7934);
var IconButton_default = /*#__PURE__*/__webpack_require__.n(IconButton_);
// EXTERNAL MODULE: external "@mui/material/Menu"
var Menu_ = __webpack_require__(5486);
var Menu_default = /*#__PURE__*/__webpack_require__.n(Menu_);
// EXTERNAL MODULE: external "@mui/icons-material/Menu"
var icons_material_Menu_ = __webpack_require__(3365);
var icons_material_Menu_default = /*#__PURE__*/__webpack_require__.n(icons_material_Menu_);
;// CONCATENATED MODULE: external "@mui/material/Avatar"
const Avatar_namespaceObject = require("@mui/material/Avatar");
// EXTERNAL MODULE: external "@mui/material/Button"
var Button_ = __webpack_require__(3819);
var Button_default = /*#__PURE__*/__webpack_require__.n(Button_);
;// CONCATENATED MODULE: external "@mui/material/Tooltip"
const Tooltip_namespaceObject = require("@mui/material/Tooltip");
// EXTERNAL MODULE: external "@mui/material/MenuItem"
var MenuItem_ = __webpack_require__(9271);
var MenuItem_default = /*#__PURE__*/__webpack_require__.n(MenuItem_);
;// CONCATENATED MODULE: external "@mui/material/Fab"
const Fab_namespaceObject = require("@mui/material/Fab");
var Fab_default = /*#__PURE__*/__webpack_require__.n(Fab_namespaceObject);
;// CONCATENATED MODULE: external "@mui/icons-material/KeyboardArrowUp"
const KeyboardArrowUp_namespaceObject = require("@mui/icons-material/KeyboardArrowUp");
var KeyboardArrowUp_default = /*#__PURE__*/__webpack_require__.n(KeyboardArrowUp_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/Zoom"
const Zoom_namespaceObject = require("@mui/material/Zoom");
var Zoom_default = /*#__PURE__*/__webpack_require__.n(Zoom_namespaceObject);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: external "react-scroll"
var external_react_scroll_ = __webpack_require__(3094);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/TopBar/TopBar.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint-disable @next/next/no-img-element */























const pages = ["Beranda", "Fitur", "Tema", "Harga"];
const settings = ["Profile", "Account", "Dashboard", "Logout"];

function HideOnScroll(props) {
  const {
    children,
    window
  } = props; // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.

  const trigger = useScrollTrigger_default()({
    target: window ? window() : undefined
  });
  return /*#__PURE__*/jsx_runtime_.jsx((Slide_default()), {
    appear: false,
    direction: "down",
    in: !trigger,
    children: children
  });
}

function ScrollTop(props) {
  const {
    children,
    window
  } = props; // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.

  const trigger = useScrollTrigger_default()({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100
  });

  const handleClick = event => {
    const anchor = (event.target.ownerDocument || document).querySelector("#back-to-top-anchor");

    if (anchor) {
      anchor.scrollIntoView({
        behavior: "smooth",
        block: "center"
      });
    }
  };

  return /*#__PURE__*/jsx_runtime_.jsx((Zoom_default()), {
    in: trigger,
    children: /*#__PURE__*/jsx_runtime_.jsx((Box_default()), {
      onClick: handleClick,
      role: "presentation",
      sx: {
        zIndex: 999,
        position: "fixed",
        bottom: 16,
        right: 16
      },
      children: children
    })
  });
}

function TopBar(props) {
  const [anchorElNav, setAnchorElNav] = external_react_.useState(null);
  const [anchorElUser, setAnchorElUser] = external_react_.useState(null);

  const handleOpenNavMenu = event => {
    setAnchorElNav(event.currentTarget);
  };

  const handleOpenUserMenu = event => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx((CssBaseline_default()), {}), /*#__PURE__*/jsx_runtime_.jsx(HideOnScroll, _objectSpread(_objectSpread({}, props), {}, {
      children: /*#__PURE__*/jsx_runtime_.jsx((AppBar_default()), {
        color: "inherit",
        sx: {
          boxShadow: "none",
          padding: "none"
        },
        children: /*#__PURE__*/jsx_runtime_.jsx((Container_default()), {
          maxWidth: "xl",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)((Toolbar_default()), {
            disableGutters: true,
            children: [/*#__PURE__*/jsx_runtime_.jsx((Box_default()), {
              sx: {
                mr: 2,
                display: {
                  xs: "none",
                  md: "flex"
                }
              },
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                style: {
                  width: 160
                },
                src: "/static/images/logo/invitelah-left-green.svg",
                alt: "logo-invitelah"
              })
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)((Box_default()), {
              sx: {
                flexGrow: 1,
                display: {
                  xs: "flex",
                  md: "none"
                }
              },
              children: [/*#__PURE__*/jsx_runtime_.jsx((IconButton_default()), {
                size: "large",
                "aria-label": "account of current user",
                "aria-controls": "menu-appbar",
                "aria-haspopup": "true",
                onClick: handleOpenNavMenu,
                color: "inherit",
                children: /*#__PURE__*/jsx_runtime_.jsx((icons_material_Menu_default()), {})
              }), /*#__PURE__*/jsx_runtime_.jsx((Menu_default()), {
                id: "menu-appbar",
                anchorEl: anchorElNav,
                anchorOrigin: {
                  vertical: "bottom",
                  horizontal: "left"
                },
                keepMounted: true,
                transformOrigin: {
                  vertical: "top",
                  horizontal: "left"
                },
                open: Boolean(anchorElNav),
                onClose: handleCloseNavMenu,
                sx: {
                  display: {
                    xs: "block",
                    md: "none",
                    fontSize: 18,
                    color: "#006C32"
                  }
                },
                children: pages.map(page => /*#__PURE__*/jsx_runtime_.jsx((MenuItem_default()), {
                  onClick: handleCloseNavMenu,
                  children: /*#__PURE__*/jsx_runtime_.jsx(external_react_scroll_.Link, {
                    activeClass: "active-landing",
                    to: page,
                    spy: true,
                    smooth: true,
                    duration: 500,
                    offset: -20,
                    children: /*#__PURE__*/jsx_runtime_.jsx((Typography_default()), {
                      textAlign: "center",
                      children: page
                    })
                  }, page)
                }, page))
              })]
            }), /*#__PURE__*/jsx_runtime_.jsx((Box_default()), {
              sx: {
                flexGrow: 1,
                display: {
                  xs: "flex",
                  md: "none"
                }
              },
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                style: {
                  width: 130
                },
                src: "/static/images/logo/invitelah-left-green.svg",
                alt: "logo-invitelah"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx((Box_default()), {
              sx: {
                flexGrow: 1,
                display: {
                  xs: "none",
                  md: "flex"
                }
              }
            }), /*#__PURE__*/jsx_runtime_.jsx((Box_default()), {
              sx: {
                display: {
                  xs: "none",
                  md: "flex"
                }
              },
              children: pages.map(page => /*#__PURE__*/jsx_runtime_.jsx(external_react_scroll_.Link, {
                activeClass: "active-landing",
                to: page,
                spy: true,
                smooth: true,
                duration: 500,
                offset: -20,
                children: /*#__PURE__*/jsx_runtime_.jsx((Button_default()), {
                  style: {
                    textTransform: "none"
                  },
                  onClick: handleCloseNavMenu,
                  sx: {
                    mx: 2,
                    my: 2,
                    color: "#006C32",
                    display: "block",
                    fontSize: 18
                  },
                  children: page
                })
              }, page))
            }), /*#__PURE__*/jsx_runtime_.jsx((Box_default()), {
              ml: 2,
              sx: {
                flexGrow: 0
              },
              children: /*#__PURE__*/jsx_runtime_.jsx((Menu_default()), {
                sx: {
                  mt: "45px"
                },
                id: "menu-appbar",
                anchorEl: anchorElUser,
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right"
                },
                keepMounted: true,
                transformOrigin: {
                  vertical: "top",
                  horizontal: "right"
                },
                open: Boolean(anchorElUser),
                onClose: handleCloseUserMenu,
                children: settings.map(setting => /*#__PURE__*/jsx_runtime_.jsx((MenuItem_default()), {
                  onClick: handleCloseNavMenu,
                  children: /*#__PURE__*/jsx_runtime_.jsx((Typography_default()), {
                    textAlign: "center",
                    children: setting
                  })
                }, setting))
              })
            })]
          })
        })
      })
    })), /*#__PURE__*/jsx_runtime_.jsx((Toolbar_default()), {
      id: "back-to-top-anchor"
    }), /*#__PURE__*/jsx_runtime_.jsx(ScrollTop, _objectSpread(_objectSpread({}, props), {}, {
      children: /*#__PURE__*/jsx_runtime_.jsx((Fab_default()), {
        className: "!bg-primary !text-white",
        size: "small",
        "aria-label": "scroll back to top",
        children: /*#__PURE__*/jsx_runtime_.jsx((KeyboardArrowUp_default()), {})
      })
    }))]
  });
}

/***/ }),

/***/ 2748:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps)
/* harmony export */ });
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Beranda_Beranda__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5967);
/* harmony import */ var _components_Fitur_Fitur__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5666);
/* harmony import */ var _components_Harga_Harga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3978);
/* harmony import */ var _components_Keunggulan_Keunggulan__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5032);
/* harmony import */ var _components_Tema_Tema__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2056);
/* harmony import */ var _components_TopBar_TopBar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(127);
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3133);
/* harmony import */ var _config_api__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5642);
/* harmony import */ var _components_Sosmed__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(9724);
/* harmony import */ var _components_Loading__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(7402);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_Beranda_Beranda__WEBPACK_IMPORTED_MODULE_2__]);
_components_Beranda_Beranda__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__)[0];
















function IndexPage({
  logo
}) {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        charset: "UTF-8"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "keywords",
        content: "undangan, pernikahan, digital"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "viewport",
        content: "width=device-width, initial-scale=1.0"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("title", {
        children: "Undangan Pernikahan Digital | Invitelah"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "description",
        content: "Undangan Pernikahan Digital Termurah dengan fitur Terlengkap berupa Website, Video & Image. Hanya Rp39.999"
      }, "desc"), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "og:title",
        content: "Undangan Pernikahan Digital | Invitelah"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "og:description",
        content: "Undangan Pernikahan Digital Termurah dengan fitur Terlengkap berupa Website, Video & Image. Hanya Rp39.999"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "og:image",
        content: logo
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "og:url",
        content: _config_api__WEBPACK_IMPORTED_MODULE_9__/* .api.home */ .h.home
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "og:type",
        content: "website"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "twitter:card",
        content: "summary_large_image"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "twitter:domain",
        content: "invitelah.com"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        property: "twitter:url",
        content: _config_api__WEBPACK_IMPORTED_MODULE_9__/* .api.home */ .h.home
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "twitter:title",
        content: "Undangan Pernikahan Digital | Invitelah"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "twitter:description",
        content: "Undangan Pernikahan Digital Termurah dengan fitur Terlengkap berupa Website, Video & Image. Hanya Rp39.999"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "twitter:image",
        content: logo
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("meta", {
        name: "google-site-verification",
        content: "SJzWLRyt9WiyPKqADzFO6XCwj9W8zJgzcu0AU3ydXIs"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("link", {
        rel: "canonical",
        href: _config_api__WEBPACK_IMPORTED_MODULE_9__/* .api.home */ .h.home
      }, "canonical")]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx("header", {
      id: "Beranda",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_TopBar_TopBar__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {})
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxs)("main", {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Beranda_Beranda__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box, {
        component: "div",
        my: 5
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Keunggulan_Keunggulan__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {})
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Fitur_Fitur__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Sosmed__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {})
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_0__.Box, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Tema_Tema__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {})
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Harga_Harga__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_12__.jsx(_components_Footer__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
      desc: true
    })]
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (IndexPage);
const getServerSideProps = async context => {
  const {
    params,
    req,
    res,
    query
  } = context; // const to = query.to;
  // ssr caching

  res.setHeader("Cache-Control", "public, s-maxage=1, stale-while-revalidate=59");
  const logo = _config_api__WEBPACK_IMPORTED_MODULE_9__/* .api.home */ .h.home + "/android-chrome-512x512.png";
  return {
    props: {
      logo
    }
  };
};
});

/***/ }),

/***/ 7915:
/***/ ((module) => {

module.exports = require("@mui/icons-material");

/***/ }),

/***/ 9193:
/***/ ((module) => {

module.exports = require("@mui/icons-material/Collections");

/***/ }),

/***/ 8507:
/***/ ((module) => {

module.exports = require("@mui/icons-material/ColorLens");

/***/ }),

/***/ 3365:
/***/ ((module) => {

module.exports = require("@mui/icons-material/Menu");

/***/ }),

/***/ 5638:
/***/ ((module) => {

module.exports = require("@mui/icons-material/Money");

/***/ }),

/***/ 5924:
/***/ ((module) => {

module.exports = require("@mui/icons-material/MyLocation");

/***/ }),

/***/ 5692:
/***/ ((module) => {

module.exports = require("@mui/material");

/***/ }),

/***/ 3882:
/***/ ((module) => {

module.exports = require("@mui/material/AppBar");

/***/ }),

/***/ 19:
/***/ ((module) => {

module.exports = require("@mui/material/Box");

/***/ }),

/***/ 3819:
/***/ ((module) => {

module.exports = require("@mui/material/Button");

/***/ }),

/***/ 4960:
/***/ ((module) => {

module.exports = require("@mui/material/CssBaseline");

/***/ }),

/***/ 5612:
/***/ ((module) => {

module.exports = require("@mui/material/Grid");

/***/ }),

/***/ 7934:
/***/ ((module) => {

module.exports = require("@mui/material/IconButton");

/***/ }),

/***/ 5486:
/***/ ((module) => {

module.exports = require("@mui/material/Menu");

/***/ }),

/***/ 9271:
/***/ ((module) => {

module.exports = require("@mui/material/MenuItem");

/***/ }),

/***/ 1431:
/***/ ((module) => {

module.exports = require("@mui/material/Toolbar");

/***/ }),

/***/ 7163:
/***/ ((module) => {

module.exports = require("@mui/material/Typography");

/***/ }),

/***/ 5574:
/***/ ((module) => {

module.exports = require("@mui/material/colors");

/***/ }),

/***/ 7986:
/***/ ((module) => {

module.exports = require("@mui/system");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 5429:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 3018:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6290:
/***/ ((module) => {

module.exports = require("react-icons/fa");

/***/ }),

/***/ 3094:
/***/ ((module) => {

module.exports = require("react-scroll");

/***/ }),

/***/ 8096:
/***/ ((module) => {

module.exports = require("react-slick");

/***/ }),

/***/ 1070:
/***/ ((module) => {

module.exports = require("react-typed");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [9400,1664,5675,7402,5642,3133], () => (__webpack_exec__(2748)));
module.exports = __webpack_exports__;

})();