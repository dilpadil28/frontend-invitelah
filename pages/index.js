import { Box } from "@mui/material";
import Head from "next/head";
import Beranda from "../components/Beranda/Beranda";
import Fitur from "../components/Fitur/Fitur";
import Harga from "../components/Harga/Harga";
import Keunggulan from "../components/Keunggulan/Keunggulan";
import Tema from "../components/Tema/Tema";
import TopBar from "../components/TopBar/TopBar";
import "aos/dist/aos.css";
import Footer from "../components/Footer";
import { api } from "../config/api";
import Sosmed from "../components/Sosmed";
import Loading from "../components/Loading";

function IndexPage({ logo }) {
  return (
    <div>
      <Head>
        <meta charset="UTF-8" />
        <meta name="keywords" content="undangan, pernikahan, digital" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Undangan Pernikahan Digital | Invitelah</title>
        <meta
          name="description"
          content="Undangan Pernikahan Digital Termurah dengan fitur Terlengkap berupa Website, Video & Image. Hanya Rp39.999"
          key="desc"
        />
        <meta
          property="og:title"
          content="Undangan Pernikahan Digital | Invitelah"
        />
        <meta
          property="og:description"
          content="Undangan Pernikahan Digital Termurah dengan fitur Terlengkap berupa Website, Video & Image. Hanya Rp39.999"
        />
        <meta property="og:image" content={logo} />
        <meta property="og:url" content={api.home} />
        <meta property="og:type" content="website" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:domain" content="invitelah.com" />
        <meta property="twitter:url" content={api.home} />
        <meta
          name="twitter:title"
          content="Undangan Pernikahan Digital | Invitelah"
        />
        <meta
          name="twitter:description"
          content="Undangan Pernikahan Digital Termurah dengan fitur Terlengkap berupa Website, Video & Image. Hanya Rp39.999"
        />
        <meta name="twitter:image" content={logo} />

        <meta
          name="google-site-verification"
          content="SJzWLRyt9WiyPKqADzFO6XCwj9W8zJgzcu0AU3ydXIs"
        />
        <link rel="canonical" href={api.home} key="canonical" />
      </Head>

      <header id="Beranda">
        <TopBar />
      </header>
      <main>
        <Beranda />
        <Box component="div" my={5} />
        <Box>
          <Keunggulan />
        </Box>
        <Fitur />
        <Box>
          <Sosmed />
        </Box>
        <Box>
          <Tema />
        </Box>
        {/* <Testimoni /> */}
        <Harga />
      </main>

      <Footer desc={true} />
    </div>
  );
}

export default IndexPage;
export const getServerSideProps = async (context) => {
  const { params, req, res, query } = context;
  // const to = query.to;
  // ssr caching
  res.setHeader(
    "Cache-Control",
    "public, s-maxage=1, stale-while-revalidate=59"
  );
  const logo = api.home + "/android-chrome-512x512.png";
  return { props: { logo } };
};
