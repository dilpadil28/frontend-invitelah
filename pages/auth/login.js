// import { Link } from "react-router-dom";

// material-ui
import { useTheme } from "@mui/material/styles";
import { Divider, Grid, Stack, Typography, useMediaQuery } from "@mui/material";
import AuthWrapper1 from "../../components/admin/authentication/AuthWrapper1";
import AuthCardWrapper from "../../components/admin/authentication/AuthCardWrapper";
import AuthLogin from "../../components/admin/authentication/auth-forms/AuthLogin";
import AuthFooter from "../../components/admin/authentication/AuthFooter";
import Logo from "../../components/admin/Logo";
import Link from "next/link";
import Head from "next/head";
import Loading from "../../components/Loading";

// project imports
// import AuthWrapper1 from "../AuthWrapper1";
// import AuthCardWrapper from "../AuthCardWrapper";
// import AuthLogin from "../auth-forms/AuthLogin";
// import Logo from "ui-component/Logo";
// import AuthFooter from "ui-component/cards/AuthFooter";

// assets

// ================================|| AUTH3 - LOGIN ||================================ //

const Login = () => {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <>
      <Head>
        <title>Invitelah - Login</title>
      </Head>
      <Loading>
        <AuthWrapper1>
          <Grid
            container
            direction="column"
            justifyContent="flex-end"
            sx={{ minHeight: "100vh" }}
          >
            <Grid item xs={12}>
              <Grid
                container
                justifyContent="center"
                alignItems="center"
                sx={{ minHeight: "calc(100vh - 68px)" }}
              >
                <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
                  <AuthCardWrapper>
                    <Grid
                      container
                      spacing={2}
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Grid item sx={{ mb: 3 }}>
                        <Logo />
                      </Grid>
                      <Grid item xs={12}>
                        <Grid
                          container
                          direction={matchDownSM ? "column-reverse" : "row"}
                          alignItems="center"
                          justifyContent="center"
                        >
                          <Grid item>
                            <Stack
                              alignItems="center"
                              justifyContent="center"
                              spacing={1}
                            >
                              <Typography
                                color={theme.palette.primary.main}
                                gutterBottom
                                variant={matchDownSM ? "h5" : "h4"}
                              >
                                Hi, Welcome Back
                              </Typography>
                            </Stack>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item xs={12}>
                        <AuthLogin />
                      </Grid>
                      <Grid item xs={12}>
                        <Divider />
                      </Grid>
                      <Grid item xs={12}>
                        <Grid
                          item
                          container
                          direction="column"
                          alignItems="center"
                          xs={12}
                        >
                          <Link passHref href={`/auth/register`}>
                            <Typography
                              variant="subtitle1"
                              sx={{ textDecoration: "none" }}
                            >
                              Don&apos;t have an account?
                            </Typography>
                          </Link>
                        </Grid>
                      </Grid>
                    </Grid>
                  </AuthCardWrapper>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sx={{ m: 3, mt: 1 }}>
              <AuthFooter />
            </Grid>
          </Grid>
        </AuthWrapper1>
      </Loading>
    </>
  );
};

export default Login;
