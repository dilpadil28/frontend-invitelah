/* eslint-disable @next/next/no-script-in-head */
/* eslint-disable @next/next/inline-script-id */
import { CssBaseline, ThemeProvider } from "@mui/material";
import { StyledEngineProvider } from "@mui/system";
import themes from "../themes";

import { store } from "../app/store";
import { Provider } from "react-redux";

import { SnackbarProvider } from "notistack";
import "../styles/globals.css";

import "../components/Beranda/Beranda.css";

import "../components/Testimoni/Testimoni.css";
import setupInterceptors from "../services/setupInterceptors";

import "moment/locale/id";
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import { useRouter } from "next/router";
import nProgress from "nprogress";

import "animate.css/animate.min.css";
import "animate.css";

function MyApp({ Component, pageProps }) {
  const router = useRouter();

  nProgress.configure({
    template: `<div class="preloader flex justify-center h-screen" role="bar">
        <img
          class="animate__animated animate__bounce animate__infinite"
          src="/static/images/logo/invitelah-left-green.svg"
          alt="invitelah.com"
          width="200"
        />
    </div>`,
  });

  useEffect(() => {
    const handleStart = () => {
      nProgress.start();
    };
    const handleStop = () => {
      nProgress.done();
    };

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleStop);
    router.events.on("routeChangeError", handleStop);

    return () => {
      router.events.off("routeChangeStart", handleStart);
      router.events.off("routeChangeComplete", handleStop);
      router.events.off("routeChangeError", handleStop);
    };
  }, [router.events]);
  useEffect(() => {
    Aos.init({
      easing: "ease-out-cubic",
      once: true,
      offset: 50,
    });
  }, []);
  return (
    <ThemeProvider theme={themes()}>
      <Provider store={store}>
        <SnackbarProvider maxSnack={3}>
          <StyledEngineProvider injectFirst>
            <CssBaseline />
            <Component {...pageProps} />
          </StyledEngineProvider>
        </SnackbarProvider>
      </Provider>
    </ThemeProvider>
  );
}

setupInterceptors(store);

export default MyApp;
