import { Box, Container } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import ListUser from "../../../components/admin/ListUser/index.js";
import ApplicationsTransactions from "../../../components/admin/Transactions/index.js";

// components

// layout for page

import Admin from "../../../layouts/Admin.js";

export default function Users() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const router = useRouter();
  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (currentUser?.roles.includes("ROLE_USER") || currentUser?.roles.includes("ROLE_MODERATOR")) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Users</title>
      </Head>
      <Admin>
        <ListUser />
      </Admin>
    </>
  );
}
