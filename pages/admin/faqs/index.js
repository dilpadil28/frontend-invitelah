/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createFaqList,
  deleteFaqList,
  getFaq,
  getFaqById,
  getFaqList,
  getFaqListById,
  updateFaq,
  updateFaqList,
} from "../../../features/faq/faqSlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/Faq/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/Faq/InputEdit.js";
import { options } from "../../../components/admin/Faq/options.js";
import InputHeader from "../../../components/admin/Faq/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStateFaq = {
  id: null,
  question: "",
  answer: "",
  faqId: "",
};
const initialStateFaqHeader = {
  id: null,
  title: "",
  description: "",
};

export default function Faqs() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { faq, faqList } = useSelector((state) => state.faq);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataFaq, setDataFaq] = useState(initialStateFaq);
  const [dataFaqHeader, setDataFaqHeader] = useState(initialStateFaqHeader);

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "question",
      label: "Question",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "answer",
      label: "Answer",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Faq" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(faqList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Faq" arrow>
                  <IconButton
                    onClick={() => handleDeleteOpen(faqList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getFaqById(faq[0].id))
      .then((response) => {
        setDataFaqHeader(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangeFaqHeader = (e) => {
    setDataFaqHeader({
      ...dataFaqHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditFaqHeader = () => {
    const { title, description } = dataFaqHeader;
    const fData = {
      id: faq[0].id,
      result: { title: title, description: description },
    };
    dispatch(updateFaq(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataFaqHeader(initialStateFaqHeader);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteFaqList(dialogId));
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getFaqListById(id))
      .then((response) => {
        setDataFaq(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeFaq = (e) => {
    if (e.target.name != "image") {
      setDataFaq({
        ...dataFaq,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataFaq({
        ...dataFaq,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateFaq = () => {
    const { question, answer } = dataFaq;
    dispatch(createFaqList({ question, answer }))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  const handleEditFaq = () => {
    const { question, answer } = dataFaq;
    const fData = {
      id: dialogId,
      result: { question, answer },
    };
    dispatch(updateFaqList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  useEffect(() => {
    dispatch(getFaq());
    dispatch(getFaqList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (
      currentUser?.roles.includes("ROLE_USER") ||
      currentUser?.roles.includes("ROLE_MODERATOR")
    ) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Faqs</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Faqs
            </Typography>
          </Grid>
          <Grid item>
            <Button
              className="!bg-primary"
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Faq
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={faqList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Faq"}
        create={handleCreateFaq}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={<InputCreate onValueChangeFaq={onValueChangeFaq} />}
      />
      <EditDialog
        name={"Faq"}
        edit={handleEditFaq}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit dataFaq={dataFaq} onValueChangeFaq={onValueChangeFaq} />
        }
      />
      <EditDialog
        name={"Faq Header"}
        edit={handleEditFaqHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataFaqHeader={dataFaqHeader}
            onValueChangeFaqHeader={onValueChangeFaqHeader}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
