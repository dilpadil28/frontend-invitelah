/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createPriceList,
  deletePriceList,
  getPrice,
  getPriceById,
  getPriceList,
  getPriceListById,
  updatePrice,
  updatePriceList,
} from "../../../features/price/priceSlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/Price/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/Price/InputEdit.js";
import { options } from "../../../components/admin/Price/options.js";
import InputHeader from "../../../components/admin/Price/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStatePrice = {
  id: null,
  label: "",
  harga: "",
  discountPrice: "",
  discountAmount: "",
  type: "",
  list: [],
  priceId: "",
};
const initialStatePriceHeader = {
  id: null,
  title: "",
  description: "",
  discount: "",
  discountTitle: "",
  discountDescription: "",
  discountExpired: "",
};

export default function Prices() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { price, priceList } = useSelector((state) => state.price);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataPrice, setDataPrice] = useState(initialStatePrice);
  const [dataPriceHeader, setDataPriceHeader] = useState(
    initialStatePriceHeader
  );

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "label",
      label: "Label",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "harga",
      label: "Price",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "discountPrice",
      label: "Discount Price",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "discountAmount",
      label: "Discount Amount",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "type",
      label: "Type",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "list",
      label: "List",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Price" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(priceList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Price" arrow>
                  <IconButton
                    onClick={() => handleDeleteOpen(priceList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getPriceById(price[0].id))
      .then((response) => {
        setDataPriceHeader(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangePriceHeader = (e) => {
    setDataPriceHeader({
      ...dataPriceHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditPriceHeader = () => {
    const {
      title,
      description,
      discount,
      discountTitle,
      discountDescription,
      discountExpired,
    } = dataPriceHeader;
    const fData = {
      id: price[0].id,
      result: {
        title,
        description,
        discount,
        discountTitle,
        discountDescription,
        discountExpired,
      },
    };
    dispatch(updatePrice(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataPriceHeader(initialStatePriceHeader);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deletePriceList(dialogId))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getPriceListById(id))
      .then((response) => {
        setDataPrice(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangePrice = (e) => {
    if (e.target.name != "image") {
      setDataPrice({
        ...dataPrice,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataPrice({
        ...dataPrice,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreatePrice = () => {
    const { label, harga, discountPrice, discountAmount, type, list } =
      dataPrice;
    dispatch(
      createPriceList({
        label,
        harga,
        discountPrice,
        discountAmount,
        type,
        list,
      })
    )
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);

        console.log(e);
      });
  };
  const handleEditPrice = () => {
    const { label, harga, discountPrice, discountAmount, type, list } =
      dataPrice;
    const fData = {
      id: dialogId,
      result: { label, harga, discountPrice, discountAmount, type, list },
    };
    dispatch(updatePriceList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);

        console.log(e);
      });
  };
  useEffect(() => {
    dispatch(getPrice());
    dispatch(getPriceList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (
      currentUser?.roles.includes("ROLE_USER") ||
      currentUser?.roles.includes("ROLE_MODERATOR")
    ) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Prices</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Prices
            </Typography>
          </Grid>
          <Grid item>
            <Button
              className="!bg-primary"
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Price
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={priceList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Price"}
        create={handleCreatePrice}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={<InputCreate onValueChangePrice={onValueChangePrice} />}
      />
      <EditDialog
        name={"Price"}
        edit={handleEditPrice}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataPrice={dataPrice}
            onValueChangePrice={onValueChangePrice}
          />
        }
      />
      <EditDialog
        name={"Price Header"}
        edit={handleEditPriceHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataPriceHeader={dataPriceHeader}
            onValueChangePriceHeader={onValueChangePriceHeader}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
