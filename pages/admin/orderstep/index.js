/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createOrderStepList,
  deleteOrderStepList,
  getOrderStep,
  getOrderStepById,
  getOrderStepList,
  getOrderStepListById,
  updateOrderStep,
  updateOrderStepList,
} from "../../../features/orderStep/orderStepSlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/OrderStep/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/OrderStep/InputEdit.js";
import { options } from "../../../components/admin/OrderStep/options.js";
import InputHeader from "../../../components/admin/OrderStep/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStateOrderStep = {
  id: null,
  title: "",
  image: "",
  orderStepId: "",
};
const initialStateOrderStepHeader = {
  id: null,
  title: "",
  description: "",
};

export default function OrderSteps() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { orderStep, orderStepList } = useSelector((state) => state.orderStep);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataOrderStep, setDataOrderStep] = useState(initialStateOrderStep);
  const [dataOrderStepHeader, setDataOrderStepHeader] = useState(
    initialStateOrderStepHeader
  );

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Order Step" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(orderStepList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Order Step" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(orderStepList[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getOrderStepById(orderStep[0].id))
      .then((response) => {
        setDataOrderStepHeader(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangeOrderStepHeader = (e) => {
    setDataOrderStepHeader({
      ...dataOrderStepHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditOrderStepHeader = () => {
    const { title, description } = dataOrderStepHeader;
    const fData = {
      id: orderStep[0].id,
      result: { title: title, description: description },
    };
    dispatch(updateOrderStep(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataOrderStepHeader(initialStateOrderStepHeader);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteOrderStepList(dialogId))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getOrderStepListById(id))
      .then((response) => {
        setDataOrderStep(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeOrderStep = (e) => {
    if (e.target.name != "image") {
      setDataOrderStep({
        ...dataOrderStep,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataOrderStep({
        ...dataOrderStep,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateOrderStep = () => {
    const formData = new FormData();
    const { title, image } = dataOrderStep;
    formData.append("title", title);
    formData.append("image", image);
    formData.append("orderStepId", orderStep[0].id);
    dispatch(createOrderStepList(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  const handleEditOrderStep = () => {
    const formData = new FormData();
    const { title, image } = dataOrderStep;
    formData.append("title", title);
    formData.append("image", image);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateOrderStepList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  useEffect(() => {
    dispatch(getOrderStep());
    dispatch(getOrderStepList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (
      currentUser?.roles.includes("ROLE_USER") ||
      currentUser?.roles.includes("ROLE_MODERATOR")
    ) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Order Step</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Order Step
            </Typography>
          </Grid>
          <Grid item>
            <Button
              className="!bg-primary"
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Order Step
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={orderStepList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Order Step"}
        create={handleCreateOrderStep}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeOrderStep={onValueChangeOrderStep} />
        }
      />
      <EditDialog
        name={"Order Step"}
        edit={handleEditOrderStep}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataOrderStep={dataOrderStep}
            onValueChangeOrderStep={onValueChangeOrderStep}
          />
        }
      />
      <EditDialog
        name={"Order Step Header"}
        edit={handleEditOrderStepHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataOrderStepHeader={dataOrderStepHeader}
            onValueChangeOrderStepHeader={onValueChangeOrderStepHeader}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
