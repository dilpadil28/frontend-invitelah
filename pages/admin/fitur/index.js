/* eslint-disable @next/next/no-img-element */
import { Alert, Button, Grid, IconButton, Snackbar, Tooltip, Typography } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createFiturList,
  deleteFiturList,
  getFitur,
  getFiturById,
  getFiturList,
  getFiturListById,
  updateFitur,
  updateFiturList,
} from "../../../features/fitur/fiturSlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/Fitur/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/Fitur/InputEdit.js";
import { options } from "../../../components/admin/Fitur/options.js";
import InputHeader from "../../../components/admin/Fitur/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStateFitur = {
  id: null,
  title: "",
  description: "",
  image: "",
  fiturId: "",
};
const initialStateFiturHeader = {
  id: null,
  title: "",
  description: "",
};

export default function Fiturs() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { fitur, fiturList } = useSelector((state) => state.fitur);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataFitur, setDataFitur] = useState(initialStateFitur);
  const [dataFiturHeader, setDataFiturHeader] = useState(
    initialStateFiturHeader
  );
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };


  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "description",
      label: "Description",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Fitur" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(fiturList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Fitur" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(fiturList[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getFiturById(fitur[0].id))
      .then((response) => {
        setDataFiturHeader(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangeFiturHeader = (e) => {
    setDataFiturHeader({
      ...dataFiturHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditFiturHeader = () => {
    const { title, description } = dataFiturHeader;
    const fData = {
      id: fitur[0].id,
      result: { title: title, description: description },
    };
    dispatch(updateFitur(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose(); setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataFiturHeader(initialStateFiturHeader);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteFiturList(dialogId)).unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getFiturListById(id))
      .then((response) => {
        setDataFitur(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeFitur = (e) => {
    if (e.target.name != "image") {
      setDataFitur({
        ...dataFitur,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataFitur({
        ...dataFitur,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateFitur = () => {
    const formData = new FormData();
    const { title, description, image } = dataFitur;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("image", image);
    formData.append("fiturId", fitur[0].id);
    dispatch(createFiturList(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose(); setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  const handleEditFitur = () => {
    const formData = new FormData();
    const { title, description, image } = dataFitur;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("image", image);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateFiturList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose(); setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  useEffect(() => {
    dispatch(getFitur());
    dispatch(getFiturList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (currentUser?.roles.includes("ROLE_USER") || currentUser?.roles.includes("ROLE_MODERATOR")) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Fitur</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Fitur
            </Typography>
          </Grid>
          <Grid item>
            <Button
            className="!bg-primary"
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
            className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Fitur
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={fiturList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Fitur"}
        create={handleCreateFitur}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={<InputCreate onValueChangeFitur={onValueChangeFitur} />}
      />
      <EditDialog
        name={"Fitur"}
        edit={handleEditFitur}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataFitur={dataFitur}
            onValueChangeFitur={onValueChangeFitur}
          />
        }
      />
      <EditDialog
        name={"Fitur Header"}
        edit={handleEditFiturHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataFiturHeader={dataFiturHeader}
            onValueChangeFiturHeader={onValueChangeFiturHeader}
          />
        }
      />
      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}
