/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createSuperiorityList,
  deleteSuperiorityList,
  getSuperiority,
  getSuperiorityById,
  getSuperiorityList,
  getSuperiorityListById,
  updateSuperiority,
  updateSuperiorityList,
} from "../../../features/superiority/superioritySlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/Superiority/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/Superiority/InputEdit.js";
import { options } from "../../../components/admin/Superiority/options.js";
import InputHeader from "../../../components/admin/Superiority/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStateSuperiority = {
  id: null,
  title: "",
  image: "",
  superiorityId: "",
};
const initialStateSuperiorityHeader = {
  id: null,
  title: "",
  description: "",
};

export default function Superioritys() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { superiority, superiorityList } = useSelector(
    (state) => state.superiority
  );
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataSuperiority, setDataSuperiority] = useState(
    initialStateSuperiority
  );
  const [dataSuperiorityHeader, setDataSuperiorityHeader] = useState(
    initialStateSuperiorityHeader
  );

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Superiority" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(superiorityList[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Superiority" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(superiorityList[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getSuperiorityById(superiority[0].id))
      .then((response) => {
        setDataSuperiorityHeader(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangeSuperiorityHeader = (e) => {
    setDataSuperiorityHeader({
      ...dataSuperiorityHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditSuperiorityHeader = () => {
    const { title, description } = dataSuperiorityHeader;
    const fData = {
      id: superiority[0].id,
      result: { title: title, description: description },
    };
    dispatch(updateSuperiority(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataSuperiorityHeader(initialStateSuperiorityHeader);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteSuperiorityList(dialogId))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getSuperiorityListById(id))
      .then((response) => {
        setDataSuperiority(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeSuperiority = (e) => {
    if (e.target.name != "image") {
      setDataSuperiority({
        ...dataSuperiority,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataSuperiority({
        ...dataSuperiority,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateSuperiority = () => {
    const formData = new FormData();
    const { title, image } = dataSuperiority;
    formData.append("title", title);
    formData.append("image", image);
    formData.append("superiorityId", superiority[0].id);
    dispatch(createSuperiorityList(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditSuperiority = () => {
    const formData = new FormData();
    const { title, image } = dataSuperiority;
    formData.append("title", title);
    formData.append("image", image);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateSuperiorityList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  useEffect(() => {
    dispatch(getSuperiority());
    dispatch(getSuperiorityList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (
      currentUser?.roles.includes("ROLE_USER") ||
      currentUser?.roles.includes("ROLE_MODERATOR")
    ) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Superiority</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Superiority
            </Typography>
          </Grid>
          <Grid item>
            <Button
              className="!bg-primary"
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Superiority
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={superiorityList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Superiority"}
        create={handleCreateSuperiority}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeSuperiority={onValueChangeSuperiority} />
        }
      />
      <EditDialog
        name={"Superiority"}
        edit={handleEditSuperiority}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataSuperiority={dataSuperiority}
            onValueChangeSuperiority={onValueChangeSuperiority}
          />
        }
      />
      <EditDialog
        name={"Superiority Header"}
        edit={handleEditSuperiorityHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataSuperiorityHeader={dataSuperiorityHeader}
            onValueChangeSuperiorityHeader={onValueChangeSuperiorityHeader}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
