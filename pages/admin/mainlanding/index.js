/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createMainLandingList,
  deleteMainLandingList,
  getMainLanding,
  getMainLandingById,
  getMainLandingList,
  getMainLandingListById,
  updateMainLanding,
  updateMainLandingList,
} from "../../../features/mainLanding/mainLandingSlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/MainLanding/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/MainLanding/InputEdit.js";
import { options } from "../../../components/admin/MainLanding/options.js";
import InputHeader from "../../../components/admin/MainLanding/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStateMainLanding = {
  id: null,
  title: "",
  description: "",
  image: "",
  mainLandingId: "",
};
const initialStateMainLandingHeader = {
  id: null,
  title: "",
  description: "",
};

export default function MainLandings() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { mainLanding, mainLandingList } = useSelector(
    (state) => state.mainLanding
  );
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataMainLanding, setDataMainLanding] = useState(
    initialStateMainLanding
  );
  const [dataMainLandingHeader, setDataMainLandingHeader] = useState(
    initialStateMainLandingHeader
  );

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "description",
      label: "Description",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Main Landing" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(mainLandingList[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Main Landing" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(mainLandingList[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getMainLandingById(mainLanding[0].id))
      .then((response) => {
        setDataMainLandingHeader(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangeMainLandingHeader = (e) => {
    setDataMainLandingHeader({
      ...dataMainLandingHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditMainLandingHeader = () => {
    const { title, description } = dataMainLandingHeader;
    const fData = {
      id: mainLanding[0].id,
      result: { title: title, description: description },
    };
    dispatch(updateMainLanding(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataMainLanding(initialStateMainLanding);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteMainLandingList(dialogId))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getMainLandingListById(id))
      .then((response) => {
        setDataMainLanding(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeMainLanding = (e) => {
    if (e.target.name != "image") {
      setDataMainLanding({
        ...dataMainLanding,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataMainLanding({
        ...dataMainLanding,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateMainLanding = () => {
    const formData = new FormData();
    const { title, description, image } = dataMainLanding;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("image", image);
    formData.append("mainLandingId", mainLanding[0].id);
    dispatch(createMainLandingList(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  const handleEditMainLanding = () => {
    const formData = new FormData();
    const { title, description, image } = dataMainLanding;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("image", image);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateMainLandingList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  useEffect(() => {
    dispatch(getMainLanding());
    dispatch(getMainLandingList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (
      currentUser?.roles.includes("ROLE_USER") ||
      currentUser?.roles.includes("ROLE_MODERATOR")
    ) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>Main Landing</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Main Landing
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Main Landing
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={mainLandingList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Main Landing"}
        create={handleCreateMainLanding}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeMainLanding={onValueChangeMainLanding} />
        }
      />
      <EditDialog
        name={"Main Landing"}
        edit={handleEditMainLanding}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataMainLanding={dataMainLanding}
            onValueChangeMainLanding={onValueChangeMainLanding}
          />
        }
      />
      <EditDialog
        name={"Main Landing Header"}
        edit={handleEditMainLandingHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataMainLandingHeader={dataMainLandingHeader}
            onValueChangeMainLandingHeader={onValueChangeMainLandingHeader}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
