/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createSocialMedia,
  deleteSocialMedia,
  getSocialMediaById,
  updateSocialMedia,
  getSocialMediaByInvitationId,
} from "../../../../../features/socialMedia/socialMediaSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/SocialMedia/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/SocialMedia/InputEdit.js";
import { options } from "../../../../../components/admin/SocialMedia/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStateSocialMedia = {
  id: null,
  name: "instagram",
  url: "",
  type: "pria",
  invitationId: "",
};

export default function SocialMedia() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { socialMedia } = useSelector((state) => state.socialMedia);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "type",
      label: "Type",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return <>{value?.namaPria + " & " + value?.namaWanita}</>;
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ||
              currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit SocialMedia" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(socialMedia[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ||
              currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete SocialMedia" arrow>
                  <IconButton
                    onClick={() => handleDeleteOpen(socialMedia[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const [dataSocialMedia, setDataSocialMedia] = useState(
    initialStateSocialMedia
  );
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataSocialMedia(initialStateSocialMedia);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        });
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getSocialMediaByInvitationId(router.query.invitationId));
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteSocialMedia(dialogId))
      .then((response) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getSocialMediaByInvitationId(router.query.invitationId));
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getSocialMediaById(id))
      .then((response) => {
        setDataSocialMedia(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeSocialMedia = (e) => {
    if (e.target.name != "image") {
      setDataSocialMedia({
        ...dataSocialMedia,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataSocialMedia({
        ...dataSocialMedia,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateSocialMedia = () => {
    const formData = new FormData();
    const { name, url, type } = dataSocialMedia;
    formData.append("name", name);
    formData.append("url", url);
    formData.append("type", type);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createSocialMedia(formData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        handleCreateModalClose();
        dispatch(getSocialMediaByInvitationId(router.query.invitationId));
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditSocialMedia = () => {
    const formData = new FormData();
    const { name, url, type } = dataSocialMedia;
    formData.append("name", name);
    formData.append("url", url);
    formData.append("type", type);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateSocialMedia(fData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getSocialMediaByInvitationId(router.query.invitationId));
        handleEditModalClose();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, socialMedia]);

  return (
    <>
      <Head>
        <title>Social Media</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Social Media
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Social Media
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={socialMedia || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Social Media"}
        create={handleCreateSocialMedia}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeSocialMedia={onValueChangeSocialMedia} />
        }
      />
      <EditDialog
        name={"Social Media"}
        edit={handleEditSocialMedia}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataSocialMedia={dataSocialMedia}
            onValueChangeSocialMedia={onValueChangeSocialMedia}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
