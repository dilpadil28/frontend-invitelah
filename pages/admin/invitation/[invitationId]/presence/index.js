/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import { Alert, Button, Chip, Grid, IconButton, Snackbar, Tooltip, Typography } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createPresence,
  deletePresence,
  getPresenceById,
  updatePresence,
  getPresenceByInvitationId,
} from "../../../../../features/presence/presenceSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/Presence/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/Presence/InputEdit.js";
import { options } from "../../../../../components/admin/Presence/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStatePresence = {
  id: null,
  name: "",
  phoneNumber: "",
  message: "",
  confirmation: "hadir",
  total: "1",
  published: true,
  invitationId: ""
};

export default function Presences() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { presence } = useSelector((state) => state.presence);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "message",
      label: "Message",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "phoneNumber",
      label: "Phone Number",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "confirmation",
      label: "Confirmation",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "published",
      label: "Published",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {
                value ?
                  <Chip label="Published" color="success" variant="outlined" />
                  :
                  <Chip label="Draft" color="error" variant="outlined" />

              }

            </>
          );
        },
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value?.namaPria + ' & ' + value?.namaWanita}
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit Presence" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(presence[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete Presence" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(presence[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const [dataPresence, setDataPresence] = useState(
    initialStatePresence
  );
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataPresence(initialStatePresence)
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        })
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getPresenceByInvitationId(router.query.invitationId));
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deletePresence(dialogId))
      .then((response) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getPresenceByInvitationId(router.query.invitationId))
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getPresenceById(id))
      .then((response) => {
        setDataPresence(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangePresence = (e) => {
    if (e.target.name != "image") {
      setDataPresence({
        ...dataPresence,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataPresence({
        ...dataPresence,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreatePresence = () => {
    const formData = new FormData();
    const { name, phoneNumber, message, total, confirmation, published } = dataPresence;
    formData.append("name", name);
    formData.append("phoneNumber", phoneNumber);
    formData.append("message", message);
    formData.append("confirmation", confirmation);
    formData.append("total", total);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createPresence(formData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        handleCreateModalClose();
        dispatch(getPresenceByInvitationId(router.query.invitationId))
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditPresence = () => {
    const formData = new FormData();
    const { name, phoneNumber, message, total, confirmation, published } = dataPresence;
    formData.append("name", name);
    formData.append("phoneNumber", phoneNumber);
    formData.append("message", message);
    formData.append("confirmation", confirmation);
    formData.append("total", total);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updatePresence(fData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getPresenceByInvitationId(router.query.invitationId))
        handleEditModalClose();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, presence]);

  return (
    <>
      <Head>
        <title>Presences</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Presences
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
            className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create presence
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={presence || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Presence"}
        create={handleCreatePresence}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangePresence={onValueChangePresence} />
        }
      />
      <EditDialog
        name={"Presence"}
        edit={handleEditPresence}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataPresence={dataPresence}
            onValueChangePresence={onValueChangePresence}
          />
        }
      />
      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}

