/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import { Alert, Button, Chip, Grid, IconButton, Snackbar, Tooltip, Typography } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createPhotoGallery,
  deletePhotoGallery,
  getPhotoGalleryById,
  updatePhotoGallery,
  getPhotoGalleryByInvitationId,
} from "../../../../../features/photoGallery/photoGallerySlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/PhotoGallery/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/PhotoGallery/InputEdit.js";
import { options } from "../../../../../components/admin/PhotoGallery/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStatePhotoGallery = {
  id: null,
  title: "",
  description: "",
  published: true,
  image: "",
  invitationId: ""
};

export default function PhotoGallery() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { photoGallery } = useSelector((state) => state.photoGallery);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState(""); const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value?.namaPria + ' & ' + value?.namaWanita}
            </>
          );
        },
      },
    },
    {
      name: "published",
      label: "Published",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {
                value ?
                  <Chip label="Published" color="success" variant="outlined" />
                  :
                  <Chip label="Draft" color="error" variant="outlined" />

              }

            </>
          );
        },
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },

    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit PhotoGallery" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(photoGallery[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete PhotoGallery" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(photoGallery[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const [dataPhotoGallery, setDataPhotoGallery] = useState(
    initialStatePhotoGallery
  );
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataPhotoGallery(initialStatePhotoGallery)
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        })
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getPhotoGalleryByInvitationId(router.query.invitationId));
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deletePhotoGallery(dialogId))
      .then((response) => {
        dispatch(getPhotoGalleryByInvitationId(router.query.invitationId))
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getPhotoGalleryById(id))
      .then((response) => {
        setDataPhotoGallery(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangePhotoGallery = (e) => {
    if (e.target.name != "image") {
      setDataPhotoGallery({
        ...dataPhotoGallery,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataPhotoGallery({
        ...dataPhotoGallery,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreatePhotoGallery = () => {
    const formData = new FormData();
    const { title, image, description, published } = dataPhotoGallery;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("published", published);
    formData.append("image", image);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createPhotoGallery(formData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        handleCreateModalClose();
        dispatch(getPhotoGalleryByInvitationId(router.query.invitationId))
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log('err', err.response);
      });
  };
  const handleEditPhotoGallery = () => {
    const formData = new FormData();
    const { title, image, description, published } = dataPhotoGallery;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("published", published);
    formData.append("image", image);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updatePhotoGallery(fData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getPhotoGalleryByInvitationId(router.query.invitationId))
        handleEditModalClose();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, photoGallery]);

  return (
    <>
      <Head>
        <title>Photo Gallerys</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Photo Gallerys
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
            className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Photo Gallery
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={photoGallery || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"PhotoGallery"}
        create={handleCreatePhotoGallery}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangePhotoGallery={onValueChangePhotoGallery} />
        }
      />
      <EditDialog
        name={"PhotoGallery"}
        edit={handleEditPhotoGallery}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataPhotoGallery={dataPhotoGallery}
            onValueChangePhotoGallery={onValueChangePhotoGallery}
          />
        }
      />
      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}

