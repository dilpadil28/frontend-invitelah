/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createTheme,
  deleteTheme,
  getThemeById,
  updateTheme,
  getThemeByInvitationId,
} from "../../../../../features/theme/themeSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/Theme/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/Theme/InputEdit.js";
import { options } from "../../../../../components/admin/Theme/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";
import { getMusic } from "../../../../../features/music/musicSlice.js";

const initialStateTheme = {
  id: null,
  name: "White-Flower",
  galleryType: "basic",
  fontType1: "Niconne",
  fontType2: "Bebas Neue",
  fontColor1: "",
  fontColor2: "",
  backgroundColor: "",
  backgroundImage: "",
  cardColor: "",
  publishProkes: true,
  musicId: 3,
  invitationId: "",
  musicId: "",
};

export default function Theme() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const thm = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { theme } = useSelector((state) => state.theme);
  const { message } = useSelector((state) => state.message);
  const { music } = useSelector((state) => state.music);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return <>{value?.namaPria + " & " + value?.namaWanita}</>;
        },
      },
    },
    {
      name: "musicId",
      label: "Music",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return <>{value}</>;
        },
      },
    },

    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ||
              currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit Theme" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(theme[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: thm.palette.primary.light,
                      },
                      color: thm.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ||
              currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete Theme" arrow>
                  <IconButton
                    onClick={() => handleDeleteOpen(theme[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: thm.palette.error.light,
                      },
                      color: thm.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const [dataTheme, setDataTheme] = useState(initialStateTheme);
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataTheme(initialStateTheme);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        });
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getThemeByInvitationId(router.query.invitationId));
      dispatch(getMusic());
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteTheme(dialogId))
      .unwrap()
      .then((result) => {
        dispatch(getThemeByInvitationId(router.query.invitationId));
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getThemeById(id))
      .then((response) => {
        setDataTheme(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeTheme = (e) => {
    if (e.target.name != "image") {
      setDataTheme({
        ...dataTheme,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataTheme({
        ...dataTheme,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateTheme = () => {
    const formData = new FormData();
    const {
      name,
      galleryType,
      publishProkes,
      backgroundImage,
      fontType1,
      fontType2,
      fontColor1,
      fontColor2,
      backgroundColor,
      cardColor,
      musicId,
    } = dataTheme;
    formData.append("name", name);
    formData.append("galleryType", galleryType);
    formData.append("fontType1", fontType1);
    formData.append("fontType2", fontType2);
    formData.append("fontColor1", fontColor1);
    formData.append("fontColor2", fontColor2);
    formData.append("backgroundColor", backgroundColor);
    formData.append("cardColor", cardColor);
    formData.append("publishProkes", publishProkes);
    formData.append("backgroundImage", backgroundImage);
    formData.append("musicId", musicId);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createTheme(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        dispatch(getThemeByInvitationId(router.query.invitationId));
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  const handleEditTheme = () => {
    const formData = new FormData();
    const {
      name,
      galleryType,
      publishProkes,
      backgroundImage,
      fontType1,
      fontType2,
      fontColor1,
      fontColor2,
      backgroundColor,
      cardColor,
      musicId,
    } = dataTheme;
    formData.append("name", name);
    formData.append("galleryType", galleryType);
    formData.append("fontType1", fontType1);
    formData.append("fontType2", fontType2);
    formData.append("fontColor1", fontColor1);
    formData.append("fontColor2", fontColor2);
    formData.append("publishProkes", publishProkes);
    formData.append("backgroundImage", backgroundImage);
    formData.append("backgroundColor", backgroundColor);
    formData.append("cardColor", cardColor);
    formData.append("musicId", musicId);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateTheme(fData))
      .unwrap()
      .then((result) => {
        dispatch(getThemeByInvitationId(router.query.invitationId));
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, theme]);

  return (
    <>
      <Head>
        <title>Theme</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Theme
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
              className="!bg-primary"
              disabled={theme?.length > 0 ? true : false}
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create theme
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={theme || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Theme"}
        create={handleCreateTheme}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        inputCreate={
          <InputCreate music={music} onValueChangeTheme={onValueChangeTheme} />
        }
      />
      <EditDialog
        name={"Theme"}
        edit={handleEditTheme}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        inputEdit={
          <InputEdit
            music={music}
            dataTheme={dataTheme}
            onValueChangeTheme={onValueChangeTheme}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
