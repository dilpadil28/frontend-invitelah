/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Chip,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createYoutube,
  deleteYoutube,
  getYoutubeById,
  updateYoutube,
  getYoutubeByInvitationId,
} from "../../../../../features/youtube/youtubeSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/Youtube/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/Youtube/InputEdit.js";
import { options } from "../../../../../components/admin/Youtube/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStateYoutube = {
  id: null,
  title: "",
  url: "",
  published: true,
  invitationId: "",
};

export default function Youtube() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { youtube } = useSelector((state) => state.youtube);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "published",
      label: "Published",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value ? (
                <Chip label="Published" color="success" variant="outlined" />
              ) : (
                <Chip label="Draft" color="error" variant="outlined" />
              )}
            </>
          );
        },
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return <>{value?.namaPria + " & " + value?.namaWanita}</>;
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ||
              currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit Youtube" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(youtube[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ||
              currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete Youtube" arrow>
                  <IconButton
                    onClick={() => handleDeleteOpen(youtube[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const [dataYoutube, setDataYoutube] = useState(initialStateYoutube);
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataYoutube(initialStateYoutube);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        });
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getYoutubeByInvitationId(router.query.invitationId));
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteYoutube(dialogId))
      .then((response) => {
        dispatch(getYoutubeByInvitationId(router.query.invitationId));
        setSuccessful(true);
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getYoutubeById(id))
      .then((response) => {
        setDataYoutube(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeYoutube = (e) => {
    if (e.target.name != "image") {
      setDataYoutube({
        ...dataYoutube,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataYoutube({
        ...dataYoutube,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateYoutube = () => {
    const formData = new FormData();
    const { title, url, published } = dataYoutube;
    formData.append("title", title);
    formData.append("url", url);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createYoutube(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        dispatch(getYoutubeByInvitationId(router.query.invitationId));
        setSuccessful(true);
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditYoutube = () => {
    const formData = new FormData();
    const { title, url, published } = dataYoutube;
    formData.append("title", title);
    formData.append("url", url);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateYoutube(fData))
      .unwrap()
      .then((result) => {
        dispatch(getYoutubeByInvitationId(router.query.invitationId));
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, youtube]);

  return (
    <>
      <Head>
        <title>Youtube</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Youtube
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create youtube
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={youtube || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Youtube"}
        create={handleCreateYoutube}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeYoutube={onValueChangeYoutube} />
        }
      />
      <EditDialog
        name={"Youtube"}
        edit={handleEditYoutube}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataYoutube={dataYoutube}
            onValueChangeYoutube={onValueChangeYoutube}
          />
        }
      />
      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
