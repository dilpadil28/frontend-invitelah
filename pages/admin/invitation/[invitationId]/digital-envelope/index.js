/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import { Button, Snackbar, Alert, Chip, Grid, IconButton, Tooltip, Typography } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createDigitalEnvelope,
  deleteDigitalEnvelope,
  getDigitalEnvelopeById,
  updateDigitalEnvelope,
  getDigitalEnvelopeByInvitationId,
} from "../../../../../features/digitalEnvelope/digitalEnvelopeSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/DigitalEnvelope/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/DigitalEnvelope/InputEdit.js";
import { options } from "../../../../../components/admin/DigitalEnvelope/options.js";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStateDigitalEnvelope = {
  id: null,
  name: "mandiri",
  number: "",
  published: true,
  atasNama: "",
  invitationId: ""
};

export default function DigitalEnvelopes() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { digitalEnvelope } = useSelector((state) => state.digitalEnvelope);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "number",
      label: "Number",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "published",
      label: "Published",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {
                value ?
                  <Chip label="Published" color="success" variant="outlined" />
                  :
                  <Chip label="Draft" color="error" variant="outlined" />

              }

            </>
          );
        },
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value?.namaPria + ' & ' + value?.namaWanita}
            </>
          );
        },
      },
    },
    {
      name: "atasNama",
      label: "Atas Nama",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value}
            </>
          );
        },
      },
    },

    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit DigitalEnvelope" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(digitalEnvelope[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete DigitalEnvelope" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(digitalEnvelope[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const [dataDigitalEnvelope, setDataDigitalEnvelope] = useState(
    initialStateDigitalEnvelope
  );
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataDigitalEnvelope(initialStateDigitalEnvelope)
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        })
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getDigitalEnvelopeByInvitationId(router.query.invitationId));
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteDigitalEnvelope(dialogId))
      .then((response) => {
        dispatch(getDigitalEnvelopeByInvitationId(router.query.invitationId))
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getDigitalEnvelopeById(id))
      .then((response) => {
        setDataDigitalEnvelope(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeDigitalEnvelope = (e) => {
    if (e.target.name != "image") {
      setDataDigitalEnvelope({
        ...dataDigitalEnvelope,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataDigitalEnvelope({
        ...dataDigitalEnvelope,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateDigitalEnvelope = () => {
    const formData = new FormData();
    const { name, number, published, atasNama } = dataDigitalEnvelope;
    formData.append("name", name);
    formData.append("number", number);
    formData.append("published", published);
    formData.append("atasNama", atasNama);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createDigitalEnvelope(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        dispatch(getDigitalEnvelopeByInvitationId(router.query.invitationId))
        setSuccessful(true);
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditDigitalEnvelope = () => {
    const formData = new FormData();
    const { name, number, published, atasNama } = dataDigitalEnvelope;
    formData.append("name", name);
    formData.append("number", number);
    formData.append("published", published);
    formData.append("atasNama", atasNama);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateDigitalEnvelope(fData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getDigitalEnvelopeByInvitationId(router.query.invitationId))
        handleEditModalClose();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, digitalEnvelope]);

  return (
    <>
      <Head>
        <title>DigitalEnvelopes</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              DigitalEnvelopes
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
            className="!bg-primary"
              disabled={digitalEnvelope?.length >= 4}
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create Digital Envelope
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={digitalEnvelope || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"DigitalEnvelope"}
        create={handleCreateDigitalEnvelope}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeDigitalEnvelope={onValueChangeDigitalEnvelope} />
        }
      />
      <EditDialog
        name={"DigitalEnvelope"}
        edit={handleEditDigitalEnvelope}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataDigitalEnvelope={dataDigitalEnvelope}
            onValueChangeDigitalEnvelope={onValueChangeDigitalEnvelope}
          />
        }
      />
      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}

