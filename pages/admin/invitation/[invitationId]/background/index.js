/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import { Alert, Button, Chip, Grid, IconButton, Snackbar, Tooltip, Typography } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createBackground,
  deleteBackground,
  getBackgroundById,
  updateBackground,
  getBackgroundByInvitationId,
} from "../../../../../features/background/backgroundSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/Background/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/Background/InputEdit.js";
import { options } from "../../../../../components/admin/Background/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStateBackground = {
  id: null,
  name: "",
  image: "",
  published: true,
  invitationId: "",
};

export default function Backgrounds() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { background } = useSelector((state) => state.background);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [successful, setSuccessful] = useState(false);
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value?.namaPria + ' & ' + value?.namaWanita}
            </>
          );
        },
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },
    {
      name: "published",
      label: "Published",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {
                value ?
                  <Chip label="Published" color="success" variant="outlined" />
                  :
                  <Chip label="Draft" color="error" variant="outlined" />

              }

            </>
          );
        },
      },
    },
    {
      name: "Invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value?.namaPria + ' & ' + value?.namaWanita}
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit Background" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(background[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete Background" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(background[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const [dataBackground, setDataBackground] = useState(
    initialStateBackground
  );
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataBackground(initialStateBackground)
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        })
      dispatch(getBackgroundByInvitationId(router.query.invitationId));
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteBackground(dialogId))
      .then((response) => {
        dispatch(getBackgroundByInvitationId(router.query.invitationId))
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getBackgroundById(id))
      .then((response) => {
        setDataBackground(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeBackground = (e) => {
    if (e.target.name != "image") {
      setDataBackground({
        ...dataBackground,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataBackground({
        ...dataBackground,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateBackground = () => {
    const formData = new FormData();
    const { name, published, image } = dataBackground;
    formData.append("name", name);
    formData.append("image", image);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createBackground(formData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        handleCreateModalClose();
        dispatch(getBackgroundByInvitationId(router.query.invitationId))
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditBackground = () => {
    const formData = new FormData();
    const { name, published, image } = dataBackground;
    formData.append("name", name);
    formData.append("image", image);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateBackground(fData))
      .unwrap()
      .then((result) => {
        dispatch(getBackgroundByInvitationId(router.query.invitationId))
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);

        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, background]);

  return (
    <>
      <Head>
        <title>Backgrounds</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Backgrounds
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
            className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create background
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={background || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Background"}
        create={handleCreateBackground}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        inputCreate={
          <InputCreate onValueChangeBackground={onValueChangeBackground} />
        }
      />
      <EditDialog
        name={"Background"}
        edit={handleEditBackground}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        inputEdit={
          <InputEdit
            dataBackground={dataBackground}
            onValueChangeBackground={onValueChangeBackground}
          />
        }
      />
      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}

