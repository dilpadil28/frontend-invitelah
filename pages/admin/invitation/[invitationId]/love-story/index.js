/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import { Button, Grid, IconButton, Tooltip, Typography, Chip, Snackbar, Alert } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../../../layouts/Admin.js";
import {
  createLoveStory,
  deleteLoveStory,
  getLoveStoryById,
  updateLoveStory,
  getLoveStoryByInvitationId,
} from "../../../../../features/loveStory/loveStorySlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../../../components/admin/LoveStory/InputCreate.js";
import EditDialog from "../../../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../../../components/admin/LoveStory/InputEdit.js";
import { options } from "../../../../../components/admin/LoveStory/options.js";
import Link from "next/link";
import { getInvitationById } from "../../../../../features/invitation/invitationSlice.js";

const initialStateLoveStory = {
  id: null,
  title: "",
  description: "",
  date: "",
  published: true,
  invitationId: ""
};

export default function LoveStorys() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { loveStory } = useSelector((state) => state.loveStory);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };
  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "description",
      label: "Description",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "date",
      label: "Date",
      options: {
        filter: false,
        sort: false,
      },
    },
    {
      name: "published",
      label: "Published",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {
                value ?
                  <Chip label="Published" color="success" variant="outlined" />
                  :
                  <Chip label="Draft" color="error" variant="outlined" />
              }
            </>
          );
        },
      },
    },
    {
      name: "invitation",
      label: "Invitaion",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {value?.namaPria + ' & ' + value?.namaWanita}
            </>
          );
        },
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit LoveStory" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(loveStory[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Delete LoveStory" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(loveStory[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const [dataLoveStory, setDataLoveStory] = useState(
    initialStateLoveStory
  );
  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataLoveStory(initialStateLoveStory)
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    if (router.query.invitationId != undefined) {
      dispatch(getInvitationById(router.query.invitationId))
        .unwrap()
        .then((response) => {
          if (response === undefined) {
            router.back();
          }
        })
      if (currentUser?.roles.includes("ROLE_USER")) {
        router.back();
      }
      dispatch(getLoveStoryByInvitationId(router.query.invitationId));
    }
  }, [currentUser?.roles, dispatch, router, router.query.invitationId]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteLoveStory(dialogId))
      .then((response) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getLoveStoryByInvitationId(router.query.invitationId))
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getLoveStoryById(id))
      .then((response) => {
        setDataLoveStory(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeLoveStory = (e) => {
    if (e.target.name != "image") {
      setDataLoveStory({
        ...dataLoveStory,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataLoveStory({
        ...dataLoveStory,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateLoveStory = () => {
    const formData = new FormData();
    const { title, description, date, published } = dataLoveStory;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("date", date);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    dispatch(createLoveStory(formData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        handleCreateModalClose();
        dispatch(getLoveStoryByInvitationId(router.query.invitationId))
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditLoveStory = () => {
    const formData = new FormData();
    const { title, description, date, published } = dataLoveStory;
    formData.append("title", title);
    formData.append("description", description);
    formData.append("date", date);
    formData.append("published", published);
    formData.append("invitationId", router.query.invitationId);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateLoveStory(fData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        setOpen(true);
        dispatch(getLoveStoryByInvitationId(router.query.invitationId))
        handleEditModalClose();
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router, loveStory]);

  return (
    <>
      <Head>
        <title>Love Story</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Love Story
            </Typography>
          </Grid>
          <Grid item>
            <Button
              onClick={() => router.back()}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="outlined"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Back
            </Button>
            <Button
            className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create loveStory
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={loveStory || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"LoveStory"}
        create={handleCreateLoveStory}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeLoveStory={onValueChangeLoveStory} />
        }
      />
      <EditDialog
        name={"LoveStory"}
        edit={handleEditLoveStory}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataLoveStory={dataLoveStory}
            onValueChangeLoveStory={onValueChangeLoveStory}
          />
        }
      />
      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}

