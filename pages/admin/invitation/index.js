/* eslint-disable @next/next/no-img-element */
import { Alert, Box, Button, Grid, IconButton, Snackbar, Tooltip, Typography } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";
import moment from "moment";
import Admin from "../../../layouts/Admin.js";
import {
  createInvitation,
  deleteInvitation,
  getInvitation,
  getInvitationById,
  updateInvitation,
} from "../../../features/invitation/invitationSlice.js";
import MUIDataTable from "mui-datatables";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/Invitation/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/Invitation/InputEdit.js";
import { options } from "../../../components/admin/Invitation/options.js";
import slugify from "react-slugify";
import WallpaperIcon from '@mui/icons-material/Wallpaper';
import EmailIcon from '@mui/icons-material/Email';
import FavoriteIcon from '@mui/icons-material/Favorite';
import CollectionsIcon from '@mui/icons-material/Collections';
import MessageIcon from '@mui/icons-material/Message';
import ConnectWithoutContactIcon from '@mui/icons-material/ConnectWithoutContact';
import ColorLensIcon from '@mui/icons-material/ColorLens';
import OndemandVideoIcon from '@mui/icons-material/OndemandVideo';
import { getUser } from "../../../features/user/userSlice.js";
import { clearMessage } from "../../../features/message/messageSlice.js";

const initialStateInvitation = {
  id: null,
  userId: "",
  slug: "",
  avatarPria: "",
  namaPria: "",
  namaPendekPria: "",
  namaOrangTuaPria: "",
  avatarWanita: "",
  namaWanita: "",
  namaPendekWanita: "",
  namaOrangTuaWanita: "",
  avatarPasangan: "",
  alamatKado: "",
  tanggalNikah: "",
  jamNikah: "",
  alamatNikah: "",
  mapsNikah: "",
  tanggalResepsi: "",
  jamResepsi: "",
  alamatResepsi: "",
  mapsResepsi: "",
  bissmillah: 0,
  salamPembuka: "",
  salamPembukaDeskripsi: "",
  salamPenutup: "",
  salamPenutupDeskripsi: "",
  doa: "",
  turutMengundang: [],
};

export default function Invitations() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { invitation } = useSelector((state) => state.invitation);
  const { message } = useSelector((state) => state.message);
  const { user } = useSelector((state) => state.user);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [open, setOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "slug",
      label: "Slug",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <Box component={'a'} color="blueviolet" target={'_blank'} href={api.home + '/' + value}>{value}</Box>
            </>
          );
        },
      },
    },
    {
      name: "namaPria",
      label: "Nama Pria",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "namaWanita",
      label: "Nama Wanita",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "tanggalNikah",
      label: "Tanggal Nikah",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {moment(value).locale('id').format("Do MMMM YYYY")}
            </>
          );
        },
      },
    },
    {
      name: "tanggalResepsi",
      label: "Tanggal Resepsi",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              {moment(value).locale('id').format("Do MMMM YYYY")}
            </>
          );
        },
      },
    },
    {
      name: "avatarPasangan",
      label: "Avatar Pasangan",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },

    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Edit" arrow>
                  <IconButton
                    onClick={() =>
                      handleEditOpen(invitation[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete" arrow>
                  <IconButton
                    onClick={() =>
                      handleDeleteOpen(invitation[dataIndex].id)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Background" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/background`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <WallpaperIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Digital Envelope" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/digital-envelope`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EmailIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Love Story" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/love-story`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <FavoriteIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Photo Gallery" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/photo-gallery`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <CollectionsIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Presence" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/presence`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <MessageIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Social Media" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/social-media`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <ConnectWithoutContactIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Theme" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/theme`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <ColorLensIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
              {currentUser?.roles.includes("ROLE_ADMIN") || currentUser?.roles.includes("ROLE_MODERATOR") ? (
                <Tooltip title="Youtube" arrow>
                  <IconButton
                    onClick={() => router.push(`invitation/${invitation[dataIndex].id}/youtube`)
                    }
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.secondary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <OndemandVideoIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )
              }
            </>
          );
        },
      },
    },
  ];

  const [dataInvitation, setDataInvitation] = useState(
    initialStateInvitation
  );
  const handleCreateOpen = () => {
    setDataInvitation(initialStateInvitation)
    setOpenCreate(true);
    setSuccessful(false);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  useEffect(() => {
    dispatch(getInvitation());
    dispatch(getUser());
    dispatch(clearMessage());
  }, [dispatch]);

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteInvitation(dialogId)).unwrap()
      .then((result) => {
        setSuccessful(true);
        handleCreateModalClose();
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        handleCreateModalClose();
        setOpen(true);
        console.log(err);
      });;
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getInvitationById(id))
      .then((response) => {
        setDataInvitation(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeInvitation = (e) => {
    if (e.target.name == "avatarPria" || e.target.name == "avatarWanita" || e.target.name == "avatarPasangan") {
      setDataInvitation({
        ...dataInvitation,
        [e.target.name]: e.target.files[0],
      });
    } else if (e.target.name == 'slug') {
      setDataInvitation({
        ...dataInvitation,
        [e.target.name]: slugify(e.target.value),
      });
    } else if (e.target.type == 'datetime-local') {
      setDataInvitation({
        ...dataInvitation,
        [e.target.name]: moment(e.target.value).format("YYYY-MM-DD HH:mm:ss"),
      });
    } else {
      setDataInvitation({
        ...dataInvitation,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleCreateInvitation = () => {
    const formData = new FormData();
    const { slug, userId, avatarPria, namaPria, namaPendekPria, namaOrangTuaPria, avatarWanita, namaWanita, namaPendekWanita, namaOrangTuaWanita, avatarPasangan, alamatKado, tanggalNikah, jamNikah, alamatNikah, mapsNikah, tanggalResepsi, jamResepsi, alamatResepsi, mapsResepsi, bissmillah, salamPembuka, salamPembukaDeskripsi, salamPenutup, salamPenutupDeskripsi, doa, turutMengundang } = dataInvitation;
    formData.append("slug", slug);
    formData.append("userId", userId);
    formData.append("avatarPria", avatarPria);
    formData.append("namaPria", namaPria);
    formData.append("namaPendekPria", namaPendekPria);
    formData.append("namaOrangTuaPria", namaOrangTuaPria);
    formData.append("avatarWanita", avatarWanita);
    formData.append("namaWanita", namaWanita);
    formData.append("namaPendekWanita", namaPendekWanita);
    formData.append("namaOrangTuaWanita", namaOrangTuaWanita);
    formData.append("avatarPasangan", avatarPasangan);
    formData.append("alamatKado", alamatKado);
    formData.append("tanggalNikah", tanggalNikah);
    formData.append("jamNikah", jamNikah);
    formData.append("alamatNikah", alamatNikah);
    formData.append("mapsNikah", mapsNikah);
    formData.append("tanggalResepsi", tanggalResepsi);
    formData.append("jamResepsi", jamResepsi);
    formData.append("alamatResepsi", alamatResepsi);
    formData.append("mapsResepsi", mapsResepsi);
    formData.append("bissmillah", bissmillah);
    formData.append("salamPembuka", salamPembuka);
    formData.append("salamPembukaDeskripsi", salamPembukaDeskripsi);
    formData.append("salamPenutup", salamPenutup);
    formData.append("salamPenutupDeskripsi", salamPenutupDeskripsi);
    formData.append("doa", doa);
    formData.append("turutMengundang", turutMengundang);
    dispatch(createInvitation(formData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        handleCreateModalClose();
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        setOpen(true);
        console.log(err);
      });
  };
  const handleEditInvitation = () => {
    const formData = new FormData();
    const { slug, userId, avatarPria, namaPria, namaPendekPria, namaOrangTuaPria, avatarWanita, namaWanita, namaPendekWanita, namaOrangTuaWanita, avatarPasangan, alamatKado, tanggalNikah, jamNikah, alamatNikah, mapsNikah, tanggalResepsi, jamResepsi, alamatResepsi, mapsResepsi, bissmillah, salamPembuka, salamPembukaDeskripsi, salamPenutup, salamPenutupDeskripsi, doa, turutMengundang } = dataInvitation;
    formData.append("slug", slug);
    formData.append("userId", userId);
    formData.append("avatarPria", avatarPria);
    formData.append("namaPria", namaPria);
    formData.append("namaPendekPria", namaPendekPria);
    formData.append("namaOrangTuaPria", namaOrangTuaPria);
    formData.append("avatarWanita", avatarWanita);
    formData.append("namaWanita", namaWanita);
    formData.append("namaPendekWanita", namaPendekWanita);
    formData.append("namaOrangTuaWanita", namaOrangTuaWanita);
    formData.append("avatarPasangan", avatarPasangan);
    formData.append("alamatKado", alamatKado);
    formData.append("tanggalNikah", tanggalNikah);
    formData.append("jamNikah", jamNikah);
    formData.append("alamatNikah", alamatNikah);
    formData.append("mapsNikah", mapsNikah);
    formData.append("tanggalResepsi", tanggalResepsi);
    formData.append("jamResepsi", jamResepsi);
    formData.append("alamatResepsi", alamatResepsi);
    formData.append("mapsResepsi", mapsResepsi);
    formData.append("bissmillah", bissmillah);
    formData.append("salamPembuka", salamPembuka);
    formData.append("salamPembukaDeskripsi", salamPembukaDeskripsi);
    formData.append("salamPenutup", salamPenutup);
    formData.append("salamPenutupDeskripsi", salamPenutupDeskripsi);
    formData.append("doa", doa);
    formData.append("turutMengundang", turutMengundang);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateInvitation(fData))
      .unwrap()
      .then((result) => {
        setSuccessful(true);
        handleEditModalClose();
        setOpen(true);
      })
      .catch((err) => {
        setSuccessful(false);
        handleEditModalClose();
        setOpen(true);
        console.log(err);
      });
  };

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (currentUser?.roles.includes("ROLE_USER")) {
      router.back();
    }
  }, [currentUser, router, invitation]);

  return (
    <>
      <Head>
        <title>Invitations</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              Invitations
            </Typography>
          </Grid>
          <Grid item>
            <Button
            className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create invitation
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={invitation || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"Invitation"}
        create={handleCreateInvitation}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        inputCreate={
          <InputCreate
            user={user}
            onValueChangeInvitation={onValueChangeInvitation} />
        }
      />
      <EditDialog
        name={"Invitation"}
        edit={handleEditInvitation}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        inputEdit={
          <InputEdit

            user={user}
            dataInvitation={dataInvitation}
            onValueChangeInvitation={onValueChangeInvitation}
          />
        }
      />

      {
        message && (
          <Snackbar open={open} autoHideDuration={4000} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} onClose={handleClose} >
            <Alert severity={successful ? 'success' : 'error'} sx={{ width: '100%' }} onClose={handleClose}>
              {message}
            </Alert>
          </Snackbar>
        )
      }
    </>
  );
}
