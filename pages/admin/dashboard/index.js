import { Box, Container } from "@mui/material";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import DashboardCrypto from "../../../components/admin/Crypto/index.js";
import ApplicationsTransactions from "../../../components/admin/Transactions/index.js";
import ManagementUserProfile from "../../../components/admin/Users/profile/index.js";
import ManagementUserSettings from "../../../components/admin/Users/settings/index.js";

// components

// layout for page

import Admin from "../../../layouts/Admin.js";

export default function Dashboard() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const router = useRouter();
  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
  }, [currentUser, router]);

  return (
    <>
      <Admin>
        <ManagementUserSettings />
      </Admin>
    </>
  );
}
