/* eslint-disable @next/next/no-img-element */
import {
  Alert,
  Button,
  Grid,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditTwoToneIcon from "@mui/icons-material/EditTwoTone";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";

import AddTwoToneIcon from "@mui/icons-material/AddTwoTone";

import Admin from "../../../layouts/Admin.js";
import {
  createMyThemeList,
  deleteMyThemeList,
  getMyTheme,
  getMyThemeById,
  getMyThemeList,
  getMyThemeListById,
  updateMyTheme,
  updateMyThemeList,
} from "../../../features/myTheme/myThemeSlice.js";
import { api } from "../../../config/api.js";
import { useTheme } from "@emotion/react";
import DeleteDialog from "../../../components/admin/dialog/deleteDialog.js";
import CreateDialog from "../../../components/admin/dialog/CreateDialog.js";
import InputCreate from "../../../components/admin/MyTheme/InputCreate.js";
import EditDialog from "../../../components/admin/dialog/EditDialog.js";
import InputEdit from "../../../components/admin/MyTheme/InputEdit.js";
import { options } from "../../../components/admin/MyTheme/options.js";
import InputHeader from "../../../components/admin/MyTheme/InputHeader.js";
import MUIDataTable from "mui-datatables";

const initialStateMyTheme = {
  id: null,
  name: "",
  image: "",
  url: "",
  myThemeId: "",
};
const initialStateMyThemeHeader = {
  id: null,
  title: "",
  description: "",
};

export default function MyThemes() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const theme = useTheme();
  const router = useRouter();
  const dispatch = useDispatch();
  const { myTheme, myThemeList } = useSelector((state) => state.myTheme);
  const { message } = useSelector((state) => state.message);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editModalHeaderOpen, setEditModalHeaderOpen] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [dialogId, setDialogId] = useState("");
  const [dataMyTheme, setDataMyTheme] = useState(initialStateMyTheme);
  const [dataMyThemeHeader, setDataMyThemeHeader] = useState(
    initialStateMyThemeHeader
  );

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "url",
      label: "Url",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "image",
      label: "Image",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRender: (value, tableMeta, updateValue) => {
          return (
            <>
              <img
                style={{ width: 100 }}
                src={api.fileUrl + value}
                alt="image"
              />
            </>
          );
        },
      },
    },

    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <>
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Edit Theme" arrow>
                  <IconButton
                    onClick={() => handleEditOpen(myThemeList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.primary.light,
                      },
                      color: theme.palette.primary.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <EditTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
              {currentUser?.roles.includes("ROLE_ADMIN") ? (
                <Tooltip title="Delete Theme" arrow>
                  <IconButton
                    onClick={() => handleDeleteOpen(myThemeList[dataIndex].id)}
                    sx={{
                      "&:hover": {
                        background: theme.palette.error.light,
                      },
                      color: theme.palette.error.main,
                    }}
                    color="inherit"
                    size="small"
                  >
                    <DeleteTwoToneIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          );
        },
      },
    },
  ];

  const handleEditHeaderOpen = () => {
    dispatch(getMyThemeById(myTheme[0].id))
      .then((response) => {
        setDataMyThemeHeader(response.payload);
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });

    setEditModalHeaderOpen(true);
    setSuccessful(false);
  };

  const handleEditHeaderModalClose = () => {
    setEditModalHeaderOpen(false);
  };
  const onValueChangeMyThemeHeader = (e) => {
    setDataMyThemeHeader({
      ...dataMyThemeHeader,
      [e.target.name]: e.target.value,
    });
  };
  const handleEditMyThemeHeader = () => {
    const { title, description } = dataMyThemeHeader;
    const fData = {
      id: myTheme[0].id,
      result: { title: title, description: description },
    };
    dispatch(updateMyTheme(fData))
      .unwrap()
      .then((result) => {
        handleEditHeaderModalClose();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleCreateOpen = () => {
    setOpenCreate(true);
    setSuccessful(false);
    setDataMyThemeHeader(initialStateMyThemeHeader);
  };

  const handleCreateModalClose = () => {
    setOpenCreate(false);
  };

  const handleDeleteOpen = (id) => {
    setDialogId(id);
    setDeleteModalOpen(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const handleDelete = () => {
    dispatch(deleteMyThemeList(dialogId));
    setDeleteModalOpen(false);
  };

  const handleEditOpen = (id) => {
    setDialogId(id);
    dispatch(getMyThemeListById(id))
      .then((response) => {
        setDataMyTheme(response.payload);
      })
      .catch((e) => {
        console.log(e);
      });

    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const onValueChangeMyTheme = (e) => {
    if (e.target.name != "image") {
      setDataMyTheme({
        ...dataMyTheme,
        [e.target.name]: e.target.value,
      });
    } else {
      setDataMyTheme({
        ...dataMyTheme,
        [e.target.name]: e.target.files[0],
      });
    }
  };

  const handleCreateMyTheme = () => {
    const formData = new FormData();
    const { name, url, image } = dataMyTheme;
    formData.append("name", name);
    formData.append("image", image);
    formData.append("url", url);
    formData.append("myThemeId", myTheme[0].id);
    dispatch(createMyThemeList(formData))
      .unwrap()
      .then((result) => {
        handleCreateModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  const handleEditMyTheme = () => {
    const formData = new FormData();
    const { name, url, image } = dataMyTheme;
    formData.append("name", name);
    formData.append("url", url);
    formData.append("image", image);
    const fData = {
      id: dialogId,
      result: formData,
    };
    dispatch(updateMyThemeList(fData))
      .unwrap()
      .then((result) => {
        handleEditModalClose();
        setSuccessful(true);
        setOpen(true);
      })
      .catch((e) => {
        setSuccessful(false);
        setOpen(true);
        console.log(e);
      });
  };
  useEffect(() => {
    dispatch(getMyTheme());
    dispatch(getMyThemeList());
  }, [dispatch]);

  useEffect(() => {
    if (!currentUser) {
      router.push("/auth/login");
    }
    if (
      currentUser?.roles.includes("ROLE_USER") ||
      currentUser?.roles.includes("ROLE_MODERATOR")
    ) {
      router.back();
    }
  }, [currentUser, router]);

  return (
    <>
      <Head>
        <title>My Themes</title>
      </Head>
      <Admin>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom>
              My Themes
            </Typography>
          </Grid>
          <Grid item>
            <Button
              className="!bg-primary"
              onClick={handleEditHeaderOpen}
              sx={{ my: 1, mr: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<EditTwoToneIcon fontSize="small" />}
            >
              Edit Header
            </Button>
            <Button
              className="!bg-primary"
              onClick={handleCreateOpen}
              sx={{ my: 1, mt: { xs: 2, md: 0 } }}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Create My Theme
            </Button>
          </Grid>
        </Grid>
        {typeof window !== "undefined" && (
          <MUIDataTable
            title={""}
            data={myThemeList || undefined}
            columns={columns}
            options={options}
          />
        )}
      </Admin>
      <DeleteDialog
        deleteModalOpen={deleteModalOpen}
        handleDeleteModalClose={handleDeleteModalClose}
        handleDelete={handleDelete}
      />
      <CreateDialog
        name={"My Theme"}
        create={handleCreateMyTheme}
        openCreate={openCreate}
        handleCreateModalClose={handleCreateModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputCreate={
          <InputCreate onValueChangeMyTheme={onValueChangeMyTheme} />
        }
      />
      <EditDialog
        name={"My Theme"}
        edit={handleEditMyTheme}
        editModalOpen={editModalOpen}
        handleEditModalClose={handleEditModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputEdit
            dataMyTheme={dataMyTheme}
            onValueChangeMyTheme={onValueChangeMyTheme}
          />
        }
      />
      <EditDialog
        name={"My Theme Header"}
        edit={handleEditMyThemeHeader}
        editModalOpen={editModalHeaderOpen}
        handleEditModalClose={handleEditHeaderModalClose}
        successful={successful}
        setSuccessful={setSuccessful}
        inputEdit={
          <InputHeader
            dataMyThemeHeader={dataMyThemeHeader}
            onValueChangeMyThemeHeader={onValueChangeMyThemeHeader}
          />
        }
      />

      {message && (
        <Snackbar
          open={open}
          autoHideDuration={4000}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          onClose={handleClose}
        >
          <Alert
            severity={successful ? "success" : "error"}
            sx={{ width: "100%" }}
            onClose={handleClose}
          >
            {message}
          </Alert>
        </Snackbar>
      )}
    </>
  );
}
