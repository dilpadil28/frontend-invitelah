/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable @next/next/no-img-element */
import React from "react";

import NotificationsActiveIcon from "@mui/icons-material/NotificationsActive";
import MyLocationIcon from "@mui/icons-material/MyLocation";
import LiveTvIcon from "@mui/icons-material/LiveTv";
import MasksIcon from "@mui/icons-material/Masks";
import SocialDistanceIcon from "@mui/icons-material/SocialDistance";
import Timeline from "@mui/lab/Timeline";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineOppositeContent from "@mui/lab/TimelineOppositeContent";
import TimelineDot from "@mui/lab/TimelineDot";
import {
  FaInstagram,
  FaFacebookSquare,
  FaTwitter,
  FaTiktok,
  FaYoutube,
  FaTelegram,
  FaSnapchat,
  FaPinterest,
  FaWhatsapp,
  FaHandsWash,
  FaHandshakeAltSlash,
  FaHeart,
} from "react-icons/fa";
import { CardGiftcard, ContentCopy, Money } from "@mui/icons-material";

import "lightgallery.js/dist/css/lightgallery.css";
import { LightgalleryProvider, LightgalleryItem } from "react-lightgallery";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  ImageList,
  ImageListItem,
  Slide,
  Snackbar,
} from "@mui/material";
import { QRCodeCanvas } from "qrcode.react";
import Footer from "../../components/Footer";
import Slider from "react-slick";
import YouTube from "react-youtube";
import Countdown from "react-countdown";
import moment from "moment";
import { useState } from "react";
import ReactPlayer from "react-player";

import { Link } from "react-scroll";

import { RiHomeHeartLine, RiHeartsLine, RiChatHeartLine } from "react-icons/ri";
import { BiCalendarHeart } from "react-icons/bi";
import {
  BottomNavigation,
  BottomNavigationAction,
  Container,
  Fab,
  Paper,
} from "@mui/material";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PlayDisabledIcon from "@mui/icons-material/PlayDisabled";
import CopyToClipboard from "react-copy-to-clipboard";
import { Box } from "@mui/system";
const settings = {
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 10000,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 0,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function test() {
  const [openInvitation, setOpenInvitation] = useState(true);
  const [play, setPlay] = useState(false);
  const handleCloseInvitation = () => {
    setOpenInvitation(false);
    setPlay(!play);
  };
  const handlePlayPause = () => {
    setPlay(!play);
  };

  const [open, setOpen] = React.useState(false);
  const [openKado, setOpenKado] = useState(false);
  const [openAngpau, setOpenAngpau] = useState(false);
  const [copied, setCopied] = useState(false);
  const handleClickCopy = () => {
    setOpen(true);
  };
  const handleCloseCopy = () => {
    setOpen(false);
  };

  const handleClickKado = () => {
    setOpenKado(true);
  };

  const handleCloseKado = () => {
    setOpenKado(false);
  };
  const handleClickAngpau = () => {
    setOpenAngpau(true);
  };

  const handleCloseAngpau = () => {
    setOpenAngpau(false);
  };
  return (
    <>
      <div className="bg-black">
        <section
          className="w-full h-screen !bg-no-repeat !bg-cover !bg-center "
          style={{
            background:
              "url('https://images.unsplash.com/photo-1520854221256-17451cc331bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80')",
          }}
        >
          <div
            id="home"
            style={{
              backgroundImage:
                "linear-gradient(to bottom, rgba(0,0,0,0),rgba(0,0,0,100))",
            }}
            className=" w-full h-full"
          >
            <div className="bg-black/30 backdrop-opacity-30 h-full w-full py-12 px-5 flex justify-center">
              <div className="max-w-xl flex flex-col justify-between text-center  text-white">
                <div>
                  <div className="mb-3 text-sm">Undangan</div>
                  <div
                    className="font-bold text-[35px] tracking-wide "
                    style={{ fontFamily: "Great Vibes" }}
                  >
                    Raja & Ratu
                  </div>
                </div>
                <div>
                  <div className=" text-sm">
                    Kami berharap anda menjadi bagian dari hari istimewa kami.
                  </div>
                  <div className="my-6">
                    <Countdown
                      date={moment(Date.now() + 3000000000000)
                        .local("id")
                        .format("YYYY-MM-DDTHH:mm")}
                      renderer={({
                        days,
                        hours,
                        minutes,
                        seconds,
                        completed,
                      }) => {
                        if (completed) {
                          // Render a complete state
                          return (
                            <div className="font-bold text-sm">
                              Acara Telah Selesai
                            </div>
                          );
                        } else {
                          // Render a countdown
                          return (
                            <div className="flex justify-center">
                              <div className="flex gap-5 justify-center">
                                <div className="rounded-xl">
                                  <div>
                                    <div className="text-2xl font-bold mb-1">
                                      {days}
                                    </div>
                                    <div className="text-xs">Hari</div>
                                  </div>
                                </div>
                                <div className="rounded-xl">
                                  <div>
                                    <div className="text-2xl font-bold mb-1">
                                      {hours}
                                    </div>
                                    <div className="text-xs">Jam</div>
                                  </div>
                                </div>
                                <div className="rounded-xl">
                                  <div>
                                    <div className="text-2xl font-bold mb-1">
                                      {minutes}
                                    </div>
                                    <div className="text-xs">Menit</div>
                                  </div>
                                </div>
                                <div className="rounded-xl">
                                  <div>
                                    <div className="text-2xl font-bold mb-1">
                                      {seconds}
                                    </div>
                                    <div className="text-xs">Detik</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        }
                      }}
                    />
                  </div>
                  <div className="flex items-center justify-center">
                    <a
                      target="_blank"
                      className="rounded-full py-2 bg-white text-black px-6 text-xs shadow-md"
                      href={"#"}
                      rel="noreferrer"
                    >
                      <span className="mr-2">
                        <NotificationsActiveIcon className=" w-5" />
                      </span>
                      Pengingat Tanggal
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section
          id="couple"
          className="h-full bg-black flex items-start justify-center py-12 px-5"
        >
          <div className="px-5 py-10 max-w-xl bg-white rounded-xl border border-gray-200 shadow-md text-center">
            <div>
              <div id="couple" className="text-sm">
                Bismillahirrahmanirrahim
              </div>
              <div className="text-sm my-1">
                Assalamualaikum Warahmatullahi Wabarakatuh
              </div>
              <p className="text-xs my-3">
                Tanpa mengurangi rasa hormat, Kami mengundang
                Bapak/Ibu/Saudara/i serta kerabat sekalian untuk menghadiri
                acara pernikahan kami.
              </p>
            </div>
            <div>
              <div className="flex justify-center py-3 md:py-5">
                <div>
                  <img
                    className="h-[100px] sm:h-[200px]"
                    src="/static/images/avatars/avatar-frame-man.png"
                    alt="avatar-frame-man"
                  />
                </div>
              </div>
              <div className="mt-2">
                <div
                  className="font-bold text-xl tracking-wide "
                  style={{ fontFamily: "Great Vibes" }}
                >
                  Raja Saputra Spd, Mpd
                </div>
                <div className="text-xs py-2">
                  Putra dari Bapak Pertama & Ibu Pertama
                </div>
                <div className="flex justify-center gap-2">
                  <a target="_blank" href={"#"}>
                    <FaInstagram />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaFacebookSquare />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaTwitter />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaTiktok />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaYoutube />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaTelegram />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaSnapchat />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaPinterest />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaWhatsapp />
                  </a>
                </div>
              </div>
            </div>
            <div className="pt-4">
              <div className="flex justify-center py-3 md:py-5">
                <div>
                  <img
                    className="h-[100px] sm:h-[200px]"
                    src="/static/images/avatars/avatar-frame-woman.png"
                    alt="avatar-frame-woman"
                  />
                </div>
              </div>
              <div className="mt-2">
                <div
                  className="font-bold text-xl tracking-wide "
                  style={{ fontFamily: "Great Vibes" }}
                >
                  Dr. Ratu Pertiwi Sukma Spd, Mpd
                </div>
                <div className="text-xs py-2">
                  Putra dari Bapak Pertama & Ibu Pertama
                </div>
                <div className="flex justify-center gap-2">
                  <a target="_blank" href={"#"}>
                    <FaInstagram />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaFacebookSquare />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaTwitter />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaTiktok />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaYoutube />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaTelegram />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaSnapchat />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaPinterest />
                  </a>
                  <a target="_blank" href={"#"}>
                    <FaWhatsapp />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="h-full bg-black text-center flex justify-center text-white text-xs py-12 px-5 ">
          <blockquote className="text-sm italic font-semibold text-white  max-w-xl">
            <svg
              aria-hidden="true"
              className="w-10 h-10 text-white"
              viewBox="0 0 24 27"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.017 18L14.017 10.609C14.017 4.905 17.748 1.039 23 0L23.995 2.151C21.563 3.068 20 5.789 20 8H24V18H14.017ZM0 18V10.609C0 4.905 3.748 1.038 9 0L9.996 2.151C7.563 3.068 6 5.789 6 8H9.983L9.983 18L0 18Z"
                fill="currentColor"
              ></path>
            </svg>
            <p>
              Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan
              pasangan-pasangan untukmu dari jenismu sendiri, agar kamu
              cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di
              antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu
              benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang
              berpikir. (Q.S Ar Rum:21)
            </p>
          </blockquote>
        </section>
        <section
          id="event"
          className="h-full bg-black text-center text-black text-xs flex items-start justify-center py-12 px-5"
        >
          <div className="px-5 py-10 w-full max-w-xl bg-white rounded-xl border border-gray-200 shadow-md text-center">
            <div>
              <div
                className="font-bold text-[35px] tracking-wide "
                style={{ fontFamily: "Great Vibes" }}
              >
                Akad Nikah
              </div>
              <div className="pt-5 text-sm">Minggu, 17 April 2022</div>
              <div className="py-2 text-sm font-bold">
                Pukul 08.00 WIB s/d Selesai
              </div>
              <p className=" text-xs">
                Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan
                Bojonggede, Kabupaten Bogor, Jawa Barat 16920
              </p>
              <div className="flex items-center justify-center py-4">
                <div className="border-2 border-black rounded-xl w-full h-full">
                  <iframe
                    className="rounded-xl"
                    src={
                      "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15858.278816888016!2d106.79900288587307!3d-6.449249125712187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e9f194676a43%3A0xb65f455356b0b55d!2sGedung%20Bulu%20Tangkis%20GOR%20Sakinah!5e0!3m2!1sid!2sid!4v1650022589525!5m2!1sid!2sid"
                    }
                    style={{ border: 0, width: "100%", height: "100%" }}
                    allowFullScreen=""
                    loading="lazy"
                  ></iframe>
                </div>
              </div>
            </div>
            <div className="pt-6">
              <div
                className="font-bold text-[35px] tracking-wide "
                style={{ fontFamily: "Great Vibes" }}
              >
                Resepsi
              </div>
              <div className="pt-5 text-sm">Minggu, 17 April 2022</div>
              <div className="py-2 text-sm font-bold">
                Pukul 08.00 WIB s/d Selesai
              </div>
              <p className=" text-xs">
                Gor Sakinah, Jl. Kp. Panjang No.44, Rw. Panjang, Kecamatan
                Bojonggede, Kabupaten Bogor, Jawa Barat 16920
              </p>
              <div className="flex items-center justify-center pt-4">
                <div className="border-2 border-black rounded-xl w-full h-full">
                  <iframe
                    className="rounded-xl"
                    src={
                      "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15858.278816888016!2d106.79900288587307!3d-6.449249125712187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e9f194676a43%3A0xb65f455356b0b55d!2sGedung%20Bulu%20Tangkis%20GOR%20Sakinah!5e0!3m2!1sid!2sid!4v1650022589525!5m2!1sid!2sid"
                    }
                    style={{ border: 0, width: "100%", height: "100%" }}
                    allowFullScreen=""
                    loading="lazy"
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="h-full bg-black text-center text-black text-xs flex items-start justify-center py-12 px-5">
          <div className="px-5 pt-10 pb-7 w-full max-w-xl bg-white rounded-xl border border-gray-200 shadow-md text-center">
            <div>
              <div
                className="font-bold text-[35px] tracking-wide "
                style={{ fontFamily: "Great Vibes" }}
              >
                Live Streaming
              </div>
              <div className="pt-5 text-sm">
                Acara akan disiarakan secara langsung.
              </div>
              <div className="flex items-center gap-2 flex-wrap justify-center pt-4">
                <a
                  target="_blank"
                  className="rounded-full py-2 bg-black text-white px-6 text-xs"
                  href={"#"}
                  rel="noreferrer"
                >
                  <span className="mr-2">
                    <LiveTvIcon className=" w-5" />
                  </span>
                  Live Raja
                </a>
                <a
                  target="_blank"
                  className="rounded-full py-2 bg-black text-white px-6 text-xs"
                  href={"#"}
                  rel="noreferrer"
                >
                  <span className="mr-2">
                    <LiveTvIcon className=" w-5" />
                  </span>
                  Live Ratu
                </a>
              </div>
            </div>
          </div>
        </section>
        <section className="h-full bg-black text-center text-white text-xs flex items-start justify-center py-12 px-5">
          <div className="max-w-xl">
            <div
              className="font-bold text-[35px] pb-6 tracking-wide "
              style={{ fontFamily: "Great Vibes" }}
            >
              Protokol Keseahatan
            </div>
            <p>
              Untuk mencegah penyebaran Covid-19, diharapkan bagi tamu undangan
              yang hadir untuk mematuhi Protokol Kesehatan dibawah ini :
            </p>

            <div className="py-6 ">
              <div className="grid !text-left grid-cols-1 md:grid-cols-2 gap-3">
                <div className="flex flex-row py-2 px-4 gap-3 items-center border border-white rounded-xl">
                  <div>
                    <MasksIcon className="text-5xl" />
                  </div>
                  <div>Menggunakan Masker</div>
                </div>
                <div>
                  <div className="flex flex-row py-2 px-4 gap-3 items-center border border-white rounded-xl">
                    <div>
                      <FaHandsWash className="text-5xl" />
                    </div>
                    <div>Cuci Tangan dan Gunakan Sabun</div>
                  </div>
                </div>
                <div>
                  <div className="flex flex-row py-2 px-4 gap-3 items-center border border-white rounded-xl">
                    <div>
                      <FaHandshakeAltSlash className="text-5xl" />
                    </div>
                    <div>Dilarang Salaman</div>
                  </div>
                </div>
                <div>
                  <div className="flex flex-row py-2 px-4 gap-3 items-center border border-white rounded-xl">
                    <div>
                      <SocialDistanceIcon className="text-5xl" />
                    </div>
                    <div>Jaga Jarak dan Jauhkan Kerumunan</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="h-full bg-black text-center text-white flex justify-center text-xs py-12 px-5">
          <div className=" max-w-xl">
            <div
              className="font-bold text-[35px] pb-6 tracking-wide "
              style={{ fontFamily: "Great Vibes" }}
            >
              Cerita Cinta
            </div>
            <div className="pt-6">
              <Timeline position="alternate">
                <TimelineItem>
                  <TimelineOppositeContent
                    sx={{ m: "auto 0" }}
                    align="right"
                    variant="body2"
                    color="text.secondary"
                  >
                    28 Juni 2016
                  </TimelineOppositeContent>
                  <TimelineSeparator>
                    <TimelineConnector />
                    <TimelineDot>
                      <FaHeart />
                    </TimelineDot>
                    <TimelineConnector />
                  </TimelineSeparator>
                  <TimelineContent sx={{ py: "12px", px: 2 }}>
                    <div className="font-bold text-sm">Perkenalan</div>
                    <p className=" text-xs">
                      Awal Bertemu dan Berkenal di tempat kerja
                    </p>
                  </TimelineContent>
                </TimelineItem>
                <TimelineItem>
                  <TimelineOppositeContent
                    sx={{ m: "auto 0" }}
                    variant="body2"
                    color="text.secondary"
                  >
                    28 Mei 2022
                  </TimelineOppositeContent>
                  <TimelineSeparator>
                    <TimelineConnector />
                    <TimelineDot className="!bg-red-500 text-red-300">
                      <FaHeart />
                    </TimelineDot>
                    <TimelineConnector />
                  </TimelineSeparator>
                  <TimelineContent sx={{ py: "12px", px: 2 }}>
                    <div className="font-bold text-sm">Lamaran</div>
                    <p className="text-xs">
                      Dalam waktu yang cukup lama kenal akhirnya kami memutuskan
                      untuk kejenjang yang serius
                    </p>
                  </TimelineContent>
                </TimelineItem>
                <TimelineItem>
                  <TimelineOppositeContent
                    sx={{ m: "auto 0" }}
                    variant="body2"
                    color="text.secondary"
                  >
                    12 Desember 2022
                  </TimelineOppositeContent>
                  <TimelineSeparator>
                    <TimelineConnector />
                    <TimelineDot className="!bg-red-500 text-red-300">
                      <FaHeart />
                    </TimelineDot>
                    <TimelineConnector />
                  </TimelineSeparator>
                  <TimelineContent sx={{ py: "12px", px: 2 }}>
                    <div className="font-bold text-sm">Nikah</div>
                    <p className="text-xs">
                      Atas kesepakatan keluarga akhirnya kami memutuskan untuk
                      menikah pada tanggal 12 Desember 2022
                    </p>
                  </TimelineContent>
                </TimelineItem>
              </Timeline>
            </div>
          </div>
        </section>
        <section
          id="wishes"
          className="m-auto bg-fixed bg-center bg-cover "
          style={{
            backgroundImage:
              'url("https://images.unsplash.com/photo-1509927083803-4bd519298ac4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80")',
          }}
        >
          <div className="text-white w-full  h-full  backdrop-blur-sm">
            <div className="bg-black/40 backdrop-opacity-30 h-full w-full py-12 px-5 flex justify-center">
              <div className="max-w-xl">
                <div>
                  <div
                    className="text-center font-bold text-[35px] tracking-wide "
                    style={{ fontFamily: "Great Vibes" }}
                  >
                    Reservasi Kehadiran
                  </div>
                  <p className=" text-xs text-center mt-2">
                    Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila
                    Bapak/Ibu/Saudara/i berkenan hadir untuk memberikan doa
                    restu. Atas kehadiran serta doa restu, kami ucapkan terima
                    kasih.
                  </p>
                  <div className="p-5 shadow-md my-4 text-white backdrop-blur-3xl bg-white/30 rounded-xl w-full">
                    <div className="mb-4">
                      <label
                        htmlFor="nama"
                        className="block mb-2 text-sm font-medium"
                      >
                        Nama *
                      </label>
                      <input
                        placeholder="Masukan Nama"
                        type="text"
                        id="nama"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 "
                      />
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="nama"
                        className="block mb-2 text-sm font-medium"
                      >
                        No WhatsApp *
                      </label>
                      <input
                        placeholder="Masukan No WhatsApp"
                        type="text"
                        id="nama"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 "
                      />
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="pesan"
                        className="block mb-2 text-sm font-medium"
                      >
                        Pesan
                      </label>
                      <textarea
                        id="pesan"
                        rows="4"
                        className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 "
                        placeholder="Masukan Pesan ..."
                      ></textarea>
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="countries"
                        className="block mb-2 text-sm font-medium "
                      >
                        Konfirmasi Kehadiran *
                      </label>
                      <select
                        id="countries"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                      >
                        <option>Hadir</option>
                        <option>Tidak Hadir</option>
                        <option>Masih Ragu</option>
                      </select>
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="countries"
                        className="block mb-2 text-sm font-medium  "
                      >
                        Jumlah yang Datang
                      </label>
                      <select
                        id="countries"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                      >
                        <option>1 Orang</option>
                        <option>2 Orang</option>
                        <option>3 Orang</option>
                      </select>
                    </div>
                    <div className="flex items-center justify-center">
                      <button className="w-full md:w-1/2 rounded-full py-2 bg-black text-white px-6 text-xs">
                        Konfirmasi
                      </button>
                    </div>
                  </div>
                </div>
                <div className="mt-6">
                  <div
                    className="text-center font-bold text-[35px] tracking-wide "
                    style={{ fontFamily: "Great Vibes" }}
                  >
                    Ucapan dan Doa
                  </div>
                  <p className=" text-xs text-center mt-2">
                    Kirimkan Ucapan dan Doa kepada kami, semoga doanya berbalik
                    ke anda.
                  </p>
                  <div className="p-5 shadow-md my-4 text-white backdrop-blur-3xl bg-white/30 rounded-xl w-full">
                    <div className="mb-4">
                      <label
                        htmlFor="nama"
                        className="block mb-2 text-sm font-medium"
                      >
                        Nama *
                      </label>
                      <input
                        placeholder="Masukan Nama"
                        type="text"
                        id="nama"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 "
                      />
                    </div>
                    <div className="mb-4">
                      <label
                        htmlFor="pesan"
                        className="block mb-2 text-sm font-medium"
                      >
                        Ucapan dan Doa Restu *
                      </label>
                      <textarea
                        id="pesan"
                        rows="4"
                        className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 "
                        placeholder="Masukan Ucapan dan Doa Restu  ..."
                      ></textarea>
                    </div>
                    <div className="flex items-center justify-center">
                      <button className="w-full md:w-1/2 rounded-full py-2 bg-black text-white px-6 text-xs">
                        Kirim
                      </button>
                    </div>
                    <div className="mt-6 max-h-80 overflow-y-auto">
                      {[1, 2, 3, 4, 5, 6].map((v, i) => (
                        <div
                          key={i}
                          className="bg-white mb-5 text-black rounded-xl border px-4 py-3 border-gray-200 shadow-md"
                        >
                          <div className="text-sm font-medium">
                            Nama Pengirim
                          </div>
                          <div className="text-xs font-medium">
                            Selamaaaat ya, semoga Sakinah Mawadah Warahmah
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="h-full bg-black text-center text-black text-xs flex items-start justify-center py-12 px-5">
          <div className="px-5 pt-10 pb-7 w-full max-w-xl bg-white rounded-xl border border-gray-200 shadow-md text-center">
            <div>
              <div
                className="font-bold text-[35px] tracking-wide "
                style={{ fontFamily: "Great Vibes" }}
              >
                Kirim Hadiah
              </div>
              <div className="pt-5 text-sm">
                Kami sangat berterima kasih kepada Bapak/Ibu/Saudara/i yang
                berkenan memberikan tanda kasih kepada kami.
              </div>
              <div className="flex items-center gap-2 flex-wrap justify-center pt-4">
                <button
                  className="rounded-full py-2 bg-black text-white px-6 text-xs"
                  onClick={handleClickKado}
                >
                  <span className="mr-2">
                    <CardGiftcard className=" w-5" />
                  </span>
                  Kado
                </button>
                <button
                  className="rounded-full py-2 bg-black text-white px-6 text-xs"
                  onClick={handleClickAngpau}
                >
                  <span className="mr-2">
                    <Money className=" w-5" />
                  </span>
                  Angpao
                </button>
              </div>
            </div>
          </div>
        </section>
        <section className="h-full bg-black text-center text-black text-xs flex items-start justify-center py-12 px-5">
          <div className="px-5 pt-10 pb-7 w-full max-w-xl bg-white rounded-xl border border-gray-200 shadow-md text-center">
            <div>
              <div
                className="font-bold text-[35px] tracking-wide "
                style={{ fontFamily: "Great Vibes" }}
              >
                Galeri
              </div>
              <div className=" mt-8">
                <LightgalleryProvider>
                  <ImageList variant="masonry" cols={2} gap={8}>
                    {[
                      "https://images.unsplash.com/photo-1523438885200-e635ba2c371e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fHdlZGRpbmd8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=80",
                      "https://images.unsplash.com/photo-1522673607200-164d1b6ce486?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzJ8fHdlZGRpbmd8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=80",
                      "https://images.unsplash.com/photo-1509927083803-4bd519298ac4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzN8fHdlZGRpbmd8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=80",
                      "https://images.unsplash.com/photo-1545232979-8bf68ee9b1af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
                      "https://images.unsplash.com/photo-1515626553181-0f218cb03f14?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzZ8fHdlZGRpbmd8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=80",
                    ].map((v, i) => (
                      <ImageListItem key={i}>
                        <LightgalleryItem group={"group2"} src={v}>
                          <img
                            className="rounded-xl"
                            src={`${v}`}
                            srcSet={`${v}`}
                            style={{ width: "100%" }}
                            loading="lazy"
                            alt={"v?.image"}
                          />
                        </LightgalleryItem>
                      </ImageListItem>
                    ))}
                  </ImageList>
                </LightgalleryProvider>
              </div>
              <div className="my-6">
                <Slider {...settings}>
                  {[1, 2, 3].map((v, i) => (
                    <div className="px-1">
                      <YouTube
                        className="rounded-xl w-full h-40 md:h-64"
                        key={i}
                        videoId={"3SEvazR47SE"}
                      />
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
          </div>
        </section>
        <section
          className="w-full h-screen !bg-no-repeat !bg-cover !bg-center "
          style={{
            background:
              "url('https://images.unsplash.com/photo-1520854221256-17451cc331bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80')",
          }}
        >
          <div
            style={{
              backgroundImage:
                "linear-gradient(to top, rgba(0,0,0,0),rgba(0,0,0,100))",
            }}
            className=" w-full h-full "
          >
            <div className="bg-black/30 backdrop-opacity-30 h-full w-full py-12 px-5 flex justify-center">
              <div className="max-w-xl flex flex-col justify-between text-center  text-white">
                <div>
                  <div className="mb-3 text-xs">
                    Merupakan suatu kehormatan dan kebahagiaan bagi kami,
                    apabila Bapak/Ibu/Saudara/i berkenan hadir dan memberiakn
                    doa restu.Atas Kehadiran dan doa restunya, kami mengucapkan
                    terima kasih.
                  </div>
                  <div className="mb-3 text-sm">
                    Wassalamualaikum Warahmatullahi Wabarakatuh
                  </div>
                </div>
                <div>
                  <div
                    className="font-bold text-[35px] tracking-wide "
                    style={{ fontFamily: "Great Vibes" }}
                  >
                    Raja & Ratu
                  </div>
                  <div className="flex justify-center mt-3 mb-6">
                    <QRCodeCanvas value={"Raja & Ratu"} />
                  </div>
                  <div className="mb-3 text-xs">
                    *Tampilkan barcode ini kepada panitia acara pernikahan untuk
                    mengisi buku tamu
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer desc={false} />
        <div className="my-10"></div>
        <ReactPlayer
          style={{ display: "none" }}
          playing={play}
          url={"/music/Wedding Invitation 1.mp3"}
        />
        <Paper
          sx={{
            zIndex: 99,
            position: "fixed",
            bottom: 0,
            left: 0,
            right: 0,
          }}
          elevation={3}
        >
          <Container maxWidth="md" sx={{ position: "relative" }}>
            <button
              style={{
                background: "#FFF",
                color: "#000",
                padding: 5,
                margin: 0,
                position: "absolute",
                bottom: 65,
                right: 30,
              }}
              className="rounded-xl"
              onClick={handlePlayPause}
            >
              {play ? <PlayDisabledIcon /> : <PlayArrowIcon />}
            </button>
            {/* <Fab
          onClick={handlePlayPause}
          sx={{
            position: 'absolute',
            bottom: 65,
            right: 30,
          }}
          size="small" color="primary" aria-label="add"
        >
          {play ? <PlayDisabledIcon /> : <PlayArrowIcon />}
        </Fab> */}
            <BottomNavigation showLabels>
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="home"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Home
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="home"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <RiHomeHeartLine fontSize={23} />
                  </Link>
                }
              />
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="couple"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Couple
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="couple"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <RiHeartsLine fontSize={23} />
                  </Link>
                }
              />
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="event"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Event
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="event"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <BiCalendarHeart fontSize={23} />
                  </Link>
                }
              />
              <BottomNavigationAction
                className="!text-xs"
                label={
                  <Link
                    activeClass="active"
                    to="wishes"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    Wishes
                  </Link>
                }
                icon={
                  <Link
                    activeClass="active"
                    to="wishes"
                    spy={true}
                    smooth={true}
                    duration={500}
                    offset={-10}
                  >
                    <RiChatHeartLine fontSize={23} />
                  </Link>
                }
              />
            </BottomNavigation>
          </Container>
        </Paper>
        <Dialog
          maxWidth={"md"}
          fullScreen
          open={openInvitation}
          onClose={handleCloseInvitation}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogContent sx={{ padding: "0px 0px" }}>
            <section
              className="w-full h-screen !bg-no-repeat !bg-cover !bg-center "
              style={{
                background:
                  "url('https://images.unsplash.com/photo-1520854221256-17451cc331bf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80')",
              }}
            >
              <div
                style={{
                  backgroundImage:
                    "linear-gradient(to bottom, rgba(0,0,0,0),rgba(0,0,0,100))",
                }}
                className=" w-full h-full"
              >
                <div className="bg-black/30 backdrop-opacity-30 h-full w-full py-12 px-5 flex justify-center">
                  <div className="max-w-xl flex flex-col justify-between text-center  text-white">
                    <div>
                      <div className="mb-3 text-sm">Undangan</div>
                      <div
                        className="font-bold text-[35px] tracking-wide "
                        style={{ fontFamily: "Great Vibes" }}
                      >
                        Raja & Ratu
                      </div>
                    </div>
                    <div>
                      <div className=" text-xs">
                        Kepada Yth. Bapak/Ibu/Saudara/i
                      </div>
                      <div className="my-3 font-bold text-xl">
                        Didi dan Partner
                      </div>
                      <div className="mb-3 text-xs">
                        Tanpa mengurangi rasa hormat, kami mengundangan anda
                        untuk hadir di acara pernikahan kami.
                      </div>
                      <div className="flex items-center justify-center">
                        <button
                          onClick={handleCloseInvitation}
                          className="rounded-full py-2 bg-white text-black px-6 text-xs shadow-md"
                        >
                          Buka Undangan
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </DialogContent>
        </Dialog>
        <Dialog
          fullWidth
          open={openKado}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseKado}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Alamat"}</DialogTitle>
          <DialogContent dividers={true}>
            <Box component="div">
              <Box
                component="div"
                style={{
                  border: "2px dashed grey",
                  padding: 5,
                  position: "relative",
                }}
              >
                <Box
                  component="div"
                  style={{
                    position: "absolute",
                    right: 0,
                    backgroundColor: "white",
                    height: 30,
                  }}
                  onClick={handleClickCopy}
                >
                  <CopyToClipboard
                    text={"invitationSlug?.alamatKado"}
                    onCopy={() => setCopied(true)}
                  >
                    <IconButton color="primary" component="span">
                      <ContentCopy fontSize="small" className="!text-xs" />
                    </IconButton>
                  </CopyToClipboard>
                </Box>
                {"invitationSlug?.alamatKado"}
              </Box>
            </Box>
          </DialogContent>
        </Dialog>
        <Dialog
          fullWidth
          open={openAngpau}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseAngpau}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle>{"Angpau"}</DialogTitle>
          <div className="px-1 md:px-5">
            {/* {invitationSlug?.digitalenvelopes?.map((v, i) => ( */}
            <Box my={2} component="div">
              <div className=" w-60 sm:w-full md:w-full md:h-56 h-56 m-auto bg-red-100 rounded-xl relative text-white shadow-2xl transition-transform transform bg-gradient-to-tr from-gray-900 to-gray-600 bg-gradient-to-r">
                <div className="w-full px-6 absolute top-8">
                  <div className="flex justify-between">
                    <div className=""></div>
                    <img
                      className="h-6 md:h-8"
                      src={`/static/images/digitalEnvelope/dana.png`}
                      alt={"v.name"}
                    />
                  </div>
                  <div className="pt-4">
                    <p className="font-light text-xs">Nama</p>
                    <p className="font-medium tracking-widest text-base">
                      v.atasNama
                    </p>
                  </div>
                  <div className="pt-3 ">
                    <p className="font-light text-xs">
                      Nomor Rekening / Handphone
                    </p>
                    <div className="flex gap-3 text-lg">
                      <div className="font-medium tracking-more-wider">
                        v.number
                      </div>
                      <div
                        className=" cursor-pointer "
                        onClick={handleClickCopy}
                      >
                        <CopyToClipboard
                          text="v.number"
                          onCopy={() => setCopied(true)}
                        >
                          <IconButton
                            size="small"
                            color="inherit"
                            sx={{ padding: 0 }}
                            component="span"
                          >
                            <ContentCopy className="!text-xs" />
                          </IconButton>
                        </CopyToClipboard>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Box>
            {/* ))} */}
          </div>
        </Dialog>
        {copied ? (
          <Snackbar
            open={open}
            onClose={handleCloseCopy}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
            message="Copied"
          />
        ) : null}
      </div>
    </>
  );
}

export default test;
