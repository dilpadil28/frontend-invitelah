/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Box, Container } from "@mui/material";
import MyBottomNavigation from "../../components/user/MyBottomNavigation";
import "react-vertical-timeline-component/style.min.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import ReactPlayer from "react-player";
import Head from "next/head";
import { api } from "../../config/api";
import Home from "../../components/user/Home";
import Couple from "../../components/user/Couple";
import LoveStory from "../../components/user/LoveStory";
import Wasalam from "../../components/user/Wasalam";
import FirstDialog from "../../components/user/Dialog/FirstDialog";
import Event from "../../components/user/Event";
import Gallery from "../../components/user/Gallery";
import Wishes from "../../components/user/Wishes";
import { useDispatch, useSelector } from "react-redux";
import { slugReducer } from "../../features/invitationSlug/invitationSlugSlice";
import { getMessageApiByInvitationId } from "../../features/messageApi/messageApiSlice";
import Prokes from "../../components/user/Prokes";
import ContainerFrame from "../../components/user/Container/ContainerFrame";
import Footer from "../../components/Footer";
import ModernBlack from "../../components/Themes/ModernBlack";
import ModernGold from "../../components/Themes/ModernGold";
import Loading from "../../components/Loading";
import ModernGreen from "../../components/Themes/ModernGreen";
import Minimalis from "../../components/Themes/Minimalis";
import ModernBrown from "../../components/Themes/ModernBrown";
import ModernPink from "../../components/Themes/ModernPink";
import ModernRed from "../../components/Themes/ModernRed";
import ModernBlue from "../../components/Themes/ModernBlue";
import manifest from "../../public/manifest.json";
import WavesPurple from "../../components/Themes/WavesPurple";
import WavesCream from "../../components/Themes/WavesCream";
import WavesDark from "../../components/Themes/WavesDark";
import WavesGreen from "../../components/Themes/WavesGreen";
import WavesPink from "../../components/Themes/WavesPink";
import WavesBrown from "../../components/Themes/WavesBrown";
import WavesRed from "../../components/Themes/WavesRed";
import WavesBlue from "../../components/Themes/WavesBlue";
import WavesGrey from "../../components/Themes/WavesGrey";
import WavesYellow from "../../components/Themes/WavesYellow";

function Invitation({ invitation, logo }) {
  const router = useRouter();
  const dispatch = useDispatch();
  const [play, setPlay] = useState(false);
  const { invitationSlug } = useSelector((state) => state.invitationSlug);

  useEffect(() => {
    if (invitation.message === "error") {
      router.push("/404");
    }
    dispatch(slugReducer(invitation.data));
    if (invitationSlug?.id) {
      dispatch(getMessageApiByInvitationId(invitationSlug?.id));
    }
    // console.log("invitationSlug?.backgrounds", invitationSlug?.backgrounds[3]);
    // console.log(
    //   "invitation.data.backgrounds[1]",
    //   invitation.data.backgrounds[1]
    // );
    // console.log("invitation.data", invitation.data);
    // console.log('sdfa', invitationSlug?.socialmedia.filter((v) => v.type === 'pria').filter((v) => v.name === 'live').map((v) => v.url))
  }, [dispatch, invitation, invitationSlug, router]);

  // useEffect(() => {
  //   // console.log(
  //   //   `/${invitation.data.slug}?to=${
  //   //     router.query.to ? router.query.to : "Kamu dan Partner"
  //   //   }`
  //   // );
  //   const manifestElement = document.getElementById("manifest");
  //   const manifestString = JSON.stringify({
  //     ...manifest,
  //     start_url: `/${invitation.data.slug}?to=${
  //       router.query.to ? router.query.to : "Kamu dan Partner"
  //     }`,
  //     name: `Undangan Pernikahan ${invitation.data.namaPendekPria} & ${invitation.data.namaPendekWanita} - Invitelah`,
  //   });
  //   manifestElement?.setAttribute(
  //     "href",
  //     "data:application/json;charset=utf-8," +
  //       encodeURIComponent(manifestString)
  //   );
  // }, []);

  useEffect(() => {
    const stringManifest = JSON.stringify({
      ...manifest,
      start_url: `/${invitation.data.slug}?to=${
        router.query.to ? router.query.to : "Kamu dan Partner"
      }`,
      name: `Undangan Pernikahan ${invitation.data.namaPendekPria} & ${invitation.data.namaPendekWanita} - Invitelah`,
    });
    const blob = new Blob([stringManifest], { type: "application/json" });
    const manifestURL = URL.createObjectURL(blob);
    document.querySelector("#manifest").setAttribute("href", manifestURL);

    var link = document.createElement("Link");
    link.rel = "manifest";
    link.setAttribute(
      "href",
      "data:application/json;charset=8" + stringManifest
    );
  }, [
    invitation.data.namaPendekPria,
    invitation.data.namaPendekWanita,
    invitation.data.slug,
    router.query.to,
  ]);
  return (
    <>
      <Head>
        <title>
          {"Undangan Pernikahan " +
            invitation.data.namaPendekPria +
            " & " +
            invitation.data.namaPendekWanita +
            " - invitelah"}
        </title>
        <meta
          property="title"
          content={
            "Undangan Pernikahan " +
            invitation.data.namaPendekPria +
            " & " +
            invitation.data.namaPendekWanita +
            " - invitelah"
          }
        />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="image_src"
          href={
            invitation.data.avatarPasangan
              ? api.fileUrl + invitation.data.avatarPasangan
              : logo
          }
        />
        <meta content="strict-origin-when-cross-origin" name="referrer" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta
          property="og:image"
          content={
            invitation.data.avatarPasangan
              ? api.fileUrl + invitation.data.avatarPasangan
              : logo
          }
        />
        <meta name="description" content="Powered by invitelah" />
        {/* Facebook Meta Tags  */}
        <meta property="og:url" content={api.home} />
        <meta property="og:type" content="website" />
        <meta
          property="og:site_name"
          content={
            "Undangan Pernikahan " +
            invitation.data.namaPendekPria +
            " & " +
            invitation.data.namaPendekWanita +
            " - invitelah"
          }
        ></meta>
        <link
          itemProp="thumbnailUrl"
          href={
            invitation.data.avatarPasangan
              ? api.fileUrl + invitation.data.avatarPasangan
              : logo
          }
        />
        <link
          itemProp="url"
          href={
            invitation.data.avatarPasangan
              ? api.fileUrl + invitation.data.avatarPasangan
              : logo
          }
        />
        <meta
          property="og:title"
          content={
            "Undangan Pernikahan " +
            invitation.data.namaPendekPria +
            " & " +
            invitation.data.namaPendekWanita +
            " - invitelah"
          }
        />
        <meta property="og:description" content="Powered by invitelah" />
        <meta
          property="og:image"
          content={
            invitation.data.avatarPasangan != ""
              ? api.fileUrl + invitation.data.avatarPasangan
              : logo
          }
        />

        {/* Twitter Meta Tags */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:domain" content="invitelah" />
        <meta property="twitter:url" content={api.home} />
        <meta
          name="twitter:title"
          content={
            "Undangan Pernikahan " +
            invitation.data.namaPendekPria +
            " & " +
            invitation.data.namaPendekWanita +
            " - invitelah"
          }
        />
        <meta name="twitter:description" content="Powered by invitelah" />
        <meta
          name="twitter:image"
          content={
            invitation.data.avatarPasangan
              ? api.fileUrl + invitation.data.avatarPasangan
              : logo
          }
        />
      </Head>
      {invitationSlug?.namaPendekPria != undefined && (
        <>
          {invitationSlug?.theme.name === "Modern-Black" ? (
            <ModernBlack invitation={invitation} />
          ) : invitationSlug?.theme.name === "Modern-Gold" ? (
            <ModernGold invitation={invitation} />
          ) : invitationSlug?.theme.name === "Modern-Green" ? (
            <ModernGreen invitation={invitation} />
          ) : invitationSlug?.theme.name === "Modern-Blue" ? (
            <ModernBlue invitation={invitation} />
          ) : invitationSlug?.theme.name === "Modern-Brown" ? (
            <ModernBrown invitation={invitation} />
          ) : invitationSlug?.theme.name === "Modern-Pink" ? (
            <ModernPink invitation={invitation} />
          ) : invitationSlug?.theme.name === "Modern-Red" ? (
            <ModernRed invitation={invitation} />
          ) : invitationSlug?.theme.name === "Minimalis" ? (
            <Minimalis invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Blue" ? (
            <WavesBlue invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Brown" ? (
            <WavesBrown invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Cream" ? (
            <WavesCream invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Dark" ? (
            <WavesDark invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Green" ? (
            <WavesGreen invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Grey" ? (
            <WavesGrey invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Pink" ? (
            <WavesPink invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Purple" ? (
            <WavesPurple invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Red" ? (
            <WavesRed invitation={invitation} />
          ) : invitationSlug?.theme.name === "Waves-Yellow" ? (
            <WavesYellow invitation={invitation} />
          ) : (
            <Box component="div">
              <Container
                maxWidth="md"
                sx={{
                  paddingLeft: 0,
                  paddingRight: 0,
                }}
              >
                <ContainerFrame>
                  <Box
                    id="home"
                    component="div"
                    textAlign="center"
                    height="100%"
                  >
                    <Home />
                  </Box>
                </ContainerFrame>
                <ContainerFrame>
                  <Box
                    component="div"
                    id="couple"
                    textAlign="center"
                    height="100%"
                  >
                    <Couple />
                  </Box>
                </ContainerFrame>
                <ContainerFrame>
                  <LoveStory />
                </ContainerFrame>
                <ContainerFrame>
                  <Box component="div" id="event" height="100%">
                    <Event />
                  </Box>
                </ContainerFrame>
                {invitation.data.photogalleries?.length > 0 ||
                invitation.data.youtubes?.length > 0 ? (
                  <ContainerFrame>
                    <Box component="div" height="100%" position="relative">
                      <Gallery />
                    </Box>
                  </ContainerFrame>
                ) : (
                  <></>
                )}
                <ContainerFrame>
                  <Box
                    id="wishes"
                    component="div"
                    height="100%"
                    position="relative"
                  >
                    <Wishes />
                  </Box>
                </ContainerFrame>
                {invitationSlug?.theme?.publishProkes ? (
                  <ContainerFrame>
                    <Prokes />
                  </ContainerFrame>
                ) : (
                  <></>
                )}
                <ContainerFrame>
                  <Wasalam to={router.query.to} />
                </ContainerFrame>
                <Footer desc={false} />
                <Box my={7} />
                <ReactPlayer
                  style={{ display: "none" }}
                  playing={play}
                  className="react-player"
                  url={
                    invitation.data.theme?.music?.song != ""
                      ? api.fileUrl + invitation.data.theme?.music?.song
                      : "/music/Wedding Invitation 1.mp3"
                  }
                />

                <FirstDialog
                  play={play}
                  setPlay={setPlay}
                  to={router.query.to}
                />
                <MyBottomNavigation play={play} setPlay={setPlay} />
              </Container>
            </Box>
          )}
        </>
      )}
    </>
  );
}

export default Invitation;

export const getServerSideProps = async (context) => {
  const { params, req, res, query } = context;
  // const to = query.to;
  // ssr caching
  res.setHeader(
    "Cache-Control",
    "public, s-maxage=1, stale-while-revalidate=59"
  );
  const logo = api.home + "/android-chrome-512x512.png";
  const [invitationRes] = await Promise.all([
    fetch(`${api.baseUrl}/invitation/slug/${query.invitation}`),
  ]);
  const [invitation] = await Promise.all([invitationRes.json()]);
  return { props: { invitation, logo } };
};
