/**
 * Typography used in theme
 * @param {JsonObject} theme theme customization object
 */

export default function themeTypography(theme) {
  return {
    fontFamily: [
      "Quicksand",
      "Alex Brush",
      "Anonymous Pro",
      "Bebas Neue",
      "Forum",
      "Great Vibes",
      "Josefin Sans",
      "Lora",
      "Montserrat",
      "Mr Dafoe",
      "Niconne",
      "Noto Serif",
      "Open Sans",
      "Pacifico",
      "Petit Formal Script",
      "Pinyon Script",
      "Playball",
      "Playfair Display",
      "Parisienne",
      "Allison",
      "Cabin",
      "Clicker Script",
      "Cookie",
      "Dancing Script",
      "Euphoria Script",
      "Hurricane",
      "Montez",
      "Petemoss",
      "Rouge Script",
      "Sacramento",
      "Satisfy",
      "Smooch",
      "Style Script",
    ].join(","),
    button: {
      textTransform: "capitalize",
    },
    customInput: {
      marginTop: 1,
      marginBottom: 1,
      "& > label": {
        top: 23,
        left: 0,
        color: theme.grey500,
        '&[data-shrink="false"]': {
          top: 5,
        },
      },
      "& > div > input": {
        padding: "30.5px 14px 11.5px !important",
      },
      "& legend": {
        display: "none",
      },
      "& fieldset": {
        top: 0,
      },
    },
    mainContent: {
      backgroundColor: theme.background,
      width: "100%",
      minHeight: "calc(100vh - 88px)",
      flexGrow: 1,
      padding: "20px",
      marginTop: "88px",
      marginRight: "20px",
      borderRadius: `${theme?.customization?.borderRadius}px`,
    },
    menuCaption: {
      fontSize: "0.875rem",
      fontWeight: 500,
      color: theme.heading,
      padding: "6px",
      textTransform: "capitalize",
      marginTop: "10px",
    },
    subMenuCaption: {
      fontSize: "0.6875rem",
      fontWeight: 500,
      color: theme.darkTextSecondary,
      textTransform: "capitalize",
    },
    commonAvatar: {
      cursor: "pointer",
      borderRadius: "8px",
    },
    smallAvatar: {
      width: "22px",
      height: "22px",
      fontSize: "1rem",
    },
    mediumAvatar: {
      width: "34px",
      height: "34px",
      fontSize: "1.2rem",
    },
    largeAvatar: {
      width: "44px",
      height: "44px",
      fontSize: "1.5rem",
    },
  };
}
