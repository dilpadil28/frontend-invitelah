# *
User-agent: *
Disallow: /admin

# *
User-agent: *
Disallow: /auth

# *
User-agent: *
Disallow: /ComingSoon

# *
User-agent: *
Disallow: /Maintenance

# *
User-agent: *
Disallow: /test

# *
User-agent: *
Allow: /

# *
User-agent: *
Allow: /raja-ratu?to=didi+dan+partner

# *
User-agent: *
Allow: /modern-gold?to=didi+dan+partner

# *
User-agent: *
Allow: /modern-green?to=didi+dan+partner

# *
User-agent: *
Allow: /minimalis?to=didi+dan+partner

# Host
Host: https://invitelah.com

# Sitemaps
Sitemap: https://invitelah.com/sitemap.xml
